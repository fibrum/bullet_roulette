﻿using UnityEngine;

public class AnimatorSync : MonoBehaviour {
    private Animator anim;
    private void Awake() {
        anim=GetComponent<Animator>();
    }

    private void Update() {
        var state = anim.GetCurrentAnimatorStateInfo(0);
        Debug.Log(state.shortNameHash+" "+state.IsName("Fear"));
        
    }
}
