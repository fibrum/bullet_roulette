﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiBlinkingSystem : MonoBehaviour {

    public static event Action AntiBlink;
    private float lastEvent;
    private void Update() {
        if (Time.time - lastEvent > 0.5f) {
            AntiBlink?.Invoke();
            lastEvent = Time.time;
        }
    }
}
