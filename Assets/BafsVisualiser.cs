﻿using System;
using System.Collections;
using System.Collections.Generic;
using SaloonSpin;
using UnityEngine;

public class BafsVisualiser : MonoBehaviour {
    [SerializeField] private GameObject policeprotection;
    [SerializeField] private GameObject extraLife;
    [SerializeField] private GameObject publicEnemy;
    public int pos;
    private string currentPlayerUserId;
    private void Awake() {
        PlayingCardsManager.Instance.CardVisualise += Instance_CardVisualise;
    }
    private void OnDestroy() {
        PlayingCardsManager.Instance.CardVisualise -= Instance_CardVisualise;
    }
    private void Instance_CardVisualise(string userid, PlayingCardType card, bool state)
    {
        if(NetworkManager.Instance.gameState.GetPos(Photon.Pun.PhotonNetwork.LocalPlayer.UserId) != pos)
            return;
        Debug.Log($"<color=green>[BafsVisualiser]{userid} {card} {state}</color>");

        switch (card) {
            case PlayingCardType.ExtraLife:
                if (state) {
                    if(Photon.Pun.PhotonNetwork.LocalPlayer.UserId==userid)
                        extraLife.SetActive(true);
                }else
                    extraLife.SetActive(state);
                break;
            case PlayingCardType.SetTarget:
                if (state)
                {
                    if (Photon.Pun.PhotonNetwork.LocalPlayer.UserId == userid)
                        publicEnemy.SetActive(true);
                }
                else
                    publicEnemy.SetActive(state);
                //publicEnemy.SetActive(state);
                
                break;
            case PlayingCardType.ProtectYourself:
                if (state)
                {
                    if (Photon.Pun.PhotonNetwork.LocalPlayer.UserId == userid)
                        policeprotection.SetActive(true);
                }
                else
                    policeprotection.SetActive(state);
                //policeprotection.SetActive(state);
                
                break;
            
        }
    }

    
}
