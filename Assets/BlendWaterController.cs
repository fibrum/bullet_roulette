﻿using UnityEngine;

public class BlendWaterController : MonoBehaviour
{
    int blendShapeCount;
    SkinnedMeshRenderer skinnedMeshRenderer;
    Mesh skinnedMesh;

    public float weight1 {
        set {
            if (blendShapeCount >= 1)
                skinnedMeshRenderer.SetBlendShapeWeight(0, value);
        }
    }

    void Awake()
    {
        skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
        skinnedMesh = GetComponent<SkinnedMeshRenderer>().sharedMesh;
        blendShapeCount = skinnedMesh.blendShapeCount;
        //Debug.Log(blendShapeCount);
    }


    
}
