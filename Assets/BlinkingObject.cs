﻿using System.Collections;
using System.Collections.Generic;
using SaloonSpin;
using UnityEngine;

public class BlinkingObject : MonoBehaviour {
    private Rigidbody rb;
    private VRTK.VRTK_InteractableObject interactable;
    private Cigarette cig;
    private void Awake() {
        rb = GetComponent<Rigidbody>();
        interactable = GetComponent<VRTK.VRTK_InteractableObject>();
        cig = GetComponent<Cigarette>();
        AntiBlinkingSystem.AntiBlink += AntiBlinkingSystem_AntiBlink;
    }

    private void OnDestroy() {
        AntiBlinkingSystem.AntiBlink -= AntiBlinkingSystem_AntiBlink;
    }

    private void AntiBlinkingSystem_AntiBlink() {
        if (interactable == null) { 
            if (!rb.isKinematic&&rb.velocity.magnitude < 0.1f) {
                unblink();
            }
        } else {
            if (cig != null) {
                if (!interactable.IsTouched() && !interactable.IsGrabbed() && !cig.cigareteInMouthNow && !rb.isKinematic && rb.velocity.magnitude < 0.1f) {
                    unblink();
                }

            } else {
                if(!interactable.IsTouched()&&!interactable.IsGrabbed()&& !rb.isKinematic && rb.velocity.magnitude < 0.1f) {
                    unblink();
                }
            }
            
        }
    }

    private void unblink() {
        var colDetMod = rb.collisionDetectionMode;
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        rb.isKinematic = true;
        rb.Sleep();
        StartCoroutine(turnKinematic(colDetMod));
    }

    private IEnumerator turnKinematic(CollisionDetectionMode mode) {
        yield return new WaitForFixedUpdate();
        yield return null;
        rb.isKinematic = false;
        rb.collisionDetectionMode = mode;
    }
}
