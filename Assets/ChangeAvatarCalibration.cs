﻿using System;
using System.Collections;
using System.Collections.Generic;
using SaloonSpin;
using UnityEngine;
using UnityEngine.UI;

public class ChangeAvatarCalibration : MonoBehaviour {
    public GameObject[] gentelman;
    public GameObject[] PoliceFemale;
    public GameObject[] Bandit;
    public GameObject[] Dama;
    public GameObject[] Ranger;
    public GameObject[] Indian;
    public PlayerType currentType = PlayerType.Gentlman;

    private IEnumerator Start() {
        yield return new WaitForSeconds(0.25f);
        currentController = "undefined";
        string sticks = "";
        foreach (var VARIABLE in Input.GetJoystickNames())
        {
            Debug.Log(VARIABLE);
            sticks += VARIABLE;
            if (VARIABLE.ToLower().Contains("windowsmr"))
            {
                Debug.Log("windowsmr");
                currentController="windowsmr";
                break;
            }
            if (VARIABLE.ToLower().Contains("knuckles"))
            {
                Debug.Log("knuckles");

                currentController = "knuckles";
                break;
            }
            if (VARIABLE.ToLower().Contains("vive"))
            {
                Debug.Log("vive");
                currentController = "vive";
                break;
            }
            if (VARIABLE.ToLower().Contains("oculus touch"))
            {
                Debug.Log("oculus touch");
                currentController = "oculus touch";
                break;
            }
            
        }

        if (currentController == "undefined") {
            Debug.LogWarning("Controllers are undefined report to developer: "+ sticks);
        }
        controllerText.text = $"Your controllers: {currentController}";
        Populate();

    }

    [SerializeField] private Text controllerText;

    public void OnChange(int value) {
        foreach (GameObject o in gentelman) {
            o.SetActive(false);
        }
        foreach (GameObject o in PoliceFemale)
        {
            o.SetActive(false);
        }
        foreach (GameObject o in Bandit)
        {
            o.SetActive(false);
        }
        foreach (GameObject o in Dama)
        {
            o.SetActive(false);
        }
        foreach (GameObject o in Ranger)
        {
            o.SetActive(false);
        }
        foreach (GameObject o in Indian)
        {
            o.SetActive(false);
        }

        switch (value) {
            case 0:
                foreach (GameObject o in gentelman)
                {
                    o.SetActive(true);
                }

                currentType = PlayerType.Gentlman;
                Populate();
                break;
            case 1:
                foreach (GameObject o in PoliceFemale)
                {
                    o.SetActive(true);
                }

                currentType = PlayerType.PoliceFemale; 
                Populate();
                break;
            case 2:
                foreach (GameObject o in Bandit)
                {
                    o.SetActive(true);
                }

                currentType = PlayerType.Bandit;
                Populate();
                break;
            case 3:
                foreach (GameObject o in Dama)
                {
                    o.SetActive(true);
                }

                currentType = PlayerType.Dama;
                Populate();
                break;
            case 4:
                foreach (GameObject o in Ranger)
                {
                    o.SetActive(true);
                }

                currentType = PlayerType.Ranger;
                Populate();
                break;
            case 5:
                foreach (GameObject o in Indian)
                {
                    o.SetActive(true);
                }

                currentType = PlayerType.Indian;
                Populate();
                break;
        }


        

    }

    public Hand currentHand=Hand.Left;
    public void OnChangeHand(int value) {
        switch (value) {
            case 0:
                currentHand = Hand.Left;
                Populate();
                break;
            case 1:
                currentHand = Hand.Right;
                Populate();
                break;
        }
    }

    private int changableObjectId = 0;
    public void OnChangeObject(int value) {
        changableObjectId = value;
        switch (value) {
            case 0:
                var opt = new List<Dropdown.OptionData>();
                opt.Add(new Dropdown.OptionData("x"));
                opt.Add(new Dropdown.OptionData("y"));
                opt.Add(new Dropdown.OptionData("z"));
                opt.Add(new Dropdown.OptionData("Euler X"));
                opt.Add(new Dropdown.OptionData("Euler Y"));
                opt.Add(new Dropdown.OptionData("Euler Z"));
                axisDropDown.options = opt;
                axisDropDown.value = 0;
                axis = 0;
                Populate();
                break;
            case 1:
                var opt2 = new List<Dropdown.OptionData>();
                opt2.Add(new Dropdown.OptionData("x"));
                opt2.Add(new Dropdown.OptionData("y"));
                opt2.Add(new Dropdown.OptionData("z"));
                opt2.Add(new Dropdown.OptionData("R"));
                axisDropDown.options = opt2;
                axisDropDown.value = 0;
                axis = 0;
                Populate();
                break;
        }
    }
    private int axis;
    public void OnChangeAxis(int value) {
        axis = value;
        Populate();

    }

    public void ChangeAxisToNext() {
        if (axisDropDown.value == axisDropDown.options.Count - 1) {
            axisDropDown.value = 0;
        } else {
            axisDropDown.value++;
        }
        
        axis = axisDropDown.value;
        Populate();
    }

    public void AddDeltaByController(bool add)
    {
        var cur = current;
        if (add)
        {
            if (Math.Abs(cur) < 1e-2)
                current = cur + 1e-3f;
            else
                current = cur + cur * 0.002f;
        }
        else
        {
            if (Math.Abs(cur) < 1e-2)
                current = cur - 1e-3f;
            else
                current = cur - cur * 0.002f;
        }

        inputValue.text = current.ToString();
    }

    private void Populate() {
        inputValue.text = current.ToString();
    }
    [SerializeField] private Dropdown axisDropDown;
    [SerializeField] private InputField inputValue;


    public void OnChangeValue(string value) {
        var newValue = float.Parse(value);
        if(Mathf.Abs(current-newValue)>1e-5)
            current=newValue;
    }

    public void AddDelta(bool add) {
        var cur = current;
        if (add) {
            if (Math.Abs(cur) < 1e-2)
                current = cur + 1e-2f;
            else
                current = cur + cur*0.01f;
        } else {
            if (Math.Abs(cur) < 1e-2)
                current = cur - 1e-2f;
            else
                current = cur - cur * 0.01f;
        }

        inputValue.text = current.ToString();
    }

    public string currentController = "knuckles";
    private float current {
        get {
            switch (changableObjectId) {
                case 0://change hand
                    var hand=getCurrentHand();
                    return hand.GetValue(axis);
                case 1://change collider
                    var colliders = FindObjectsOfType<ColliderPositionSetter>();
                    foreach (ColliderPositionSetter setter in colliders) {
                        if (setter.enabled && setter.hand == currentHand && setter.controller == currentController) {
                            return setter.GetValue(axis);
                        }
                    }
                    break;
            }
            
            return 0f;
        }
        set {
            switch (changableObjectId)
            {
                case 0://change hand
                    var hand = getCurrentHand();
                    hand.SetValue(axis,value, currentController);
                    break;
                case 1://change collider
                    var colliders = FindObjectsOfType<ColliderPositionSetter>();
                    foreach (ColliderPositionSetter setter in colliders)
                    {
                        if (setter.enabled && setter.hand == currentHand && setter.controller == currentController)
                        {
                             setter.SetValue(axis,value);
                             return;
                        }
                    }
                    break;
            }
        }
    }

    

    public SetPosition getCurrentHand() {
        switch (currentType) {
            case PlayerType.Gentlman:
                foreach (GameObject o in gentelman) {
                    var anim=o.GetComponent<HandAnimatorController>();
                    if(anim)
                        if (anim.Hand == currentHand)
                            return o.GetComponent<SetPosition>();
                }
                break;
            case PlayerType.Bandit:
                foreach (GameObject o in Bandit)
                {
                    var anim = o.GetComponent<HandAnimatorController>();
                    if (anim)
                        if (anim.Hand == currentHand)
                            return o.GetComponent<SetPosition>();
                }
                break;
            case PlayerType.PoliceFemale:
                foreach (GameObject o in PoliceFemale)
                {
                    var anim = o.GetComponent<HandAnimatorController>();
                    if (anim)
                        if (anim.Hand == currentHand)
                            return o.GetComponent<SetPosition>();
                }
                break;
            case PlayerType.Dama:
                foreach (GameObject o in Dama)
                {
                    var anim = o.GetComponent<HandAnimatorController>();
                    if (anim)
                        if (anim.Hand == currentHand)
                            return o.GetComponent<SetPosition>();
                }
                break;
            case PlayerType.Ranger:
                foreach (GameObject o in Ranger)
                {
                    var anim = o.GetComponent<HandAnimatorController>();
                    if (anim)
                        if (anim.Hand == currentHand)
                            return o.GetComponent<SetPosition>();
                }
                break;
            case PlayerType.Indian:
                foreach (GameObject o in Indian)
                {
                    var anim = o.GetComponent<HandAnimatorController>();
                    if (anim)
                        if (anim.Hand == currentHand)
                            return o.GetComponent<SetPosition>();
                }
                break;
        }

        return null;
    }
}