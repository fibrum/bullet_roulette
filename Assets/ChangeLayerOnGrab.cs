﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class ChangeLayerOnGrab : MonoBehaviour {
    [SerializeField] private string grabbedLayer = "Barrel";
    private int defaultLayer;
    private void Awake() {
        GetComponent<VRTK_InteractableObject>().InteractableObjectGrabbed += ChangeLayerOnGrab_InteractableObjectGrabbed;
        GetComponent<VRTK_InteractableObject>().InteractableObjectUngrabbed += ChangeLayerOnGrab_InteractableObjectUngrabbed;
        defaultLayer = gameObject.layer;
    }

    private void ChangeLayerOnGrab_InteractableObjectUngrabbed(object sender, InteractableObjectEventArgs e) {
        gameObject.layer = defaultLayer;
    }

    private void ChangeLayerOnGrab_InteractableObjectGrabbed(object sender, InteractableObjectEventArgs e) {
        gameObject.layer = LayerMask.NameToLayer(grabbedLayer);
    }
}
