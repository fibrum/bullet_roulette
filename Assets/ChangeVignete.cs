﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.PostProcessing;

public class ChangeVignete : MonoBehaviour {
    [SerializeField] private AnimationCurve curve;
    private float loopTime=0.85f;
    private PostProcessVolume post;
    private Vignette vignete;
    public static ChangeVignete Instance;
    private void Awake() {
        post = GetComponent<PostProcessVolume>();
        vignete = post.profile.GetSetting<Vignette>();
        Instance = this;
    }

    public void StartVignete() {
        toNarmal = false;
        StopAllCoroutines();
        maxIntensity = easyIntensity;
        loopTime = easyLoopTime;
        if (beatSource) beatSource.volume = easyVolume;
        StartCoroutine(heartBeat());
        started = true;
    }

    private bool started;
    private float maxIntensity;
    public void NormalIntensity() {
        toNarmal = false;
        maxIntensity = normalIntensity;
        loopTime = normalLoopTime;
        if (beatSource) beatSource.volume = normalVolume;
        if(!started) StartCoroutine(heartBeat());
    }
    public void HardIntensity() {
        toNarmal = false;
        maxIntensity = hardIntensity;
        loopTime = hardLoopTime;
        if (beatSource) beatSource.volume = hardVolume;
        if (!started) StartCoroutine(heartBeat());
    }
    private bool toNarmal=false;
    public void ReturnToNormal() {
        toNarmal = true;
    }

    private bool played;
    private IEnumerator heartBeat() {
        float t=0f;
        while (!toNarmal) {
            t += Time.deltaTime;
            if (t > loopTime) { 
                t = 0;
                played = false;
            }
            var k=curve.Evaluate(t / loopTime);
            vignete.intensity.value = k * maxIntensity;
            if (k >= 0.95f && !played) {
                played = true;
                onBeat.Invoke();
                if (beatSource) beatSource.Play();
            }
            yield return null;
        }

        //for (int i = 0; i < 1;)
        //{
        //    t += Time.deltaTime;
        //    if (t > loopTime)
        //    {
        //        t = 0;
        //        i++;
        //        maxIntensity *= 0.8f;
        //        loopTime *= 1.15f;
        //        if (beatSource) beatSource.volume *= 0.9f;
        //        played = false;
        //    }
        //    var k = curve.Evaluate(t / loopTime);
        //    vignete.intensity.value = k * maxIntensity;
        //    if (k >= 0.9f && !played)
        //    {
        //        played = true;
        //        onBeat.Invoke();
        //        if (beatSource) beatSource.Play();
        //    }
        //    yield return null;
        //}
        toNarmal = false;
        vignete.intensity.value = 0;
    }

    [SerializeField] private UnityEvent onBeat;
    [SerializeField] private AudioSource beatSource;

    [Header("EasyIntensity")]
    [SerializeField] private float easyIntensity = 0.25f;
    [SerializeField] private float easyLoopTime = 0.85f;
    [SerializeField] private float easyVolume=0.6f;
    [Header("NormalIntensity")]
    [SerializeField] private float normalIntensity = 0.35f;
    [SerializeField] private float normalLoopTime = 0.75f;
    [SerializeField] private float normalVolume = 0.8f;
    [Header("HardIntensity")]
    [SerializeField] private float hardIntensity = 0.5f;
    [SerializeField] private float hardLoopTime = 0.5f;
    [SerializeField] private float hardVolume = 1f;
}
