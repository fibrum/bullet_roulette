﻿using System;
using System.Collections;
using System.Collections.Generic;
using SaloonSpin;
using UnityEngine;

public class ChargingHints : MonoBehaviour {
    private SaloonSpin.PlayerController pc;
    void Awake() {
        pc = GetComponent<SaloonSpin.PlayerController>();
        NetworkManager.Instance.OnTurnSwitched.AddListener(OnTurnSwitched);
    }

    
    void OnDestroy() {
        GameManager.Instance.playersPositions[NetworkManager.Instance.gameState.GetMyPos()].takeGun.gameObject.SetActive(false);
        NetworkManager.Instance.OnTurnSwitched.RemoveListener(OnTurnSwitched);
    }
    
    void OnTurnSwitched(){
        if (pc.PlayerPhotonView.IsMine && !NetworkManager.Instance.gameState.MyTurn) {
            currentState = ChargeState.None;
            ShowHintForState();
        }
        if (pc.PlayerPhotonView.IsMine && NetworkManager.Instance.gameState.MyTurn&&!MainPistol.Instance.IsGunCharged) {
            currentState = ChargeState.TakeGun;
            ShowHintForState();
            MainPistol.Instance.OnGrab += PistolGrab;
            MainPistol.Instance.OnUngrab += PistolUngrab;
            Bullet.Instance.intaractable.InteractableObjectGrabbed += BulletGrab;
            Bullet.Instance.intaractable.InteractableObjectUngrabbed += BulletUngrab; ;
            MainPistol.Instance.OnShot.AddListener(Shoot);
        }

    }

    private void Shoot() {
        currentState = ChargeState.None;
        ShowHintForState();
    }

    private void PistolGrab()
    {
        Debug.Log("[ChargingHints]pistol grabbed");
        if (MainPistol.Instance.IsGunCharged) {
            if (MainPistol.Instance.GunSpinned) {
                currentState = ChargeState.Shoot;
                ShowHintForState();
            } else {
                currentState = ChargeState.SpinBarrel;
                ShowHintForState();
            }

        } else {//не заряжен
            currentState = ChargeState.OpenBarrel;
            ShowHintForState();
        }
    }
    private void PistolUngrab() {

        Debug.Log("[ChargingHints]PistolUngrab");
        currentState = ChargeState.TakeGun;
        ShowHintForState();
    }

    private void Update() {
        switch (currentState) {
            case ChargeState.OpenBarrel:
                if (MainPistol.Instance.IsGunCharged) {
                    currentState = ChargeState.CloseBarrel;

                    ShowHintForState();
                }

                if (/*MainPistol.Instance.IsBarrelOpened ||*/ MainPistol.Instance.IsBarellOpenedByJoint) {
                    currentState = ChargeState.TakeBullet;
                    
                    ShowHintForState();
                }

                break;
            case ChargeState.CloseBarrel:
                if (MainPistol.Instance.IsBarellOpenedByJoint == false&& currentState != ChargeState.SpinBarrel) {
                    currentState = ChargeState.SpinBarrel;

                    ShowHintForState();
                }
                break;
            case ChargeState.SpinBarrel:
                if (MainPistol.Instance.GunSpinned) {
                    currentState = ChargeState.Shoot;
                    ShowHintForState();
                }
                break;
            case ChargeState.TakeBullet:
                if (MainPistol.Instance.IsGunCharged && MainPistol.Instance.GunSpinned) {
                    currentState = ChargeState.Shoot;
                    ShowHintForState();
                }

                if (MainPistol.Instance.IsGunCharged) {
                    currentState = ChargeState.CloseBarrel;

                    ShowHintForState();
                }
                break;
            case ChargeState.InsertBullet:
                if (MainPistol.Instance.IsGunCharged)
                {
                    currentState = ChargeState.CloseBarrel;

                    ShowHintForState();
                }
                break;

        }
    }

    private void BulletGrab(object sender, VRTK.InteractableObjectEventArgs e){
        Debug.Log("[ChargingHints]BulletGrab");
        if (currentState == ChargeState.TakeBullet) {
            currentState = ChargeState.InsertBullet;
            ShowHintForState();
        }
    }
    
    private void BulletUngrab(object sender, VRTK.InteractableObjectEventArgs e) {
        Debug.Log("[ChargingHints]BulletUngrab");
        if (MainPistol.Instance.IsGunCharged) {

        } else {
            currentState = ChargeState.TakeBullet;
            ShowHintForState();
        }
    }

    private ChargeState currentState;
    private void ShowHintForState() {
        switch (currentState) {
            case ChargeState.None:
                GameManager.Instance.playersPositions[NetworkManager.Instance.gameState.GetMyPos()].takeGun.gameObject.SetActive(false);
                MainPistol.Instance.OnGrab -= PistolGrab;
                MainPistol.Instance.OnUngrab -= PistolUngrab;
                Bullet.Instance.intaractable.InteractableObjectGrabbed -= BulletGrab;
                Bullet.Instance.intaractable.InteractableObjectUngrabbed -= BulletUngrab; ;
                MainPistol.Instance.OnShot.RemoveListener(Shoot);
                MainPistol.Instance.RechargingHintText.text = "";
                break;
            case ChargeState.TakeGun:
                GameManager.Instance.playersPositions[NetworkManager.Instance.gameState.GetMyPos()].takeGun.gameObject.SetActive(true);
                MainPistol.Instance.RechargingHintText.text = "";
                break;
            case ChargeState.OpenBarrel:
                GameManager.Instance.playersPositions[NetworkManager.Instance.gameState.GetMyPos()].takeGun.gameObject.SetActive(false);
                MainPistol.Instance.RechargingHintText.text = I2.Loc.ScriptLocalization.hints_recharging_open_barrel;//"Open cylinder";
                break;
            case ChargeState.TakeBullet:
                GameManager.Instance.playersPositions[NetworkManager.Instance.gameState.GetMyPos()].takeGun.gameObject.SetActive(false);
                MainPistol.Instance.RechargingHintText.text = I2.Loc.ScriptLocalization.hints_recharging_take_bullet;//"Take the bullet";
                break;
            case ChargeState.InsertBullet:
                GameManager.Instance.playersPositions[NetworkManager.Instance.gameState.GetMyPos()].takeGun.gameObject.SetActive(false);
                MainPistol.Instance.RechargingHintText.text = I2.Loc.ScriptLocalization.hints_recharging_insert_bullet;//"Insert the bullet";
                break;
            case ChargeState.CloseBarrel:
                GameManager.Instance.playersPositions[NetworkManager.Instance.gameState.GetMyPos()].takeGun.gameObject.SetActive(false);
                MainPistol.Instance.RechargingHintText.text = I2.Loc.ScriptLocalization.hints_recharging_close_barrel;//"Close cylinder";
                break;
            case ChargeState.SpinBarrel:
                GameManager.Instance.playersPositions[NetworkManager.Instance.gameState.GetMyPos()].takeGun.gameObject.SetActive(false);
                MainPistol.Instance.RechargingHintText.text = I2.Loc.ScriptLocalization.hints_recharging_spin_barrel;//"Spin cylinder";
                break;
            case ChargeState.Shoot:
                GameManager.Instance.playersPositions[NetworkManager.Instance.gameState.GetMyPos()].takeGun.gameObject.SetActive(false);
                MainPistol.Instance.RechargingHintText.text = "";//"Shoot!";
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

}

public enum ChargeState {
    None,
    TakeGun,
    OpenBarrel,
    TakeBullet,
    InsertBullet,
    CloseBarrel,
    SpinBarrel,
    Shoot
}
