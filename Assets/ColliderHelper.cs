﻿using System.Collections;
using System.Collections.Generic;
using SaloonSpin;
using UnityEngine;
using VRTK;

public class ColliderHelper : MonoBehaviour {
    [SerializeField] private Transform rootTransform;
    private VRTK_InteractableObject interact;
    private Collider col;
    void OnEnable() {
        col = GetComponent<Collider>();
        interact = rootTransform.GetComponent<VRTK_InteractableObject>();
        interact.InteractableObjectGrabbed += pistolGrabbed;
        interact.InteractableObjectUngrabbed += pistolUngrabbed;
    }
    void OnDisable() {
        interact.InteractableObjectGrabbed -= pistolGrabbed;
        interact.InteractableObjectUngrabbed -= pistolUngrabbed;
    }

    private bool inHand;
    private void pistolUngrabbed(object sender, InteractableObjectEventArgs e)
    {
        inHand = false;
    }

    private void pistolGrabbed(object sender, InteractableObjectEventArgs e) {
        inHand = true;
        col.enabled = false;
    }

    
    // Update is called once per frame
    void Update()
    {
        if (!inHand && col.enabled == false&&Vector3.Distance(GameManager.Instance.tableCenter.position,rootTransform.position)<0.5f) {
            col.enabled = true;
        }
    }
}
