﻿using System;
using System.Collections;
using EasyButtons;
using SaloonSpin;
using UnityEngine;

public class ColliderPositionSetter : MonoBehaviour {
    private PositionOfObjects pos;
    private SphereCollider sphere;

    private void Awake() {
        sphere = GetComponent<SphereCollider>();
        pos = Resources.Load<PositionOfObjects>("IntaractableObjectsPositions");
        StartCoroutine(GetCollider());
    }

    private IEnumerator GetCollider()    {
        yield return null;
        var hnd = GetComponentInParent<PlayerHand>();
        if (hnd == null)
            yield break;
        hand =hnd.Hand;
        
        var col = pos.GetColliderSettings(controller);
        switch (hand) {
            case Hand.Left:
                sphere.center = col.posL;
                sphere.radius = col.radiusL;

                break;
            case Hand.Right:
                sphere.center = col.posR;
                sphere.radius = col.radiusR;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    public float GetValue(int index)
    {
        switch (index)
        {
            case 0:
                return sphere.center.x;
            case 1:
                return sphere.center.y;
            case 2:
                return sphere.center.z;
            case 3:
                return sphere.radius;
            
        }

        return 0;
    }

    public void SetValue(int index, float newValue) {
        var center = sphere.center;
        var rad = sphere.radius;
        switch (index)
        {
            case 0:
                center.x = newValue;
                break;
            case 1:
                center.y = newValue;
                break;
            case 2:
                center.z = newValue;
                break;
            case 3:
                rad = newValue;
                break;
        }
        sphere.center = center;
        sphere.radius = rad;
        pos.SetColliderSettings(controller, hand, sphere.center, sphere.radius);
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(pos);

#endif
    }

    public string controller;
    public Hand hand;
    [Button]
    private void SavePos() {
        sphere = GetComponent<SphereCollider>();
        pos = Resources.Load<PositionOfObjects>("IntaractableObjectsPositions");
        pos.SetColliderSettings(controller,hand,sphere.center,sphere.radius);
    }
}
