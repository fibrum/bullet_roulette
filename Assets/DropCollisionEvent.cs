﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DropCollisionEvent : MonoBehaviour {
    private float lastCollisionTime;
    [SerializeField] private float minTimeBeetweenCollisions=3f;
    [SerializeField] private float minForce=1f;
    [SerializeField] private UnityEvent collisionEvent;
    public bool IgnoreCollision { get; set; }
    private void OnCollisionEnter(Collision collision) {

        if (IgnoreCollision)
            return;

        if (Time.time - lastCollisionTime > minTimeBeetweenCollisions&& collision.impulse.magnitude > minForce) {
            lastCollisionTime = Time.time;
            collisionEvent.Invoke();
        } else {
            //Debug.Log("[DropCollisionEvent]OnCollisionEnter: magnitude="+collision.impulse.magnitude+"  time beetween last collision="+(Time.time - lastCollisionTime));
        }
    }
}
