﻿using System.Collections;
using System.Collections.Generic;
using SaloonSpin;
using UnityEngine;

public class FakeMechanismCreate : MonoBehaviour {
    [SerializeField] private GameObject prefab;
    [SerializeField] private MainPistol pistol;
    [SerializeField] private PistolBarrelMechanism barrel;
    [SerializeField] private Transform currentFakeObject;

    private void Awake() {
        GetComponentInParent<VRTK.VRTK_InteractableObject>().InteractableObjectGrabbed += FakeMechanismCreate_InteractableObjectGrabbed;
        GetComponentInParent<VRTK.VRTK_InteractableObject>().InteractableObjectUngrabbed += FakeMechanismCreate_InteractableObjectUngrabbed;

    }

    private void FakeMechanismCreate_InteractableObjectUngrabbed(object sender, VRTK.InteractableObjectEventArgs e)
    {
        StopAllCoroutines();
    }

    private void FakeMechanismCreate_InteractableObjectGrabbed(object sender, VRTK.InteractableObjectEventArgs e)
    {
        StartCoroutine(check());
        if (currentFakeObject.localEulerAngles.x <= 3f)
            return;
        if (Mathf.Abs(360f-currentFakeObject.localEulerAngles.x ) > 86f) {
            Debug.LogError("Recreate fake barrel"+ currentFakeObject.localEulerAngles.x);
            Destroy(currentFakeObject.gameObject);
            currentFakeObject=Instantiate(prefab, transform).transform;
            currentFakeObject.GetComponent<HingeJoint>().connectedBody = pistol.GetComponent<Rigidbody>();
            pistol.RigidbodyGunMechanism = currentFakeObject.GetComponent<Rigidbody>();
            barrel.Target = currentFakeObject;

        }
    }
    private IEnumerator check()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
            if (currentFakeObject.localEulerAngles.x <= 3f)
                continue;
            if (Mathf.Abs(360f - currentFakeObject.localEulerAngles.x) > 86f)
            {
                Debug.LogError("Recreate fake barrel"+ currentFakeObject.localEulerAngles.x);
                Destroy(currentFakeObject.gameObject);
                currentFakeObject = Instantiate(prefab, transform).transform;
                currentFakeObject.GetComponent<HingeJoint>().connectedBody = pistol.GetComponent<Rigidbody>();
                pistol.RigidbodyGunMechanism = currentFakeObject.GetComponent<Rigidbody>();
                barrel.Target = currentFakeObject;
                currentFakeObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;


            }

        }
    }
}
