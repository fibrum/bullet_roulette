﻿using System.Collections;
using System.Collections.Generic;
using SaloonSpin;
using UnityEngine;
using VRTK;

public class FakePlayerController : MonoBehaviour
{
    public VRTK_SDKSetup sDKSetup;
    public PlayerParts playerModel; //модель игрока
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (sDKSetup != null)
        {
            playerModel.UpdateHeadTransform(sDKSetup.actualHeadset.transform);
            playerModel.UpdateHandsTransform(sDKSetup.actualLeftController.transform, sDKSetup.actualRightController.transform);
            playerModel.UpdateBodyTransform();
        }
    }
}
