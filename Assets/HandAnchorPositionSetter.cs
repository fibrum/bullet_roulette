﻿using System;
using UnityEngine;

namespace SaloonSpin
{
    public class HandAnchorPositionSetter : MonoBehaviour
    {
        [Header("Dama")]
        [SerializeField] private Vector3 damaPosition;
        [SerializeField] private Vector3 damaRotation;
        [Header("Gentelman")]
        [SerializeField] private Vector3 gentelmanPosition;
        [SerializeField] private Vector3 gentelmanRotation;
        [Header("Bandit")]
        [SerializeField] private Vector3 banditPosition;
        [SerializeField] private Vector3 banditRotation;
        [Header("Polisman")]
        [SerializeField] private Vector3 policemanPosition;
        [SerializeField] private Vector3 policemanRotation;

        [Header("Indian")]
        [SerializeField] private Vector3 indPosition;
        [SerializeField] private Vector3 indRotation;

        [Header("Ranger")]
        [SerializeField] private Vector3 rangerPosition;
        [SerializeField] private Vector3 rangerRotation;
        private Transform _transform;
        private void Awake()
        {
            _transform = gameObject.transform;
        }

        private void Start()
        {
            if(NetworkManager.Instance) NetworkManager.Instance.onSyncGameState += PlayersTurnSwithed;
        }
        private void OnDisable()
        {
            if (NetworkManager.Instance) NetworkManager.Instance.onSyncGameState -= PlayersTurnSwithed;
        }
        private void PlayersTurnSwithed(GameState state)
        {
            var ut = state.userTurn;

            if (state.GetPlayerState(ut).isNPC)
                return;

            int index = state.GetPos(ut);
            this.UpdatePositionAndRotation(NetworkManager.Instance.typesToLoad[index]);
        }
        public void UpdatePositionAndRotation(PlayerType pType)
        {
            switch (pType)
            {
                case PlayerType.Gentlman:
                    _transform.localPosition = gentelmanPosition;
                    _transform.localEulerAngles = gentelmanRotation;
                    break;
                case PlayerType.Bandit:
                    _transform.localPosition = banditPosition;
                    _transform.localEulerAngles = banditRotation;
                    break;
                case PlayerType.PoliceFemale:
                    _transform.localPosition = policemanPosition;
                    _transform.localEulerAngles = policemanRotation;
                    break;
                case PlayerType.Dama:
                    _transform.localPosition = damaPosition;
                    _transform.localEulerAngles = damaRotation;
                    break;
                case PlayerType.Indian:
                    _transform.localPosition = indPosition;
                    _transform.localEulerAngles = indRotation;
                    break;
                case PlayerType.Ranger:
                    _transform.localPosition = rangerPosition;
                    _transform.localEulerAngles = rangerRotation;
                    break;
            }
        }


        [ContextMenu("SaveDama")]
        private void saveAnchorDama()
        {
            damaPosition = transform.localPosition;
            damaRotation = transform.localEulerAngles;
        }
        [ContextMenu("SaveGentelman")]
        private void saveAnchorGentelman()
        {
            gentelmanPosition = transform.localPosition;
            gentelmanRotation = transform.localEulerAngles;
        }
        [ContextMenu("SaveBandit")]
        private void saveAnchorBandit()
        {
            banditPosition = transform.localPosition;
            banditRotation = transform.localEulerAngles;
        }
        [ContextMenu("SavePolisman")]
        private void saveAnchorPolisman()
        {
            policemanPosition = transform.localPosition;
            policemanRotation = transform.localEulerAngles;
        }
        [ContextMenu("SaveIndian")]
        private void saveAnchorind()
        {
            indPosition = transform.localPosition;
            indRotation = transform.localEulerAngles;
        }
        [ContextMenu("SaveRanger")]
        private void saveAnchorRanger()
        {
            rangerPosition = transform.localPosition;
            rangerRotation = transform.localEulerAngles;
        }
    }
}
