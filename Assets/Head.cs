﻿using UnityEngine;
using UnityEngine.Events;

namespace SaloonSpin
{
    public class Head : MonoBehaviour
    {
        [SerializeField] private OutlineController outliner;
        private bool aimingEffectOn = true;
        public UnityEvent AimAtMyself, StopAimAtMyself;
        private void Awake()
        {
            if(outliner == null)
                outliner = GetComponentInParent<OutlineController>();
            AimingEffectTurnOff();
            AimAtMyself.AddListener(() => { ChangeVignete.Instance.StartVignete();ChangeVignete.Instance.HardIntensity(); });
            StopAimAtMyself.AddListener(() => { ChangeVignete.Instance.ReturnToNormal(); });

            AimAtMyself.AddListener(() => { /*Debug.Log("aim at myself start"); */});
            StopAimAtMyself.AddListener(() => { /*Debug.Log("aim at myself stop");*/ });
        }

        public void AimingEffectTurnOn()
        {
            if (aimingEffectOn)
                return;

            outliner.SetOutline(true);
            aimingEffectOn = true;
        }
        public void AimingEffectTurnOff()
        {
            if (!aimingEffectOn)
                return;

            outliner.SetOutline(false);
            aimingEffectOn = false;
        }
    }
}
