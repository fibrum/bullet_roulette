﻿using UnityEngine;

public class HoleController : MonoBehaviour {
    [SerializeField] private Transform endPoint;
    [SerializeField] private Transform bone1;
    [SerializeField] private Transform bone2;

    public void MakeHoleScale(Vector3 point1,Vector3 point2,Vector3 normal1,Vector3 normal2,Vector3 up) {
        //var scale=transform.localScale;
        //scale.z = scale.z * Vector3.Distance(point2, point1) / Vector3.Distance(transform.position, endPoint.position);
        //transform.localScale = scale;
        bone1.position = point1;
        bone1.rotation= Quaternion.LookRotation(normal1,up);

        bone2.position = point2;
        bone2.rotation = Quaternion.LookRotation(normal2,up);
    }
}
