﻿using System.Collections;
using System.Collections.Generic;
using SaloonSpin;
using UnityEngine;

public class InputRedirector : MonoBehaviour {
    private VRTK.VRTK_ControllerEvents events;

    [SerializeField] private Hand hand;

    [SerializeField] private ChangeAvatarCalibration change;
    // Start is called before the first frame update
    void Start() {
        events = GetComponent<VRTK.VRTK_ControllerEvents>();
    }

    private bool changeaxisPressed;
    // Update is called once per frame
    void Update()
    {
        if (events.touchpadPressed) {
            if (changeaxisPressed == false) { 
                if(events.GetTouchpadAxis().magnitude < 0.25f) { 
                    change.ChangeAxisToNext();
                    //Debug.Log("touchpad pressed "+hand+" change axis "+ events.GetTouchpadAxis());
                }
                changeaxisPressed = true;
            }

            if (events.GetTouchpadAxis().y > 0.5f) {
                change.AddDeltaByController(true);
                //Debug.Log("touchpad pressed " + hand + " add event " + events.GetTouchpadAxis());
            }
            if (events.GetTouchpadAxis().y < -0.5f)
            {
                change.AddDeltaByController(false);
                //Debug.Log("touchpad pressed " + hand + " minus event " + events.GetTouchpadAxis());
            }
        } else {
            changeaxisPressed = false;
        }
    }
}
