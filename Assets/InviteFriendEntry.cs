﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InviteFriendEntry : MonoBehaviour {
    [SerializeField] private Button button;
    [SerializeField] private Text nameText;
    [SerializeField] private RawImage avatarImg;
    [SerializeField] private Image checkImage;
    [SerializeField] private Image onlinestatus;
    [SerializeField] private Sprite offlineSprite;
    [SerializeField] private Sprite onlineSprite;
    public void Init(Friend friend,int roomId) {
        button.onClick.AddListener(() => {
            Debug.Log("Invite click");
            if ((friend as SteamFriend) != null) {
                if (SteamManager.InviteFriend(((SteamFriend) friend).steamId, roomId)) {
                    checkImage.gameObject.SetActive(true);
                    button.interactable = false;
                    checkImage.enabled = true;
                }
            }
        });
        nameText.text = friend.name;
        avatarImg.texture = friend.avatarTexture;

        if (friend.status == Friend.OnlineState.Online) {
            onlinestatus.sprite = onlineSprite;
        } else {
            onlinestatus.sprite = offlineSprite;
        }

        checkImage.enabled = false;
    }
}
