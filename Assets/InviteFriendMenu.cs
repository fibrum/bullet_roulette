﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using SaloonSpin;
using UnityEngine;

public class InviteFriendMenu : MonoBehaviour {
    [SerializeField] private GameObject entry;
    public void Init() {
        var roomName= Lobby.Instance.RoomId;
        Debug.Log(roomName);
        List<GameObject> gosForDell=new List<GameObject>();
        var entryParent= entry.transform.parent;
        for (int i = 1; i < entryParent.childCount; i++) {
            gosForDell.Add(entryParent.GetChild(i).gameObject);
        }
        foreach (GameObject o in gosForDell) {
            Destroy(o);
        }
        gosForDell.Clear();


        foreach (var friend in SteamManager.Friends) {
            var newEntry=Instantiate(entry, entryParent);
            newEntry.GetComponent<InviteFriendEntry>().Init(friend, roomName);
            newEntry.SetActive(true);
        }

        var rootRect = (entryParent as RectTransform);
        rootRect.sizeDelta=new Vector2(rootRect.sizeDelta.x,80* SteamManager.Friends.Count);
        gameObject.SetActive(true);
    }
}
