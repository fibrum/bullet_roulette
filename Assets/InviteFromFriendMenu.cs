﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InviteFromFriendMenu : MonoBehaviour {
    [SerializeField] private Text inviteText;
    private ulong _lobbyId;
    public void Init(string userName, ulong lobbyId) {
        // inviteText.text= "You friend "+ userName + " invites you";
        inviteText.text= I2.Loc.ScriptLocalization.invite_from_friend_menu_description_part1+" "+ userName + " "+I2.Loc.ScriptLocalization.invite_from_friend_menu_description_part2;
        _lobbyId = lobbyId;
        gameObject.SetActive(true);
    }

    public void ClickAccept() {
        SteamManager.JoinLobby(_lobbyId);
        gameObject.SetActive(false);
    }
}
