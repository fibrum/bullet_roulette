﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OculusOpenOverlay : MonoBehaviour
{
    private void Update() {
        if (OVRInput.GetDown(OVRInput.Button.Start)) {
            SteamManager.OpenOverlay();
        }
    }
}
