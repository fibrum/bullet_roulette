﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class OnPointerEnterEvent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
    [SerializeField] private UnityEvent onPointerEnter;
    [SerializeField] private UnityEvent onPointerExit;
    public void OnPointerEnter(PointerEventData eventData) {
        onPointerEnter.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData) {
        onPointerExit.Invoke();
    }
}
