﻿using cakeslice;
using UnityEngine;

public class OutlineController : MonoBehaviour {
    [SerializeField] private Outline[] outlines;

    public void SetOutline(bool viewOutline) {
        for (int index = 0; index < outlines.Length; index++) {
            outlines[index].enabled = viewOutline;
        }
    }

    void Reset() {
        outlines = GetComponentsInChildren<Outline>(true);
    }

    [ContextMenu("turnOffOutliners")]
    private void TurnOff() {
        SetOutline(false);
    }
}
