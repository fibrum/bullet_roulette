﻿using UnityEngine;

public class RatateWater : MonoBehaviour {
    private BlendWaterController blendController;

    [SerializeField] private float angle = 15f;
    void Start() {
        blendController = GetComponentInChildren<BlendWaterController>();

    }

    // Update is called once per frame
    void Update() {
        Vector3 rootEulerAngles = transform.parent.eulerAngles;
        if ((Mathf.Abs(rootEulerAngles.x) < 1f || Mathf.Abs(rootEulerAngles.x) > 379f)
            && (Mathf.Abs(rootEulerAngles.z) < 1f || Mathf.Abs(rootEulerAngles.z) > 379f)) {
            //blendController.weight1 = 0f;
            return;

        }
        var z = rootEulerAngles.z;
        var x = rootEulerAngles.x;
        if (x > 180)
            x = 360 - x;

        if (z > 180)
            z = 360 - z;

        var kz = Mathf.Clamp01(Mathf.Abs(z) / angle);
        var kx = Mathf.Clamp01(Mathf.Abs(x) / angle);

        if (kx > kz)
        {
            if (rootEulerAngles.x < 180)
            {
                var k = 0.5f;
                if (rootEulerAngles.z > 180)
                    k = Mathf.Clamp(0.5f + Mathf.Abs(kz / 2f / kx), 0.5f, 1f);
                else
                    k = Mathf.Clamp(0.5f - Mathf.Abs(kz / 2f / kx), 0, 0.5f);
                transform.localEulerAngles = new Vector3(0, 225 + k * 90f, 0);
            } else {
                var k = 0.5f;
                if (rootEulerAngles.z > 180)
                    k = Mathf.Clamp(0.5f - Mathf.Abs(kz / 2f / kx), 0f, 0.5f);
                else
                    k = Mathf.Clamp(0.5f + Mathf.Abs(kz / 2f / kx), 0.5f, 1f);
                transform.localEulerAngles = new Vector3(0, 45 + k * 90f, 0);

            }

            blendController.weight1 = kx * 100f;
            //if (anim) anim.SetFloat("Blend", kx);

        }
        else
        {
            if (rootEulerAngles.z < 180)
            {
                var k = 0.5f;
                if (rootEulerAngles.x > 180)
                {
                    k = Mathf.Clamp(0.5f - Mathf.Abs(kx / 2f / kz), 0f, 0.5f);
                    // Debug.Log(k);
                }

                else
                    k = Mathf.Clamp(0.5f + Mathf.Abs(kx / 2f / kz), 0.5f, 1f);
                transform.localEulerAngles = new Vector3(0, 135 + k * 90f, 0);
            }
            else
            {
                var k = 0.5f;
                if (rootEulerAngles.x > 180)
                    k = Mathf.Clamp(0.5f + Mathf.Abs(kx / 2f / kz), 0.5f, 1f);
                else
                    k = Mathf.Clamp(0.5f - Mathf.Abs(kx / 2f / kz), 0f, 0.5f);
                transform.localEulerAngles = new Vector3(0, 315 + k * 90f, 0);
            }
            blendController.weight1 = kz * 100f;
                //if (anim) anim.SetFloat("Blend", kz);

        }
    }
}
