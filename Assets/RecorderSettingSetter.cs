﻿using Photon.Pun;
using Photon.Voice.DemoVoiceUI;
using Photon.Voice.Unity;
using SaloonSpin;
using UnityEngine;

public class RecorderSettingSetter : MonoBehaviour {
    private Recorder rec;
    void Start() {
        if(!GetComponent<PhotonView>().IsMine)
            return;

        rec = GetComponent<Recorder>();
        if (GameManager.Instance.isMicSetted) {
            MicDropdownValueChanged(GameManager.Instance.microphoneSettings);
        }
    }

    
    private void MicDropdownValueChanged(MicRef mic)
    {
        rec.MicrophoneType = mic.MicType;

        switch (mic.MicType)
        {
            case Recorder.MicType.Unity:
                rec.UnityMicrophoneDevice = mic.Name;
                break;
            case Recorder.MicType.Photon:
                rec.PhotonMicrophoneDeviceId = mic.PhotonId;
                break;
        }

        if (rec.RequiresRestart)
        {
            rec.RestartRecording();
        }
    }
}
