﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using RootMotion;
using SaloonSpin;
using UnityEngine;
using VRTK;

public class ReturnerBullet : Singleton<ReturnerBullet> {
    private Vector3 pos0 =new Vector3(-0.04f, 1.088862f, 1.838f);
    private Vector3 pos1=new Vector3(-0.337f, 1.088862f, 2.168f);
    private Vector3 pos2 = new Vector3(-0.71f, 1.088862f, 1.857f);
    private Vector3 pos3 = new Vector3(-0.337f, 1.088862f, 1.523f);
    public void SetPosition() {
        
        switch (NetworkManager.Instance.gameState.GetPos(NetworkManager.Instance.gameState.userTurn)){
            case 0:
                //if (Vector3.Distance(transform.position, pos0) > 0.07f)
                    transform.position = pos0;
                break;
            case 1:
                //if (Vector3.Distance(transform.position, pos1) > 0.07f) 
                transform.position = pos1;
                break;
            case 2:
                
                //if (Vector3.Distance(transform.position, pos2) > 0.07f) 
                transform.position = pos2;
                break;
            case 3:

                //if (Vector3.Distance(transform.position, pos3) > 0.07f) 
                transform.position = pos3;
                break;
        }
    }

    private void Start() {
        TurnColliderOff();
    }

    public void TurnColliderOn() {
        //if (PhotonNetwork.LocalPlayer.UserId!= NetworkManager.Instance.gameState.userTurn)
        //    return;
        Debug.Log($"<color=pink>[ReturnerBullet]TurnColliderOn </color>");
        SetPosition();
        gameObject.SetActive(true);
    }
    public void TurnColliderOff() {
        Debug.Log("TurnColliderOff");
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Bullet")) {
            if (bulletToReturn != null) {
                StopAllCoroutines();
                bulletToReturn = null;
            } 
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Bullet")) {
            bulletToReturn = other.GetComponentInParent<Bullet>();
            StartCoroutine(returnEnumerator());
            
        }
    }


    private IEnumerator returnEnumerator() {
        yield return new WaitForSeconds(1f);
        var vrtk = bulletToReturn.GetComponent<VRTK_InteractableObject>();
        while (vrtk.IsGrabbed()) {
            yield return new WaitForSeconds(.5f);
        }
        Return();
    }

    private Bullet bulletToReturn;

    private void Return() {
        if (bulletToReturn!=null)
        {
            bulletToReturn.EnableBullet();
            bulletToReturn = null;
        }
    }
}
