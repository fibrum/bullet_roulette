%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: hand mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Base HumanPelvis
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit11
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit11/Base HumanLDigit12
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit11/Base HumanLDigit12/Base HumanLDigit13
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit21
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit21/Base HumanLDigit22
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit21/Base HumanLDigit22/Base HumanLDigit23
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit31
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit31/Base HumanLDigit32
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit31/Base HumanLDigit32/Base HumanLDigit33
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit41
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit41/Base HumanLDigit42
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit41/Base HumanLDigit42/Base HumanLDigit43
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit51
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit51/Base HumanLDigit52
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLDigit51/Base HumanLDigit52/Base HumanLDigit53
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Cylinder001
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanNeck1
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanNeck1/Base HumanNeck2
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanNeck1/Base HumanNeck2/Base HumanHead
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanNeck1/Base HumanNeck2/Base HumanHead/Base HumanHeadBone001
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanNeck1/Base HumanNeck2/Base HumanHead/Base HumanHeadBone002
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanNeck1/Base HumanNeck2/Base HumanHead/Base HumanHeadBone003
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanNeck1/Base HumanNeck2/Base HumanHead/Base HumanHeadBone004
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanNeck1/Base HumanNeck2/Base HumanHead/gentlemanhat_LP
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/ 1
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/ 1/CartoonGunHammer
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/ 1/CartoonGunMehanism
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/ 1/CartoonGunMehanism/cilinder
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/ 1/CartoonGunTrigger
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit11
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit11/Base HumanRDigit12
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit11/Base HumanRDigit12/Base HumanRDigit13
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit21
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit21/Base HumanRDigit22
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit21/Base HumanRDigit22/Base HumanRDigit23
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit31
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit31/Base HumanRDigit32
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit31/Base HumanRDigit32/Base HumanRDigit33
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit41
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit41/Base HumanRDigit42
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit41/Base HumanRDigit42/Base HumanRDigit43
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit51
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit51/Base HumanRDigit52
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRDigit51/Base HumanRDigit52/Base HumanRDigit53
    m_Weight: 1
  - m_Path: gentleman_LP
    m_Weight: 1
  - m_Path: Head
    m_Weight: 1
