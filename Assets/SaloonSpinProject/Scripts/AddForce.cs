﻿using UnityEngine;

public class AddForce : MonoBehaviour {
    [SerializeField] private Transform root;
    [SerializeField] private float ForceMultiplayer = 100f;
    [SerializeField] private Force force=Force.SimpleForce;
    [SerializeField] private float EplosionRadius=1f;
    [SerializeField] private bool addTorque=false;
    [SerializeField] private float torqueMultiplayer=1f;
    public enum Force {
        SimpleForce=0,
        ExplosionForce=1
    }
	// Use this for initialization
	void OnEnable () {
	    var rb = GetComponentInChildren<Rigidbody>();
        if(force==Force.SimpleForce)

            rb.AddForce(-root.forward * ForceMultiplayer);
        else 
            rb.AddExplosionForce(ForceMultiplayer,root.position, EplosionRadius);//Force(-root.forward * ForceMultiplayer);
		//Debug.Log("force added"+(-root.forward * ForceMultiplayer));
        if(addTorque)
            rb.AddTorque(Random.onUnitSphere * torqueMultiplayer);

    }

    void OnDisable() {
        GetComponentInChildren<Rigidbody>().velocity=Vector3.zero;

    }
}
