﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class AnimatorEvents : MonoBehaviour {

    [SerializeField] private GameObject[] OnEvent1Activate;
    [SerializeField] private AudioSource[] OnEvent1Play;
    [SerializeField] private MonoBehaviour[] OnEvent1monoActivate;
    [SerializeField] private UnityEvent OnEvent1;
    [SerializeField] private GameObject[] OnEvent2Activate;
    [SerializeField] private AudioSource[] OnEvent2Play;
    [SerializeField] private UnityEvent OnEvent2;
    [SerializeField] private GameObject[] OnEvent3Activate;
    [SerializeField] private AudioSource[] OnEvent3Play;
    [SerializeField] private UnityEvent OnEvent3;
    [SerializeField] private GameObject[] OnEvent4Activate;
    [SerializeField] private AudioSource[] OnEvent4Play;
    [SerializeField] private UnityEvent OnEvent4;
    [SerializeField] private GameObject[] OnEvent5Activate;
    [SerializeField] private AudioSource[] OnEvent5Play;
    [SerializeField] private UnityEvent OnEvent5;
    public void Event1 () {
        //Debug.Log("event1 call"+name);
        foreach (var go in OnEvent1Activate)
            if (go)
                go.SetActive(true);
        foreach (var go in OnEvent1Play)
            if (go)
                go.Play();
        foreach (var go in OnEvent1monoActivate)
            if (go)
                go.enabled = true;
        OnEvent1.Invoke();
    }
	
	public void Event2() {
        foreach (var go in OnEvent2Activate)
            if (go)
                go.SetActive(true);
        foreach (var go in OnEvent2Play)
            if (go)
                go.Play();
        OnEvent2.Invoke();
    }
    public void Event3()
    {
        foreach (var go in OnEvent3Activate)
            if (go)
                go.SetActive(true);
        foreach (var go in OnEvent3Play)
            if (go)
                go.Play();
        OnEvent3.Invoke();
    }

    public void Event4()
    {
        foreach (var go in OnEvent4Activate)
            if (go)
                go.SetActive(true);
        foreach (var go in OnEvent4Play)
            if (go)
                go.Play();
        OnEvent4.Invoke();
    }
    public void Event5()
    {
        foreach (var go in OnEvent4Activate)
            if (go)
                go.SetActive(true);
        foreach (var go in OnEvent4Play)
            if (go)
                go.Play();
        OnEvent5.Invoke();
    }


    public void SetBoolTrue(string str) {
       GetComponent<Animator>().SetBool(str,true); 
    }
}
