﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class CardsVFX : MonoBehaviour
    {
        [Header("Default card material")]
        public Material defaultCardMaterial;
        [Header("Reverse Direction")]
        public GameObject reverseDirectionVFX;
        public GameObject reverseDirectionVFX_Table;
        [Header("Skip")]
        public GameObject skip;
        [Header("Cancel card")]
        public GameObject cancelCard;
        [Header("Divide all money")]
        public GameObject divideMoney;
        public ParticleSystem[] prtcls;
        public AudioSource cashSound;
        [Header("Sheriff protection")]
        public GameObject sheriffProtection;
        [Header("Set Target")]
        public GameObject setTarget;
        [Header("SecondBullet")]
        public GameObject secondBullet;
        [Header("DrumCheck")]
        public GameObject drumCheck;
        [Header("Extra life")]
        public GameObject extraLife;


        private FollowPlayerEffect extraLifeEffect, playerAimEffect, playerShieldEffect;
        private Coroutine showing;
        public FollowPlayerEffect ExtraLifeEffect
        {
            get { return extraLifeEffect; }
            set {
                extraLifeEffect = value;
                StartCoroutine(CalculateOffsets(PlayingCardType.ExtraLife));
            }
        }
        public FollowPlayerEffect PlayerAimEffect
        {
            get { return playerAimEffect; }
            set
            {
                playerAimEffect = value;
                StartCoroutine(CalculateOffsets(PlayingCardType.SetTarget));
            }
        }
        public FollowPlayerEffect PlayerShieldEffect
        {
            get { return playerShieldEffect; }
            set
            {
                playerShieldEffect = value;
                StartCoroutine(CalculateOffsets(PlayingCardType.ProtectYourself));
            }
        }
      
        private void HideAllEffects()
        {
            reverseDirectionVFX.SetActive(false);
            reverseDirectionVFX_Table.SetActive(false);
            skip.SetActive(false);
            cancelCard.SetActive(false);
            divideMoney.SetActive(false);
            sheriffProtection.SetActive(false);
            secondBullet.SetActive(false);
            drumCheck.SetActive(false);
            setTarget.SetActive(false);
            extraLife.SetActive(false);
            if (showing != null)
                StopCoroutine(showing);
        }
        [PunRPC]
        public void EnableVFX(int _type)
        {
            PlayingCardType type = (PlayingCardType)_type;
            HideAllEffects();
            switch (type)
            {
                case PlayingCardType.TurnMoveDirection:
                    showing = StartCoroutine(Show(reverseDirectionVFX));
                    StartCoroutine(TurnMoveDirection());
                    break;
                case PlayingCardType.SkipTurn:
                    showing = StartCoroutine(Show(skip));
                    break;
                case PlayingCardType.UndoLastPlayed:
                    showing = StartCoroutine(Show(cancelCard));
                    break;
                case PlayingCardType.DivideAllMoney:
                    showing = StartCoroutine(Show(divideMoney));
                    foreach (var ps in prtcls)
                        ps.Play();
                    cashSound.Play();
                    break;
                case PlayingCardType.ProtectYourself:
                    showing = StartCoroutine(Show(sheriffProtection));
                    break;
                case PlayingCardType.SecondBullet:
                    showing = StartCoroutine(Show(secondBullet));
                    break;
                case PlayingCardType.SeeBullet:
                    if(PhotonNetwork.LocalPlayer.UserId != PlayingCardsManager.Instance.LastUserID)
                        showing = StartCoroutine(Show(drumCheck));
                    break;
                case PlayingCardType.SetTarget:
                    showing = StartCoroutine(Show(setTarget));
                    break;
                case PlayingCardType.ExtraLife:
                    showing = StartCoroutine(Show(extraLife));
                    break;
            }
        }
        public void SetRefference(FollowPlayerEffect refference, PlayingCardType cType)
        {
            switch (cType)
            {
                case PlayingCardType.ExtraLife:
                    ExtraLifeEffect = refference;
                    break;
                case PlayingCardType.ProtectYourself:
                    PlayerShieldEffect = refference;
                    break;
                case PlayingCardType.SetTarget:
                    PlayerAimEffect = refference;
                    break;
            }
        }
        private IEnumerator Show(GameObject obj, float time = 2f)
        {
            string userid = PhotonNetwork.LocalPlayer.UserId;
            int pos = NetworkManager.Instance.gameState.GetPos(userid);
            Transform target = GameManager.Instance.playersPositions[pos].playerPlace;
            obj.transform.LookAt(target);
            Vector3 rot = obj.transform.eulerAngles;
            rot.x = 0;
            obj.transform.eulerAngles = rot;

            var scale = obj.GetComponent<TweenScale>();
            scale.ResetToBeginning();
            obj.SetActive(true);
            yield return new WaitForSeconds(time);
            scale.PlayReverse();
            yield return new WaitForSeconds(0.27f);
            obj.SetActive(false);
            scale.ResetToBeginning();

        }

        private IEnumerator TurnMoveDirection()
        {
            reverseDirectionVFX_Table.SetActive(true);
            yield return new WaitForSeconds(2f);
            reverseDirectionVFX_Table.SetActive(false);
        }
        //private IEnumerator SkipTurn()
        //{
        //    skip.SetActive(true);
        //    yield return new WaitForSeconds(2f);
        //    skip.SetActive(false);
        //}
        //private IEnumerator UndoLastPlayed()
        //{
        //    cancelCard.SetActive(true);
        //    yield return new WaitForSeconds(2f);
        //    cancelCard.SetActive(false);
        //}

        public void EnablingEffects(string userID, bool enable)
        {
            if (ExtraLifeEffect != null)
            {
                if (NetworkManager.Instance.gameState.GetPlayerState(userID).lifeCount == 2)
                {
                    ExtraLife(enable);
                }
            }

            if (PlayerAimEffect != null)
            {
                if(PlayingCardsManager.Instance.TargetUserID == userID)
                {
                    PlayerAim(enable);
                }
            }

            if(PlayerShieldEffect != null)
            {
                if(PlayingCardsManager.Instance.ProtectedUserID == userID)
                {
                    PlayerShield(enable);
                }
            }
        }
        private void ExtraLife(bool enable)
        {
            if(ExtraLifeEffect != null)
            {
                ExtraLifeEffect.effectObject.SetActive(enable);
            }
        }
        private void PlayerAim(bool enable)
        {
            if (PlayerAimEffect != null)
            {
                PlayerAimEffect.effectObject.SetActive(enable);
            }
        }
        private void PlayerShield(bool enable)
        {
            if (PlayerShieldEffect != null)
            {
                PlayerShieldEffect.effectObject.SetActive(enable);
            }
        }

        private IEnumerator CalculateOffsets(PlayingCardType cType)
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();

            if (!PhotonNetwork.IsMasterClient) { yield break; }

            Transform target = transform;
            switch (cType)
            {
                case PlayingCardType.ExtraLife:
                    target = ExtraLifeEffect == null ? null : ExtraLifeEffect.Target;
                    break;
                case PlayingCardType.ProtectYourself:
                    target = PlayerShieldEffect == null ? null : PlayerShieldEffect.Target;
                    break;
                case PlayingCardType.SetTarget:
                    target = PlayerAimEffect == null ? null : PlayerAimEffect.Target;
                    break;
            }

            List<FollowPlayerEffect> effectsList = new List<FollowPlayerEffect>();

            effectsList.Add(ExtraLifeEffect);
            effectsList.Add(PlayerShieldEffect);
            effectsList.Add(PlayerAimEffect);

            effectsList.RemoveAll((e) => e == null);
            effectsList.RemoveAll((e) => !e.Target.Equals(target));

            switch (effectsList.Count)
            {
                case 1:
                    effectsList[0].SetOffset(0f);
                    break;
                case 2:
                    effectsList[0].SetOffset(-.125f);
                    effectsList[1].SetOffset(.125f);
                    break;
                case 3:
                    effectsList[0].SetOffset(-.25f);
                    effectsList[1].SetOffset(.25f);
                    effectsList[0].SetOffset(0);
                    break;
            }
        }
    }
}
