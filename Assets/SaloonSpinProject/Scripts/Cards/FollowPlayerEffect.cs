﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class FollowPlayerEffect : MonoBehaviour
    {
        [SerializeField] private float Y_HeadOffset = .2f;
        [SerializeField] public GameObject effectObject;
        [SerializeField] private PlayingCardType effect;
        public string TargetID { get; private set; }
        public float X_Offset { get; set; }
        public float Z_Offset { get; set; }
        private bool inited;
        public Transform Target { get; private set; }
        public PlayingCardType Effect { get {return effect;} }
        public void SetTarget(Transform target, string id)
        {
            this.Target = target;
            inited = true;
            GetComponent<PhotonView>().RPC("SetTargetID", RpcTarget.All, id);
        }
        private void Update()
        {
            if (inited)
            {
                Vector3 position = Target.position;
                position.y += Y_HeadOffset;
                position.x += X_Offset;
                position.z += Z_Offset;
                transform.position = position;
            }
        }
        [PunRPC]
        private void SetTargetID(string id)
        {
            TargetID = id;
        }
        public void SetOffset(float offset)
        {
            switch (NetworkManager.Instance.gameState.GetPos(TargetID))
            {
                case 0:
                    Z_Offset = offset;
                    break;
                case 1:
                    X_Offset = offset;
                    break;
                case 2:
                    Z_Offset = offset;
                    break;
                case 3:
                    X_Offset = offset;
                    break;
            }
        }
        private void Start()
        {
            PlayingCardsManager.Instance.SetEffectReference(this, effect);
        }
    }
}
