﻿using System;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SaloonSpin
{
    public class PlayingCard : MonoBehaviour
    {
        public bool IsCardMoving { get; set; }
        public  string HolderUserID { get; private set; }
        public  PlayingCardType CardType {
            get { return cardType; }
        }
        public int cardID = -1;
        [SerializeField] private PlayingCardType cardType;
        [SerializeField] private string cardDescription;
        [SerializeField] private bool debug = true;
        private Rigidbody cardRigidbody;
        private PhotonView photonView;
        private VRTK.VRTK_InteractableObject interactableObject;
        public int CardPointIndex { get; private set; }

        private Quaternion defaultRotation = Quaternion.identity;
        private Vector3 defaultPosition = Vector3.zero;
        private Coroutine DelayReturning;
        private bool used;
        private bool npcCard;
        private MeshRenderer meshRenderer;
        #region Moving variables
        private Vector3 startMarker;
        private Vector3 endMarker;
        private float startTime;
        private float journeyLength;
        private float speed = 2f;
        #endregion

        [HideInInspector] public UnityEvent OnCardUsed;
        public UnityEvent UseCardVFX;
        public UnityEvent UseCardFailed;
        private void Awake()
        {
            photonView = GetComponent<PhotonView>();
            interactableObject = GetComponent<VRTK.VRTK_InteractableObject>();
            cardRigidbody = GetComponent<Rigidbody>();
            meshRenderer = GetComponentInChildren<MeshRenderer>();
        }
        private void Start()
        {
            UseCardFailed.AddListener(() => Debug.Log("fail"));
            OnCardUsed.AddListener(() => 
            {
                photonView.RPC("HideMeshCard", RpcTarget.All);
                StartCoroutine(DelayDestroyCard());
                PlayingCardsManager.Instance.UseCard(this.cardType);
                photonView.RPC("RPC_CardUsing", RpcTarget.All);
                used = true;
                GetComponent<VRTK.VRTK_InteractableObject>().enabled = false;
            });
        }
        private void Update()
        {
            if(IsCardMoving)
            {
                float distCovered = (Time.time - startTime) * speed;
                float fracJourney = distCovered / journeyLength;
                transform.position = Vector3.Lerp(startMarker, endMarker, fracJourney);

                if (Vector3.Distance(transform.position, endMarker) < 0.01f)
                {
                    transform.position = endMarker;
                    IsCardMoving = false;
                    cardRigidbody.isKinematic = false;
                }
            }
        }

        private void DebugCardNotUsed(string msg) {
            RPC_UseCardFailed();
            if (debug) {
                Debug.Log(msg);
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("ReloadTrigger") && photonView.IsMine && !used)
            {
                if (MainPistol.Instance.transform.GetComponentInParent<NPC>() != null)
                {
                    DebugCardNotUsed("npc hold pistol");
                    return;
                }

                if (NetworkManager.Instance.gameState.userTurn != HolderUserID) {
                    DebugCardNotUsed("not my turn to use card");
                    return;
                }// нельзя использовать карту не в свою очередь
                
                if (IsCardMoving)
                {
                    DebugCardNotUsed("card moving and cant be used");
                    return;
                }//если карта попала в триггер т.к. двигалась в свое дефолтное положение, она не используется   
                
                if (!PlayingCardsManager.Instance.CanUseCard(HolderUserID))
                {
                    DebugCardNotUsed("one card alredy used this turn");
                    return;
                }//нельзя использовать две карты в один ход

                if (!CheckCardSpecific())
                {
                    DebugCardNotUsed("cant use that card");
                    return;
                }
                
                if (DelayReturning != null)
                    StopCoroutine(DelayReturning);

                if (!used)
                {
                    PlayingCardsManager.Instance.PV.RPC("SetLastCardUsedUser", RpcTarget.All, HolderUserID);
                    Debug.Log("on trigger enter card - " + transform.name);
                    OnCardUsed?.Invoke();
                }
            }
        }
        [PunRPC]
        private void RPC_CardUsing()
        {
            UseCardVFX?.Invoke();
        }
        public void EnableNetworkSync(bool enable)
        {
            GetComponent<Rigidbody>().isKinematic = !enable;
            GetComponent<PhotonTransformView>().enabled = enable;
            GetComponent<PhotonRigidbodyView>().enabled = enable;
        }
        private IEnumerator DelayReturnCard()
        {
            yield return new WaitForSeconds(2f);
            photonView.RPC("ReturnCard", RpcTarget.All);
        }
        private IEnumerator DelayDestroyCard()
        {
            yield return new WaitForSeconds(2f);
            PhotonNetwork.Destroy(gameObject);
        }
        private bool CheckCardSpecific() {
            switch (cardType)
            {
                case PlayingCardType.TurnMoveDirection:
                    return MainPistol.Instance.transform.GetComponentInParent<NPC>() == null;
                case PlayingCardType.ExtraLife:
                    return !PlayingCardsManager.Instance.StatusOnPlayer(HolderUserID, PlayingCardType.SetTarget) &&
                        !PlayingCardsManager.Instance.StatusOnPlayer(HolderUserID, PlayingCardType.ProtectYourself);
                case PlayingCardType.SecondBullet:
                    return MainPistol.Instance.IsGunCharged;
                case PlayingCardType.SeeBullet:
                    return MainPistol.Instance.IsGunCharged;
                case PlayingCardType.DivideAllMoney:
                    break;
                case PlayingCardType.SkipTurn:
                    return MainPistol.Instance.transform.GetComponentInParent<NPC>() == null;
                case PlayingCardType.SetTarget:
                    break;
                case PlayingCardType.ProtectYourself:
                    return !PlayingCardsManager.Instance.StatusOnPlayer(HolderUserID, PlayingCardType.SetTarget) &&
                        !PlayingCardsManager.Instance.StatusOnPlayer(HolderUserID, PlayingCardType.ExtraLife);
                case PlayingCardType.UndoLastPlayed:
                    return LastCardCanBeCanceled();
                case PlayingCardType.SpinPistol:
                    return MainPistol.Instance.IsGunCharged;
            }
            return true;
        }//для использования некоторых карт, необходимо чтобы пистолет был заряжен, проверка проиходит здесь
        private bool LastCardCanBeCanceled() //проверка возможна ли отмента предыдущей сыгранной карты
        {
            if (PlayingCardsManager.Instance.LastUsedCard == null)
                return false;

            switch (PlayingCardsManager.Instance.LastPlayedCard)
            {
                case PlayingCardType.ExtraLife:
                    return PlayingCardsManager.Instance.IsExtraLifeEffectValid();
                case PlayingCardType.SecondBullet:
                    return MainPistol.Instance.SecondBulletInserted;
                case PlayingCardType.SetTarget:
                    return PlayingCardsManager.Instance.TargetUserID != string.Empty;
                case PlayingCardType.ProtectYourself:
                    return PlayingCardsManager.Instance.ProtectedUserID != string.Empty;
                case PlayingCardType.TurnMoveDirection:
                    return true;
            }

            return false;
        }

        [PunRPC]
        private void InitCard(int cardPointIndex, int cardNumber, int cardID)
        {
            GameState state = NetworkManager.Instance.gameState;
            this.HolderUserID = NetworkManager.Instance.gameState.players[cardPointIndex].UserID;
            this.CardPointIndex = cardPointIndex;
            defaultPosition = PlayingCardsManager.Instance.GetCardPoint(cardPointIndex, cardNumber).position;
            defaultRotation = PlayingCardsManager.Instance.GetCardPoint(cardPointIndex, cardNumber).rotation;
            ReturnCard();
            this.cardID = cardID;
            int localPlayerIndex = NetworkManager.Instance.gameState.GetMyPos();
            if (localPlayerIndex != cardPointIndex)
            {
                interactableObject.enabled = false;
            }
            else
            {
                interactableObject.InteractableObjectGrabbed += (object sender, VRTK.InteractableObjectEventArgs e) =>
                {
                    //PlayingCardsManager.Instance.ShowCardDescription(this.CardPointIndex, this.cardDescription);
                    PlayingCardsManager.Instance.ShowCardDescription(this.CardPointIndex, GetLocalizeCardDescription(cardType));

                    if (DelayReturning != null)
                        StopCoroutine(DelayReturning);

                    photonView.RPC("StopMoving", RpcTarget.All);
                };
                interactableObject.InteractableObjectUngrabbed += (object sender, VRTK.InteractableObjectEventArgs e) =>
                {
                    PlayingCardsManager.Instance.HideCardDescription(this.CardPointIndex);                
                    DelayReturning = StartCoroutine(DelayReturnCard());
                };
            }
            npcCard = state.players[cardPointIndex].isNPC;

            if (npcCard)
                meshRenderer.material = PlayingCardsManager.Instance.DefaultMaterial;
            else if(state.GetMyPos() != cardPointIndex)
                meshRenderer.material = PlayingCardsManager.Instance.DefaultMaterial;


        }

        private string GetLocalizeCardDescription(PlayingCardType playingCardType)
        {
            string result = "Need localization for card description";
            switch (playingCardType)
            {
                case PlayingCardType.TurnMoveDirection:
                    result = I2.Loc.ScriptLocalization.card_turm_move_direction_description;
                    break;
                case PlayingCardType.ExtraLife:
                    result = I2.Loc.ScriptLocalization.card_extra_live_description;
                    break;
                case PlayingCardType.SecondBullet:
                    result = I2.Loc.ScriptLocalization.card_second_bullet_description;
                    break;
                case PlayingCardType.SeeBullet:
                    result = I2.Loc.ScriptLocalization.card_see_bullet_description;
                    break;
                case PlayingCardType.DivideAllMoney:
                    result = I2.Loc.ScriptLocalization.card_divide_all_money_description;
                    break;
                case PlayingCardType.SkipTurn:
                    result = I2.Loc.ScriptLocalization.card_skip_turn_description;
                    break;
                case PlayingCardType.SetTarget:
                    result = I2.Loc.ScriptLocalization.card_set_target_description;
                    break;
                case PlayingCardType.ProtectYourself:
                    result = I2.Loc.ScriptLocalization.card_protect_yourself_description;
                    break;
                case PlayingCardType.UndoLastPlayed:
                    result = I2.Loc.ScriptLocalization.card_undo_last_played_description;
                    break;
                case PlayingCardType.SpinPistol:
                    result = I2.Loc.ScriptLocalization.card_spin_pistol_description;
                    break;
            }

            return result;
        }

        [PunRPC]
        private void SetHolderiD(string id)
        {
            HolderUserID = id;
        }
        [PunRPC]
        public void ReturnCard()
        {
            startMarker = transform.position;
            endMarker = defaultPosition;
            journeyLength = Vector3.Distance(startMarker, endMarker);
            transform.rotation = defaultRotation;
            startTime = Time.time;
            IsCardMoving = true;
            cardRigidbody.isKinematic = true;
        }

        [PunRPC]
        private void StopMoving()
        {
            IsCardMoving = false;
        }

        [PunRPC]
        private void HideMeshCard()
        {
            GetComponentInChildren<Renderer>().enabled = false;
        }
        [PunRPC]
        private void RPC_UseCardFailed()
        {
            UseCardFailed?.Invoke();
        }
    }
}
