﻿using System;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace SaloonSpin
{
    public class PlayingCardsManager : MonoBehaviour
    {
        public static PlayingCardsManager Instance;
        [SerializeField] private GameObject koloda;
        [SerializeField] private List<Text> cardsDescriptionList = new List<Text>();
        [SerializeField] private List<PlayerCardsPoints> cardPoints = new List<PlayerCardsPoints>();
        [SerializeField] private List<PlayingCardPrefab> cardPrefabs = new List<PlayingCardPrefab>();
        [SerializeField] private GameObject spinCardPrefab;
        [SerializeField] private Transform tableCenter;
        [SerializeField] private BarrelUI barrelWithBullets;
        public List<GameObject> allPlayersCoins = new List<GameObject>();

        [Header("Player effects prefabs")]
        [SerializeField] private GameObject playerAimPrefab;
        [SerializeField] private GameObject extraLifePrefab;
        [SerializeField] private GameObject playerShieldPrefab;

        private List<AbstractPlayer> players = new List<AbstractPlayer>();
        private PhotonView photonView;
        private Player LastPlayerUsedCard;
        private List<PlayingCard> playingCards = new List<PlayingCard>(); //список созданных карт
        private string lastUserID;//id игрока, последним использовавшего карту, для того чтобы один игрок не смог использовать две карты за ход
        private PlayingCardType lastPlayedCard;
        private Coroutine showingBarrel;
        private CardsVFX cardsVFX;
        public  PlayingCardType LastPlayedCard {
            get { return lastPlayedCard; }
        }
        public string LastUserID { get { return lastUserID; } }
        public string TargetUserID { get; private set; }
        public string ProtectedUserID { get; private set; }
        public Player LastUsedCard { get { return LastPlayerUsedCard; } }
        public Material DefaultMaterial { get { return cardsVFX.defaultCardMaterial; } }
        public PhotonView PV { get { return photonView; } }

        private void Awake()
        {
            if(Instance == null)
            {
                Instance = this;
            }

            cardsVFX = GetComponent<CardsVFX>();
            ProtectedUserID = string.Empty;
            TargetUserID = string.Empty;
            photonView = GetComponent<PhotonView>();
        }

        public void ChangeCardHolders(string previousHoldeID, string newholderID)
        {
            playingCards.RemoveAll((pc) => pc == null);
            var prevHolderCards = playingCards.FindAll((pc) => pc.HolderUserID == previousHoldeID);
            foreach(var c in prevHolderCards)
            {
                PhotonView pv = c.GetComponent<PhotonView>();
                pv.RPC("SetHolderiD", RpcTarget.All, newholderID);
                pv.RPC("ReturnCard", RpcTarget.All);
            }
        }
        public bool IsExtraLifeEffectValid()
        {
            for (int i = 0; i < 4; i++)
                if (NetworkManager.Instance.gameState.players[i].lifeCount == 2)
                    return true;
            return false;
        }
        public void SetEffectReference(FollowPlayerEffect refference, PlayingCardType cType)
        {
            if (!PhotonNetwork.IsMasterClient)
                cardsVFX.SetRefference(refference, cType);
        }
        public void EnablingCardEffect(string userID, bool enable)
        {
            cardsVFX.EnablingEffects(userID, enable);
        }
        public bool IsPlayerHasCard(string playerID, PlayingCardType cardT)
        {
            return playingCards.Find((c) => (c.CardType == cardT && c.HolderUserID == playerID)) != null;
        }
        public bool StatusOnPlayer(string player, PlayingCardType pct)
        {
            switch (pct)
            {
                case PlayingCardType.ExtraLife:
                    PlayerState state = NetworkManager.Instance.gameState.GetPlayerState(player);
                    return state.lifeCount == 2;
                case PlayingCardType.SetTarget:
                    return TargetUserID == player;
                case PlayingCardType.ProtectYourself:
                    return ProtectedUserID == player;
            }

            return false;
        }
        [PunRPC]
        private void GetPlayersRefs()
        {
            players.Clear();
            players.AddRange(FindObjectsOfType<AbstractPlayer>());
        }
        public void SwitchOwnership(int cardID, Player switchTo)
        {
            PlayingCard card = playingCards.Find((_card) => _card.cardID == cardID);
            if(card != null)
            {
                card.GetComponent<PhotonView>().TransferOwnership(switchTo);
            }
            else
            {
                Debug.LogError("card not found, id: " + cardID);
            }
        }
        public void ResetData()
        {
            allPlayersCoins.Clear();
            players.Clear();
            LastPlayerUsedCard = null;
            TargetUserID = string.Empty;
        }
        public PlayingCard GetCard(string playerID, PlayingCardType cardType)
        {
            return playingCards.Find((card) => card.CardType == cardType && card.HolderUserID == playerID);
        }
        [PunRPC]
        public void PistolMakedShot(bool isBullet) {
            switch (LastPlayedCard) {
                case PlayingCardType.TurnMoveDirection:
                    break;
                case PlayingCardType.ExtraLife:
                    break;
                case PlayingCardType.SecondBullet:
                    break;
                case PlayingCardType.SeeBullet:
                    if(showingBarrel != null)
                        StopCoroutine(showingBarrel);
                    barrelWithBullets.Enable(false);
                    break;
                case PlayingCardType.DivideAllMoney:
                    break;
                case PlayingCardType.SkipTurn:
                    break;
                case PlayingCardType.SetTarget:
                    break;
                case PlayingCardType.ProtectYourself:
                    break;
                case PlayingCardType.UndoLastPlayed:
                    break;
                case PlayingCardType.SpinPistol:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (isBullet && PhotonNetwork.IsMasterClient)
            {
                DisableExtraLife(false);
                DisableProtectYourSelf();
                DisableSetTarget();
            }
        }
        [PunRPC]
        private void SetLastCardUsedUser(string id, PhotonMessageInfo info)
        {
            lastUserID = id;
            LastPlayerUsedCard = info.Sender;
        }
        [PunRPC]
        private void SetLastCardUsed(int card)
        {
            lastPlayedCard = (PlayingCardType)card;
        }
        [PunRPC]
        private void RPC_ResetIDs()
        {
            LastPlayerUsedCard = null;
            TargetUserID = string.Empty;
        }
        public bool CanUseCard(string userID)
        {
            return lastUserID != userID;
        }
        public void UseCard(PlayingCardType cardType)
        {
            photonView.RPC("RPC_UseCard", RpcTarget.MasterClient, (int)cardType);
        }
        [PunRPC]
        private void RemoveUsedCard(int _cardType, string holder)
        {
            int index = -1;
            PlayingCardType cardType = (PlayingCardType)_cardType;
            index = playingCards.FindIndex((c) => c.CardType == cardType && c.HolderUserID == holder);
            if(0 <= index && index < playingCards.Count)
            {
                playingCards.RemoveAt(index);
            }
        }
        [PunRPC]
        private void RPC_UseCard(int _cardType)
        {
            PlayingCardType cardType = (PlayingCardType)_cardType;
            Debug.Log("card used: " + cardType.ToString());

            if (!PhotonNetwork.IsMasterClient)
                return;

            PlayingCard pCard = playingCards.Find((card) => card.CardType == cardType);
            AbstractPlayer player = null;

            if (pCard == null) {
                Debug.LogError("card not found: " + cardType.ToString());
                return;
            }

            player = players.Find((p) => p.UserID == lastUserID);
            if (player == null)
            {
                Debug.LogError("player with id " + lastUserID + " not found");
            }

            photonView.RPC("EnableVFX", RpcTarget.All, _cardType);

            switch (pCard.CardType)
            {
                case PlayingCardType.TurnMoveDirection:
                    NetworkManager.Instance.gameState.turnsClockwise = true;
                    NetworkManager.Instance.gameState.SwitchTurnToNextUser();
                    NetworkManager.Instance.RequestToSync();
                    NetworkManager.Instance.SyncPistolState();
                    MainPistol.Instance.PhotonView.RPC("PutPistolOnTableCenter", RpcTarget.All);
                    break;
                case PlayingCardType.ExtraLife:
                    photonView.RPC("IncreaseLifesCount", LastPlayerUsedCard);
                    photonView.RPC("EnableExtraLifeIcon", RpcTarget.All, player.UserID);
                    break;
                case PlayingCardType.SecondBullet:
                    MainPistol.Instance.InsertSecondBulletStart();
                    break;
                case PlayingCardType.SeeBullet:
                    if (!NetworkManager.Instance.gameState.GetPlayerState(lastUserID).isNPC)
                        photonView.RPC("ShowWhereBulletIS", LastPlayerUsedCard);
                    break;
                case PlayingCardType.DivideAllMoney:
                    Coins.Instance.DivideCoins();
                    NetworkManager.Instance.RequestToSync();
                    break;
                case PlayingCardType.SkipTurn:
                    NetworkManager.Instance.gameState.SwitchTurnToNextUser();
                    NetworkManager.Instance.RequestToSync();
                    NetworkManager.Instance.SyncPistolState();
                    MainPistol.Instance.PhotonView.RPC("PutPistolOnTableCenter", RpcTarget.All);
                    break;
                case PlayingCardType.SetTarget:
                    photonView.RPC("EnableHandPointer", LastPlayerUsedCard);
                    break;
                case PlayingCardType.ProtectYourself:
                    photonView.RPC("SetProtectedID", RpcTarget.All, lastUserID);
                    break;
                case PlayingCardType.UndoLastPlayed:
                    DisableLastUsed();
                    break;

                case PlayingCardType.SpinPistol:
                    player = players.Find((p) => p.UserID == LastPlayerUsedCard.UserId);
                    if (player is PlayerController) {
                        PhotonView pv = player.GetComponent<PhotonView>();
                        pv.RPC("HandSpinPistol", LastPlayerUsedCard);
                    }
                    break;
            }
            photonView.RPC("SetLastCardUsed", RpcTarget.All, _cardType);
            photonView.RPC("RemoveUsedCard", RpcTarget.All, _cardType, lastUserID);
        }
        [PunRPC]
        private void EnableHandPointer()
        {
            AbstractPlayer ap = players.Find((p) => p.UserID == lastUserID);
            PlayerController pc = (PlayerController)ap;
            pc.EnableHandPointer();
        }
        [PunRPC]
        private void IncreaseLifesCount()
        {
            int myID = NetworkManager.Instance.gameState.GetPos(lastUserID);
            NetworkManager.Instance.gameState.players[myID].lifeCount++;
        }
        [PunRPC]
        private void ShowWhereBulletIS()
        {
            showingBarrel = StartCoroutine(ShowBarrelWithBullet());
        }

        public event Action<string, PlayingCardType, bool> CardVisualise; 
        [PunRPC]
        private void EnableExtraLifeIcon(string playerID)
        {
            Debug.Log($"<color=green>[PlayingCardsManager]EnableExtraLifeIcon{playerID} </color>");
            AbstractPlayer player = players.Find((p) => p.UserID == playerID);
            if (player == null)
            {
                Debug.LogError("player with id " + playerID + " not found");
            }

            player.PlayerInfo.EnableCardEffect(PlayingCardType.ExtraLife, true);
            CardVisualise?.Invoke(playerID,PlayingCardType.ExtraLife,true);
        }
        public void DealCards()
        {
            if (players.Count != 4) {
                players.AddRange(FindObjectsOfType<AbstractPlayer>());
            }
            photonView.RPC("RPC_ResetIDs", RpcTarget.All);
            StopAllCoroutines();
            StartCoroutine(DealingCard());
        }
        private IEnumerator DealingCard()
        {
            DisableExtraLife();
            yield return new WaitForEndOfFrame();
            DisableProtectYourSelf();
            yield return new WaitForEndOfFrame();
            DisableSetTarget();

            yield return StartCoroutine(RemoveCards());
            List<PlayingCardPrefab> card_prefabs = new List<PlayingCardPrefab>();
            card_prefabs.AddRange(this.cardPrefabs);
            int indexToRemove = Random.Range(0, cardPrefabs.Count);
            card_prefabs.RemoveAt(indexToRemove);
            Shuffle(ref card_prefabs);
            var cardList = FindObjectsOfType<PlayingCard>();
            if (cardList.Length != 0) {
                DestroyCards(cardList);
                Debug.LogError($"[PLayingCardsManager]DealingCard: before deal not zero count on table {cardList.Length}  new count{FindObjectsOfType<PlayingCard>().Length}");
            }
            yield return StartCoroutine(CreateCards(card_prefabs));
            photonView.RPC("GetCardsReferencesToOthers", RpcTarget.Others);
            cardList = FindObjectsOfType<PlayingCard>();
            if (cardList.Length != 12)
                Debug.LogError($"[PLayingCardsManager]DealingCard: after deal count {FindObjectsOfType<PlayingCard>().Length}");
        }

        public static void DestroyCards(PlayingCard[] cardList) {
            foreach (var card in cardList) {
                card.gameObject.SetActive(false);
                PhotonNetwork.Destroy(card.gameObject);
            }
        }

        public bool IsDealing => koloda.activeInHierarchy;

        [PunRPC]
        private void EnableColoda(bool enable)
        {

            koloda.SetActive(enable);
        }
        private IEnumerator RemoveCards()
        {
            yield return new WaitForEndOfFrame();
            photonView.RPC("ClearCardsReferences", RpcTarget.All);
            yield return new WaitForEndOfFrame();
        }
        private IEnumerator CreateCards(List<PlayingCardPrefab> card_prefabs)
        {
            Vector3 instPos = tableCenter.position;
            instPos.y += .15f;
            photonView.RPC("EnableColoda", RpcTarget.All, true);
            yield return new WaitForSeconds(1.8f);
            for (int i = 0; i < card_prefabs.Count; i++)
            {
                int playerNumber = i % 4;
                int cardNumer = i / 4;
                GameObject card = PhotonNetwork.InstantiateSceneObject("Cards/" + card_prefabs[i].playingCardPrefab.name, instPos, tableCenter.rotation);
                card.GetComponent<PhotonView>().RPC("InitCard", RpcTarget.All, playerNumber, cardNumer, i);
                playingCards.Add(card.GetComponent<PlayingCard>());
                yield return new WaitForSeconds(.1f);
            }

            for (int i = 0; i < 4; i++)
            {
                GameObject card = PhotonNetwork.InstantiateSceneObject("Cards/" + spinCardPrefab.name, instPos, tableCenter.rotation);
                card.GetComponent<PhotonView>().RPC("InitCard", RpcTarget.All, i, 2, 8 + i);
                playingCards.Add(card.GetComponent<PlayingCard>());
                yield return new WaitForSeconds(.1f);
            }
            photonView.RPC("EnableColoda", RpcTarget.All, false);
        }
        public Transform GetCardPoint(int playerIndex, int cardNumer)
        {
            switch(cardNumer)
            {
                case 0:
                    return cardPoints[playerIndex].firstCardPoint;
                case 1:
                    return cardPoints[playerIndex].secondCardPoint;
                case 2:
                    return cardPoints[playerIndex].reloadCardPoint;
            }

            return null;
        }
        public void ShowCardDescription(int playerIndex, string message)
        {
            cardsDescriptionList[playerIndex].text = message;
            cardsDescriptionList[playerIndex].gameObject.SetActive(true);
        }
        public void HideCardDescription(int playerIndex)
        {
            cardsDescriptionList[playerIndex].gameObject.SetActive(false);
        }
        public void Shuffle<T>(ref List<T> list)
        {
            int n = list.Count;
            System.Random rnd = new System.Random();
            while (n > 1)
            {
                int k = (rnd.Next(0, n) % n);
                n--;
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
        public void ClearLastUsed() {
            lastUserID = string.Empty;
        }

        [PunRPC]
        public void GetCardsReferencesToOthers()
        {
            playingCards.AddRange(FindObjectsOfType<PlayingCard>());
            if (players.Count != 4)
            {
                players.AddRange(FindObjectsOfType<AbstractPlayer>());
            }
        }
        [PunRPC]
        private void ClearCardsReferences()
        {
            foreach (var card in playingCards)
            {
                if (card != null && card.GetComponent<PhotonView>().IsMine)
                {
                    PhotonNetwork.Destroy(card.gameObject);
                }
            }

            playingCards.Clear();
        }

        [PunRPC]
        private void SetTargetID(string targetUserID)
        {
            this.TargetUserID = targetUserID;
            if (targetUserID != string.Empty)
            {
                AbstractPlayer player = players.Find((p) => p.UserID == targetUserID);
                if (player == null)
                {
                    Debug.LogError("player with id " + targetUserID + " not found");
                }
                Debug.Log($"<color=green>[PlayingCardsManager]SetTargetID{targetUserID} </color>");
                player.PlayerInfo.EnableCardEffect(PlayingCardType.SetTarget, true);
                CardVisualise?.Invoke(targetUserID, PlayingCardType.SetTarget, true);
            }
            else
            {

                Debug.Log($"<color=green>[PlayingCardsManager]SetTargetID {targetUserID} </color>");
                foreach (var p in players)
                    p.PlayerInfo.EnableCardEffect(PlayingCardType.SetTarget, false);
                CardVisualise?.Invoke(targetUserID, PlayingCardType.SetTarget, false);
            }
        }
        [PunRPC]
        private void SetProtectedID(string protectedUserID)
        {
            Debug.Log($"<color=green>[PlayingCardsManager]SetProtectedID {protectedUserID} </color>");
            this.ProtectedUserID = protectedUserID;
            if (protectedUserID != string.Empty)
            {

                AbstractPlayer player = players.Find((p) => p.UserID == protectedUserID);
                if (player == null)
                {
                    Debug.LogError("player with id " + protectedUserID + " not found");
                }
                player.PlayerInfo.EnableCardEffect(PlayingCardType.ProtectYourself, true);
                CardVisualise?.Invoke(protectedUserID, PlayingCardType.ProtectYourself, true);
            }
            else
            {
                foreach(var p in players)
                    p.PlayerInfo.EnableCardEffect(PlayingCardType.ProtectYourself, false);
                CardVisualise?.Invoke(protectedUserID, PlayingCardType.ProtectYourself, false);
            }
        }
        [PunRPC]
        public void DisableLastUsed()
        {
            switch(lastPlayedCard)
            {
                case PlayingCardType.DivideAllMoney:
                    break;
                case PlayingCardType.ExtraLife:
                    DisableExtraLife();
                    break;
                case PlayingCardType.ProtectYourself:
                    DisableProtectYourSelf();
                    break;
                case PlayingCardType.SecondBullet:
                    DisableSecondBullet();
                    break;
                case PlayingCardType.SeeBullet:
                    break;
                case PlayingCardType.SetTarget:
                    DisableSetTarget();
                    break;
                case PlayingCardType.SkipTurn:
                    break;
                case PlayingCardType.TurnMoveDirection:
                    DisableTurnMoveDirection();
                    break;
            }
        }

        private IEnumerator ShowBarrelWithBullet()
        {

            int playerIndex = NetworkManager.Instance.gameState.GetPos(lastUserID);
            barrelWithBullets.barrelFull.transform.LookAt(GameManager.Instance.playersPositions[playerIndex].place);
            Vector3 barrelEulers = barrelWithBullets.barrelFull.transform.eulerAngles;
            barrelEulers.x = 0f;
            barrelEulers.z = 0f;
            barrelWithBullets.pointer.transform.eulerAngles = barrelEulers;
            barrelEulers.z = 60 * NetworkManager.Instance.gameState.currIndex;
            barrelWithBullets.barrelFull.transform.eulerAngles = barrelEulers;
            barrelWithBullets.HideBullets();
            barrelWithBullets.bullets[NetworkManager.Instance.gameState.bulletIndex - 1].SetActive(true);
            if (MainPistol.Instance.SecondBulletInserted)
            {
                int secondBullet = Random.Range(NetworkManager.Instance.gameState.bulletIndex, 7);
                barrelWithBullets.bullets[secondBullet - 1].SetActive(true);
            }
            barrelWithBullets.Enable(true);
            yield return new WaitForSeconds(10f);
            barrelWithBullets.Enable(false);
        }

        public void CheckKilledUser(string killedID)
        {
            if(TargetUserID == killedID)
            {
                DisableSetTarget();
            }

            if(ProtectedUserID == killedID)
            {
                DisableProtectYourSelf();
            }
        }
        [PunRPC]
        private void CardsEffectRefs(string playerID)
        {
            List<FollowPlayerEffect> fpe = new List<FollowPlayerEffect>();
            fpe.AddRange(FindObjectsOfType<FollowPlayerEffect>());
            List<FollowPlayerEffect> fpe_to_delete = fpe.FindAll((card) => card.TargetID == playerID);
            foreach (var c in fpe_to_delete)
                PhotonNetwork.Destroy(c.gameObject);

            cardsVFX.ExtraLifeEffect = fpe.Find((c) => c.Effect == PlayingCardType.ExtraLife);
            cardsVFX.PlayerShieldEffect = fpe.Find((c) => c.Effect == PlayingCardType.ProtectYourself);
            cardsVFX.PlayerAimEffect = fpe.Find((c) => c.Effect == PlayingCardType.SetTarget);
        }
        #region Disable Effects
        [PunRPC]
        private void DisableSecondBullet()
        {
            NetworkManager.Instance.gameState.Spin();
            MainPistol.Instance.PhotonView.RPC("InsertSecondBulletStop", RpcTarget.All);
        }
        [PunRPC]
        private void DisableProtectYourSelf()
        {
            Debug.Log("disable protect yourself");
            photonView.RPC("SetProtectedID", RpcTarget.All, string.Empty);
        }
        [PunRPC]
        private void DisableSetTarget()
        {
            Debug.Log("disable set target");
            photonView.RPC("SetTargetID", RpcTarget.All, string.Empty);
            cardsVFX.PlayerAimEffect = null;
        }
        [PunRPC]
        private void DisableExtraLife(bool syncState = true)
        {
            Debug.Log("disable extra life! extra life is null? " + (cardsVFX.ExtraLifeEffect == null));
            PlayerState[] players = NetworkManager.Instance.gameState.players;
            for (int i = 0; i < players.Length; i++)
            {
                if (!players[i].IsKilled)
                    players[i].lifeCount = 1;
            }

            Debug.Log($"<color=green>[PlayingCardsManager]DisableExtraLife {syncState} </color>");
            photonView.RPC("DisableExtraLifeIcon", RpcTarget.All);
        }
        [PunRPC]
        private void DisableExtraLifeIcon()
        {
            foreach (var p in players) {
                p.PlayerInfo.EnableCardEffect(PlayingCardType.ExtraLife, false);
                CardVisualise?.Invoke(p.UserID, PlayingCardType.ExtraLife, false);
            }

        }
        [PunRPC]
        private void DisableTurnMoveDirection()
        {
            NetworkManager.Instance.gameState.turnsClockwise = false;
            NetworkManager.Instance.gameState.SwitchTurnToNextUser();
            NetworkManager.Instance.RequestToSync();
            NetworkManager.Instance.SyncPistolState();
            MainPistol.Instance.PistolMoving.PutPistolOnTableCenter();
            MainPistol.Instance.PhotonView.RPC("PutPistolOnTableCenter", RpcTarget.All);
        }
        #endregion
    }

    [System.Serializable]
    public class PlayerCardsPoints
    {
        public Transform firstCardPoint;
        public Transform secondCardPoint;
        public Transform reloadCardPoint;
    }

    [System.Serializable]
    public class PlayingCardPrefab
    {
        public PlayingCardType playingCardType;
        public GameObject playingCardPrefab;
    }

    [System.Serializable]
    public class BarrelUI {
        public GameObject barrelFull;
        public List<GameObject> bullets;
        public GameObject pointer;
        public void HideBullets() {

            for (int i = 0; i < bullets.Count; i++) {
                if(bullets[i].activeSelf)
                    bullets[i].SetActive(false);
            }
        }

        public void Enable(bool b) {
            barrelFull.SetActive(b);
            pointer.SetActive(b);
        }
    }
}