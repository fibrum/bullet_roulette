﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class Cigarette : MonoBehaviour
    {
        public static Cigarette Instance;
        public int cigaretteID = 1;
        [SerializeField]
        private Vector3 inMouthLocalPosition;
        [SerializeField]
        private Vector3 inMouthLocalRotation;

        private PhotonView photonView;
        private Transform mouthTransform;
        new private Rigidbody rigidbody;
        public bool cigareteInMouthNow { get; private set; }
        [SerializeField] private List<CigaretteNetworkPositions> cigaretteNetworkPositions = new List<CigaretteNetworkPositions>();

        private void Awake()
        {
            if(Instance == null)
            {
                Instance = this;
            }
            rigidbody = GetComponent<Rigidbody>();
            photonView = GetComponent<PhotonView>();
        }
        private void Start()
        {
            cigaretteID = photonView.ViewID;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Mouth"))
                return;

            var mouth=
            other.GetComponent<PlayerMouth>();
            if (mouth == null) {
                Debug.LogError("ERROR "+other.name + " root: " + other.transform.root.name);
            }

            if (mouth.IsMouthOpen) return;

            mouth.OpenMouth();

            var mouthHolder = other.GetComponentInParent<PlayerController>().GetComponent<PhotonView>().Owner;

            if(photonView.Owner == mouthHolder)
            {
                cigareteInMouthNow = true;
            }
            mouthTransform = other.transform;
        }
        private void OnTriggerExit(Collider other)
        {
            if (!other.CompareTag("Mouth"))
                return;

            var mouthHolder = other.GetComponentInParent<PlayerController>().GetComponent<PhotonView>().Owner;
            if (photonView.Owner == mouthHolder)
            {
                cigareteInMouthNow = false;
                //mouthTransform = null;
            }
            other.GetComponent<PlayerMouth>().CloseMouth();
        }
        private IEnumerator AttachNumeratorNetwork()
        {
            yield return new WaitForSeconds(.1f);
            var players = FindObjectsOfType<PlayerController>();

            for(int i = 0; i < players.Length; i++)
            {
                if(players[i].UserID == photonView.Owner.UserId)
                {
                    mouthTransform = players[i].GetComponentInChildren<PlayerMouth>().transform;
                    break;
                }
            }

            if (mouthTransform != null)
            {
                cigareteInMouthNow = true;
                rigidbody.isKinematic = true;
                Debug.Log("name of player which mouth: " + mouthTransform.root.name);
                GetComponent<PhotonTransformView>().enabled = false;
                transform.SetParent(mouthTransform);
                var type = transform.root.GetComponent<AbstractPlayer>().typeOfPlayer;
                transform.localPosition = cigaretteNetworkPositions.Find(c => c.playerType == type).localPosition;
                transform.localRotation = cigaretteNetworkPositions.Find(c => c.playerType == type).localRotation; 
            }
            else
            {
                Debug.LogError("Mouth transform is null");
            }
        }
        private IEnumerator AttachNumerator()
        {
            yield return new WaitForSeconds(.1f);

            if (mouthTransform != null)
            {
                rigidbody.isKinematic = true;
                Debug.Log("name is: " + mouthTransform.name);
                GetComponent<PhotonTransformView>().enabled = false;
                transform.SetParent(mouthTransform);
                transform.localEulerAngles = this.inMouthLocalRotation;
                transform.localPosition = this.inMouthLocalPosition;
            }
            else
            {
                Debug.LogError("Mouth transform is null");
            }
            mouthTransform = null;
        }
        public void PutCigaretteOnTable()
        {
            GetComponent<GrabbaleObjects>().StopGrab(false);

            this.gameObject.transform.GetChild(0).localEulerAngles = new Vector3(-60, 0f, 0f);
            Vector3 fromTo = GameManager.Instance.tableCenter.position - this.gameObject.transform.position;
            Vector3 fromToXZ = new Vector3(fromTo.x, 0f, fromTo.z);

            this.gameObject.transform.rotation = Quaternion.LookRotation(fromToXZ, Vector3.up);

            float g = Physics.gravity.y;
            float x = fromToXZ.magnitude;
            float y = fromTo.y;

            float AngleInRadians = 60 * Mathf.PI / 180;

            float v2 = (g * x * x) / (2 * (y - Mathf.Tan(AngleInRadians) * x) * Mathf.Pow(Mathf.Cos(AngleInRadians), 2));
            float v = Mathf.Sqrt(Mathf.Abs(v2));

            if (photonView.IsMine)
                photonView.RPC("RPC_PutCigaretteOnTable", RpcTarget.All, gameObject.transform.GetChild(0).forward, v);
            

        }
       
        public void AttachCigaretteToMouth()
        {
            if (photonView.IsMine)
            {
                if(mouthTransform != null) //нельзя вставить больше одной сигареты в рот
                {
                    if (mouthTransform.GetComponentInChildren<Cigarette>() != null)
                        return;
                }

                if (!cigareteInMouthNow)
                    return;

                PitCigaretteInMouth();

                photonView.RPC("EnableDisableKinimatic", RpcTarget.All, true);
                photonView.RPC("PitCigaretteInMouthNetwork", RpcTarget.Others);
            }
        }

        [PunRPC]
        private void RPC_StopGrabbCigarette()
        {
            bool enabled = GetComponent<PhotonTransformView>().enabled;

            if (enabled)
                return;

            GetComponent<PhotonTransformView>().enabled = true;
            GetComponent<PhotonRigidbodyView>().enabled = true;
        }

        public void StopGrabCigarette()
        {
            photonView.RPC("RPC_StopGrabbCigarette", RpcTarget.All);
        }
        public void SetUser(Player player)
        {
            photonView.TransferOwnership(player);            
            photonView.OwnershipWasTransfered = false;
        }

        [PunRPC]
        private void PitCigaretteInMouth()
        {
            StartCoroutine(AttachNumerator());
        }
        [PunRPC]
        private void PitCigaretteInMouthNetwork()
        {
            StartCoroutine(AttachNumeratorNetwork());
        }
        [PunRPC]
        private void EnableDisableKinimatic(bool b)
        {
            rigidbody.isKinematic = b;
            GetComponent<PhotonRigidbodyView>().enabled = !b;
        }
        [PunRPC]
        private void RPC_PutCigaretteOnTable(Vector3 dir, float speed)
        {
            rigidbody.isKinematic = false;
            rigidbody.velocity = dir * speed;
        }

        #region ContextMenu
        [ContextMenu("Save gentelman pos")]
        private void SaveLocalPosition()
        {
            inMouthLocalPosition = transform.localPosition;
            inMouthLocalRotation = transform.eulerAngles;
        }

        [ContextMenu("Save gentelman pos")]
        private void SaveGentelman()
        {
            cigaretteNetworkPositions.Find(c => c.playerType == PlayerType.Gentlman).localPosition = transform.localPosition;
            cigaretteNetworkPositions.Find(c => c.playerType == PlayerType.Gentlman).localRotation = transform.localRotation;
        }
        [ContextMenu("Save dame pos")]
        private void SaveDame()
        {
            cigaretteNetworkPositions.Find(c => c.playerType == PlayerType.Dama).localPosition = transform.localPosition;
            cigaretteNetworkPositions.Find(c => c.playerType == PlayerType.Dama).localRotation = transform.localRotation;
        }
        [ContextMenu("Save police pos")]
        private void SavePolice()
        {
            cigaretteNetworkPositions.Find(c => c.playerType == PlayerType.PoliceFemale).localPosition = transform.localPosition;
            cigaretteNetworkPositions.Find(c => c.playerType == PlayerType.PoliceFemale).localRotation = transform.localRotation;
        }
        [ContextMenu("Save bandit pos")]
        private void SaveBandit()
        {
            cigaretteNetworkPositions.Find(c => c.playerType == PlayerType.Bandit).localPosition = transform.localPosition;
            cigaretteNetworkPositions.Find(c => c.playerType == PlayerType.Bandit).localRotation = transform.localRotation;
        }
        [ContextMenu("Save ranger pos")]
        private void SaveRanger()
        {
            cigaretteNetworkPositions.Find(c => c.playerType == PlayerType.Ranger).localPosition = transform.localPosition;
            cigaretteNetworkPositions.Find(c => c.playerType == PlayerType.Ranger).localRotation = transform.localRotation;
        }
        [ContextMenu("Save indian pos")]
        private void SaveIndian()
        {
            cigaretteNetworkPositions.Find(c => c.playerType == PlayerType.Indian).localPosition = transform.localPosition;
            cigaretteNetworkPositions.Find(c => c.playerType == PlayerType.Indian).localRotation = transform.localRotation;
        }

        [ContextMenu("Empty")]
        private void Empty()
        {

        }
        #endregion
    }

    [System.Serializable]
    public class CigaretteNetworkPositions
    {
        public PlayerType playerType;
        public Vector3 localPosition;
        public Quaternion localRotation;
    }
}