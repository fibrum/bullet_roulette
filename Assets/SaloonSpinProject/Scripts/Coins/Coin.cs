﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

namespace SaloonSpin {

    public class Coin : MonoBehaviour {
        public PhotonView photonView;
        public string HolderID;
        private Rigidbody rigidbody;
        private bool move;
        #region Moving variables
        private Vector3 startMarker;
        private Vector3 endMarker;
        private float startTime;
        private float journeyLength;
        private float speed = 2f;
        #endregion
        private void Awake() {
            rigidbody = GetComponent<Rigidbody>();
            photonView = GetComponent<PhotonView>();
        }

        private void Update()
        {
            if (move)
            {
                float distCovered = (Time.time - startTime) * speed;
                float fracJourney = distCovered / journeyLength;
                transform.position = Vector3.Lerp(startMarker, endMarker, fracJourney);

                if (Vector3.Distance(transform.position, endMarker) < 0.01f)
                {
                    transform.position = endMarker;
                    move = false;
                    rigidbody.isKinematic = false;
                }
            }
        }

        public void MoveCoinToTableCenter() {
            startMarker = transform.position;
            endMarker = GameManager.Instance.tableCenter.position;
            endMarker.y += Random.Range(0.05f, 0.12f);
            journeyLength = Vector3.Distance(startMarker, endMarker);
            startTime = Time.time;
            rigidbody.isKinematic = true;
            move = true;
        }

        public void MoveCoin(Vector3 target, float Y_Offset = 0f) {
            startMarker = transform.position;
            endMarker = target;
            endMarker.y += Y_Offset;
            journeyLength = Vector3.Distance(startMarker, endMarker);
            startTime = Time.time;
            rigidbody.isKinematic = true;
            move = true;
        }
        public void SetCoinOwner(string userID)
        {
            photonView.RPC("SetOwner", RpcTarget.All, userID);
        }

        [PunRPC]
        private void RPC_MoveCoin(Vector3 start, Vector3 end)
        {
            startMarker = start;
            endMarker = end;
            journeyLength = Vector3.Distance(startMarker, endMarker);
            startTime = Time.time;
            rigidbody.isKinematic = true;
            move = true;
        }

        [PunRPC]
        private void ChangeOwner() {
            photonView.TransferOwnership(PhotonNetwork.LocalPlayer);
        }

        [PunRPC]
        private void SetOwner(string id) {
            HolderID = id;
        }

        
    }
}