﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class Coins : MonoBehaviour
    {
        public static Coins Instance;
        [SerializeField] private GameObject coinPrefab;
        private PhotonView photonView;
        private List<Coin> coinsList = new List<Coin>();

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            photonView = GetComponent<PhotonView>();
        }

        public void CreateCoin(string userID, int count)
        {
            photonView.RPC("RPC_CreateCoin", RpcTarget.MasterClient, userID, count);
        }
        public void RemoveCoin(string userID, int count)
        {
            photonView.RPC("RPC_RemoveCoin", RpcTarget.MasterClient, userID, count);
        }
        [PunRPC]
        public void GetCoinsRefs()
        {
            coinsList.Clear();
            coinsList.AddRange(FindObjectsOfType<Coin>());
        }
        [PunRPC]
        private void RPC_RemoveCoin(string userID, int count)
        {
            if(count == -1)
            {
                StopAllCoroutines();
                StartCoroutine(RemoveAllUserCoins(userID));
            }
            else
            {
                RemoveUserCoin(userID);
            }
        }
        [PunRPC]
        private void RPC_CreateCoin(string userID, int count)
        {
            StartCoroutine(CreateCoinsNumerator(userID, count));
        }

        private IEnumerator CreateCoinsNumerator(string userID, int count)
        {
            yield return new WaitForSeconds(.5f);
            int index = NetworkManager.Instance.gameState.GetPos(userID);
            Vector3 position = GameManager.Instance.playersPositions[index].coinSpawnPont.position;
            Quaternion rotation = GameManager.Instance.playersPositions[index].coinSpawnPont.rotation;

            for(int i = 0; i < count; i++)
            {
                Coin coin = PhotonNetwork.Instantiate(coinPrefab.name, position, rotation).GetComponent<Coin>();
                coin.SetCoinOwner(userID);
                coinsList.Add(coin);
                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
            }
        }

        private void RemoveUserCoin(string userID)
        {
            Coin coin = coinsList.Find((c) => c.HolderID == userID);
            coinsList.Remove(coin);
            PhotonNetwork.Destroy(coin.gameObject);
        }
        private IEnumerator RemoveAllUserCoins(string userID)
        {
            List<Coin> coins = coinsList.FindAll((c) => c.HolderID == userID);
            for(int i = 0; i < coins.Count; i++)
            {
                coinsList.Remove(coins[i]);

                if (coins[i] != null)
                    PhotonNetwork.Destroy(coins[i].gameObject);

                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
            }
        }

        public void DivideCoins()
        {
            StartCoroutine(ShuffleCoinsNumerator());
        }

        private IEnumerator ShuffleCoinsNumerator()
        {
            yield return new WaitForSeconds(.2f);
            coinsList.RemoveAll((c) => c == null);

            List<int> indexesOfAlivePlayers = new List<int>();

            for (int i = 0; i < 4; i++)
            {
                if (!NetworkManager.Instance.gameState.players[i].IsKilled)
                    indexesOfAlivePlayers.Add(i);

                NetworkManager.Instance.gameState.players[i].money = 0;
            }

            for (int i = 0; i < coinsList.Count; i++)
            {
                coinsList[i].MoveCoinToTableCenter();
                yield return new WaitForSeconds(.1f);
            }

            int divider = indexesOfAlivePlayers.Count;
            int startPlayerIndex = Random.Range(0, divider);

            for (int i = 0; i < coinsList.Count; i++)
            {
                int index = (startPlayerIndex + i) % divider;
                int playerIndex = indexesOfAlivePlayers[index];
                Vector3 position = GameManager.Instance.playersPositions[playerIndex].coinSpawnPont.position;
                NetworkManager.Instance.gameState.players[playerIndex].money++;
                position.y -= 1f;
                coinsList[i].MoveCoin(position);
                coinsList[i].SetCoinOwner(NetworkManager.Instance.gameState.players[playerIndex].UserID);
            }

            NetworkManager.Instance.RequestToSync();
        }
    }
}
