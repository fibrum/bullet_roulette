﻿namespace SaloonSpin
{
    public enum GrabbObject
    {
        Pistol,
        ReloadCard,
        Bullet,
        Cigarette,
        Glass,
        AFK_kill_Pistol,
        Pepelka,
        MainPistol,
        PlayingCard
    }
}
