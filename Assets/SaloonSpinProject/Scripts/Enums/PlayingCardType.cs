﻿
public enum PlayingCardType
{
   TurnMoveDirection,
   ExtraLife,
   SecondBullet,
   SeeBullet,
   DivideAllMoney,
   SkipTurn,
   SetTarget,
   ProtectYourself,
   UndoLastPlayed,
   SpinPistol
}
