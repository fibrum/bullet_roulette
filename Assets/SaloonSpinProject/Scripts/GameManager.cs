﻿using Photon.Pun;
using Photon.Voice.DemoVoiceUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SaloonSpin
{
    [System.Serializable]
    public class InteractObjects
    {
        public Transform objTransform;
        public GrabbObject objType;
    }
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;
        [SerializeField]
        public GameObject startButton;
        public List<InteractObjects> interactObjects = new List<InteractObjects>(); 

        public List<PlayerPostion> playersPositions = new List<PlayerPostion>(); /*список объектов для отображения состояния игороков*/
        public Transform winPanel; /*панель которая отображается когда игрок победил*/
        public Transform tableCenter; /*Исходная точка пистолета*/
        public Transform reloadCardBox; //сюда бросать карты для прокрутки

        private void Awake()
        {
            Instance = this; 
        }

        private void Start()
        {
            NetworkManager.Instance.onSyncGameState += UpdateGameState;
        }

        public void WaitForRestart() {
            winPanel.gameObject.SetActive(false);
            foreach (PlayerPostion pp in playersPositions)
            {
                pp.moneyText.gameObject.SetActive(false);
                pp.turnArrow.SetActive(false);
                //pp.reloadIndicator.SetActive(false);
            }
        }

        private void UpdateGameState(GameState state) /*Обновление индикации состояния игры (Вызвается при синхронизации состояния в MainNetworking)*/
        {
            int alivePlayers = 0;
            winPanel.gameObject.SetActive(false);
            foreach (PlayerPostion pp in playersPositions)
            {
                pp.moneyText.gameObject.SetActive(false);
                pp.turnArrow.SetActive(false);
                //pp.reloadIndicator.SetActive(false);
            }
            foreach (PlayerState ps in state.players)
            {
                if (string.IsNullOrEmpty(ps.UserID)) continue;

                int currPlayerPos = state.GetPos(ps.UserID);
                if (!ps.IsKilled)
                {
                    alivePlayers++;
                    if (Photon.Pun.PhotonNetwork.LocalPlayer.UserId == ps.UserID)//на столе игрок видит только свои деньги
                    {
                        playersPositions[currPlayerPos].moneyText.gameObject.SetActive(true);
                        playersPositions[currPlayerPos].moneyText.text = ps.money.ToString();
                    }
                    //if (ps.canReload)
                    //{
                    //    playersPositions[currPlayerPos].reloadIndicator.SetActive(true);
                    //}
                }
            }
            int turnUserPos = state.GetPos(state.userTurn);
            playersPositions[turnUserPos].turnArrow.SetActive(true);
            if (state.playersCount > 1 && alivePlayers == 1)
            {
                playersPositions[turnUserPos].turnArrow.SetActive(false);
            }
            winPanel.LookAt(playersPositions[turnUserPos].place);
            if (state.playersCount > 1 && alivePlayers == 1)
            {
                int aliveUserPos = 0;
                for(int i = 0; i < state.players.Length; i++)
                {
                    if(!state.players[i].IsKilled)
                    {
                        aliveUserPos = state.GetPos(state.userTurn);
                        break;
                    }
                }

                var panelEulerAngels = winPanel.transform.localEulerAngles;
                panelEulerAngels.x = 0f;
                winPanel.transform.localEulerAngles = panelEulerAngels;
                winPanel.gameObject.SetActive(true);

                if (state.userTurn == PhotonNetwork.LocalPlayer.UserId)
                {
                    winPanel.GetComponent<WinPanel>().winPanel.SetActive(true);
                }

                if(Photon.Pun.PhotonNetwork.IsMasterClient)
                {
                    startButton.GetComponentInChildren<UnityEngine.UI.Text>().text = I2.Loc.ScriptLocalization.RESTART;//"Restart";
                    StartCoroutine(ShowStartPanelAfterWin());
                }
            }
            if(state.playersCount > 1 && alivePlayers == 0)
            {
                if (Photon.Pun.PhotonNetwork.IsMasterClient)
                {
                    NetworkManager.Instance.StartGame();
                }
            }

        }
        private IEnumerator ShowStartPanelAfterWin()
        {
            while (!MainPistol.Instance.PistolMoving.PistolOnCenterOfTable)
                yield return null;

            this.startButton.SetActive(true);
        }
        public int AlivePlayerCount(GameState state)
        {
            int count = 0;
            for(int i = 0; i < 4; i++)
            {
                if (!state.players[i].IsKilled)
                    count++;
            }
            return count;
        }

        public PlayerController GetPlayer(string id) /*Возвращает контроллер игрока по id*/
        {
            PlayerController[] players = FindObjectsOfType<PlayerController>();
            foreach (PlayerController p in players)
            {
                if (p.UserID == id)
                {
                    return p;
                }
            }
            return players[0];
        }

        public void ResetObjectsPosition()
        {
            foreach (var pp in playersPositions)
                pp.Hide();

            winPanel.gameObject.SetActive(false);
        }

        public void UpdateStartButtonPosition(Vector3 newPos, Quaternion newRot)
        {
            startButton.transform.parent.parent.localRotation = newRot;
            if(!NetworkManager.Instance.gameRuns)
            {
                startButton.SetActive(true);
            }
        }
        public void StartButtonToDefaultPosition()
        {
            startButton.transform.parent.parent.localRotation = Quaternion.identity;
        }
        public MicRef microphoneSettings;
        public bool isMicSetted;
    }
    
}
