﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class AFK_Checker : MonoBehaviour
    {
        public float timeToMakeShot = 15f;
        [SerializeField] private List<AFK_Pistol> afkPistols = new List<AFK_Pistol>();
        private List<Vector3> pistolPositions = new List<Vector3>();
        private List<Quaternion> pistolsRotation = new List<Quaternion>();
        private Coroutine countdown;
        private PhotonView photonView;
        private void Awake()
        {
            photonView = GetComponent<PhotonView>();
        }
        private void Start()
        {
            for(int i = 0; i < afkPistols.Count; i++)
            {
                pistolPositions.Add(afkPistols[i].transform.position);
                pistolsRotation.Add(afkPistols[i].transform.rotation);
                afkPistols[i].GetComponent<VRTK.VRTK_InteractableObject>().enabled = false;
                afkPistols[i].gameObject.SetActive(false);
            }
            //HidePistols();
        }
        public void StartCountDown()
        {
            photonView.RPC("RPC_StartCountDown", RpcTarget.All);
        }

        IEnumerator AFK_Countdown()
        {
            yield return new WaitForSeconds(timeToMakeShot);
            GiveOutPistols();
            countdown = null;
        }

        public void HidePistols()
        {
            for (int i = 0; i < afkPistols.Count; i++)
            {
                if (afkPistols[i].gameObject.activeSelf)
                {
                    afkPistols[i].StopGrab();
                    afkPistols[i].ReturnOnTable(pistolPositions[i], pistolsRotation[i]);
                    afkPistols[i].GetComponent<VRTK.VRTK_InteractableObject>().enabled = false;
                }
            }
        }

        private void GiveOutPistols()
        {
            //if (NetworkManager.Instance.gameState.GetPlayerState(NetworkManager.Instance.gameState.userTurn).isNPC)
            //    return;

            string myID = PhotonNetwork.LocalPlayer.UserId;
            if(myID != NetworkManager.Instance.gameState.userTurn)
            {
                int index = NetworkManager.Instance.gameState.GetMyPos(myID);
                photonView.RPC("RPC_EnablePistol", RpcTarget.All, index);
            }
        }



        [PunRPC]
        private void RPC_EnablePistol(int index)
        {
            if (NetworkManager.Instance.gameRuns)
            {
                afkPistols[index].gameObject.SetActive(true);
                bool myTurn = PhotonNetwork.LocalPlayer.UserId == NetworkManager.Instance.gameState.userTurn;

                string myID = PhotonNetwork.LocalPlayer.UserId;
                int ind_ = NetworkManager.Instance.gameState.GetMyPos(myID);//index of local player

                //afkPistols[index].GetComponent<VRTK.VRTK_InteractableObject>().isGrabbable = !myTurn; //игрок, затягивающий время не может взять пистолет
                afkPistols[index].GetComponent<VRTK.VRTK_InteractableObject>().enabled = ind_ == index; //каждому игроку принадлежит один пистолет
                var pos = pistolPositions[index];
                
                afkPistols[index].transform.position = pos;// pistolPositions[index];
                afkPistols[index].returning = false;
            }
        }
        [PunRPC]
        private void RPC_StartCountDown()
        {
            if (countdown != null)
                StopCoroutine(countdown);

            HidePistols();
            countdown = StartCoroutine(AFK_Countdown());
        }
    }
}
