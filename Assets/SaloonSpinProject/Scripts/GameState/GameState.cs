﻿using Photon.Pun;
using UnityEngine;
namespace SaloonSpin
{
    [System.Serializable]
    public class GameState
    {
        public PlayerState[] players = new PlayerState[4];  /*пользователи*/
        public int bulletIndex = 1; /*место пули в барабане*/
        public int currIndex = 0;  /*место где находится барабан*/
        public int playersCount = 0; /*кол-во пользоватлей в комнате*/
        public string userTurn = ""; /*id пользователь который должен стерлять*/
        public bool turnsClockwise = false; //очерель идет против часов стрелки, если false
        public bool npcTurnCalled = false;
        public int GetPos(string id) /*Определения индекса игрока в массиве игроков*/
        {
            for (int i = 0; i < 4; i++)
            {
                if ((players[i].UserID == id))
                {
                    return i;
                }
            }
            return 0;
        }

        public bool MyTurn { get { return PhotonNetwork.LocalPlayer.UserId == userTurn; } }

        public int GetMyPos()/*Определения своего индекса в массиве игроков*/
        {
            for (int i = 0; i < 4; i++)
            {
                if (players[i].UserID == PhotonNetwork.LocalPlayer.UserId)
                {
                    return i;
                }
            }
            return 0;
        }

        public int GetMyPos(string userID)
        {
            for (int i = 0; i < 4; i++)
            {
                if (players[i].UserID == userID)
                {
                    return i;
                }
            }
            return 0;
        }

        public void MakeNewUser(string id, bool npc = false)
        {
            playersCount++;
            for (int i = 0; i < 4; i++)
            {
                if (string.IsNullOrEmpty(players[i].UserID))
                {
                    PlayerState ps = new PlayerState();
                    ps.UserID = id;
                    ps.IsKilled = false;
                    ps.money = 0;
                    ps.canReload = true;
                    ps.isNPC = npc;
                    ps.playerWhoShotMe = new string[0];
                    players[i] = ps;
                    break;
                }
            }
        }

        public bool IsBullet() {
            return (currIndex + 1) == bulletIndex;
        }

        public void Spin() {
            Random.InitState((int)Time.time);
            var newRandomValue = Random.Range(1, 7);
            while (newRandomValue == bulletIndex)
            {
                newRandomValue = Random.Range(1, 7);
                if (newRandomValue == 1)
                {
                    float rnd = Random.Range(0f, 1f);
                    if (rnd < .9f)
                    {
                        newRandomValue = Random.Range(2, 7);
                    }
                }
            }

            bulletIndex = newRandomValue;

            currIndex = 0;
        }

        public void DoubleBulletSpin() {
            currIndex = 0;
            bulletIndex = Random.Range(1, 4);
        }


        public void DeleteUser(string id)
        {
            playersCount--;
            players[GetPos(id)] = new PlayerState();
        }

        public void SwitchTurnToNextUser()
        {
            npcTurnCalled = false;
            if (playersCount <= 1) return;

            if(turnsClockwise)
                SwitchTurClockwise();
            else SwitchTurnCounerClockwise();

            Debug.Log("TURN SWITHCED TO: " + userTurn);
        }
        public int AlivePlayers()
        {
            int count = 0;
            for (int i = 0; i < 4; i++)
                if (!NetworkManager.Instance.gameState.players[i].IsKilled)
                    count++;
            return count;
        }

        private void SwitchTurnCounerClockwise() {
            int j = 0;
            int i = GetPos(userTurn);
            while (j < 10)
            {
                if (i > players.Length - 1)
                {
                    i = 0;
                }
                if (players[i].UserID != userTurn && !string.IsNullOrEmpty(players[i].UserID) && !players[i].IsKilled)
                {
                    userTurn = players[i].UserID;
                    break;
                }
                i++;
                j++;
            }
        } //изменение очереди против часовой стрелки
        private void SwitchTurClockwise()
        {
            int j = 0;
            int i = GetPos(userTurn);
            while (j < 10)
            {
                if (i < 0)
                {
                    i = players.Length - 1;
                }
                if (players[i].UserID != userTurn && !string.IsNullOrEmpty(players[i].UserID) && !players[i].IsKilled)
                {
                    userTurn = players[i].UserID;
                    break;
                }
                i--;
                j++;
            }
        } //изменение очереди по часовой стрелке

        public void MakeRandomTurn()
        {
            int l = Random.Range(0, 4);
            userTurn = players[l].UserID;
            Debug.Log("random numbers is: " + l);
        }
        public PlayerState GetPlayerState(string id) /*получение состояния игрока*/
        {
            return players[GetPos(id)];
        }

        public void ClearUsersStates()
        {
            for (int i = 0; i < 4; i++)
                players[i] = new PlayerState();
            playersCount = 0;
        }

        public override string ToString()
        {
            return $"bullet index: {bulletIndex} \ncurr index: {currIndex} user turn: {userTurn}";
            
        }

        public void ResetState() {
            Random.InitState((int)Time.time);
            for (int i = 0; i < 4; i++)
            {
                players[i].IsKilled = false;
                players[i].money = 0;
                players[i].canReload = true;
                players[i].playerWhoShotMe = new string[0];
            }
            currIndex = bulletIndex = 0;
            turnsClockwise = false;
            npcTurnCalled = false;
        }
    }
}