﻿
using UnityEngine;
using UnityEngine.UI;
namespace SaloonSpin
{
    [System.Serializable]
    public class PlayerPostion /*объкеты для отображения состояния игороков*/
    {
        public Transform drinkSpawnPoint; //спавн напитков
        public Transform coinSpawnPont; //места спавна монеты
        public Transform pepelnitsaSpawnPoint;//спавн пепельницы
        public Transform cigaretteSpawnPoint;//спавн сигарет
        public Transform place; /*место нпс за столом*/
        public Transform playerPlace; //место игрока за столом
        public GameObject turnArrow; /*стелка указывающая на игрока который должен стрелять*/
        public GameObject reloadIndicator; /*показывает может ли игрок сделать прокрутку барабана*/
        public Text moneyText; /*отображение денег*/
        public Text takeGun;
        public GameObject reloadCardPoint;

        public void Hide()
        {
            reloadCardPoint.SetActive(false);
            moneyText.gameObject.SetActive(false);
            turnArrow.SetActive(false);
            reloadIndicator.SetActive(false);
        }
    }
}
