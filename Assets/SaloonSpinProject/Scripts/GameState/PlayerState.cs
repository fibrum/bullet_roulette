﻿using UnityEngine;

namespace SaloonSpin
{
    [System.Serializable]
    public class PlayerState {
        private bool isKilled;
        public string UserID = "";  /*id пользователя(которое задается фотоном)*/

        public bool IsKilled {
            get { return isKilled; }
            set {
                if (value) {
                    lifeCount--;

                    if (lifeCount <= 0)
                        isKilled = true;
                    else isKilled = false;
                }
                else {
                    isKilled = false;
                    lifeCount = 1;
                }
            }
        } /*жив ли игрок, да-может играть, нет - наблюдатель*/

        public int lifeCount = 1;
        public int money = 10;  /*кол-во денег*/
        public bool canReload = true;  /*может ли игрок сделать прокуртку барабана(один раз за раунд)*/
        public bool isNPC = false; //npc это или реальный игрок
        public string[] playerWhoShotMe; //игроки которые стреляли в этого нпс

        public PlayerState() {
            lifeCount = 1;
            UserID = string.Empty;
            IsKilled = false;
            money = 0;
            canReload = true;
            isNPC = false;
            playerWhoShotMe = new string[0];
        }

        public override string ToString() {
            return JsonUtility.ToJson(this);
        }
    }
}