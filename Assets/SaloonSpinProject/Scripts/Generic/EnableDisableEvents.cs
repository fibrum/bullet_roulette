﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnableDisableEvents : MonoBehaviour
{
    public UnityEvent OnEnableEv;
    public UnityEvent OnDisableEv;
    private void OnEnable()
    {
        OnEnableEv.Invoke();
    }

    private void OnDisable()
    {
        OnDisableEv.Invoke();
    }
}
