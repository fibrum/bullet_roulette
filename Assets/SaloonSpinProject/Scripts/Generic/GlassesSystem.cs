﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class GlassesSystem : MonoBehaviour
    {
        public static GlassesSystem Instance;
        [SerializeField] private GameObject wineGlassPrefab;
        [SerializeField] private GameObject roxPrefab;
        [SerializeField] private GameObject cupOfTeaPrefab;

        public List<KeyValuePair<int, GlassType>> crashedGlasses = new List<KeyValuePair<int, GlassType>>();
        private List<Glass> glasses = new List<Glass>();

        [ContextMenu("Show crashed glasses")]
        private void Show()
        {
            for(int i = 0; i < crashedGlasses.Count; i++)
            {
                Debug.Log(crashedGlasses[i].Key + " - " + crashedGlasses[i].Value);
            }
        }

        public bool HaveBrokenGlasses
        {
            get { return crashedGlasses.Count > 0 || glasses.Count > 0; }
        }
        public int BrokenGlassesCount { get { return crashedGlasses.Count; } }

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        public void GlassCrashed(GlassType type, int playerIdex)
        {
            crashedGlasses.Add(new KeyValuePair<int, GlassType>(playerIdex, type));
            Debug.Log("GLASS CRASHED: " + type.ToString() + " index: " + playerIdex);
        }

        public void CreateGlass(Transform instPoint, int number)
        {
            GameObject glass = null;

            switch (crashedGlasses[0].Value)
            {
                case GlassType.WineGlass:
                    glass = Instantiate(wineGlassPrefab, instPoint);
                    break;
                case GlassType.Rox:
                    glass = Instantiate(roxPrefab, instPoint);
                    break;
                case GlassType.CupOfTea:
                    glass = Instantiate(cupOfTeaPrefab, instPoint);
                    break;
            }
            glass.transform.localRotation = Quaternion.identity;
            glass.transform.localPosition = Vector3.zero;
            glass.transform.GetComponent<VRTK.VRTK_InteractableObject>().enabled = false;
            glass.transform.GetComponent<Rigidbody>().isKinematic = true;
            glass.GetComponent<GrabbaleObjects>().EnablingSync(false);
            glass.GetComponent<Glass>().WaitressCarry = true;
            glass.GetComponent<Glass>().RPC_SetUpGlassPoint(crashedGlasses[0].Key);
            glasses.Add(glass.GetComponent<Glass>());
            Debug.Log("glass created: " + glass.name + " " + glass.GetComponent<Glass>().glassType + " " + instPoint.name + " point: " + crashedGlasses[0].Key);
            crashedGlasses.Remove(crashedGlasses[0]);
        }

        public void GiveGlasses()
        {
            StartCoroutine(GiveGlassesNumerator());
        }

        private IEnumerator GiveGlassesNumerator()
        {
            for(int i = 0; i < glasses.Count; i++)
            {
                yield return new WaitForEndOfFrame();
                if (PhotonNetwork.IsMasterClient)
                {
                    Vector3 _pos = GameManager.Instance.playersPositions[glasses[i].GlassPointIndex].drinkSpawnPoint.position;
                    Quaternion _rot = GameManager.Instance.playersPositions[glasses[i].GlassPointIndex].drinkSpawnPoint.rotation;

                    switch (glasses[i].glassType)
                    {
                        case GlassType.WineGlass:
                            GameObject newWineGlass = PhotonNetwork.InstantiateSceneObject(wineGlassPrefab.name, _pos, _rot);
                            newWineGlass.GetComponent<PhotonView>().RPC("RPC_SetUpGlassPoint", RpcTarget.All, glasses[i].GlassPointIndex);
                            break;
                        case GlassType.Rox:
                            GameObject newRox = PhotonNetwork.InstantiateSceneObject(roxPrefab.name, _pos, _rot);
                            newRox.GetComponent<PhotonView>().RPC("RPC_SetUpGlassPoint", RpcTarget.All, glasses[i].GlassPointIndex);
                            break;
                        case GlassType.CupOfTea:
                            GameObject newCupOfTea = PhotonNetwork.InstantiateSceneObject(cupOfTeaPrefab.name, _pos, _rot);
                            newCupOfTea.GetComponent<PhotonView>().RPC("RPC_SetUpGlassPoint", RpcTarget.All, glasses[i].GlassPointIndex);
                            break;
                    }

                }
                Destroy(glasses[i].gameObject);
            }
            
            glasses.Clear();
            Debug.Log(glasses.Count + " сейчас на подносе");
            Debug.Log(crashedGlasses.Count + " надо принести");
        }

        public void ResetGlasses()
        {
            crashedGlasses.Clear();
            glasses.Clear();
        }

        public void ClearGlassesReferences()
        {
            Debug.Log("clear " + glasses.Count);
            glasses.Clear();
        }
    }
}