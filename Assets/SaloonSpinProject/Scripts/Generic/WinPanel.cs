﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin {
    public class WinPanel : MonoBehaviour
    {
        [SerializeField] private AudioSource audioSource;
        public GameObject winPanel;
        private void OnEnable()
        {
            if (Pianist.Instance != null)
                Pianist.Instance.PianistPause(audioSource.clip.length);

            NetworkManager.Instance.gameRuns = false;
            audioSource.Play();
        }

        private void OnDisable()
        {
            winPanel.gameObject.SetActive(false);
        }
    }
}