﻿using System;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public delegate void PlayerDrinked();
    public class Glass : MonoBehaviour
    {
        public int GlassID = 1;
        public static Glass Instance;
        public GlassType glassType;
        public bool WaitressCarry { get; set; }
        public int GlassPointIndex { get; private set; }
        [SerializeField] new private ParticleSystem particleSystem;
        [SerializeField] private GameObject fullnessIndicator;
        [SerializeField] private float angleToPour = 45f;//угол на который надо наклонить стакан чтобы жидскость начала литься
        [SerializeField] private float timeToDrinkFull = 3f; //время за которое жидкость выливается полностью
        private float pourTime = 0f; //время которое жидкость выливается
        private float playerDrinkedTime = 0f; //время которое жидкость выливается пока стакан находтся у рта игрока

        private PhotonView photonView;
        private bool isLiquidPours = false;
        private bool isGlassInPlayerMouth = false;
        private string UserID_ofDrinkingPlayer;
        public event PlayerDrinked OnPlayerDrinked;
        public event Action<float> onWaterInGlassChange;
        private float network_pourTime, network_playerDrinkedTime;
        private bool network_isLiquidPours, network_isGlassInPlayerMouth;

        private void Awake()
        {
            photonView = GetComponent<PhotonView>();
            Instance = this;
        }
        private void Start()
        {
            particleSystem.Stop();
            OnPlayerDrinked += PlayerDrinked;
            GlassID = photonView.ViewID;
        }
        private void Update()
        {
            CheckGlassRotation();
        }
        private void CheckGlassRotation()
        {
            if (pourTime >= timeToDrinkFull)
            {
                if (isLiquidPours)
                {
                    isLiquidPours = false;
                    particleSystem.Stop();
                }
                return;
            }

            float xRot = transform.eulerAngles.x;
            float zRot = Mathf.Abs(transform.eulerAngles.z);
            if ((angleToPour < xRot && xRot < 360 - angleToPour) || (angleToPour < zRot && zRot < 360 - angleToPour))
            {
                PourLiquid();
            }
            else if(isLiquidPours)
            {
                isLiquidPours = false;
                particleSystem.Stop();
            }
        }
        private void PourLiquid()
        {
            if (isGlassInPlayerMouth)
                playerDrinkedTime += Time.deltaTime;

            if(playerDrinkedTime >= 1f)
            {
                OnPlayerDrinked?.Invoke();
                playerDrinkedTime = 0f;
            }

            pourTime += Time.deltaTime;        
            var rate = pourTime / timeToDrinkFull;
            rate = Mathf.Clamp01(rate);
            onWaterInGlassChange?.Invoke(1f-rate);
            var locScale = fullnessIndicator.transform.localScale;
            var localPos = fullnessIndicator.transform.localPosition;
            locScale.y = (1 - rate);
            localPos.y = -(1 - locScale.y);
            fullnessIndicator.transform.localScale = locScale;
            fullnessIndicator.transform.localPosition = localPos;

            if (isLiquidPours)
                return;


            isLiquidPours = true;
            particleSystem.Play();
        }


        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Mouth"))
            {
                isGlassInPlayerMouth = true;
                //UserID_ofDrinkingPlayer = other.GetComponentInParent<AbstractPlayer>().UserID;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Mouth"))
            {
                isGlassInPlayerMouth = false;
            }
        }

        private void PlayerDrinked()
        {
            //Debug.Log(UserID_ofDrinkingPlayer + " is drinkin for 1 sec");
            photonView.RPC("RPC_PlayerDrinked", RpcTarget.All, UserID_ofDrinkingPlayer);
        }

        public void SetUser(Player player)
        {
            photonView.TransferOwnership(player);
        }

        [PunRPC]
        private void RPC_PlayerDrinked(string userID)
        {
            Debug.Log(userID + " is drinkin for 1 sec");
        }
        [PunRPC]
        public void RPC_SetUpGlassPoint(int index)
        {
            GlassPointIndex = index;
        }


        public void DisableGlass()
        {
            GetComponent<VRTK.VRTK_InteractableObject>().enabled = false;
            this.enabled = false;

            if(PhotonNetwork.IsMasterClient)
            {
                photonView.RPC("RPC_RequestToDestroyGlass", RpcTarget.All);
            }
        }

        private IEnumerator DelayedDestroyGlass()
        {
            yield return new WaitForSeconds(2);
            PhotonNetwork.Destroy(gameObject);
        }

        [PunRPC]
        private void RPC_RequestToDestroyGlass()
        {
            if (photonView.IsMine)
                StartCoroutine(DelayedDestroyGlass());
        }
    }
}
