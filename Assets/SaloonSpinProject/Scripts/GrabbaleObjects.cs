﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.GrabAttachMechanics;
namespace SaloonSpin
{
    public class GrabbaleObjects : VRTK_ChildOfControllerGrabAttach
    {
        [SerializeField]
        private PhotonView photonView;
        public GrabbObject objectType;
        private VRTK_InteractableObject interactableObject;
        public bool IsGrabbed { get; set; }
        protected override void Awake()
        {
            base.Awake();
            interactableObject = GetComponent<VRTK_InteractableObject>();
        }

        [PunRPC]
        private void RPC_AttachPistolNetwork(string userID, int hand)
        {
            List<PlayerController> players = new List<PlayerController>();
            players.AddRange(FindObjectsOfType<PlayerController>());
            var p = players.Find((_p) => _p.UserID == userID);

            Vector3 localPosition = Vector3.zero;
            Quaternion localRotation = Quaternion.identity;
            Hand _hand = (Hand)hand;

            switch (_hand)
            {
                case Hand.Left:
                    transform.SetParent(p.playerModel.LeftHand.transform);
                    localPosition = p.playerModel.LeftHand.GetComponent<PistolNetworkPositions>().GetPosition(p.typeOfPlayer);
                    localRotation = p.playerModel.LeftHand.GetComponent<PistolNetworkPositions>().GetRotation(p.typeOfPlayer);
                    break;
                case Hand.Right:
                    transform.SetParent(p.playerModel.RightHand.transform);
                    localPosition = p.playerModel.RightHand.GetComponent<PistolNetworkPositions>().GetPosition(p.typeOfPlayer);
                    localRotation = p.playerModel.RightHand.GetComponent<PistolNetworkPositions>().GetRotation(p.typeOfPlayer);
                    break;
            }

            transform.localPosition = localPosition;
            transform.localRotation = localRotation;
        }

        protected override void SnapObjectToGrabToController(GameObject obj)
        {
            PlayerController player = controllerAttachPoint.transform.GetComponentInParent<PlayerController>();
            PlayerHand playerHand = null;
            switch (objectType)
            {
                case GrabbObject.PlayingCard:
                    if (controllerAttachPoint.GetComponent<PlayerHand>() != null)
                    {
                        playerHand = controllerAttachPoint.GetComponent<PlayerHand>();
                    }
                    else
                    {
                        playerHand = controllerAttachPoint.GetComponentInParent<PlayerHand>();
                    }
                    player.GetPlayingCard(GetComponent<PlayingCard>().cardID, playerHand.Hand);
                    break;

                case GrabbObject.MainPistol:
                    GetComponent<MainPistolMoving>().CheckMoving();
                    if (controllerAttachPoint.GetComponent<PlayerHand>() != null)
                    {
                        playerHand = controllerAttachPoint.GetComponent<PlayerHand>();
                    }
                    else
                    {
                        playerHand = controllerAttachPoint.GetComponentInParent<PlayerHand>();
                    }
                    switch (playerHand.Hand)
                    {
                        case Hand.Left:
                            GetComponent<VRTK.VRTK_InteractableObject>().allowedTouchControllers = VRTK_InteractableObject.AllowedController.LeftOnly;
                            break;
                        case Hand.Right:
                            GetComponent<VRTK.VRTK_InteractableObject>().allowedTouchControllers = VRTK_InteractableObject.AllowedController.RightOnly;
                            break;
                    }
                    player.EquipMainPistol(GetComponent<VRTK_InteractableObject>(), playerHand.Hand);
                    photonView.RPC("EnablingSync", RpcTarget.All, false);
                    break;

                case GrabbObject.AFK_kill_Pistol:
                    if (controllerAttachPoint.GetComponent<PlayerHand>() != null)
                    {
                        playerHand = controllerAttachPoint.GetComponent<PlayerHand>();
                    }
                    else
                    {
                        playerHand = controllerAttachPoint.GetComponentInParent<PlayerHand>();
                    }
                    player.GetAKF_kill_Pistol(GetComponent<VRTK_InteractableObject>(), playerHand.Hand);
                    GetComponent<AFK_Pistol>().SetUser(PhotonNetwork.LocalPlayer);
                    break;
                case GrabbObject.Bullet:
                    if (player.UserID != NetworkManager.Instance.gameState.userTurn)//пулю может взять только игрок, чья очередь стрелять
                        return;
                    player.GetBullet();
                    break;
                case GrabbObject.Cigarette:
                    player.GetCigarette(GetComponent<Cigarette>().cigaretteID);
                    photonView.RPC("RPC_StopGrabbCigarette", RpcTarget.All);
                    break;
                case GrabbObject.Glass:
                    player.GetGlass(GetComponent<Glass>().GlassID);
                    break;
                case GrabbObject.ReloadCard: //карту может взять только ее владелец
                    if (player.UserID != GetComponentInChildren<ReloadCard>().OwnerUserID)
                        return;

                    if (GetComponentInChildren<ReloadCard>().moving)//надо остановить карту, движущуюся на свое место
                        GetComponentInChildren<ReloadCard>().StopMoving();
                    break;
                case GrabbObject.Pepelka:
                    player.GetPepelka(GetComponent<Pepelka>().pepelnitsaID);
                    break;
            }

            if (!precisionGrab)
            {
                SetSnappedObjectPosition(obj);
            }


            (this.gameObject).transform.SetParent(controllerAttachPoint.transform);
            if(!PhotonNetwork.IsConnected)
                return;
            photonView.RPC("SetParentToObject", RpcTarget.All, null);
            photonView.RPC("RigidbodyKinimatic", RpcTarget.All, true);

            if (objectType == GrabbObject.Pistol)
            {
                photonView.RPC("RPC_AttachPistolNetwork", RpcTarget.Others, player.UserID, (int)playerHand.Hand);
                photonView.RPC("NetworkTransformSync", RpcTarget.All, false);
            }

            if(objectType == GrabbObject.MainPistol)
            {
                photonView.RPC("RPC_AttachPistolNetwork", RpcTarget.Others, player.UserID, (int)playerHand.Hand);
            }
        }

        public override void StopGrab(bool applyGrabbingObjectVelocity)
        {
            if (controllerAttachPoint == null)
                return;

            PlayerController player = controllerAttachPoint.transform.GetComponentInParent<PlayerController>();
            PlayerHand playerHand = null;
            switch (objectType)
            {
                case GrabbObject.MainPistol:
                    GetComponent<VRTK.VRTK_InteractableObject>().allowedTouchControllers = VRTK_InteractableObject.AllowedController.Both;
                    if (controllerAttachPoint.GetComponent<PlayerHand>() != null)
                    {
                        playerHand = controllerAttachPoint.GetComponent<PlayerHand>();
                    } else {
                        playerHand = controllerAttachPoint.GetComponentInParent<PlayerHand>();
                    }
                    player.UnEquipMainPistol(GetComponent<VRTK_InteractableObject>(), playerHand.Hand);
                    photonView.RPC("ResetObjectParent", RpcTarget.All, null);
                    GetComponent<VRTK_InteractableObject>().GetPreviousState(out var prev_transform, out bool kinimatic, out bool grabbable);
                    GetComponent<VRTK_InteractableObject>().OverridePreviousState(null, false, grabbable);
                    //base.ReleaseObject(applyGrabbingObjectVelocity);                 
                    base.StopGrab(applyGrabbingObjectVelocity);
                    photonView.RPC("EnablingSync", RpcTarget.All, true);
                    break;
                case GrabbObject.AFK_kill_Pistol:
                    controllerAttachPoint.transform.GetComponentInParent<PlayerController>().DropAFK_Pistol();
                    photonView.RPC("ResetObjectParent", RpcTarget.All, null);
                    GetComponent<VRTK_InteractableObject>().GetPreviousState(out var t, out var b, out var previousGrabbable);
                    GetComponent<VRTK_InteractableObject>().OverridePreviousState(t, false, previousGrabbable);
                    base.ReleaseObject(applyGrabbingObjectVelocity);
                    base.StopGrab(applyGrabbingObjectVelocity);
                    photonView.RPC("RPC_DisableAFK_pistolGrabbale", RpcTarget.Others);
                    photonView.RPC("RigidbodyKinimatic", RpcTarget.All, false);
                    break;
                case GrabbObject.ReloadCard:
                    photonView.RPC("ResetObjectParent", RpcTarget.All, null);
                    base.ReleaseObject(applyGrabbingObjectVelocity);
                    base.StopGrab(applyGrabbingObjectVelocity);
                    photonView.RPC("RigidbodyKinimatic", RpcTarget.All, false);
                    break;

                case GrabbObject.Bullet:
                    bool dropByRigigdBody = GetComponent<Bullet>().IsAttachToHole();
                    photonView.RPC("ResetObjectParent", RpcTarget.All, null);
                    GetComponent<VRTK_InteractableObject>().OverridePreviousState(null, false, true);
                    base.ReleaseObject(applyGrabbingObjectVelocity);
                    base.StopGrab(applyGrabbingObjectVelocity);
                    photonView.RPC("RigidbodyKinimatic", RpcTarget.All, false);
                    break;

                case GrabbObject.Cigarette:
                    photonView.RPC("ResetObjectParent", RpcTarget.All, null);
                    GetComponent<VRTK_InteractableObject>().OverridePreviousState(null, false, true);
                    base.ReleaseObject(applyGrabbingObjectVelocity);
                    base.StopGrab(applyGrabbingObjectVelocity);
                    photonView.RPC("RigidbodyKinimatic", RpcTarget.All, false);
                    GetComponent<Cigarette>().AttachCigaretteToMouth();
                    break;
                case GrabbObject.Glass:
                    photonView.RPC("ResetObjectParent", RpcTarget.All, null);
                    GetComponent<VRTK_InteractableObject>().OverridePreviousState(null, false, true);
                    base.ReleaseObject(applyGrabbingObjectVelocity);
                    base.StopGrab(applyGrabbingObjectVelocity);
                    photonView.RPC("RigidbodyKinimatic", RpcTarget.All, false);
                    break;
                case GrabbObject.Pepelka:
                    photonView.RPC("ResetObjectParent", RpcTarget.All, null);
                    GetComponent<VRTK_InteractableObject>().OverridePreviousState(null, false, true);
                    base.ReleaseObject(applyGrabbingObjectVelocity);
                    base.StopGrab(applyGrabbingObjectVelocity);
                    photonView.RPC("RigidbodyKinimatic", RpcTarget.All, false);
                    break;
                case GrabbObject.PlayingCard:
                    GetComponent<VRTK_InteractableObject>().OverridePreviousState(null, false, true);
                    base.ReleaseObject(applyGrabbingObjectVelocity);
                    base.StopGrab(applyGrabbingObjectVelocity);
                    break;
            }

        }

        [PunRPC]
        private void RigidbodyKinimatic(bool value)
        {
            if (GetComponent<Rigidbody>() != null)
            {
                GetComponent<Rigidbody>().isKinematic = value;
                GetComponent<PhotonRigidbodyView>().enabled = !value;
            }
        }
        [PunRPC]
        private void NetworkTransformSync(bool value)
        {
            GetComponent<PhotonTransformView>().enabled = value;
        }
        [PunRPC]
        private void RPC_DisableAFK_pistolGrabbale()
        {
            GetComponent<VRTK.VRTK_InteractableObject>().isGrabbable = false;
        }
        [PunRPC]
        private void SetParentToObject()
        {
            var rb = gameObject.GetComponent<Rigidbody>();
            if (rb != null)
                rb.isKinematic = true;
            IsGrabbed = true;
            //(this.gameObject).transform.SetParent(controllerAttachPoint.transform);
        }
        [PunRPC]
        private void ResetObjectParent()
        {
            (this.gameObject).transform.SetParent(null);
            IsGrabbed = false;
            
            if (GetComponent<Rigidbody>() != null)
            {
                GetComponent<Rigidbody>().isKinematic = false;
            }
        }
        [PunRPC]
        public void EnablingSync(bool enable)
        {
            GetComponent<PhotonTransformView>().enabled = enable;
            GetComponent<PhotonRigidbodyView>().enabled = enable;
        }
        public bool IsOwnerTransfered()
        {
            if (photonView.OwnershipWasTransfered)
            {
                GetComponent<VRTK_InteractableObject>().OverridePreviousState(null, false, true);
                base.ReleaseObject(false);
                base.StopGrab(false);
                photonView.RPC("ResetObjectParent", RpcTarget.All, null);
                photonView.OwnershipWasTransfered = false;
                return true;
            }
            return false;
        }

    }
}
