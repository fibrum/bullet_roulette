﻿using System;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Random = UnityEngine.Random;

namespace SaloonSpin
{
    public class Lobby : MonoBehaviourPunCallbacks
    {
        public static Lobby Instance;
        [SerializeField] private GameObject localPianist;
        [SerializeField] private GameObject pianist;
        [SerializeField] private GameObject localWaitress;
        [SerializeField] private GameObject waitress;
        [SerializeField] private GameObject localBarmen;
        [SerializeField] private GameObject networkBarmen;
        [SerializeField] private GameObject lobbyPlayerPrefab;
        [SerializeField] private Transform LobbyPlayerPostion;
        [SerializeField] private GameObject lobbyCanvas;
        [SerializeField] private string gameVersion;
        [SerializeField] private LobbyUI lobbyUI;
        [SerializeField] private LobbyConsole lobbyConsole;
        [SerializeField] private GameObject loadingCircle;
        [SerializeField] private string gameSceneName;
        [SerializeField] UnityEvent OnPlayerLeftRoomEvent;

        private bool isConnected = false;
        private string nickname;
        private Vector3 defaultPosition = new Vector3(0.485f, 5.22f, 6.72f);
        private void Start()
        {
            nickname = "Player#" + Random.Range(1000, 10000);
            transform.position = defaultPosition;
            Connect();
        }
        private void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        #region PUN callbacks

        public override void OnConnectedToMaster()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
            PhotonNetwork.NickName = SteamManager.Initialized?SteamManager.GetCurrentUserName():nickname;
            Debug.Log("connected to: " + PhotonNetwork.CloudRegion);
            lobbyConsole.InitConsole("Connected to Master Server.", this);
            if(!PhotonNetwork.OfflineMode)
                PhotonNetwork.JoinLobby();
        }
        public override void OnJoinedLobby()
        {
            lobbyConsole.msg.text = "joined lobby." + "Room count = "+PhotonNetwork.CountOfRooms + " ; PhotonNetwork.CountOfPlayers = "+ PhotonNetwork.CountOfPlayers;
            lobbyUI.ShowScreenPanel(1);
            //lobbyUI.screen1.GetComponentInChildren<Text>().text = PhotonNetwork.LocalPlayer.NickName;
        }
        public override void OnLeftLobby()
        {
            lobbyConsole.msg.text = "<color=red>Left lobby.</color>";
            if (waitingLeftLobby) {
                waitingLeftLobby = false;
                PhotonNetwork.JoinRoom(roomNameForJoin);
                roomNameForJoin = "";
            }
        }
        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            lobbyConsole.msg.text = "<color=red>Join Random Failed.</color> " + message;
            lobbyUI.ShowScreenPanel(3);
        }
        public virtual void OnJoinRoomFailed(short returnCode, string message)
        {
            Debug.Log(returnCode+ message);
            if (waitingLeftLobby) {
                waitingLeftLobby = false;
                PhotonNetwork.JoinRoom(roomNameForJoin);
            }
        }

        private bool waitingLeftLobby;
        private string roomNameForJoin;
        public void LeftLobbyAndJoinRoom(string roomName) {
           // waitingLeftLobby = true;
            roomNameForJoin = roomName;
            // PhotonNetwork.LeaveLobby();


            PhotonNetwork.JoinRoom(roomNameForJoin);
        }
       
        public override void OnDisconnected(DisconnectCause cause)
        {
            Debug.Log("conncetin failed");
            lobbyConsole.msg.text = string.Format("<color=red>Disconnected: {0}.</color>", cause.ToString());
            isConnected = false;
            lobbyUI.ShowScreenPanel(6);
            if (PhotonNetwork.LocalPlayer.UserId != "offlineid")
                Connect("offlineid");
            SteamManager.LeaveLobby();
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            lobbyConsole.msg.text = string.Format("<color=red>{0}.</color>", message.ToString());
            base.OnCreateRoomFailed(returnCode, message);
        }
        public override void OnMasterClientSwitched(Player newMasterClient) {
            
            if (PhotonNetwork.IsMasterClient)
            {
                int ind = NetworkManager.Instance.gameState.GetMyPos();
                SetStartButtonNewPosition(ind);
                MasterClientLeave = true;
            }
        }

        
        private void SetStartButtonNewPosition(int ind) {
            var rot = Quaternion.Euler(0f,-90f*ind,0f);
            GameManager.Instance.UpdateStartButtonPosition(Vector3.zero, rot);
        }



        private bool MasterClientLeave = false;
        public override void OnJoinedRoom()
        {
            localWaitress.SetActive(false);
            localPianist.SetActive(false);
            localBarmen.SetActive(false);
            Hashtable playerProperties = new Hashtable();
            playerProperties.Add(RoomPropertiesKey.PlayerType, PlayerType.Gentlman);
            PhotonNetwork.LocalPlayer.SetCustomProperties(playerProperties);

            lobbyUI.ShowScreenPanel(5);
            lobbyUI.screen5.GetComponentInChildren<Text>().text = PhotonNetwork.CurrentRoom.Name + " - " + (PhotonNetwork.CurrentRoom.IsVisible == true ? "public" : "private") + " |" + gameVersion;
            lobbyUI.UpdateRoomPlayersList(PhotonNetwork.PlayerList);
            if (PhotonNetwork.LocalPlayer.IsMasterClient)
            {
                Hashtable roomProperties = new Hashtable();
                roomProperties[RoomPropertiesKey.FirstGamePlayed] = false;

                PhotonNetwork.CurrentRoom.SetCustomProperties(roomProperties);

                lobbyUI.roomUI.startGameButton.SetActive(true);
                NetworkManager.Instance.ShakePlayerTypesList();
                this.CreateBarmen();
                this.CreateWaitress();
                this.CreatePianist();
            }
            else
            {
                lobbyUI.roomUI.startGameButton.SetActive(false);
                StartCoroutine(WaitForWaitressSetUp());
            }
        }
        public override void OnPlayerLeftRoom(Player otherPlayer)
        {

            if (PhotonNetwork.LocalPlayer.IsMasterClient)
            {
                if(NetworkManager.Instance.gameState.userTurn == otherPlayer.UserId)
                {
                    NetworkManager.Instance.gameState.SwitchTurnToNextUser();
                }

                lobbyUI.roomUI.startGameButton.SetActive(true);
                if (!lobbyCanvas.gameObject.activeSelf)//это значит что игроки не находятся в лобби, а в игре
                {
                    if (!MasterClientLeave)
                        NetworkManager.Instance.PlayerLeftRoom(otherPlayer, (PlayerType)otherPlayer.CustomProperties[RoomPropertiesKey.PlayerType]);
                    else //если уже была нажата кнопка старт, ливнувший мастер перезайти не сможет, пересоздавать ботов не надо, иначе надо пересоздать новым мастером
                    {
                        var props = PhotonNetwork.CurrentRoom.CustomProperties;
                        bool gameStarted = (bool)props[RoomPropertiesKey.FirstGamePlayed];
                        if (gameStarted)
                        {
                            NetworkManager.Instance.PlayerLeftRoom(otherPlayer, (PlayerType)otherPlayer.CustomProperties[RoomPropertiesKey.PlayerType]);
                            MasterClientLeave = false;
                        }
                        else
                        {
                            NetworkManager.Instance.MasterPlayerLeftRoom(otherPlayer, (PlayerType)otherPlayer.CustomProperties[RoomPropertiesKey.PlayerType]);
                        }
                    }

                    foreach (var c in FindObjectsOfType<Cigarette>())
                        c.GetComponent<GrabbaleObjects>().IsOwnerTransfered();

                    foreach (var g in FindObjectsOfType<Glass>())
                        g.GetComponent<GrabbaleObjects>().IsOwnerTransfered();

                    foreach (var p in FindObjectsOfType<Pepelka>())
                        p.GetComponent<GrabbaleObjects>().IsOwnerTransfered();


                    if(MainPistol.Instance.transform.GetComponent<GrabbaleObjects>().IsOwnerTransfered())
                    {
                        MainPistol.Instance.PistolMoving.PutPistolOnTableCenter();

                        if(PhotonNetwork.IsMasterClient)
                        {
                            NetworkManager.Instance.SyncPistolState();
                        }
                    }

                    Bullet.Instance.transform.GetComponent<GrabbaleObjects>().IsOwnerTransfered();

                }
            }
            else lobbyUI.roomUI.startGameButton.SetActive(false);

            if (lobbyCanvas.activeSelf)//значит что игроки находятся в лобби
            {
                lobbyUI.UpdateRoomPlayersList(PhotonNetwork.PlayerList);
            }
            
            //Debug.Log("Player left room");
        }
        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            lobbyUI.UpdateRoomPlayersList(PhotonNetwork.PlayerList);
            if (PhotonNetwork.LocalPlayer.IsMasterClient)
            {
                lobbyUI.roomUI.startGameButton.SetActive(true);
                if (!lobbyCanvas.gameObject.activeSelf) //если мастер-клиент уже за столом, садим всех кто заходит в комнату
                {
                    photonView.RPC("RPC_StartGame", newPlayer);
                    NetworkManager.Instance.transform.GetComponent<PhotonView>().RPC("FirstLoadSync", newPlayer);
                }
            }
            else lobbyUI.roomUI.startGameButton.SetActive(false);


        }
        public override void OnLeftRoom()
        { 
            localWaitress.SetActive(true);
            localPianist.SetActive(true);
            localBarmen.SetActive(true);
            GameManager.Instance.startButton.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
            PlayingCardsManager.Instance.ResetData();
        }
        
        #endregion
        public void EnableOfflineMode()
        {
            PhotonNetwork.AuthValues = new AuthenticationValues("offlineid");
            PhotonNetwork.OfflineMode = true;
            lobbyUI.ShowScreenPanel(1);
            PhotonNetwork.LocalPlayer.SetOfflineUserId();
            Debug.Log("id: " + PhotonNetwork.LocalPlayer.UserId);
        }
        public void TryReconnect()
        {
            StartCoroutine(LoadingCircleEnable());
            Invoke("Connect", 2f);
        }
        private IEnumerator LoadingCircleEnable()
        {
            loadingCircle.gameObject.SetActive(true);
            yield return new WaitForSeconds(2f);
            loadingCircle.gameObject.SetActive(false);
        }

        public void CreatePianist()
        {
            PhotonNetwork.InstantiateSceneObject(this.pianist.name, localPianist.transform.position, localPianist.transform.rotation);
        }
        public void CreateBarmen()
        {
            PhotonNetwork.InstantiateSceneObject(this.networkBarmen.name, localBarmen.transform.position, localBarmen.transform.rotation);
        }
        public void CreateWaitress()
        {
            Debug.Log("<color=red>[WAITRESS create]</color>");
            PhotonNetwork.InstantiateSceneObject(this.waitress.name, Vector3.zero, Quaternion.identity);
        }

        private IEnumerator WaitForWaitressSetUp()
        {
            while (Waitress.Instance == null)
                yield return new WaitForEndOfFrame();

            Waitress.Instance.transform.GetComponent<PhotonView>().RPC("InitialTimeRequest", RpcTarget.MasterClient);
        }
        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
            lobbyUI.ShowScreenPanel(1);

            SteamManager.LeaveLobby();
        }
        [PunRPC]
        public void OnClick_LeaveRoomFromGame()
        {
            GameManager.Instance.StartButtonToDefaultPosition();
            StartCoroutine(LeaveRoomNumerator());
        }
        private IEnumerator SwitchMaster()
        {
            var players = PhotonNetwork.PlayerList;
            Player newMaster = null;
            for(int i = 0; i < players.Length; i++)
            {
                if (!players[i].IsLocal)
                {
                    newMaster = players[i];
                    break;
                }
            }
            yield return new WaitForEndOfFrame();
            PhotonNetwork.SetMasterClient(newMaster);
        }

        public void JoinToRoom() {
        }


        private IEnumerator LeaveRoomNumerator()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                if (PhotonNetwork.CurrentRoom.PlayerCount > 1)
                {
                    yield return StartCoroutine(SwitchMaster());
                }
            }

            if (PhotonNetwork.InRoom)
            {
                NetworkManager.Instance.myPlayer.RemoveCoin(-1);

                foreach (var obj in NetworkManager.Instance.myPlayer.GetComponentsInChildren<GrabbaleObjects>())
                {
                    obj.StopGrab(true);
                    if(obj.objectType == GrabbObject.Pistol)
                    {
                        MainPistol.Instance.PistolMoving.PutPistolOnTableCenter();
                    }
                }

                PhotonNetwork.Destroy(NetworkManager.Instance.myPlayer.gameObject);
                PhotonNetwork.LeaveRoom();
                StartCoroutine(InstantiateLobbyPlayer());
            }

            OnPlayerLeftRoomEvent?.Invoke();
            if (PhotonNetwork.OfflineMode) {
                NetworkManager.Instance.TryDestroyNpc();
            }
            
        }
        private IEnumerator InstantiateLobbyPlayer()
        {
            yield return new WaitForEndOfFrame();
            NetworkManager.Instance.lobbyPlayer = Instantiate(lobbyPlayerPrefab, LobbyPlayerPostion.position, LobbyPlayerPostion.rotation);
            yield return new WaitForEndOfFrame();
            if(PhotonNetwork.InLobby)
            {
                lobbyConsole.msg.text = "joined lobby.";
                lobbyUI.ShowScreenPanel(1);
                lobbyUI.screen1.GetComponentInChildren<Text>().text = PhotonNetwork.LocalPlayer.NickName;
            }
            else
                Connect();
        }
        public void CreateRoom()
        {
            lobbyUI.ShowScreenPanel(4);
        }
        public void OpenSettings()
        {
            lobbyUI.ShowScreenPanel(7);
            throw new Exception("<color=red>[Lobby]OpenSettings</color>");
        }

        private int roomId;
        public int RoomId {
            get { return roomId; }
        }
        public void CreateRoom(bool _isOpen) {
            roomId = Random.Range(0, 10000);
            PhotonNetwork.CreateRoom("Room" +roomId , new RoomOptions { MaxPlayers = 4, PublishUserId = true, IsVisible = _isOpen });
            SteamManager.CreateLobby();

        }

        private void InvitePlayer() {
            //PhotonNetwork.FindFriends()
        }

        private Coroutine searchRoom;
        public void OnClick_FindGame()
        {
            lobbyUI.ShowScreenPanel(2);
            searchRoom = StartCoroutine(SearchGame(Random.Range(4.1f, 6.2f)));
        }

        public void OnClick_StopSearchingRoom()
        {
            if (searchRoom != null)
                StopCoroutine(searchRoom);
        }
        public void JoinRandomRoom()
        {
            if (isConnected)
                PhotonNetwork.JoinRandomRoom();           
        }
        public void Connect(string customUserID)
        {
            //PhotonNetwork.Disconnect();
            //PhotonNetwork.AuthValues = new AuthenticationValues("offline id");
            //PhotonNetwork.OfflineMode = true;
            //Connect();
        }
        public void Connect()
        {
            isConnected = true;
            //var ir = Application.internetReachability;
            //Debug.Log("connect 1");
            //switch (ir)
            //{
            //    case NetworkReachability.NotReachable:
            //        if (!PhotonNetwork.OfflineMode)
            //        {
            //            PhotonNetwork.AuthValues = new AuthenticationValues("offline id");
            //           // PhotonNetwork.AuthValues.UserId = ;
            //            Debug.Log("try connect");
            //            //PhotonNetwork.ConnectUsingSettings();
            //            //return;
            //        }
            //        break;
            //    case NetworkReachability.ReachableViaCarrierDataNetwork:
            //        break;
            //    case NetworkReachability.ReachableViaLocalAreaNetwork:
            //        break;
            //}
            
            if (PhotonNetwork.IsConnected) {
                

                

                if (!PhotonNetwork.OfflineMode)
                    PhotonNetwork.JoinLobby();
                else
                {
                    lobbyUI.ShowScreenPanel(1);
                    //lobbyUI.screen1.GetComponentInChildren<Text>().text = "OFFLINE MODE";
                    Debug.Log("id: " + PhotonNetwork.LocalPlayer.UserId);
                }
            }
            else
            {
                PhotonNetwork.GameVersion = gameVersion;
                PhotonNetwork.ConnectUsingSettings();
                var args = System.Environment.GetCommandLineArgs();
                if(Application.platform != RuntimePlatform.WindowsEditor)
                {
                Debug.Log("[Lobby]Connect: commandline="+System.Environment.CommandLine);
                for (int i = 0; i < args.Length; i++)
                {
                    Debug.Log("[Lobby]Connect: arg[" + i + "]=" + args[i]);
                    if (args[i] == "-invite")
                    {
                        if (args.Length >= i + 1)
                        {
                            Debug.Log("[Lobby]Connect:Room=" + args[i + 1]);
                            StartCoroutine(DelayJoinRoom(args[i + 1]));
                            return;
                        }
                    }
                }
                }
            }
            

            //if (PhotonNetwork.OfflineMode)
            //{
            //    Debug.Log("id: " + PhotonNetwork.LocalPlayer.UserId);
            //    lobbyUI.ShowScreenPanel(1);
            //    lobbyUI.screen1.GetComponentInChildren<Text>().text = "OFFLINE MODE";
            //}
        }

        private IEnumerator DelayJoinRoom(string roomName) {
            yield return new WaitForSeconds(0.1f);
            while (!PhotonNetwork.IsConnected) {
                yield return new WaitForSeconds(0.1f);
            }
            yield return new WaitForSeconds(1f);
            Debug.Log("[Lobby]DelayJoinRoom call joinroom="+roomName);
            PhotonNetwork.JoinRoom(roomName);
            roomNameForJoin = roomName;
            waitingLeftLobby = true;
        }


        private IEnumerator DelayConnect()
        {
            yield return new WaitForSeconds(.1f);
            if (PhotonNetwork.OfflineMode)
            {
                Debug.Log("id: " + PhotonNetwork.LocalPlayer.UserId);
                lobbyUI.ShowScreenPanel(1);
                //lobbyUI.screen1.GetComponentInChildren<Text>().text = "OFFLINE MODE";
            }
        }
        public void StartGame()
        {
            photonView.RPC("RPC_StartGame", RpcTarget.All);
        }
        private IEnumerator SearchGame(float time)
        {
            yield return new WaitForSeconds(time);
            PhotonNetwork.JoinRandomRoom();
        }

        [PunRPC]
        private void RPC_StartGame()
        {
            Debug.Log("RPC Start Game");
            NetworkManager.Instance.LaunchGame();
            lobbyCanvas.SetActive(false);
        }

        public void ShowInviteDialog(string userName, ulong lobbyId)
        {
            lobbyUI.inviteScreen.GetComponent<InviteFromFriendMenu>().Init(userName, lobbyId);
        }



        [System.Serializable]
    public class LobbyUI
    {
        public GameObject screen1;
        public GameObject screen2;
        public GameObject screen3;
        public GameObject screen4;
        public GameObject screen5;
        public GameObject console;
        public GameObject inviteScreen;
        public GameObject settingsScreen;
        public RoomUI roomUI;
        public List<Sprite> elementBG;
        public void ShowScreenPanel(int screenIndex)
        {

            if(screen1.activeSelf&& screenIndex!=1)
                screen1.GetComponent<TweenScale>().PlayReverse(true);
            if (screen2.activeSelf && screenIndex != 2)
                screen2.GetComponent<TweenScale>().PlayReverse(true);
            if (screen3.activeSelf && screenIndex != 3)
                screen3.GetComponent<TweenScale>().PlayReverse(true);
            if (screen4.activeSelf && screenIndex != 4)
                screen4.GetComponent<TweenScale>().PlayReverse(true);
            if (screen5.activeSelf && screenIndex != 5)
                screen5.GetComponent<TweenScale>().PlayReverse(true);
            if (console.activeSelf && screenIndex != 6)
                console.GetComponent<TweenScale>().PlayReverse(true); 
            if (settingsScreen.activeSelf && screenIndex != 7)
                settingsScreen.GetComponent<TweenScale>().PlayReverse(true);
                switch (screenIndex)
            {
                case 1:
                    screen1.SetActive(true);
                    //screen4.SetActive(false);
                    //screen5.SetActive(false);
                    //screen2.SetActive(false);
                    //screen3.SetActive(false);
                    //console.SetActive(false);
                    break;
                case 2:
                    //screen1.SetActive(false);
                    //screen4.SetActive(false);
                    //screen5.SetActive(false);
                    screen2.SetActive(true);
                    //screen3.SetActive(false);
                    //console.SetActive(false);
                    break;
                case 3:
                    //screen1.SetActive(false);
                    //screen2.SetActive(false);
                    //screen4.SetActive(false);
                    //screen5.SetActive(false);
                    screen3.SetActive(true);
                    //console.SetActive(false);
                    break;
                case 4:
                    //screen1.SetActive(false);
                    //screen2.SetActive(false);
                    screen4.SetActive(true);
                    //screen5.SetActive(false);
                    //screen3.SetActive(false);
                    //console.SetActive(false);
                    break;
                case 5:
                    //screen1.SetActive(false);
                    //screen2.SetActive(false);
                    //screen4.SetActive(false);
                    screen5.SetActive(true);
                    //screen3.SetActive(false);
                    //console.SetActive(false);
                    break;
                case 6:
                    //screen1.SetActive(false);
                    //screen2.SetActive(false);
                    //screen4.SetActive(false);
                    //screen5.SetActive(false);
                    //screen3.SetActive(false);
                    console.SetActive(true);
                    break;
                case 7:
                    //screen1.SetActive(false);
                    //screen2.SetActive(false);
                    //screen4.SetActive(false);
                    //screen5.SetActive(false);
                    //screen3.SetActive(false);
                    settingsScreen.SetActive(true);
                    break;
                }

            
        }


        public void ShowErrorPanel()
        {

        }

        public void UpdateRoomPlayersList(Player[] playerArray)
        {
            foreach (Transform child in roomUI.roomListPanel)
                Lobby.Destroy(child.gameObject);

            if (playerArray == null)
                return;

            bool MasterClient = PhotonNetwork.IsMasterClient;
            
            for(int i = 0; i < playerArray.Length; i++)
            {
                var element = Lobby.Instantiate(roomUI.listERlementPrefab);
                element.transform.SetParent(roomUI.roomListPanel, false);
                //element.GetComponent<RectTransform>().localPosition = Vector3.zero;
                //var localPos = element.GetComponent<RectTransform>().localPosition;
                //localPos.y = -50f * i + 100f;
                //element.GetComponent<RectTransform>().localPosition = localPos;
                element.GetComponentInChildren<Text>().text = playerArray[i].NickName;
                element.GetComponent<Image>().sprite = elementBG[i];
                var p = playerArray[i];
                if (playerArray[i].IsMasterClient)
                {
                    element.GetComponent<Image>().color = Color.yellow;
                }
                if(MasterClient)
                {
                    if (p.IsMasterClient)
                    {
                        element.GetComponentInChildren<Button>().gameObject.SetActive(false);
                    }
                    else
                    {
                        element.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
                        element.GetComponentInChildren<Button>().onClick.AddListener(() =>
                        {
                            PhotonNetwork.CloseConnection(p);
                        });
                    }
                }
                else
                {
                    element.GetComponentInChildren<Button>().gameObject.SetActive(false);
                }
                
            }
        }

        public void HideAllScreens()
        {
            screen1.SetActive(false);
            screen2.SetActive(false);
            screen3.SetActive(false);
            screen4.SetActive(false);
            screen5.SetActive(false);
            console.SetActive(false);
            settingsScreen.SetActive(false);
        }
    }


    [System.Serializable]
    public class RoomUI
    {
        public Transform roomListPanel;
        public GameObject listERlementPrefab;
        public GameObject startGameButton;
    }

    [System.Serializable]
    public class LobbyConsole
    {
        public Text ping;
        public Text region;
        public Text msg;

        public void InitConsole(string msg, MonoBehaviour behaviour)
        {
            region.text = "region: " + PhotonNetwork.CloudRegion;
            ping.text = "ping: " + PhotonNetwork.GetPing();
            this.msg.text = msg;
            behaviour.StartCoroutine(PingNumerator());
        }
        private IEnumerator PingNumerator()
        {
            while(true)
            {
                yield return new WaitForSeconds(2f);
                ping.text = "ping: " + PhotonNetwork.GetPing();
            }
        }
    }

    }
}