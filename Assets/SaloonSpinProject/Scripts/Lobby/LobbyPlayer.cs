﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Valve.VR;


namespace SaloonSpin
{
    public class LobbyPlayer : MonoBehaviour
    {
        [Header("Oculus")]
        [SerializeField] private Transform cameraRigOculus;
        [SerializeField] private Transform centerEyeAnchorsOculus;
        [Header("SteamVR")]
        [SerializeField] private Transform cameraRigSteamVR;
        [SerializeField] private Transform centerEyeAnchorsSteamVR;
        [SerializeField] private SteamVR_TrackedObject steamTrackerL, steamTrackerR;
        private bool oculus;
        private Transform target;
        private Action resetPosition;
        private void Start()
        {
            target = GameManager.Instance.playersPositions[0].place;
            oculus = XRSettings.loadedDeviceName == "Oculus";
            if (oculus) Invoke("ResetPositionOculus",1.5f);
            else Invoke("ResetPositionSteamVR", 1.5f);
            if (oculus) Invoke("ResetPositionOculus",0.15f);
            else Invoke("ResetPositionSteamVR", 0.15f);
        }
        private void ResetPositionOculus()
        {
            //Vector3 lookPos = GameManager.Instance.tableCenter.position - centerEyeAnchorsOculus.position;
            //cameraRigOculus.rotation = Quaternion.LookRotation(lookPos);

            cameraRigOculus.localEulerAngles = new Vector3(0f, -centerEyeAnchorsOculus.localEulerAngles.y, 0f);
            var difference = GameManager.Instance.playersPositions[0].playerPlace.position - centerEyeAnchorsOculus.position;
            difference.y += 0.13f;
            cameraRigOculus.position += difference;
            
            //lookPos.y = 0;
            
        }
        private void ResetPositionSteamVR()
        {
            cameraRigSteamVR.localEulerAngles=new Vector3(0f, -centerEyeAnchorsSteamVR.localEulerAngles.y, 0f);


            var difference = GameManager.Instance.playersPositions[0].playerPlace.position - centerEyeAnchorsSteamVR.position;
            difference.y += 0.13f;
            cameraRigSteamVR.position += difference;
            //Vector3 lookPos = GameManager.Instance.tableCenter.position - centerEyeAnchorsSteamVR.position;
            //lookPos.y = 0;
            //cameraRigSteamVR.rotation = Quaternion.LookRotation(lookPos);
            
            

        }
        private void Update()
        {
            if (oculus)
            {
                if (OVRInput.GetDown(OVRInput.Button.Two))
                {
                    ResetPositionOculus();
                }
            }
            else
            {
                if (steamTrackerL)
                {
                    SteamVR_Controller.Device dev = SteamVR_Controller.Input((int)steamTrackerL.index);
                    if (dev.GetPressDown(EVRButtonId.k_EButton_ApplicationMenu))
                    {
                        ResetPositionSteamVR();
                    }
                }
                if (steamTrackerR)
                {
                    SteamVR_Controller.Device dev = SteamVR_Controller.Input((int)steamTrackerR.index);
                    if (dev.GetPressDown(EVRButtonId.k_EButton_ApplicationMenu))
                    {
                        ResetPositionSteamVR();
                    }
                }
            }
        }

    }
}
