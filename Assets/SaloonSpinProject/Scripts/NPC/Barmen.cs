﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class Barmen : MonoBehaviour
    {
        public static Barmen Instance;
        public GameObject ragdoll;

        private void Awake()
        {
            Instance = this;
        }

        [PunRPC]
        private void Death()
        {
            //GameManager.Instance.startButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener(() => Lobby.Instance.CreateBarmen());
            PhotonNetwork.InstantiateSceneObject("NPC_RagDolls/"+ragdoll.name, transform.position, transform.rotation);
            PhotonNetwork.Destroy(gameObject);
        }

        private void OnDestroy() {
            Instance = null;
        }
    }
}
