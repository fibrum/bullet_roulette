﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SaloonSpin
{
    public class NPC : AbstractPlayer
    {
        [SerializeField]
        private List<GameObject> NPC_List = new List<GameObject>();
        [SerializeField]
        private Shader hologramShader;
        [SerializeField]
        private Transform rHandTarget;
        [SerializeField]
        private RagdollsManager ragdollsManager;
        private NPC_Death npcDeath;
        private RootMotion.FinalIK.LookAtIK lookAtIK;
        private RootMotion.FinalIK.LimbIK rightHandIK;
        private Animator npcAnimator;
        private bool cardUsed = false;
        public bool npcFired { get; private set; }
        public NPC_State npcState { get; private set; }
        private Transform npcTarget;
        public Transform TARGET_TO_SHOT {
            get
            {
                return npcTarget;
            }
            set
            {
                npcTarget = value;
                if(value == null)
                {
                    Debug.Log(string.Format("{0}: <color=green>NPC target set to null</color>", gameObject.name));
                }
                else
                {
                    Debug.Log(string.Format("{0}: <color=green>NPC target set to {0}</color>", gameObject.name, value.root.name));
                }
            }
        }
        private bool cardAlreadyPlayed = false;
        private PlayingCardType cardToPlay = PlayingCardType.SpinPistol;
        private Coroutine waitForUngrabPistol;
        private void Awake()
        {
            photonView = GetComponent<PhotonView>();
        }

        private void OnEnable() {
            NetworkManager.Instance.onSyncGameState += Instance_onSyncGameState;
        }
        private void OnDisable()
        {
            NetworkManager.Instance.onSyncGameState -= Instance_onSyncGameState;
        }
        private void Instance_onSyncGameState(GameState state)
        {
            CheckNPC_Freeze();
        }

        public void InitNPC(PlayerType playerType)
        {
            photonView.RPC("RPC_InitNPC", RpcTarget.AllBufferedViaServer, playerType);
        }

        public void SwitchState()
        {

            Debug.Log($"<color=red>[NPC]SwitchState: call</color>");
            if (PhotonNetwork.IsMasterClient)
            {
                StartCoroutine(WaitForPistolSpin());
            }
        }
        private IEnumerator WaitForPistolSpin()//ожидание пока открывается/закрывается барабан пистолета
        {
            yield return new WaitForEndOfFrame();

            Debug.Log($"<color=red>[NPC]WaitForPistolSpin: call {npcState}</color>");
            switch (npcState)
            {
                case NPC_State.Idle:
                    break;
                case NPC_State.TurnToShoot:
                    StartCoroutine(TurnToShootHasCome());
                    break;
            }
        }

        private void ForgetPlayerWhoShotMe(string otherPlayer)//забыть игрока который в меня стрелял
        {
            var playersWhoShotMe = NetworkManager.Instance.gameState.GetPlayerState(base.UserID).playerWhoShotMe;
            List<string> playerWhoShotMeList = new List<string>();
            playerWhoShotMeList.AddRange(playersWhoShotMe);
            playerWhoShotMeList.Remove(otherPlayer);
            NetworkManager.Instance.gameState.GetPlayerState(base.UserID).playerWhoShotMe = playerWhoShotMeList.ToArray();
        }
        private string LogicOfTheShotNotInItself() //логика выстрела не в себя, возвращает UserID игрока в которого надо выстрелить
        {
            string playerToShoot_UserID = "air";
            Debug.Log("LogicOfTheShotNotInItself() called");
            var npcState = NetworkManager.Instance.gameState.GetPlayerState(base.UserID);

            if(PlayingCardsManager.Instance.TargetUserID != string.Empty)
            {
                if (PlayingCardsManager.Instance.TargetUserID != base.UserID) //если целью для обязательного выстрела не является сам нпс, выстрел в цель
                    return PlayingCardsManager.Instance.TargetUserID;
                else return playerToShoot_UserID; //если является - выстрел в воздух
            }

            if(npcState.playerWhoShotMe.Length > 0)//есть ли за столом игрок(и), который в меня стрелял
            {
                bool isThereAnyPlayerWithTheMostMoney = false;
                if (npcState.playerWhoShotMe.Length > 1)//если таких игроков больше одного, проверка есть ли среди них игрок с наибольшим количеством денег
                {
                    List<PlayerState> players = new List<PlayerState>();//получаем состояние игров стрелявших в данного
                    for(int i = 0; i < npcState.playerWhoShotMe.Length; i++)
                    {
                        var p = NetworkManager.Instance.gameState.GetPlayerState(npcState.playerWhoShotMe[i]);
                        if(!p.IsKilled)
                            players.Add(p);
                    }

                    if (players.Count == 0)
                        return playerToShoot_UserID;

                    int playerMoney = players[0].money;
                    foreach(var p in players)
                    {
                        if(p.money != playerMoney) //если условие выполняется, значит у игроков разное кол-во денег, и надо искать игрока с наибольшим колв-вом
                        {
                            isThereAnyPlayerWithTheMostMoney = true;
                            break;
                        }
                    }

                    if (isThereAnyPlayerWithTheMostMoney)
                    {
                        players.Sort((p1, p2) => p2.money.CompareTo(p1.money));
                        playerToShoot_UserID = players[0].UserID; //найден игрок с наибольшим кол-вом денег в котрого надо выстрелить
                        ForgetPlayerWhoShotMe(playerToShoot_UserID); //забываем что это игрок в меня стрелял
                    }
                    else //у всех стрелявших игроков равное кол-во денег, стреляем в рандомного
                    {
                        playerToShoot_UserID = players[Random.Range(0, players.Count)].UserID;
                        ForgetPlayerWhoShotMe(playerToShoot_UserID); //забываем что это игрок в меня стрелял
                    }
                }
                else
                {
                    playerToShoot_UserID = npcState.playerWhoShotMe[0]; //есть всего один игрок стрелявший в меня => в него и стреляем
                    ForgetPlayerWhoShotMe(playerToShoot_UserID); //забываем что это игрок в меня стрелял

                }
    
            }
            else
            {
                var allPlayers = NetworkManager.Instance.gameState.players;
                var otherPlayers = new List<PlayerState>();

                for(int i =0; i < allPlayers.Length; i++)
                {
                    if(allPlayers[i].UserID != base.UserID)//добавляем в список всех живых игроков кроме текущего
                    {
                        if(!NetworkManager.Instance.gameState.GetPlayerState(allPlayers[i].UserID).IsKilled)
                            otherPlayers.Add(allPlayers[i]);
                    }
                }

                

                otherPlayers.Sort((p1, p2) => p2.money.CompareTo(p1.money));

                if(otherPlayers[0].money > 0) //есть игрок с наибольшим кол-вом денег
                {
                    float chanceToShootHim = Random.Range(0f, 1f);
                    if(chanceToShootHim < 0.7f)//с 70% вероятностью стреляем в только что найденного самого богатого игрока
                    {
                        playerToShoot_UserID = otherPlayers[0].UserID;
                    }
                    else //c 30% вероятностью выбираем игрока с отличным от нуля количеством денег
                    {
                        int countOfNonZeroMoneyPlayer = 0; //кол-во игроков с отличным от нуля количеством денег
                        for (int i = 0; i < otherPlayers.Count; i++)
                        {
                            if(otherPlayers[i].money > 0)
                            {
                                countOfNonZeroMoneyPlayer++;
                            }
                        }

                        chanceToShootHim = Random.Range(0f, 1f);
                        if(chanceToShootHim < 0.5f) //50% вероятность что все таки в итоге выстрел в воздух
                        {
                            playerToShoot_UserID = "air"; //выстрел в воздух 
                        }
                        else
                        {
                            playerToShoot_UserID = otherPlayers[Random.Range(0, countOfNonZeroMoneyPlayer)].UserID; //выстрел в рандомного игрок с не 0 деньгами
                        }

                    }
                }
                else
                {
                    playerToShoot_UserID = "air"; //у всех 0 денег, выстрел в воздух 
                }
            }

            return playerToShoot_UserID;
        }

        private void ShootAnotherPlayer(string anotherUserID)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                photonView.RPC("NPC_Aiming", RpcTarget.All, anotherUserID);
            }

        }
        private IEnumerator GrabPistolDelay()
        {
            yield return new WaitForSeconds(4f);
            Debug.Log("<color=blue>Delay Dealay Delay</color>");
        }
        private IEnumerator TurnToShootHasCome()
        {
            float float_chance;
            int money;
            Debug.Log($"<color=red>[NPC]TurnToShootHasCome: {NetworkManager.Instance.gameState.GetPlayerState(base.UserID).ToString()}</color>");

            if (MainPistol.Instance.IsGunCharged)//пистолет заряжен?
            {
                PlayerState myState = NetworkManager.Instance.gameState.GetPlayerState(base.UserID);
                float interim_chance = Random.Range(0, 1f) * 100;
                int stepsAfterReload = NetworkManager.Instance.gameState.currIndex + 1;
                string msg = string.Format("<color=blue>chance: " + ((stepsAfterReload / 6f) * 100).ToString("0.00") + " bullet index: " + NetworkManager.Instance.gameState.bulletIndex + "</color>");
                Debug.Log(msg);
                bool seeBulletUsed = false;
                switch(stepsAfterReload)
                {
                    case 1: //вероятность 16-33Trigger("TossCard")
                        ShootToThemSelf();
                        //NetworkManager.Instance.gameState.GetPlayerState(base.UserID).canReload = false;
                        break;
                    case 2: //вероятность 16-33
                        ShootToThemSelf();
                        break;
                    case 3: //вероятность 50-66

                        if (interim_chance > 65) //35% вероятность выстрела в себя сразу же
                        {
                            ShootToThemSelf();
                            yield break;
                        }
                        else
                        {
                            if (!cardAlreadyPlayed && myState.money > 0)
                            {
                                if(PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.SeeBullet)) //если есть карта просмотра барабана - сыграть ее
                                {
                                    PlayCard(PlayingCardType.SeeBullet);
                                    yield return new WaitForEndOfFrame();
                                    yield return new WaitForEndOfFrame();
                                    if (!NetworkManager.Instance.gameState.IsBullet()) //если следующий выстрел не убьет нпс - выстрел в себя
                                    {
                                        Debug.Log(string.Format("<color=red>see bullet & shoot itself</color>"));
                                        ShootToThemSelf();
                                        yield break;
                                    }
                                    seeBulletUsed = true;
                                }
                            }
                        }

                        money = myState.money;
                        if(money > 0 && myState.money > 0) //есть ли деньги на выстрел не в себя
                        {
                            string userIDtoShot = LogicOfTheShotNotInItself();
                            if (userIDtoShot == string.Empty)
                            {
                                Debug.LogError("My error: LogicOfTheShotNotInItself() -> not all branches of code return values normally");
                            }
                            if (seeBulletUsed)
                            {
                                Debug.Log(string.Format("<color=green>WAIT WHILE PISTOL GRABBING</color>"));
                                yield return GrabPistolDelay();
                            }
                            this.ShootAnotherPlayer(userIDtoShot);
                        }
                        else
                        {
                            if (!cardAlreadyPlayed)
                            {
                                if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.DivideAllMoney)) 
                                {
                                    bool isPlayer = false;
                                    for (int i = 0; i < 4; i++)
                                        if (NetworkManager.Instance.gameState.players[i].money >= NetworkManager.Instance.gameState.AlivePlayers()) //поиск игрока с кол-вом денег >= кол-во живых игроков
                                        {
                                            isPlayer = true;
                                            break;
                                        }

                                    if (isPlayer)
                                    {
                                        PlayCard(PlayingCardType.DivideAllMoney);
                                        yield return new WaitForEndOfFrame();
                                        yield return new WaitForEndOfFrame();
                                        string userIDtoShot = LogicOfTheShotNotInItself();
                                        if (userIDtoShot == string.Empty)
                                        {
                                            Debug.LogError("My error: LogicOfTheShotNotInItself() -> not all branches of code return values normally");
                                        }
                                        yield return GrabPistolDelay();
                                        this.ShootAnotherPlayer(userIDtoShot);
                                        yield break;
                                    }


                                }

                                if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.SpinPistol))
                                {
                                    PlayCard(PlayingCardType.SpinPistol);
                                    yield break;
                                }
                            }
                            ShootToThemSelf();
                        }
                        break;
                    case 4: //вероятность 50-66

                        if (interim_chance > 85) //15% вероятность выстрела в себя сразу же
                        {
                            ShootToThemSelf();
                            yield break;
                        }
                        else
                        {
                            if (!cardAlreadyPlayed && myState.money > 0)
                            {
                                if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.SeeBullet)) //если есть карта просмотра барабана - сыграть ее
                                {
                                    PlayCard(PlayingCardType.SeeBullet);
                                    yield return new WaitForEndOfFrame();
                                    yield return new WaitForEndOfFrame();
                                    if (!NetworkManager.Instance.gameState.IsBullet()) //если следующий выстрел не убьет нпс - выстрел в себя
                                    {
                                        Debug.Log(string.Format("<color=red>see bullet & shoot itself</color>"));
                                        ShootToThemSelf();
                                        yield break;
                                    }
                                    seeBulletUsed = true;
                                }
                            }
                        }

                        money = myState.money;
                        if (money > 0) //есть ли деньги на выстрел не в себя
                        {
                            string userIDtoShot = LogicOfTheShotNotInItself();
                            if (userIDtoShot == string.Empty)
                            {
                                Debug.LogError("My error: LogicOfTheShotNotInItself() -> not all branches of code return values normally");
                            }
                            
                            if (seeBulletUsed)
                            {
                                Debug.Log(string.Format("<color=green>WAIT WHILE PISTOL GRABBING</color>"));
                                yield return GrabPistolDelay();
                            }
                            this.ShootAnotherPlayer(userIDtoShot);
                        }
                        else
                        {
                            if (!cardAlreadyPlayed)
                            {
                                if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.DivideAllMoney)) //если есть карта просмотра барабана - сыграть ее
                                {
                                    bool isPlayer = false;
                                    for(int i = 0; i < 4; i++)
                                        if(NetworkManager.Instance.gameState.players[i].money >= NetworkManager.Instance.gameState.AlivePlayers()) //поиск игрока с кол-вом денег >= кол-во живых игроков
                                        {
                                            isPlayer = true;
                                            break;
                                        }

                                    if(isPlayer)
                                    {
                                        PlayCard(PlayingCardType.DivideAllMoney);
                                        yield return new WaitForEndOfFrame();
                                        yield return new WaitForEndOfFrame();
                                        string userIDtoShot = LogicOfTheShotNotInItself();
                                        if (userIDtoShot == string.Empty)
                                        {
                                            Debug.LogError("My error: LogicOfTheShotNotInItself() -> not all branches of code return values normally");
                                        }
                                        yield return GrabPistolDelay();
                                        this.ShootAnotherPlayer(userIDtoShot);
                                        yield break;
                                    }

                                }

                                if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.SpinPistol))
                                {
                                    PlayCard(PlayingCardType.SpinPistol);
                                    yield break;
                                }
                            }

                            ShootToThemSelf();

                        }
                        break;
                    case 5: //вероятность 80
                        if (!cardAlreadyPlayed && myState.money > 0)
                        {
                            if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.SeeBullet)) //если есть карта просмотра барабана - сыграть ее
                            {
                                PlayCard(PlayingCardType.SeeBullet);
                                yield return new WaitForEndOfFrame();
                                yield return new WaitForEndOfFrame();
                                if (!NetworkManager.Instance.gameState.IsBullet()) //если следующий выстрел не убьет нпс - выстрел в себя
                                {
                                    Debug.Log(string.Format("<color=red>see bullet & shoot itself</color>"));
                                    ShootToThemSelf();
                                    yield break;
                                }
                                seeBulletUsed = true;
                            }
                        }


                        money = myState.money;
                        if (money > 0) //есть ли деньги на выстрел не в себя
                        {
                            string userIDtoShot = LogicOfTheShotNotInItself();
                            if (userIDtoShot == string.Empty)
                            {
                                Debug.LogError("My error: LogicOfTheShotNotInItself() -> not all branches of code return values normally");
                            }
                            if (seeBulletUsed)
                            {
                                Debug.Log(string.Format("<color=green>WAIT WHILE PISTOL GRABBING</color>"));
                                yield return GrabPistolDelay();
                            }
                            this.ShootAnotherPlayer(userIDtoShot);
                        }
                        else
                        {
                            if (!cardAlreadyPlayed)
                            {
                                if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.DivideAllMoney))
                                {
                                    bool isPlayer = false;
                                    for (int i = 0; i < 4; i++)
                                        if (NetworkManager.Instance.gameState.players[i].money >= NetworkManager.Instance.gameState.AlivePlayers()) //поиск игрока с кол-вом денег >= кол-во живых игроков
                                        {
                                            isPlayer = true;
                                            break;
                                        }

                                    if (isPlayer)
                                    {
                                        PlayCard(PlayingCardType.DivideAllMoney);
                                        yield return new WaitForEndOfFrame();
                                        yield return new WaitForEndOfFrame();
                                        string userIDtoShot = LogicOfTheShotNotInItself();
                                        if (userIDtoShot == string.Empty)
                                        {
                                            Debug.LogError("My error: LogicOfTheShotNotInItself() -> not all branches of code return values normally");
                                        }
                                        yield return GrabPistolDelay();
                                        this.ShootAnotherPlayer(userIDtoShot);
                                        yield break;
                                    }
                                }
                                if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.SpinPistol))
                                {
                                    
                                    PlayCard(PlayingCardType.SpinPistol);
                                    yield break;
                                }
                            }

                            ShootToThemSelf();
                        }
                        break;
                    case 6: //вероятность 100
                        money = myState.money;
                        if (money > 0) //есть ли деньги на выстрел не в себя
                        {
                            string userIDtoShot = LogicOfTheShotNotInItself();
                            if (userIDtoShot == string.Empty)
                            {
                                Debug.LogError("My error: LogicOfTheShotNotInItself() -> not all branches of code return values normally");
                            }
                            this.ShootAnotherPlayer(userIDtoShot);
                        }
                        else
                        {
                            if (!cardAlreadyPlayed)
                            {
                                if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.DivideAllMoney)) //
                                {
                                    bool isPlayer = false;
                                    for (int i = 0; i < 4; i++)
                                        if (NetworkManager.Instance.gameState.players[i].money >= NetworkManager.Instance.gameState.AlivePlayers()) //поиск игрока с кол-вом денег >= кол-во живых игроков
                                        {
                                            isPlayer = true;
                                            break;
                                        }

                                    if (isPlayer)
                                    {
                                        PlayCard(PlayingCardType.DivideAllMoney);
                                        yield return new WaitForEndOfFrame();
                                        yield return new WaitForEndOfFrame();
                                        string userIDtoShot = LogicOfTheShotNotInItself();
                                        if (userIDtoShot == string.Empty)
                                        {
                                            Debug.LogError("My error: LogicOfTheShotNotInItself() -> not all branches of code return values normally");
                                        }
                                        yield return GrabPistolDelay();
                                        this.ShootAnotherPlayer(userIDtoShot);
                                        yield break;
                                    }

                                }

                                if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.SpinPistol))
                                {
                                    
                                    PlayCard(PlayingCardType.SpinPistol);
                                    yield break;
                                }
                            }

                            ShootToThemSelf();
                        }
                        break;
                }
               
            }
            else //нпс прокрутка и перезярдка пистолета
            {
                //Debug.Log("Перезаярдка by NPC - " + transform.root.name);
                npcAnimator.SetBool("SpinPistol", true);
                this.DisableAnimatorParameter("SpinPistol");
            }
            yield return new WaitForEndOfFrame();
        }

        private void PlayCard(PlayingCardType cardType) {
            cardAlreadyPlayed = true;
            npcAnimator.SetBool("TossingCard", true);
            photonView.RPC("RPC_SetCardToPlay", RpcTarget.All, (int) cardType);
        }

        private void ShootToThemSelf() {
            npcAnimator.SetBool("AimToMyself", true); //выстрел в себя
            this.DisableAnimatorParameter("AimToMyself", 10);
        }

        private void PutPistolOnTable(float time = .8f)
        {
            npcAnimator.SetBool("PutPistolOnTable", true);
            DisableAnimatorParameter("PutPistolOnTable", time);
            Invoke("NPC_ResetParameters", .7f);
        }

        public void AcceptPosition(int pos, string id) //Применение позиции нпс в соответсвии с его индексом
        {
            Debug.Log($"[NPC]AcceptPosition:{name} {id} {pos} ");
            transform.position = GameManager.Instance.playersPositions[pos].place.position;
            transform.rotation = GameManager.Instance.playersPositions[pos].place.rotation;
        }

        public void SetNpcName(string name, string id)
        {                    
            photonView.RPC("SetUpName", RpcTarget.AllBuffered, name, id);           
        }

        public void Fear(bool fear, Vector3 pointToLook) //включает анимацию страха у нпс и поворачивает его к игроку который целится в него
        {
            if (NetworkManager.Instance.gameState.GetPlayerState(base.UserID).IsKilled)
            {
                fear = false;
            }

            if (fear)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    if (!fearEnabling)
                    {
                        delayFear = StartCoroutine(DelayFear(true));
                    }
                }
                else
                {
                    if (!fearEnabling)
                    {
                        delayFear = StartCoroutine(DelayFearNonMaster(true));
                    }
                }
            }
            else
            {
                if (delayFear != null)
                    StopCoroutine(delayFear);
                fearEnabling = false;
                if (PhotonNetwork.IsMasterClient)
                {
                    npcAnimator.SetBool("Fear", fear);
                }
                else
                {
                    photonView.RPC("RPC_NPC_EnableFear", RpcTarget.MasterClient, fear, pointToLook);
                }
            }

        }
        private bool fearEnabling = false;
        private Coroutine delayFear;
        private IEnumerator DelayFearNonMaster(bool fear)
        {
            fearEnabling = true;
            yield return new WaitForSeconds(1f);
            photonView.RPC("RPC_NPC_EnableFear", RpcTarget.MasterClient, fear, Vector3.zero);
            fearEnabling = false;
        }
        private IEnumerator DelayFear(bool fear)
        {
            fearEnabling = true;
            yield return new WaitForSeconds(1f);
            npcAnimator.SetBool("Fear", fear);
            fearEnabling = false;
        }
        
        public void NPC_TurnToShoot()//вызывает после синхронизации из NetworkManager, отсюда нпс начинает свой ход
        {
            Debug.Log("<color=yellow>turn to shoot: " + transform.root.name + "</color>");
            ResetAnimatorParameters();
            photonView.RPC("GrabGun", RpcTarget.All);
            cardAlreadyPlayed = false;
        }

        [PunRPC]
        private void RPC_RememberPistolDirection(Vector3 dir)
        {
            moveHeadAfterDeathDir = dir;
        }
        #region AbstractPlayer implementaton

        public override void ShowPlayer()
        {
            if (alive)
                return;

            
            alive = true;
            npcDeath.currentNPC_HeadTransform.gameObject.SetActive(true);
            npcAnimator.gameObject.SetActive(true);
            ragdollsManager.GetPlayerRagdoll(base.typeOfPlayer, Vector3.zero).SetActive(false);
            if (PhotonNetwork.IsMasterClient)
            {
                //npcAnimator.SetBool("Idle", true);
                //npcAnimator.SetFloat("IdleSpeed", Random.Range(0.8f, 2.0f));
                npcState = NPC_State.Idle;
            }
        }
        public override void HidePlayer(bool withoutRagdall=false)
        {
            if (!alive)
                return;

            alive = false;
            var _players = NetworkManager.Instance.gameState.players;//после смерти, удаляем у остальных игроков из списка стрелявших в него себ
            for (int i = 0; i < _players.Length; i++)
            {
                if (_players[i].UserID != base.UserID)
                {
                    List<string> shootedMe = new List<string>();
                    shootedMe.AddRange(_players[i].playerWhoShotMe);
                    if (shootedMe.Contains(base.UserID))
                    {
                        shootedMe.Remove(base.UserID);
                        _players[i].playerWhoShotMe = shootedMe.ToArray();
                    }
                }
            }
            StartCoroutine(DelayDisablePlayer(moveHeadAfterDeathDir, withoutRagdall));
            base.RemoveCoin(-1);
        }
        public override void RetunCardOnTable()
        {

        }
        #endregion
        private IEnumerator DelayDisablePlayer(Vector3 forceHeadDir, bool withoutRagdall=false)
        {
            yield return new WaitForSeconds(.2f);
            NPC p = null;

            if (PhotonNetwork.IsMasterClient)
            {
                p = MainPistol.Instance.transform.root.GetComponent<NPC>();
                if (p != null && p.Equals(this))
                {
                    MainPistol.Instance.PhotonView.RPC("RPC_DropPistol", RpcTarget.All);
                }
            }

            npcDeath.currentNPC_HeadTransform.gameObject.SetActive(false);

            if (p != null && p.Equals(this))//если было самоубийство этого нпс, задержка перед включением рагдолла, пока пистолет вылетит из руки
            {
                while (MainPistol.Instance.transform.parent != null)
                    yield return new WaitForEndOfFrame();
            }

            npcAnimator.gameObject.SetActive(false);
            if(!withoutRagdall)
                ragdollsManager.GetPlayerRagdoll(base.typeOfPlayer, forceHeadDir, true).SetActive(true);
            else {
                ragdollsManager.GetRagdallWithoutForce(base.typeOfPlayer).SetActive(true);
            }
        }
        private IEnumerator TossCard_Numerator()
        {
            yield return new WaitForEndOfFrame();
            Transform playingCard = playerModel.RightHand.transform.GetComponentInChildren<PlayingCard>().transform;
            playingCard.GetChild(0).localEulerAngles = new Vector3(-60, 0f, 0f);
            Vector3 fromTo = GameManager.Instance.reloadCardBox.position - playingCard.position;
            Vector3 fromToXZ = new Vector3(fromTo.x, 0f, fromTo.z);

            playingCard.rotation = Quaternion.LookRotation(fromToXZ, Vector3.up);
            float g = Physics.gravity.y;
            float x = fromToXZ.magnitude;
            float y = fromTo.y;

            float AngleInRadians = 60 * Mathf.PI / 180;

            float v2 = (g * x * x) / (2 * (y - Mathf.Tan(AngleInRadians) * x) * Mathf.Pow(Mathf.Cos(AngleInRadians), 2));
            float v = Mathf.Sqrt(Mathf.Abs(v2));
            Debug.DrawRay(playingCard.GetChild(0).position, playingCard.GetChild(0).forward * 10, Color.red, 20);
            photonView.RPC("UnGrabReloadCard", RpcTarget.All, playingCard.GetChild(0).forward, v);
        }
        private IEnumerator WaitForPistolUngrabbed() //ждем пока пистолет окажется на столе и берем его
        {
            while (!MainPistol.Instance.PistolMoving.PistolOnCenterOfTable) //ожидание пока пистолет в руке другого игрока
            {
                //Debug.Log(transform.root.name + " turn -> WaitForPistolUngrabbed()");
                yield return new WaitForEndOfFrame();
            }

            if (npcAnimator == null)
            {
                while (npcAnimator == null)
                    yield return new WaitForEndOfFrame();
                Debug.Log(transform.name + " animator is null");
            }

            if (cardAlreadyPlayed)
            {
                npcAnimator.SetBool("GrabPistol", true);
                this.DisableAnimatorParameter("GrabPistol");
                npcState = NPC_State.TurnToShoot;
                yield break;
            }

            npcAnimator.SetBool("Idle", false);
            yield return new WaitForEndOfFrame();

            GameState state = NetworkManager.Instance.gameState;



            if (MainPistol.Instance.IsGunCharged) {
                if (state.GetPlayerState(base.UserID).money == 0 && state.currIndex >= 3)
                {
                    if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.SkipTurn))//если есть карты отмены, она играется
                    {
                        PlayCard(PlayingCardType.SkipTurn);
                        yield return new WaitForEndOfFrame();
                        yield return new WaitForEndOfFrame();
                        npcState = NPC_State.TurnToShoot;
                        waitForUngrabPistol = null;
                        yield break;
                    }

                    if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.TurnMoveDirection))//если есть карты отмены, она играется
                    {
                        PlayCard(PlayingCardType.TurnMoveDirection);
                        yield return new WaitForEndOfFrame();
                        yield return new WaitForEndOfFrame();
                        npcState = NPC_State.TurnToShoot;
                        waitForUngrabPistol = null;
                        yield break;
                    }
                }

                if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.UndoLastPlayed))//если есть карты отмены, она играется
                {
                    bool lastCardSetTargetItself = PlayingCardsManager.Instance.LastPlayedCard == PlayingCardType.SetTarget && base.UserID == PlayingCardsManager.Instance.TargetUserID;
                    bool lastCardExtraLife = PlayingCardsManager.Instance.LastPlayedCard == PlayingCardType.ExtraLife && PlayingCardsManager.Instance.IsExtraLifeEffectValid();
                    bool lastCardProtect = PlayingCardsManager.Instance.LastPlayedCard == PlayingCardType.ProtectYourself && PlayingCardsManager.Instance.ProtectedUserID != string.Empty;

                    if (lastCardSetTargetItself || lastCardExtraLife || lastCardProtect)
                    {
                        cardAlreadyPlayed = true;
                        // npcAnimator.SetBool("TossCard", true);
                        PlayCard(PlayingCardType.UndoLastPlayed);
                        yield return new WaitForEndOfFrame();
                        yield return new WaitForEndOfFrame();
                        npcState = NPC_State.TurnToShoot;
                        waitForUngrabPistol = null;
                        yield break;
                    }
                }
                if (!PlayingCardsManager.Instance.StatusOnPlayer(UserID, PlayingCardType.ExtraLife) &&
                    !PlayingCardsManager.Instance.StatusOnPlayer(UserID, PlayingCardType.ProtectYourself) &&
                    !PlayingCardsManager.Instance.StatusOnPlayer(UserID, PlayingCardType.SetTarget)) {

                    if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.ExtraLife))
                    {
                        PlayCard(PlayingCardType.ExtraLife);
                        yield return new WaitForEndOfFrame();
                        yield return new WaitForEndOfFrame();
                        npcState = NPC_State.TurnToShoot;
                        waitForUngrabPistol = null;
                        yield break;
                    }

                    if (PlayingCardsManager.Instance.IsPlayerHasCard(base.UserID, PlayingCardType.ProtectYourself))
                    {
                        PlayCard(PlayingCardType.ProtectYourself);
                        yield return new WaitForEndOfFrame();
                        yield return new WaitForEndOfFrame();
                        npcState = NPC_State.TurnToShoot;
                        waitForUngrabPistol = null;
                        yield break;
                    }
                }


            }

            
            
            npcAnimator.SetBool("GrabPistol", true);
            this.DisableAnimatorParameter("GrabPistol");
            npcState = NPC_State.TurnToShoot;
            waitForUngrabPistol = null;
        }

        [PunRPC]
        private void RPC_SetCardToPlay(int cardID)
        {
            this.cardToPlay = (PlayingCardType)cardID;
        }
        public void NPC_TakeCard()
        {
            Debug.Log("NPC TOSS CARD: " + cardToPlay.ToString());
            PlayingCard card = PlayingCardsManager.Instance.GetCard(base.UserID, cardToPlay);
            card.EnableNetworkSync(false);
            card.transform.SetParent(playerModel.RightHand.transform);
            var rHandAttachPoint = playerModel.RightHand.GetComponent<NPC_HandAttachPoints>().GetAttachPoint(GrabbObject.ReloadCard);
            card.transform.position = rHandAttachPoint.position;
            card.transform.rotation = rHandAttachPoint.rotation;

            npcAnimator.SetBool("Idle", false);

            switch (cardToPlay)
            {
                case PlayingCardType.ExtraLife:
                    npcAnimator.SetBool("Idle", true);
                    break;
                case PlayingCardType.ProtectYourself:
                    npcAnimator.SetBool("Idle", true);
                    break;
                case PlayingCardType.TurnMoveDirection:
                    npcAnimator.SetBool("GrabPistol", false);
                    npcAnimator.SetBool("SpinPistol", false);
                    npcAnimator.SetBool("Aiming", false);
                    npcAnimator.SetBool("Shoot", false);
                    npcAnimator.SetBool("PutPistolOnTable", false);
                    npcAnimator.SetBool("AimToMyself", false);
                    npcAnimator.SetBool("AirAiming", false);
                    npcAnimator.SetBool("Idle", true);
                    NetworkManager.Instance.RequestToSync();
                    return;
                case PlayingCardType.SkipTurn:
                    npcAnimator.SetBool("GrabPistol", false);
                    npcAnimator.SetBool("SpinPistol", false);
                    npcAnimator.SetBool("Aiming", false);
                    npcAnimator.SetBool("Shoot", false);
                    npcAnimator.SetBool("PutPistolOnTable", false); 
                    npcAnimator.SetBool("AimToMyself", false); 
                    npcAnimator.SetBool("AirAiming", false); 

                    npcAnimator.SetBool("Idle", true);
                    NetworkManager.Instance.RequestToSync();
                    return;
                case PlayingCardType.SpinPistol:
                    npcAnimator.SetBool("SpinPistol", true);
                    this.DisableAnimatorParameter("SpinPistol", 4f);
                    return;
            }

            npcAnimator.SetBool("GrabPistol", true);
            this.DisableAnimatorParameter("GrabPistol", 4f);

        }

        [PunRPC]
        private void RPC_NPC_MadeShot()
        {
            npcFired = true;
        }
        public void NPC_MakeShot(int putOnTableAfterShot = 0)//нпс выстрел, вызывается из аниматора
        {
            if (PhotonNetwork.IsMasterClient)
            {
                Debug.Log("NPC makes shot!");
                MainPistol.Instance.MakeShot(this);
                photonView.RPC("RPC_NPC_MadeShot", RpcTarget.All);
            }
        }
        public void NPC_MakeSelfShot()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                Debug.Log("NPC makes self shot!");
                MainPistol.Instance.MakeShot(this, true);
                photonView.RPC("RPC_NPC_MadeShot", RpcTarget.All);
            }
        }
        public void CheckNPC_Freeze()
        {
            if (!PhotonNetwork.IsMasterClient)
                return;

            bool isMyTurn = NetworkManager.Instance.gameState.userTurn == base.UserID;

            if (!isMyTurn)
                return;

            if (waitForUngrabPistol == null &&
                npcState == NPC_State.Idle && 
                NetworkManager.Instance.gameState.AlivePlayers() > 1)
            {
                this.NPC_TurnToShoot();
            }
        }
        public void NPC_ReloadByCard()//перезарядка пистолета после использования карты прокрутки
        {
            //Pistol.Instance.Reload();//перезарядка пистолета
        }
        public void NPC_TossCard()//нпс бросает карту перезарядки, вызывается из анимации GrabReloadCard
        {
            if (PhotonNetwork.IsMasterClient)
            {
                StartCoroutine(TossCard_Numerator());
            }
        }

        public void NPC_TakePistolInHand() //вставляет пистолет в руку нпс, вызывается из аниматора
        {
            if(PhotonNetwork.IsMasterClient)
            {
                
                if (NetworkManager.Instance.gameState.userTurn!=UserID||PlayingCardsManager.Instance.IsDealing) {
                    Debug.LogError("[NPC]NPC_TakePistolInHand: taked pistol not in his turn");
                    npcAnimator.Play("npcIdle");
                    return;
                }

                photonView.RPC("RPC_NPC_TakePistolInHand", RpcTarget.All);
                MainPistol.Instance.SetUser(PhotonNetwork.MasterClient);
            }
        }
        public void RPC_CallToUnGrabePistol() //детачит пистолет от руки нпс и кладет его в центр стола, вызывается из аниматора
        {
            if (PhotonNetwork.IsMasterClient)
            {
                photonView.RPC("UnGrabPistol", RpcTarget.All);
                npcAnimator.SetBool("Idle", true);
            }
        }
        public void NPC_ReloadPistol()//нпс - перезярядка пистлета, вызывается из анимации перезарядки 
        {
            if (PhotonNetwork.IsMasterClient)
            {
                photonView.RPC("EnablePistolSpinSFX", RpcTarget.All);
            }
        }
        public void NPC_TakeBullet()//поднимает со стола пулю
        {
            if(PhotonNetwork.IsMasterClient)
            {
                photonView.RPC("RPC_NPC_TakeBullet", RpcTarget.All);
            }
        }
        public void NPC_ClosePistolBarrel()//закрывает барабан в пистолете
        {
            photonView.RPC("RPC_NPC_ReloadPistol", RpcTarget.All);
        }
        public void NPC_DropBullet()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                photonView.RPC("RPC_NPC_DropBullet", RpcTarget.All);
            }
        }//выключает пулю, якобы нпс вставил ее в барабан пистолета
        public void ChechIK_Value()
        {
            if (rightHandIK.solver.IKPositionWeight > 0.01f) StartCoroutine(IK_OFF());
        }
        private IEnumerator IK_OFF()
        {
            while (rightHandIK.solver.IKPositionWeight > 0.01f)
            {
                lookAtIK.solver.IKPositionWeight = Mathf.Lerp(lookAtIK.solver.IKPositionWeight, 0, 8 * Time.deltaTime);
                rightHandIK.solver.IKPositionWeight = Mathf.Lerp(rightHandIK.solver.IKPositionWeight, 0, 8 * Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }
        }
        private IEnumerator NPC_AimingNumerator(string targetUserID)
        {
            npcFired = false;
            Debug.Log("aiming numerator! target: " + targetUserID);
            if (PlayingCardsManager.Instance.ProtectedUserID == targetUserID)
                targetUserID = "air";

            if (targetUserID == "air")
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    npcAnimator.SetBool("AirAiming", true);
                    //this.DisableAnimatorParameter("AirAiming");
                }
                yield break;
            }


            var playerArray = FindObjectsOfType<AbstractPlayer>();
            List<AbstractPlayer> playersList = new List<AbstractPlayer>();
            playersList.AddRange(playerArray);
            var targetPlayer = playersList.Find(p => p.UserID == targetUserID);
            if(targetPlayer == null)
            {
                Debug.LogErrorFormat("player with id - {0} not found;", targetUserID);
                if (PhotonNetwork.IsMasterClient)
                {
                    npcAnimator.SetBool("AirAiming", true);
                    //this.DisableAnimatorParameter("AirAiming");
                }
                yield break;
            }

            int layerMask = 1 << LayerMask.NameToLayer("Head");
            if (PhotonNetwork.IsMasterClient)
            {
                npcAnimator.SetBool("Aiming", true);
                //this.DisableAnimatorParameter("Aiming");

            }
            lookAtIK.solver.target = targetPlayer.playerModel.Head.transform;
            rightHandIK.solver.target = targetPlayer.playerModel.Head.transform;
            this.TARGET_TO_SHOT = targetPlayer.playerModel.Head.transform;

            while (true)
            {
                yield return new WaitForEndOfFrame();
                lookAtIK.solver.IKPositionWeight = Mathf.Lerp(lookAtIK.solver.IKPositionWeight, 1f, 5 * Time.deltaTime);
                rightHandIK.solver.IKPositionWeight = Mathf.Lerp(rightHandIK.solver.IKPositionWeight, 1f, 5 * Time.deltaTime);
                if (lookAtIK.solver.IKPositionWeight > 0.99)
                {
                    RaycastHit hit;
                    Ray ray = new Ray(MainPistol.Instance.bulletSpawnPoint.position, MainPistol.Instance.bulletSpawnPoint.forward * 10);
                    if (Physics.Raycast(ray, out hit, 10f, layerMask))
                    {
                        if (hit.transform.root.GetComponent<AbstractPlayer>() != null) //нпс прицелилися в игрока
                        {

                            if (hit.transform.GetComponentInParent<AbstractPlayer>().UserID == targetUserID) //нпс прицелилися в нужного игрока
                            {
                                yield return new WaitForSeconds(.1f);
                                Debug.Log(targetUserID + " has aimed");
                                if (PhotonNetwork.IsMasterClient)
                                {
                                    npcAnimator.SetBool("Shoot", true);
                                    //this.DisableAnimatorParameter("Shoot");
                                }
                                break;
                            }
                        }
                    }
                }
            }

            while (!npcFired) //ожидание пока нпс сделает выстрел.
                yield return new WaitForEndOfFrame();

            yield return new WaitForSeconds(0.2f);
            while (rightHandIK.solver.IKPositionWeight > 0.01f)
            {
                lookAtIK.solver.IKPositionWeight = Mathf.Lerp(lookAtIK.solver.IKPositionWeight, 0, 5 * Time.deltaTime);
                rightHandIK.solver.IKPositionWeight = Mathf.Lerp(rightHandIK.solver.IKPositionWeight, 0, 5 * Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }

            lookAtIK.solver.IKPositionWeight = 0;
            rightHandIK.solver.IKPositionWeight = 0;
            lookAtIK.solver.target = rHandTarget;
            rightHandIK.solver.target = rHandTarget;
            rHandTarget.transform.localEulerAngles = Vector3.zero;
            rHandTarget.transform.localPosition = Vector3.zero;
            PutPistolOnTable();
        }
        private void DisableAnimatorParameter(string paramName, float time = .5f)
        {
            StartCoroutine(DELAY_DisableAnimationParameter(paramName, time));
        }
        private IEnumerator DELAY_DisableAnimationParameter(string animName, float time = .5f)
        {
            yield return new WaitForSeconds(time);
            npcAnimator.SetBool(animName, false);
        }

        public void ResetAnimatorParameters()
        {
            StopAllCoroutines();
            ChechIK_Value();
            npcAnimator.SetBool("Fear", false);
            npcAnimator.SetBool("GrabPistol", false);
            npcAnimator.SetBool("SpinPistol", false);
            npcAnimator.SetBool("Aiming", false);
            npcAnimator.SetBool("PutPistolOnTable", false);
            npcAnimator.SetBool("AirAiming", false);
            npcAnimator.SetBool("AimToMyself", false);
            npcAnimator.SetBool("Shoot", false);
            //npcAnimator.ResetTrigger("TossCard");
            npcAnimator.SetBool("TossingCard", false);
            npcAnimator.Play("npcIdle",0);
            waitForUngrabPistol = null;
            
        }

        #region RPC methods
        [PunRPC]
        private void RPC_InitNPC(PlayerType playerType)
        {
            Debug.Log("npc length: " + NPC_List.Count + " index: " + (int)playerType);
            NPC_List[(int)playerType].SetActive(true);
            this.lookAtIK = NPC_List[(int)playerType].GetComponent<NPC_Components>().lookAtIK;
            this.rightHandIK = NPC_List[(int)playerType].GetComponent<NPC_Components>().rightHandIK;
            this.playerModel = NPC_List[(int)playerType].GetComponent<NPC_Components>().playerModel;
            this.npcDeath = NPC_List[(int)playerType].GetComponent<NPC_Components>().npcDeath;
            npcAnimator = GetComponentInChildren<Animator>();
            base.typeOfPlayer = playerType;

            if (PhotonNetwork.IsMasterClient)
            {
                //npcAnimator.SetBool("Idle", true);
                //npcAnimator.SetFloat("IdleSpeed", Random.Range(0.8f, 2.0f));
                npcState = NPC_State.Idle;
            }
        }
        [PunRPC]
        private void RPC_NPC_EnableFear(bool fear, Vector3 pointToLook)
        {
            npcAnimator.SetBool("Fear", fear);

            if (pointToLook != Vector3.zero)
            {
                pointToLook.y = transform.root.position.y;
            }
            else
            {
                pointToLook = GameManager.Instance.tableCenter.position;
                pointToLook.y = transform.root.position.y;
            }
            //transform.LookAt(pointToLook);
        }
        [PunRPC]
        private void NPC_Aiming(string targetUserID)
        {
            StartCoroutine(NPC_AimingNumerator(targetUserID));
        }
        [PunRPC]
        private void EnablePistolSpinSFX()
        {
            MainPistol.Instance.ChargeGunByNPC();
            MainPistol.Instance.VisualSpin();
        }
        [PunRPC]
        private void RPC_NPC_ReloadPistol()
        {
            MainPistol.Instance.CloseBarrel();
        }
        [PunRPC]
        private void RPC_NPC_TakePistolInHand()
        {
            if(!MainPistol.Instance.IsGunCharged)
            {
                MainPistol.Instance.OpenBarrel();
            }

            MainPistol.Instance.GetComponent<GrabbaleObjects>().EnablingSync(false);
            MainPistol.Instance.GetComponent<Rigidbody>().isKinematic = true;
            MainPistol.Instance.GetComponent<GrabbaleObjects>().IsGrabbed = true;

            MainPistol.Instance.transform.SetParent(playerModel.RightHand.transform);
            var rHandAttachPoint = playerModel.RightHand.GetComponent<NPC_HandAttachPoints>().GetAttachPoint(GrabbObject.Pistol);
            MainPistol.Instance.transform.position = rHandAttachPoint.position;
            MainPistol.Instance.transform.rotation = rHandAttachPoint.rotation;
            MainPistol.Instance.PistolMoving.PistolGrabbed();

            this.SwitchState();
        }
        [PunRPC]
        private void RPC_NPC_TakeBullet()
        {
            Bullet.Instance.GetComponent<Rigidbody>().isKinematic = true;
            Bullet.Instance.GetComponent<PhotonTransformView>().enabled = false;
            Bullet.Instance.GetComponent<PhotonRigidbodyView>().enabled = false;
            Bullet.Instance.transform.SetParent(playerModel.LeftHand.transform);
            var lHandAttachPoint = playerModel.LeftHand.GetComponent<NPC_HandAttachPoints>().GetAttachPoint(GrabbObject.Bullet);
            Bullet.Instance.transform.position = lHandAttachPoint.position;
            Bullet.Instance.transform.rotation = lHandAttachPoint.rotation;
        }
        [PunRPC]
        private void RPC_NPC_DropBullet()
        {
            Bullet.Instance.GetComponent<GrabbaleObjects>().IsGrabbed = false;
            Bullet.Instance.transform.SetParent(null);
            Bullet.Instance.GetComponent<PhotonTransformView>().enabled = true;
            Bullet.Instance.GetComponent<PhotonRigidbodyView>().enabled = true;

            if (PhotonNetwork.IsMasterClient)
                Bullet.Instance.DisableBullet(.05f);
        }
        [PunRPC]
        private void UnGrabReloadCard(Vector3 direction, float speed)
        {
            PlayingCard playingCard = playerModel.RightHand.transform.GetComponentInChildren<PlayingCard>();
            playingCard.EnableNetworkSync(true);
            playingCard.transform.SetParent(null);
            playingCard.GetComponent<Rigidbody>().velocity = direction * speed;
        }
        [PunRPC]
        private void SetUpName(string name, string userID)
        {
            gameObject.name = name + " " + UserID;
            base.UserID = userID;
            playerInfoUI.SetNickname(name);
        }
        [PunRPC]
        private void GrabGun()
        {
            StopAllCoroutines();
            waitForUngrabPistol = StartCoroutine(WaitForPistolUngrabbed());
        }
        [PunRPC]
        private void UnGrabPistol()
        {
            npcState = NPC_State.Idle;

            if (MainPistol.Instance.transform.parent != null)
            {
                MainPistol.Instance.GetComponent<GrabbaleObjects>().IsGrabbed = false;
                MainPistol.Instance.transform.SetParent(null);
                MainPistol.Instance.GetComponent<GrabbaleObjects>().EnablingSync(true);
                MainPistol.Instance.NPC_UngrabbPistol();
                MainPistol.Instance.GetComponent<Rigidbody>().isKinematic = false;
            }
            
        }
        #endregion
    }
}
