﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class NPC_AnimationsEvents : MonoBehaviour
    {
        private NPC npcRoot;
        private void Awake()
        {
            npcRoot = GetComponentInParent<NPC>();
            anim = GetComponent<Animator>();
        }

        private void OnEnable() {
            NetworkManager.Instance.OnTurnSwitched.AddListener(CheckIdle);
        }

        private void OnDisable() {
            NetworkManager.Instance.OnTurnSwitched.RemoveListener(CheckIdle);
        }

        


        private void CheckPistolInHand() {
            var pistol=GetComponentInChildren<MainPistol>();
            if (pistol == null) { 
                Debug.LogWarning("[NPC_AnimationsEvents]Pistol not in hand force use TakePistol method");
                NPC_TakePistolInHand();
            }
        }

        public void ChecIK()
        {
            npcRoot.ChechIK_Value();
        }
        public void NPC_TakePistolInHand()
        {
            npcRoot.NPC_TakePistolInHand();
        }
        public void NPC_MakeSelfShot()
        {
            npcRoot.NPC_MakeSelfShot();
        }
        public void NPC_MakeShot(int putOnTableAfterShot = 0)
        {
            npcRoot.NPC_MakeShot(putOnTableAfterShot);
        }

        public void RPC_CallToUnGrabePistol()
        {
            npcRoot.RPC_CallToUnGrabePistol();
        }

        public void SwitchState()
        {
            npcRoot.SwitchState();
        }

        public void NPC_ReloadPistol()
        {
            npcRoot.NPC_ReloadPistol();
        }

        public void NPC_TakeCardInhand()
        {
            //npcRoot.NPC_TakeCardInhand();
            npcRoot.NPC_TakeCard();
            GetComponent<Animator>().SetBool("TossingCard",false);
        }
        public void NPC_CheckFreeze()
        {
            npcRoot.CheckNPC_Freeze();
        }
        public void NPC_TossCard()
        {
            npcRoot.NPC_TossCard();
        }

        public void NPC_ReloadByCard()
        {
            npcRoot.NPC_ReloadByCard();
        }

        public void NPC_TakeBullet()
        {
            npcRoot.NPC_TakeBullet();
        }
        public void NPC_DropBullet()
        {
            npcRoot.NPC_DropBullet();
        }
        public void NPC_ClosePistolBarrel()
        {
            npcRoot.NPC_ClosePistolBarrel();
        }

        public void NPC_ResetParameters()
        {
            //npcRoot.ResetAnimatorParameters();
        }

        private void CheckIdle()
        {
            if (npcRoot.UserID != NetworkManager.Instance.gameState.userTurn) {
                if (checkCor != null)
                    StopCoroutine(checkCor);
                checkCor =StartCoroutine(checkIdle());
            }else {
                if(checkCor!=null)
                    StopCoroutine(checkCor);
                checkCor = null;
            }
        }

        private Coroutine checkCor;
        private IEnumerator checkIdle() {
            yield return new WaitForSeconds(6f);
            if (npcRoot.UserID != NetworkManager.Instance.gameState.userTurn) {
                var state = anim.GetCurrentAnimatorStateInfo(0);
                if (state.IsName("npcIdle") == false && state.IsName("Fear") == false &&
                    state.IsName("Gentlman_Fear_loop") == false) {
                    anim.Play("npcIdle");
                    
                    Debug.LogError("<color=green>[NPC_AnimationsEvents]CheckIdle->checkIdle: check if not idle or fear->play npcIdle</color>"+name);
                }
            }
            checkCor = null;
        }

        private Animator anim;

        private void CheckFreeze() {
            StartCoroutine(check());

        }

        private IEnumerator check() {
            yield return new WaitForSeconds(0.5f);
            var state=anim.GetCurrentAnimatorStateInfo(0);
            if (npcRoot.UserID != NetworkManager.Instance.gameState.userTurn) {
                if (state.IsName("GrabPidtol") && anim.GetBool("Idle")) {
                    anim.Play("npcIdle");
                    Debug.LogError(
                        "<color=green>[NPC_AnimationsEvents]CheckFreeze->check: try to prevent freeze switch animation to npcIdle</color>");
                }
            }
        }
    }
}
