﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SaloonSpin
{
    public class NPC_Components : MonoBehaviour
    {
        public PlayerParts playerModel;
        public RootMotion.FinalIK.LookAtIK lookAtIK;
        public RootMotion.FinalIK.LimbIK rightHandIK;
        [SerializeField]
        private List<Player_Renderers> renderers = new List<Player_Renderers>();
        [SerializeField]
        private Material ghostMaterial;
        public NPC_Death npcDeath; //deprecated
        private bool hidden = false;
        private void Start()
        {
            foreach (var r in renderers)
            {
                r.rendererMaterials = new List<Material>();
                r.rendererMaterials.AddRange(r.renderer.materials);
            }
        }
        public void HidePlayer()
        {
            if(!hidden)
            {
                foreach (var r in renderers)
                    r.ToGhost(ghostMaterial, this, true);

                hidden = true;
                onShow.Invoke();
            }
        }
        public void ShowPlayer()
        {
            if (hidden)
            {
                foreach (var r in renderers)
                    r.ToNormal(true);

                hidden = false;
                onShow.Invoke();
            }
        }

        [SerializeField] private UnityEvent onShow;

        [ContextMenu("Fill renderers")]
        public void LoadRenderersComponents()
        {
            var rendersList = GetComponentsInChildren<Renderer>();
            //this.renderers = new List<Player_Renderers>(rendersList.Length);
            for (int i = 0; i < rendersList.Length; i++)
            {
                var r = new Player_Renderers();
                r.renderer = rendersList[i];
                renderers.Add(r);
            }

            Debug.Log("fiil: " + rendersList.Length);
        }
    }

    [System.Serializable]
    public class Player_Renderers
    {
        public Renderer renderer;
        public List<Material> rendererMaterials { get; set; }

        public void ToGhost(Material ghost, MonoBehaviour beh, bool isNPC = false)
        {           
            var ghostArr = new Material[renderer.materials.Length];
            for (int i = 0; i < ghostArr.Length; i++)
                ghostArr[i] = ghost;

            renderer.materials = ghostArr;
            if(renderer.CompareTag("Hat"))
            {
                renderer.enabled = false;
            }
        }
        public void ToGhostMyHead(Material ghost)
        {
            var ghostArr = new Material[renderer.materials.Length];
            for (int i = 0; i < ghostArr.Length; i++)
                ghostArr[i] = ghost;

            renderer.materials = ghostArr;
        }
        public void ToNormal(bool isNPC = false)
        {
            renderer.materials = rendererMaterials.ToArray();
            if(!renderer.enabled)
                renderer.enabled = true;
        }
    }

}
