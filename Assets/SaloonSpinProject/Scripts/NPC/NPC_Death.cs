﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class NPC_Death : MonoBehaviour
    {
        [SerializeField] public Transform Hat;
        [SerializeField] private Transform flyingHead;
        public Transform currentNPC_HeadTransform;
        [SerializeField] private float forceMultiplayer = 3f;
        [SerializeField] private float dropHatSpeed = 1f;
        private Vector3 defaultHatPosition;
        private Coroutine dropHatRoutine;
        [SerializeField] private GameObject ragdollPrefab;
        [SerializeField] private Transform ragdollPosAndRot;
        private void Start()
        {
            defaultHatPosition = Hat.transform.localPosition;
        }
        public void InitDeath(Vector3 pointToDropHat, Vector3 headForce)
        {
            currentNPC_HeadTransform.gameObject.SetActive(false);
            flyingHead.transform.localScale = new Vector3(1, 1, 1);
            flyingHead.transform.position = currentNPC_HeadTransform.position;
            Hat.gameObject.SetActive(true);
            flyingHead.gameObject.SetActive(true);
            headForce *= forceMultiplayer;

            flyingHead.GetComponent<Rigidbody>().isKinematic = false;
            flyingHead.GetComponent<Rigidbody>().AddForce(headForce, ForceMode.Impulse);

            Hat.gameObject.SetActive(true);
            if(ragdollPosAndRot.childCount != 0)
                Destroy(ragdollPosAndRot.GetChild(0).gameObject);
            var ragdoll = Instantiate(ragdollPrefab, ragdollPosAndRot);
            
            StartCoroutine(EnableHeadCollider());
        }

        private IEnumerator DropHat(Vector3 from, Vector3 to)
        {
            Hat.position = from;
            while (true)
            {
                Hat.position = Vector3.Lerp(Hat.position, to, dropHatSpeed * Time.deltaTime);
                if(Vector3.Distance(Hat.position, to) < 0.001f)
                {
                    Hat.position = to;
                    break;
                }
                yield return new WaitForEndOfFrame();
            }
        }
        private IEnumerator EnableHeadCollider()
        {
            yield return new WaitForSeconds(1.5f);
            
            currentNPC_HeadTransform.gameObject.SetActive(true);
            yield return new WaitForSeconds(5f);
            flyingHead.GetComponent<Rigidbody>().isKinematic = true;

        }
        public void Reset()
        {
            //if (dropHatRoutine != null)
            //    StopCoroutine(dropHatRoutine);

            //Hat.transform.localPosition = defaultHatPosition;
            flyingHead.transform.position = currentNPC_HeadTransform.position;
            currentNPC_HeadTransform.gameObject.SetActive(true);
            flyingHead.gameObject.SetActive(false);
            Destroy(Hat.GetChild(1).gameObject);
            Hat.gameObject.SetActive(false);

        }
    }
}