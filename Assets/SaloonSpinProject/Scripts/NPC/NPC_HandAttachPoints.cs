﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class NPC_HandAttachPoints : MonoBehaviour
    {
        [SerializeField] private Transform pistolAttachPoint;
        [SerializeField] private Transform bulletAttachPoint;
        [SerializeField] private Transform spinCardAttachPoint;

        public Transform GetAttachPoint(GrabbObject type)
        {
            switch (type)
            {
                case GrabbObject.Pistol:
                    return pistolAttachPoint;
                case GrabbObject.ReloadCard:
                    return spinCardAttachPoint;
                case GrabbObject.Bullet:
                    return bulletAttachPoint;
            }
            return null;
        }
    }
}