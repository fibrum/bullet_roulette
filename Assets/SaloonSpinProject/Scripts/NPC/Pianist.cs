﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin{ 
    public class Pianist : MonoBehaviour
    {
        public static Pianist Instance;
        public GameObject ragdoll;

        private void Awake()
        {
            Instance = this;
        }
       /* private void Update()
        {
            if (Input.GetKeyDown(KeyCode.D))
                Death();
        }*/
        [PunRPC]
        private void Death()
        {
            //GameManager.Instance.startButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener(Lobby.Instance.CreatePianist);
            PhotonNetwork.InstantiateSceneObject("NPC_RagDolls/" + ragdoll.name, transform.position, transform.rotation);
            PhotonNetwork.Destroy(gameObject);
        }

        public void PianistPause(float time)
        {
            StartCoroutine(UnPause(time));
            GetComponent<TestDragonVoice>().Pause(time);
        }
        private IEnumerator UnPause(float time)
        {
            yield return new WaitForSeconds(time);
            GetComponent<TestDragonVoice>().UnPause();
        }

        private void OnDestroy() {
            Instance = null;
        }
    }
}
