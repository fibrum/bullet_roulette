﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SaloonSpin
{
    public class Ragdoll : MonoBehaviour
    {
        [SerializeField] private GameObject head;
        public void ForceHead(Vector3 dir)
        {
            dir *= 3;
            head.GetComponent<Rigidbody>().AddForce(dir, ForceMode.Impulse);
        }

        public void SkipForce() {
            foreach (GameObject o in disableObjectsForSkip) {
                o.SetActive(false);
            }
        }

        [SerializeField] private GameObject[] disableObjectsForSkip;
    }
}
