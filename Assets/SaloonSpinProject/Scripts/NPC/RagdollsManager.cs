﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SaloonSpin
{
    public class RagdollsManager : MonoBehaviour
    {
        [SerializeField] private List<GameObject> ragdollsList = new List<GameObject>();
        private Ragdoll rd = null;
        public GameObject GetPlayerRagdoll(PlayerType playerType, Vector3 dir, bool forceHead = false)
        {
            foreach (Transform child in transform)
                Destroy(child.gameObject);

            rd = Instantiate(ragdollsList[(int)playerType], transform).GetComponent<Ragdoll>();

            if (forceHead)
                StartCoroutine(ForceHead(dir));

            return rd.gameObject;
        }

        public GameObject GetRagdallWithoutForce(PlayerType playerType) {
            foreach (Transform child in transform)
                Destroy(child.gameObject);

            rd = Instantiate(ragdollsList[(int)playerType], transform).GetComponent<Ragdoll>();

            rd.SkipForce();

            return rd.gameObject;
        }

        private IEnumerator ForceHead(Vector3 dir)
        {
            yield return new WaitForEndOfFrame();
            if(rd != null)
                rd.ForceHead(dir); 
        }
    }
}
