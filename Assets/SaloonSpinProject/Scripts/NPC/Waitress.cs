﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace SaloonSpin
{
    public enum WaitressState
    {
        Walk,
        CarryGlass
    }

    public class Waitress : MonoBehaviourPunCallbacks
    {      
        public static Waitress Instance;
        [SerializeField] private GameObject waitressDefaultGlass;
        [SerializeField] private PlayableDirector walk, carryGlasses;
        [SerializeField] private GameObject ragdoll;
        [SerializeField] private Transform[] pointsForGlasses;

        private PlayableDirector playableDirector;
        private double time = 0;
        private WaitressState waitressState;
        private Coroutine checkForBrokenGlasses;
        private void Awake()
        {
            if(Instance == null)
                Instance = this;
        }
        private void Start()
        {
            waitressState = WaitressState.Walk;
            walk.gameObject.SetActive(true);
            playableDirector = walk;
            Debug.Log("<color=red>WAITRESS created</color>");
        }

        [PunRPC]
        private void InitialTimeRequest()
        {
            photonView.RPC("InitialTimeAccept", RpcTarget.Others, (float)playableDirector.time);
        }

        [PunRPC]
        private void InitialTimeAccept(float time)
        {
            playableDirector.time = (double)time;
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.D))
                Death();
        }
        [PunRPC]
        private void Death()
        {
            Debug.Log("<color=red>WAITRESS DIE</color>");
           

            //GameManager.Instance.startButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener(Lobby.Instance.CreateWaitress);
            GameManager.Instance.startButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener(()=> Debug.Log("<color=red>WAITRESS must be created</color>"));
            PhotonNetwork.InstantiateSceneObject("NPC_RagDolls/" + ragdoll.name, transform.GetChild(0).position, transform.GetChild(0).rotation);
            PhotonNetwork.Destroy(gameObject);
        }
        private void OnDestroy()
        {
            GlassesSystem.Instance.ClearGlassesReferences();
            Instance = null;
        }
        private void SwitchState(WaitressState switchTO, float time)
        {
            if(waitressState == switchTO)
            {
                Debug.LogError("waitress state repeated!");
                return;
            }
            carryGlasses.gameObject.SetActive(false);
            walk.gameObject.SetActive(false);
            Debug.Log("SWITCH TO " + switchTO.ToString());
            //double time = playableDirector.time;
            switch (switchTO)
            {
                case WaitressState.Walk:
                    playableDirector = walk;
                    walk.time = 0;
                    walk.initialTime = 0;
                    walk.gameObject.SetActive(true);
                    break;
                case WaitressState.CarryGlass:
                    playableDirector = carryGlasses;
                    carryGlasses.time = 0;
                    carryGlasses.initialTime = time + .1;
                    carryGlasses.gameObject.SetActive(true);
                    break;
            }
            waitressState = switchTO;
        }
        [PunRPC]
        private void SwitchState_RPC(int state, float time)
        {
            SwitchState((WaitressState)state, time);
        }
        public void GiveGlasses()
        {
            if(waitressState == WaitressState.CarryGlass)
            {
                GlassesSystem.Instance.GiveGlasses();
                carryGlasses.initialTime = 0f;
            }
            else
            {
                Debug.LogError("Give glasses call on Walk state!");
            }

            playableDirector.initialTime = 0f;
        }
        public void CheckGlasses()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                if (GlassesSystem.Instance.HaveBrokenGlasses && waitressState != WaitressState.CarryGlass)
                {
                    photonView.RPC("SwitchState_RPC", RpcTarget.All, (int)WaitressState.CarryGlass, (float)playableDirector.time);
                }
                else if (!GlassesSystem.Instance.HaveBrokenGlasses && waitressState != WaitressState.Walk)
                {
                    photonView.RPC("SwitchState_RPC", RpcTarget.All, (int)WaitressState.Walk, (float)playableDirector.time);
                }
            }
        }
        public void CreateGlasses()
        {
            StartCoroutine(CreateGlassesNumerator());
        }
        private IEnumerator CreateGlassesNumerator()
        {
            for (int i = 0; i < GlassesSystem.Instance.BrokenGlassesCount; i++)
            {
                GlassesSystem.Instance.CreateGlass(GetFreePoint(), i);
                yield return new WaitForEndOfFrame();
            }
        }

        private Transform GetFreePoint()
        {
            for(int i = 0; i < 4; i++)
            {
                if (pointsForGlasses[i].GetComponentInChildren<Glass>() == null)
                    return pointsForGlasses[i];
            }
            return null;
        }
    }
}