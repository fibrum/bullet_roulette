﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SaloonSpin
{
    public class WaitressAnimatorReceiver : MonoBehaviour
    {
        private void CreateGlasses()
        {
            if (Waitress.Instance != null)
                Waitress.Instance.CreateGlasses();
        }

        private void GiveGlasses()
        {
            if (Waitress.Instance != null)
                Waitress.Instance.GiveGlasses();
        }

        private void CheckGlasses()
        {
            if (Waitress.Instance != null)
                Waitress.Instance.CheckGlasses();
        }
    }
}
