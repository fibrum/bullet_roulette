﻿using System;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace SaloonSpin
{
    [System.Serializable]
    public class ObjectToInstantiate
    {
        public GameObject Prefab;
        private Transform InstantiatePoint;
    }

    [System.Serializable]
    public class InstantiateObjectsList
    {
        public PlayerType Player;
        public List<ObjectToInstantiate> objToInstantiate = new List<ObjectToInstantiate>();
    }

    public class NetworkManager : MonoBehaviourPunCallbacks
    {
        public GameObject lobbyPlayer;
        [SerializeField] private Transform pointToInstantiatePlayers;
        [SerializeField] private GameObject startButton;
        [HideInInspector] public PlayerController myPlayer; //контроллер своего игрока
        private List<NPC> npcList = new List<NPC>();//список npc в комнате
        public static NetworkManager Instance;
        public GameState gameState = new GameState(); //Состояние игры
        public bool inited { get; set; }//синициализован ли объект игрока
        public bool firsMasterLoadSync { get; set; } //была ли синхронизация после загрузки мастера
        [SerializeField]
        private List<GameObject> playerUIlist = new List<GameObject>();
        bool isConnecting; //подключен ли к фотону
        public GameObject playerPrefab; //префаб игрока
        public GameObject npcPrefab; //npc префаб
        public GameObject bulletPrefab, pistolPrefab; //префаб пули и пистолета
        public List<ObjectToInstantiate> sceneObjectsPrefabs = new List<ObjectToInstantiate>();

        public List<InstantiateObjectsList> objToInstantiate = new List<InstantiateObjectsList>();

        //public GameObject reloadCardPrefab; //префаб карты прокуртки
        public List<PlayerType> typesToLoad = new List<PlayerType>();
        public delegate void OnSyncGameState(GameState state);
        public event OnSyncGameState onSyncGameState; /*Событие синхронизации игры*/
        public UnityEvent OnTurnSwitched;
        public UnityEvent OnePlayerStayed;
        public bool gameRuns = false;

        #region UNITY
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }

            
        }
        #endregion

        [PunRPC]
        private void RPC_GameRuns(bool runs)
        {
            gameRuns = runs;
        }
        [PunRPC]
        private void RPC_SyncLoadTypesArray(int []arr)
        {
            typesToLoad.Clear();
            PlayerType[] t = new PlayerType[arr.Length];
            for(int i = 0; i < arr.Length; i++)
            {
                t[i] = (PlayerType)arr[i];
            }
            this.typesToLoad.AddRange(t);
        }
        public void ShakePlayerTypesList()
        {
            typesToLoad.Clear();
            System.Random rand = new System.Random();
            int[] arr = new int[6] { 0, 1, 2, 3, 4, 5 };
            for (int i = 0; i < 6; i++)
            {
                int j = rand.Next(i, 6);
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
            for (int i = 0; i < arr.Length; i++)
            {
                typesToLoad.Add((PlayerType)arr[i]);
            }

            photonView.RPC("RPC_SyncLoadTypesArray", RpcTarget.OthersBuffered, arr);
        }

        private string GetNPCname(PlayerType type)
        {
            switch (type)
            {                
                case PlayerType.Gentlman:
                    return "dr. Dreary";
                case PlayerType.Bandit:
                    return "Jerry Jails";
                case PlayerType.PoliceFemale:
                    return "Disaster Ann";
                case PlayerType.Dama:
                    return "Margot le Venin";
                case PlayerType.Ranger:
                    return "Joe Reed";
                case PlayerType.Indian:
                    return "Snorring Oak";
            }
            return "default";
        }
        public void PlayerLeftRoom(Player player, PlayerType leftPlayerType) {

            var leftPl=gameState.GetPlayerState(player.UserId);
            Debug.Log(leftPl.IsKilled+"  "+leftPl.money + "  " + leftPl.isNPC);
            var lifeCount=leftPl.lifeCount;
            var isKilled=leftPl.IsKilled;
            var money = leftPl.money;
            var canReload=leftPl.canReload;
            var plWhoShootMe=leftPl.playerWhoShotMe;

            gameState.DeleteUser(player.UserId);
            int botID = Random.Range(10000, 100000000);
            gameState.MakeNewUser(botID.ToString(), true);

            NPC npc = PhotonNetwork.InstantiateSceneObject(this.npcPrefab.name, pointToInstantiatePlayers.position, pointToInstantiatePlayers.rotation, 0).GetComponent<NPC>();
            npc.UserID = botID.ToString();
            npc.SetNpcName(GetNPCname(leftPlayerType), botID.ToString());
            npc.InitNPC(leftPlayerType);

            if(isKilled) npc.GetComponent<NPC>().HidePlayer(true);
            var botid=gameState.GetPos(botID.ToString());
            gameState.players[botid].lifeCount = lifeCount;
            gameState.players[botid].IsKilled = isKilled;
            gameState.players[botid].money = money;
            gameState.players[botid].canReload = canReload;
            gameState.players[botid].playerWhoShotMe = plWhoShootMe;
            Debug.Log(gameState.players[botid].IsKilled + "  " + gameState.players[botid].money + "  " + gameState.players[botid].isNPC);
            UpdateNPC_List();
            Debug.Log(gameState.players[botid].IsKilled + "  " + gameState.players[botid].money + "  " + gameState.players[botid].isNPC);

            npc.AcceptPosition(gameState.GetMyPos(npc.UserID), npc.UserID);
            Coins.Instance.GetCoinsRefs();
            Coins.Instance.RemoveCoin(player.UserId, -1);
            PlayingCardsManager.Instance.PV.RPC("GetPlayersRefs", RpcTarget.All);
            PlayingCardsManager.Instance.ChangeCardHolders(player.UserId, botID.ToString());
            PlayingCardsManager.Instance.PV.RPC("CardsEffectRefs", RpcTarget.MasterClient, player.UserId);
            photonView.RPC("RPC_SyncGameState", RpcTarget.All, ObjectToByteArray(gameState));
            SyncPistolStateRequest();
            Debug.Log(gameState.players[botid].IsKilled + "  " + gameState.players[botid].money+"  "+gameState.players[botid].isNPC);
        }

        public void MasterPlayerLeftRoom(Player player, PlayerType leftPlayerType)
        {
            gameState.DeleteUser(player.UserId);
            for (int i = 0; i < gameState.players.Length; i++)
            {
                if (gameState.players[i].isNPC)
                    gameState.DeleteUser(gameState.players[i].UserID);
            }
            foreach (var n in FindObjectsOfType<NPC>())
            {
                PhotonNetwork.Destroy(n.gameObject);
            }
            npcList.Clear();
            for (int i = 0; i < gameState.players.Length; i++)
            {
                if (!string.IsNullOrEmpty(gameState.players[i].UserID))
                    continue;

                int botID = Random.Range(10000, 100000000);
                gameState.MakeNewUser(botID.ToString(), true);
                NPC npc = PhotonNetwork.InstantiateSceneObject(this.npcPrefab.name, pointToInstantiatePlayers.position, pointToInstantiatePlayers.rotation, 0).GetComponent<NPC>();
                npc.UserID = botID.ToString();
                npc.GetComponent<NPC>().SetNpcName(GetNPCname(typesToLoad[i]), botID.ToString());
                npc.GetComponent<NPC>().InitNPC(typesToLoad[i]);
                npc.AcceptPosition(gameState.GetMyPos(botID.ToString()), npc.UserID);
                npcList.Add(npc);
            }

            Coins.Instance.RemoveCoin(player.UserId, -1);
            photonView.RPC("RPC_SyncGameState", RpcTarget.All, ObjectToByteArray(gameState));
        }

        public void LaunchGame()
        {
            Debug.Log("launch game");
            gameState.ClearUsersStates();
            TryDestroyNpc();


            npcList.Clear();
            inited = false;
            firsMasterLoadSync = false;
            StartCoroutine(Launch());
        }

        public void TryDestroyNpc() {//if it not destroyed
            if (npcList.Count > 0) {
                foreach (NPC npc in npcList) {
                    if (npc != null) {
                        try {
                            PhotonNetwork.Destroy(npc.gameObject);
                        } catch (Exception e) {
                        }

                        try {
                            if (npc != null)
                                Destroy(npc.gameObject);
                        } catch {
                        }
                    }
                }
            }

            foreach (var n in FindObjectsOfType<NPC>()) {
                try {
                    PhotonNetwork.Destroy(n.gameObject);
                } catch {
                }

                try {
                    if (n != null)
                        Destroy(n.gameObject);
                } catch {
                }
            }

            if (MainPistol.Instance != null) {
                try {
                    DestroyPistol();
                } catch (Exception e) {
                    
                }
                try
                {
                    if (MainPistol.Instance != null)
                        Destroy(MainPistol.Instance.gameObject);
                }
                catch
                {
                }
            }

            if (Bullet.Instance) {
                Bullet.Instance.DisableBullet(0);
            }
            PlayingCardsManager.DestroyCards(FindObjectsOfType<PlayingCard>());
            if (Waitress.Instance != null)
                PhotonNetwork.Destroy(Waitress.Instance.gameObject);
            if (Barmen.Instance != null)
                PhotonNetwork.Destroy(Barmen.Instance.gameObject);
            if (Pianist.Instance != null)
                PhotonNetwork.Destroy(Pianist.Instance.gameObject);
            foreach (var c in FindObjectsOfType<Cigarette>())
                PhotonNetwork.Destroy(c.gameObject);

            foreach (var g in FindObjectsOfType<Glass>())
                PhotonNetwork.Destroy(g.gameObject);

            foreach (var p in FindObjectsOfType<Pepelka>())
                PhotonNetwork.Destroy(p.gameObject);

        }

        private IEnumerator Launch()
        {
            Destroy(lobbyPlayer.gameObject);//.SetActive(false);
            yield return new WaitForEndOfFrame();
            GameStarts();
        }
        private IEnumerator CreateSceneObjects()
        {
            var pos = GameManager.Instance.interactObjects.Find((x) => x.objType.Equals(GrabbObject.Bullet)).objTransform;
            PhotonNetwork.InstantiateSceneObject(bulletPrefab.name, pos.position, pos.rotation);
            yield return new WaitForEndOfFrame();

            pos = GameManager.Instance.interactObjects.Find((x) => x.objType.Equals(GrabbObject.Pistol)).objTransform;
            PhotonNetwork.InstantiateSceneObject(pistolPrefab.name, pos.position, pos.rotation);
            yield return new WaitForEndOfFrame();

            for(int i = 0; i < 4; i++)
            {
                var _prefab = objToInstantiate.Find((obj) => obj.Player.Equals(typesToLoad[i]));
                for(int j = 0; j < _prefab.objToInstantiate.Count; j++)
                {
                    Quaternion _rotation = Quaternion.identity;
                    Vector3 _pos = Vector3.zero;

                    switch (_prefab.objToInstantiate[j].Prefab.GetComponent<GrabbaleObjects>().objectType)
                    {
                        case GrabbObject.Cigarette:
                            _pos = GameManager.Instance.playersPositions[i].cigaretteSpawnPoint.transform.position;
                            _rotation = GameManager.Instance.playersPositions[i].cigaretteSpawnPoint.transform.rotation;
                            var c = PhotonNetwork.InstantiateSceneObject(_prefab.objToInstantiate[j].Prefab.name, _pos, _rotation);
                            break;

                        //case GrabbObject.Glass:
                        //    _pos = GameManager.Instance.playersPositions[i].drinkSpawnPoint.transform.position;
                        //    _rotation = GameManager.Instance.playersPositions[i].drinkSpawnPoint.transform.rotation;
                        //    var g = PhotonNetwork.InstantiateSceneObject(_prefab.objToInstantiate[j].Prefab.name, _pos, _rotation);
                        //    g.GetComponent<PhotonView>().RPC("RPC_SetUpGlassPoint", RpcTarget.AllBuffered, i);
                        //    break;

                        //case GrabbObject.Pepelka:
                        //    _pos = GameManager.Instance.playersPositions[i].pepelnitsaSpawnPoint.transform.position;
                        //    _rotation = GameManager.Instance.playersPositions[i].pepelnitsaSpawnPoint.transform.rotation;
                        //    var p = PhotonNetwork.InstantiateSceneObject(_prefab.objToInstantiate[j].Prefab.name, _pos, _rotation);
                        //    break;                      
                    }

                    yield return new WaitForEndOfFrame();
                    yield return new WaitForEndOfFrame();
                    yield return new WaitForEndOfFrame();
                }
            }
            
            startButton.GetComponentInChildren<UnityEngine.UI.Text>().text = I2.Loc.ScriptLocalization.START;
            startButton.SetActive(true);
        }
        private void GameStarts()
        {
            myPlayer = PhotonNetwork.Instantiate(this.playerPrefab.name, pointToInstantiatePlayers.position, pointToInstantiatePlayers.rotation, 0).GetComponent<PlayerController>();
            myPlayer.GetComponent<PlayerController>().SetPlayerName(SteamManager.Initialized?SteamManager.GetCurrentUserName():PhotonNetwork.LocalPlayer.NickName, PhotonNetwork.LocalPlayer.UserId);

            if (!PhotonNetwork.IsMasterClient)
                StartCoroutine(NotMasterCreatePlayer());

            if (PhotonNetwork.IsMasterClient)
            {
                StartCoroutine(CreateSceneObjects());

                gameState.MakeNewUser(PhotonNetwork.LocalPlayer.UserId);
                gameState.userTurn = PhotonNetwork.LocalPlayer.UserId;

                var props = PhotonNetwork.MasterClient.CustomProperties;
                props["PlayerType"] = typesToLoad[gameState.GetPos(PhotonNetwork.MasterClient.UserId)];
                PhotonNetwork.MasterClient.SetCustomProperties(props);
                
                for (int i = gameState.playersCount; i < gameState.players.Length; i++)
                {
                    int botID = Random.Range(10000, 100000000);
                    gameState.MakeNewUser(botID.ToString(), true);
                    NPC npc = PhotonNetwork.InstantiateSceneObject(this.npcPrefab.name, pointToInstantiatePlayers.position, pointToInstantiatePlayers.rotation, 0).GetComponent<NPC>();
                    npc.UserID = botID.ToString();
                    npc.GetComponent<NPC>().SetNpcName(GetNPCname(typesToLoad[i]), botID.ToString());
                    npc.GetComponent<NPC>().InitNPC(typesToLoad[i]);
                    npcList.Add(npc);
                }

                var nextPlayer = PhotonNetwork.LocalPlayer.GetNext();

                if (nextPlayer == null)
                {
                    Debug.Log("next player is null");
                    photonView.RPC("FirstLoadSync", RpcTarget.AllBuffered);
                }
                else
                {
                    photonView.RPC("FirstLoadSync", nextPlayer);
                }
                firsMasterLoadSync = true;
                RPC_SyncGameState(ObjectToByteArray(gameState));
            }


        }
        private IEnumerator NotMasterCreatePlayer()
        {
            while(!this.firsMasterLoadSync)
            {
                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
            }

            photonView.RPC("RequestToSyncAfterLoad", RpcTarget.MasterClient);
        }
        private void UpdateNPC_List()
        {
            List<NPC> NPCs = new List<NPC>();
            NPCs.AddRange(FindObjectsOfType<NPC>());
            npcList.Clear();

            for (int i = 0; i < 4; i++)
            {
                var npc = NPCs.Find((n) => n.typeOfPlayer == typesToLoad[i]);

                if (npc != null)
                    npcList.Add(npc);
            }
        
            foreach(var n in npcList)
            {
                Debug.Log(n.typeOfPlayer);
            }
        }

        #region RPC methods
        [PunRPC]
        private void RequestToSyncAfterLoad(PhotonMessageInfo info)
        {
            if (npcList[0] != null) //если к комнате присоеденился новый игрок, удаляем бота и садим игрока на его место
            {
                var npcToRemove = npcList[0];
                npcList.Remove(npcToRemove);
                gameState.DeleteUser(npcToRemove.UserID);
                PhotonNetwork.Destroy(npcToRemove.gameObject);
            }
            gameState.MakeNewUser(info.Sender.UserId);

            var props = info.Sender.CustomProperties;
            props["PlayerType"] = typesToLoad[gameState.GetPos(info.Sender.UserId)];
            info.Sender.SetCustomProperties(props);

            photonView.RPC("RPC_SyncGameState", RpcTarget.All, ObjectToByteArray(gameState));

            var nextPlayer = info.Sender.GetNext();
            if (nextPlayer == null)
            {
                Debug.Log("next player is null");
                photonView.RPC("FirstLoadSync", RpcTarget.AllBuffered);
            }
            else
            {
                photonView.RPC("FirstLoadSync", nextPlayer);
            }            
        }
        [PunRPC]
        private void FirstLoadSync()
        {
            firsMasterLoadSync = true;
        }
        [PunRPC]
        public void RPC_SyncGameState(byte[] arr) /*Синхронизация игрового состояния*/
        {
            gameState = (GameState)ByteArrayToObject(arr);

            foreach (var npc in npcList)
                npc.AcceptPosition(gameState.GetMyPos(npc.UserID), npc.UserID);

            if (!firsMasterLoadSync)
                return;

            if (!inited)
            {
                inited = true;

                myPlayer.AcceptPosition(gameState.GetMyPos(), PhotonNetwork.LocalPlayer.UserId); /*Сажает игрока на его место*/
                myPlayer.InitPlayer(typesToLoad[gameState.GetMyPos()]);

            }

            //firsMasterLoadSync = true;

            List<AbstractPlayer> playersList = new List<AbstractPlayer>();
            playersList.AddRange(FindObjectsOfType<AbstractPlayer>());
            int killedUsers = 0;
            //Если игрок мертв, то нужно его скрыть
            for (int i = 0; i < 4; i++)
            {
                if (gameState.players[i].IsKilled)
                {
                    if (PhotonNetwork.IsMasterClient) //снимает эффекты от карт с убитого игрока
                        PlayingCardsManager.Instance.CheckKilledUser(gameState.players[i].UserID);

                    playersList.Find(p => p.UserID == gameState.players[i].UserID).HidePlayer();
                    gameState.players[i].money = 0;
                    killedUsers++;

                    if (gameState.userTurn == gameState.players[i].UserID) //убитый игрок пропускает очередь стрелять
                    {
                        gameState.SwitchTurnToNextUser();
                    }
                }
            }

            if (gameState.playersCount - killedUsers > 1 && gameRuns)
                OnTurnSwitched?.Invoke();
            else if(gameState.playersCount - killedUsers <= 1)
            {
                OnePlayerStayed?.Invoke();
                gameRuns = false;
            }

            Debug.Log(gameState.ToString());
            //Если был выстрел, то нужно перезарядить пистолет
            if (PhotonNetwork.IsMasterClient)
            {
                if (gameState.currIndex >= gameState.bulletIndex)
                {
                    //string msg = string.Format("<color=green>reload from sync request-> gameState.currIndex = {0},  gameState.bulletIndex = {1}</color>", gameState.currIndex, gameState.bulletIndex);
                    //Debug.Log(msg);                    
                    //Pistol.Instance.Reload();
                    //MainPistol.Instance.GetComponent<PhotonView>().RPC("RPC_PistolUncharged", RpcTarget.All);
                    //photonView.RPC("RPC_SyncGameState", RpcTarget.All, ObjectToByteArray(gameState));                    
                    Invoke("PreparePistolToCharge", 1f);                   
                }

                if(gameState.playersCount - killedUsers > 1)// если пистолет заряжен и за столом больше одного живого игрока, проверка возможно очередь стрелять - у нпс
                {
                    var npcPlayer = playersList.Find(p => p.UserID == gameState.userTurn );
                    
                    if (npcPlayer is NPC)
                    {
                        NPC npc = (NPC)npcPlayer;
                        npc.NPC_TurnToShoot();
                        gameState.npcTurnCalled = true;
                    }
                }
            }

            onSyncGameState?.Invoke(gameState);
            if(killedUsers >= 3)
            {
                gameState.userTurn = string.Empty;
                Debug.Log("SET TURN TO EMPTY");
            }
        }
        
        [PunRPC]
        public void SyncPistolState()
        {
            MainPistol.Instance.PhotonView.RPC("SyncPistolState", RpcTarget.All, ObjectToByteArray(gameState));
        }
        [PunRPC]
        public void RPC_AcceptSyncRequest(byte[] arr) /*Запрос любого игрока к мастер клиенту на синхронизацию данных*/
        {
            if (PhotonNetwork.IsMasterClient)
            {
                gameState = (GameState)ByteArrayToObject(arr);
                photonView.RPC("RPC_SyncGameState", RpcTarget.All, ObjectToByteArray(gameState));
            }
        }
        [PunRPC]
        public void RPC_ResetAllUsers() //включить отбражение мешей всех игроков, вернуть карту, обнулить монеты
        {

            foreach (AbstractPlayer p in FindObjectsOfType<AbstractPlayer>())
            {
                p.ShowPlayer();
                p.RetunCardOnTable();
                p.RemoveCoin(-1);

                if (p is NPC)
                {                
                    p.StopAllCoroutines();
                    NPC npc = (NPC)p;
                    npc.ResetAnimatorParameters();
                }
            }
        }
        [PunRPC]
        private void EnblePlayerUI()
        {
            gameRuns = true;
            foreach (var go in playerUIlist)
                go.SetActive(true);
        }

        [PunRPC]
        private void RPC_MovePistolOnTable()
        {
            MainPistol.Instance.PistolMoving.PutPistolOnTableCenter();
        }
        [PunRPC]
        private void RPC_ResetNPC()
        {
            var players = FindObjectsOfType<AbstractPlayer>();
            foreach (var player in players)
            {
                if (player.UserID == gameState.userTurn)
                {
                    if (player is NPC)
                    {
                        var npc = (NPC)player;
                        npc.StopAllCoroutines();
                        var go = npc.GetComponentInChildren<Animator>().gameObject;
                        go.SetActive(false);
                        go.gameObject.SetActive(true);
                        npc.GetComponentInChildren<Animator>().SetBool("Idle", true);
                    }
                }
            }
        }
        #endregion

        private byte[] ObjectToByteArray(object obj) /*Сериализация объекта в массив байт*/
        {
            if (obj == null)
                return null;

            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);

            return ms.ToArray();
        }

        public object ByteArrayToObject(byte[] arrBytes) /*Десериализация массива байт в объект*/
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            object obj = (object)binForm.Deserialize(memStream);

            return obj;
        }

        public void StartGame() {
            ResetAllUsers();
            MainPistol.Instance.PhotonView.RPC("RPC_DropPistol", RpcTarget.All);
            MainPistol.Instance.CloseBarrel();
            PlayingCardsManager.Instance.DealCards();
            // PhotonNetwork.CurrentRoom.IsVisible = false;
            StartCoroutine(DelayStartGame());
            GameManager.Instance.startButton.SetActive(false);
        }

        [PunRPC]
        private void WaitingStartGame() {
            GameManager.Instance.WaitForRestart();
        }

        [PunRPC]
        private void DisableWinPanel()
        {
            GameManager.Instance.winPanel.gameObject.SetActive(false);
            int pos = gameState.GetMyPos(PhotonNetwork.LocalPlayer.UserId);
            GameManager.Instance.playersPositions[pos].moneyText.text = string.Empty;
        }
        private void CreateLobbyNPC()
        {
            if (Waitress.Instance == null)
                Lobby.Instance.CreateWaitress();
            if (Barmen.Instance == null)
                Lobby.Instance.CreateBarmen();
            if (Pianist.Instance == null)
                Lobby.Instance.CreatePianist();
        }
        private string prevUserFirstTurn;
        private IEnumerator DelayStartGame()
        {
            photonView.RPC("WaitingStartGame", RpcTarget.All);
            photonView.RPC("DisableWinPanel", RpcTarget.All);
            photonView.RPC("EnblePlayerUI", RpcTarget.All);
            bool firstGamePlayed = (bool)PhotonNetwork.CurrentRoom.CustomProperties[RoomPropertiesKey.FirstGamePlayed];
            Debug.Log("first game played? " + firstGamePlayed);
            startButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick.RemoveAllListeners();
            yield return new WaitForEndOfFrame();
            ResetAllUsers();
            yield return new WaitForSeconds(1f);
            CreateLobbyNPC();
            yield return new WaitForSeconds(3f);
            Debug.Log("<color=red>remove listeners</color>");

            if (firstGamePlayed) {
                //gameState.userTurn = PhotonNetwork.MasterClient.UserId;
                gameState.MakeRandomTurn();
                while (gameState.userTurn == prevUserFirstTurn) {
                    gameState.MakeRandomTurn();
                }
                prevUserFirstTurn = gameState.userTurn;
            }
            else
            {
                gameState.userTurn = PhotonNetwork.MasterClient.UserId;
                prevUserFirstTurn = PhotonNetwork.MasterClient.UserId;
                var props = PhotonNetwork.CurrentRoom.CustomProperties;
                props[RoomPropertiesKey.FirstGamePlayed] = true;
                PhotonNetwork.CurrentRoom.SetCustomProperties(props);
            }

            NetworkManager.Instance.gameState.currIndex = 10;
            //yield return StartCoroutine(ReCreatePistol());
            yield return new WaitForSeconds(.1f);
            MainPistol.Instance.PistolMoving.PutPistolOnTableCenter();
            OnTurnSwitched?.Invoke();
            RequestToSync();
            SyncPistolState();
            Bullet.Instance.UpdateBulletPosition();
            yield return new WaitForSeconds(0.25f);
            List<AbstractPlayer> playersList = new List<AbstractPlayer>();
            playersList.AddRange(FindObjectsOfType<AbstractPlayer>());
            var npcPlayer = playersList.Find(p => p.UserID == gameState.userTurn );
            Debug.Log($"[NetworKManager]DelayStartGame: {npcPlayer} {gameState.ToString()} {playersList.Count}" );
            if (npcPlayer is NPC)
            {
                NPC npc = (NPC)npcPlayer;
                npc.NPC_TurnToShoot();
                gameState.npcTurnCalled = true;
            }
        }
        
        private IEnumerator ReCreatePistol()
        {
            photonView.RPC("DestroyPistol", RpcTarget.All);
            yield return new WaitForEndOfFrame();
            var pos = GameManager.Instance.interactObjects.Find((x) => x.objType.Equals(GrabbObject.Pistol)).objTransform;
            PhotonNetwork.InstantiateSceneObject(pistolPrefab.name, pos.position, pos.rotation);
        }
        [PunRPC]
        private void DestroyPistol()
        {
            PhotonNetwork.Destroy(MainPistol.Instance.gameObject);
        }
        public void PreparePistolToCharge()
        {
            //Pistol.Instance.GetComponent<PhotonView>().RPC("RPC_PrepareToCharge", RpcTarget.All);
            //Bullet.Instance.GetComponent<PhotonView>().RPC("RPC_EnableBullet", RpcTarget.All);
        }
        public void Connect() /*Подключение к серверу*/
        {
            PhotonNetwork.AutomaticallySyncScene = true;
            isConnecting = true;

            if (PhotonNetwork.IsConnected)
            {
                PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                PhotonNetwork.ConnectUsingSettings();
            }
        }

        public void ResetAllUsers() /*Востановление исходного состояния всех игроков*/
        {
            //Pistol.Instance.Reload();
            
            gameState.ResetState();
            photonView.RPC("RPC_ResetAllUsers", RpcTarget.All);
        }

        public void RequestToSync() /*посылает запрос масетр клиенту на синхронизацию*/
        {
            photonView.RPC("RPC_AcceptSyncRequest", RpcTarget.MasterClient, ObjectToByteArray(gameState));
        }

        public void SyncPistolStateRequest() {
            photonView.RPC("SyncPistolState", RpcTarget.MasterClient);
        }

        public bool NPC_turnToShoot
        {
            get
            {
                return gameState.GetPlayerState(gameState.userTurn).isNPC;
            }
        }
    }
}
