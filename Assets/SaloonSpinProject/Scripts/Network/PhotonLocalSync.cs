﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    [RequireComponent(typeof(PhotonView))]
    public class PhotonLocalSync : MonoBehaviour, IPunObservable
    {
        public bool SyncEnabled = false;
        private PhotonView photonView;

        private Vector3 networkLocalPosition, direction, storedPosition;
        private Quaternion networkLocalRotation;
        private float distance;
        private float angle;

        private void Awake()
        {
            photonView = GetComponent<PhotonView>();
        }
        private void Start()
        {
            this.networkLocalRotation = Quaternion.identity;
            this.networkLocalPosition = Vector3.zero;
            this.storedPosition = transform.localPosition;
        }
        private void Update()
        {
            if (!photonView.IsMine)
            {
                if (SyncEnabled)
                {
                    transform.localPosition = Vector3.MoveTowards(transform.localPosition, this.networkLocalPosition, this.distance * (1.0f / PhotonNetwork.SerializationRate));
                    transform.localRotation = Quaternion.RotateTowards(transform.localRotation, this.networkLocalRotation, this.angle * (1.0f / PhotonNetwork.SerializationRate));
                }
            }
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if(stream.IsWriting)
            {
                if (SyncEnabled)
                {
                    this.direction = transform.localPosition - storedPosition;
                    this.storedPosition = transform.localPosition;

                    stream.SendNext(transform.localPosition);
                    stream.SendNext(this.direction);

                    stream.SendNext(transform.localRotation);
                }
            }
            else
            {
                if(SyncEnabled)
                {
                    this.networkLocalPosition = (Vector3)stream.ReceiveNext();
                    this.direction = (Vector3)stream.ReceiveNext();

                    float lag = Mathf.Abs((float)(PhotonNetwork.Time - info.SentServerTime));
                    this.networkLocalPosition += this.direction * lag;
                    this.distance = Vector3.Distance(transform.localPosition, this.networkLocalPosition);

                    this.networkLocalRotation = (Quaternion)stream.ReceiveNext();
                    this.angle = Quaternion.Angle(transform.localRotation, this.networkLocalRotation);
                }
            }
        }



    }
}
