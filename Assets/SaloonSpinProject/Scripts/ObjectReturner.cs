﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SaloonSpin
{
    public class ObjectReturner : MonoBehaviour
    {
        [SerializeField]
        private Transform tableCenter;


        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("ReloadCard"))
            {
                other.GetComponentInChildren<ReloadCard>().ResetCardPostion();
            }
            if (other.CompareTag("Bullet"))
            {
                Bullet.Instance.EnableBullet();
            }
            if(other.CompareTag("Cigarette"))
            {
                if (other.GetComponent<Cigarette>() != null)
                {
                    other.GetComponent<Cigarette>().PutCigaretteOnTable();
                    return;
                }
                if (other.GetComponentInParent<Cigarette>() != null)
                {
                    other.GetComponentInParent<Cigarette>().PutCigaretteOnTable();
                    return;
                }
            }
            if(other.CompareTag("Pepelnitsa"))
            {
                other.GetComponentInParent<Pepelka>().PutPeplkaOnTable();
            }
            if(other.CompareTag("MainPistol"))
            {
                MainPistol.Instance.PistolMoving.PutPistolOnTableCenter();
            }
            if(other.CompareTag("Glass"))
            {                
                Glass glass = other.transform.GetComponent<Glass>();
                if (glass.WaitressCarry)
                    return;

                GlassesSystem.Instance.GlassCrashed(glass.glassType, glass.GlassPointIndex);
                glass.DisableGlass();
            }
        }
    }
}
