﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class Pepelka : MonoBehaviour
    {
        public static Pepelka Instance;
        public int pepelnitsaID = 1;
        private PhotonView photonView;
        new private Rigidbody rigidbody;
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            rigidbody = GetComponent<Rigidbody>();
            photonView = GetComponent<PhotonView>();
            pepelnitsaID = photonView.ViewID;
        }
        public void PutPeplkaOnTable()
        {
            GetComponent<GrabbaleObjects>().StopGrab(false);

            this.gameObject.transform.GetChild(0).localEulerAngles = new Vector3(-60, 0f, 0f);
            Vector3 fromTo = GameManager.Instance.tableCenter.position - this.gameObject.transform.position;
            Vector3 fromToXZ = new Vector3(fromTo.x, 0f, fromTo.z);

            this.gameObject.transform.rotation = Quaternion.LookRotation(fromToXZ, Vector3.up);
            float g = Physics.gravity.y;
            float x = fromToXZ.magnitude;
            float y = fromTo.y;

            float AngleInRadians = 60 * Mathf.PI / 180;

            float v2 = (g * x * x) / (2 * (y - Mathf.Tan(AngleInRadians) * x) * Mathf.Pow(Mathf.Cos(AngleInRadians), 2));
            float v = Mathf.Sqrt(Mathf.Abs(v2));

            if (photonView.IsMine)
                photonView.RPC("RPC_PutPepelkaOnTable", RpcTarget.All, gameObject.transform.GetChild(0).forward, v);


        }
        public void SetUser(Player player)
        {
            photonView.TransferOwnership(player);
            photonView.OwnershipWasTransfered = false;
        }

        [PunRPC]
        private void RPC_PutPepelkaOnTable(Vector3 dir, float speed)
        {
            rigidbody.isKinematic = false;
            rigidbody.velocity = dir * speed;
        }
    }
}
