﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class AFK_Pistol : MonoBehaviour
    {
        [SerializeField] private ParticleSystem shotVFX;
        [SerializeField] private AudioClip shotSFX, dryShotSFX;
        [SerializeField] private Transform bulletSpawnPoint;
       
        private AudioSource audioSource;
        private PhotonView photonView;
        private VRTK.VRTK_InteractableObject interactableObject;
        private Animator animator;
        private int ShotIndex = Animator.StringToHash("PistolMakeShot");
        private bool canShoot = true;
        private void Awake()
        {
            photonView = GetComponent<PhotonView>();
            interactableObject = GetComponent<VRTK.VRTK_InteractableObject>();
            audioSource = GetComponent<AudioSource>();
            animator = GetComponent<Animator>();
        }
        private void Start()
        {
            interactableObject.InteractableObjectUsed += MakeShot;
            audioSource.clip = shotSFX;
        }
        private void OnEnable()
        {
            GetComponent<Rigidbody>().isKinematic = false;
        }
        private void OnDisable() {
            turnOffOutline();
            canShoot = true;
            returning = false;
        }

        #region Moving variables
        Coroutine movingNumerator;
        float startTime;
        float journeyLength;
        public bool returning;
        float speed = 3f;
        Vector3 startMarker, endMarker;
        Quaternion endRotation;
        #endregion

        private void Update()
        {
            if (photonView.IsMine&&interactableObject.IsGrabbed()) {
                RayCast();
            } else {
                turnOffOutline();
            }

            if (returning)
            {
                float distCovered = (Time.time - startTime) * speed;

                float fracJourney = distCovered / journeyLength;

                transform.position = Vector3.Lerp(startMarker, endMarker, fracJourney);

                if (Vector3.Distance(transform.position, endMarker) < 0.005f)
                {
                    returning = false;
                    transform.position = endMarker;
                    transform.rotation = endRotation;
                    //GetComponent<Rigidbody>().isKinematic = false;
                    gameObject.SetActive(false);
                }
            }
        }

        private void turnOffOutline() {
            if (lastAimedPlayer != null) {
                lastAimedPlayer.HidePlayerInfo();
                lastAimedPlayer = null;
            }
        }

        

        private AbstractPlayer lastAimedPlayer;

        private void RayCast() {
            int layerMask = 1 << LayerMask.NameToLayer("Head");
            Ray ray = new Ray(bulletSpawnPoint.transform.position, bulletSpawnPoint.transform.forward * 3f);
            if (Physics.Raycast(ray, out var hit, 30f, layerMask)) {
                if (hit.transform.CompareTag("Head")) {
                    var aim=hit.transform.GetComponentInParent<AbstractPlayer>();
                    if (aim != null) {
                        if (aim != lastAimedPlayer) {
                            turnOffOutline();
                            lastAimedPlayer =aim;
                            lastAimedPlayer.ShowPlayerInfo(transform.GetComponentInParent<PlayerController>().PlayerCameraTransform);
                            return;

                        } else {
                            return;
                        }
                    }
                }
            }
            turnOffOutline();
        }

        public void ReturnOnTable(Vector3 returnPosition, Quaternion rotation)
        {
            startTime = Time.time;
            startMarker = transform.position;
            endMarker = returnPosition;
            journeyLength = Vector3.Distance(startMarker, endMarker);
            endRotation = rotation;
            GetComponent<Rigidbody>().isKinematic = true;
            returning = true;
        }
        public void StopGrab()
        {
            GetComponent<GrabbaleObjects>().StopGrab(false);
        }
        public void SetUser(Player player)
        {
            photonView.TransferOwnership(player);
        }

        private void MakeShot(object sender, VRTK.InteractableObjectEventArgs e)
        {
            if(photonView.IsMine && canShoot)
            {
                int layerMask = 1 << LayerMask.NameToLayer("Head");
                RaycastHit hit;
                Ray ray = new Ray(bulletSpawnPoint.position, bulletSpawnPoint.forward * 10);

                if (Physics.Raycast(ray, out hit, 10f, layerMask))
                {
                    if(hit.transform.CompareTag("Head"))
                    {
                        if(hit.transform.root.GetComponent<AbstractPlayer>().UserID == 
                            NetworkManager.Instance.gameState.userTurn)
                        {
                            NetworkManager.Instance.gameState.GetPlayerState(hit.transform.GetComponentInParent<AbstractPlayer>().UserID).IsKilled = true;
                            NetworkManager.Instance.gameState.GetPlayerState(hit.transform.GetComponentInParent<AbstractPlayer>().UserID).IsKilled = true;
                            photonView.RPC("RPC_ShotVFX", RpcTarget.All, false);
                            photonView.RPC("RPC_ShotSFX", RpcTarget.All, false);
                            StartCoroutine(DelayRequestToSync());
                            MainPistol.Instance.PhotonView.RPC("MovePistolDelayed", RpcTarget.All, 1f);
                            
                            //Bullet.Instance.UpdateBulletPosition();
                            //NetworkManager.Instance.RequestToSync();
                            NetworkManager.Instance.GetComponent<PhotonView>().RPC("SyncPistolState", RpcTarget.MasterClient);
                        }
                        else
                        {
                            photonView.RPC("RPC_ShotVFX", RpcTarget.All, true);
                            photonView.RPC("RPC_ShotSFX", RpcTarget.All, true);
                        }
                    }
                }
                else
                {
                    photonView.RPC("RPC_ShotVFX", RpcTarget.All, true);
                    photonView.RPC("RPC_ShotSFX", RpcTarget.All, true);
                }


                StartCoroutine(ShootingDelay());
            }
        }
        private IEnumerator DelayRequestToSync()
        {
            yield return new WaitForSeconds(0.25f);
            //MainPistol.Instance.GetComponent<MainPistolMoving>().PutPistolOnTableCenter();
            Bullet.Instance.UpdateBulletPosition();
            NetworkManager.Instance.RequestToSync();
            NetworkManager.Instance.GetComponent<PhotonView>().RPC("SyncPistolState", RpcTarget.MasterClient);
        }

        private IEnumerator ShootingDelay()
        {
            canShoot = false;
            yield return new WaitForSeconds(1f);
            canShoot = true;
        }

        [PunRPC]
        private void RPC_ShotSFX(bool dryShot)
        {
            if(dryShot)
            {
                audioSource.clip = dryShotSFX;
            }
            else
            {
                audioSource.clip = shotSFX;
            }
            audioSource.Play();
        }

        [PunRPC]
        private void RPC_ShotVFX(bool dryShot)
        {
            animator.SetTrigger(ShotIndex);

            if(!dryShot)
                shotVFX.Play();
        }
    }
}
