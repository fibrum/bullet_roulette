﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
namespace SaloonSpin
{
    public class Bullet : MonoBehaviour
    {
        public static Bullet Instance;
        public int BulletDroppedIndex { get; private set; }
        private DropCollisionEvent collisionEvent;
        private Transform bulletHole = null;
        private PhotonView photonView;
        new private Rigidbody rigidbody;
        private Coroutine enabling, disabling;
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            collisionEvent = GetComponent<DropCollisionEvent>();
            photonView = GetComponent<PhotonView>();
            rigidbody = GetComponent<Rigidbody>();
            intaractable = GetComponent<VRTK_InteractableObject>();
        }

        public VRTK_InteractableObject intaractable;
        private void Start()
        {
            NetworkManager.Instance.onSyncGameState += InteractableSetter;
            BulletDroppedIndex = -1;
        }
        private void OnDisable()
        {
            NetworkManager.Instance.onSyncGameState -= InteractableSetter;
        }
        public void InteractableSetter(GameState gameState) {
            StopAllCoroutines();
            StartCoroutine(InteractableSetterNumerator(gameState));
        }
        private IEnumerator InteractableSetterNumerator(GameState gameState)
        {
            yield return new WaitForSeconds(2f);
            string userTurn = gameState.userTurn;

            if (gameState.GetPlayerState(userTurn).isNPC)
            {
                intaractable.Unhighlight();
                intaractable.enabled = false;
                yield break;
            }

            string myID = PhotonNetwork.LocalPlayer.UserId;
            intaractable.enabled = myID == userTurn;
            if(myID == userTurn)
                ReturnerBullet.instance.TurnColliderOn();

        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("BulletHole") && photonView.IsMine && intaractable.IsGrabbed())
            {
                if (MainPistol.Instance.IsBarrelOpened || MainPistol.Instance.IsBarellOpenedByJoint)
                {
                    bulletHole = other.transform;
                    GetComponent<GrabbaleObjects>().StopGrab(false);
                    bulletHole.GetComponent<BulletPistolHole>().ShowBullet();
                    photonView.RPC("RPC_DisableBullet", RpcTarget.All, 0f);
                }
                //DisableBullet();
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("BulletHole") && photonView.IsMine)
            {
                if (bulletHole == other.transform)
                    bulletHole = null;
            }
        }

        public void SetUser(Player player)
        {            
            photonView.TransferOwnership(player);
        }
        public bool IsAttachToHole()
        {
            return bulletHole != null;
        }

        public void UpdateBulletPosition()
        {
            if(GetComponentInChildren<MeshRenderer>().enabled)
                photonView.RPC("RPC_EnableBullet", RpcTarget.All);
        }

        public void EnableBullet()
        {
            if (enabling != null)
                StopCoroutine(enabling);

            enabling = StartCoroutine(DelayEnabling(.1f, false));
            //RPC_EnableBullet();
        }
        public void DisableBullet(float time = .5f)
        {
            photonView.RPC("RPC_DisableBullet", RpcTarget.All, time);
        }
        [PunRPC]
        private void RPC_EnableBullet()
        {
            if (MainPistol.Instance.IsGunCharged) {
                if (GetComponentInChildren<MeshRenderer>().enabled) {
                    Debug.Log("<color=yellow>[Bullet]RPC_EnableBullet: bullet enables but gun is charged-> disabling bullet</color>");
                    collisionEvent.IgnoreCollision = true;
                    if (transform.parent != null)
                        transform.SetParent(null);

                    bulletHole = null;
                    GetComponentInChildren<MeshRenderer>().enabled = false;
                    transform.position = new Vector3(-0.475f, 1.5f, 2f);
                    rigidbody.isKinematic = true;
                }
                return;
            }
            Debug.Log("<color=yellow>[Bullet]RPC_EnableBullet: bullet enables</color>");
            if (enabling != null)
                StopCoroutine(enabling);

            enabling = StartCoroutine(DelayEnabling(.1f));
        }
        [PunRPC]
        private void RPC_DisableBullet(float time)
        {
            if (disabling != null)
                StopCoroutine(disabling);

            disabling = StartCoroutine(DelayDisabling(time));
        }

        private IEnumerator DelayEnabling(float time, bool checkPoint = true)
        {
            if (GameManager.Instance.AlivePlayerCount(NetworkManager.Instance.gameState) <= 1)
            {
                BulletDroppedIndex = -1;
                yield break;
            }


            int index = NetworkManager.Instance.gameState.GetPos(NetworkManager.Instance.gameState.userTurn);
            if (checkPoint)
            {
                if (BulletDroppedIndex == index && GetComponentInChildren<MeshRenderer>().enabled)
                    yield break;
            }

            collisionEvent.IgnoreCollision = false;
            GetComponent<DropCollisionEvent>().enabled = true;
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
            rigidbody.isKinematic = false;
            yield return new WaitForSeconds(time);
            GetComponentInChildren<MeshRenderer>().enabled = true;
            BulletDroppedIndex = index;
            var bulletStartPosition = GameManager.Instance.playersPositions[index].moneyText.transform.position;
            bulletStartPosition.y = 1.1f;
            transform.position = bulletStartPosition;
            transform.rotation= Quaternion.identity;
            ReturnerBullet.instance.TurnColliderOn();
        }

        private IEnumerator DelayDisabling(float time)
        {
            ReturnerBullet.instance.TurnColliderOff();
            yield return new WaitForSeconds(time);

            collisionEvent.IgnoreCollision = true;
            if (transform.parent != null)
                transform.SetParent(null);

            bulletHole = null;
            GetComponentInChildren<MeshRenderer>().enabled = false;
            transform.position = new Vector3(-0.475f, 1.5f, 2f);
            rigidbody.isKinematic = true;
        }

        private void OnDestroy()
        {
            Debug.Log("Destroy bullet");
        }

    }
}
