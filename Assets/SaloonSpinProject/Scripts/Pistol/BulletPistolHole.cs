﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class BulletPistolHole : MonoBehaviour
    {
        public void ShowBullet()
        {
            string ut = NetworkManager.Instance.gameState.userTurn;
            int i = NetworkManager.Instance.gameState.GetPos(ut);

            //if(!NetworkManager.Instance.gameState.players[i].isNPC)
            //    MainPistol.Instance.ChargePistol(transform.localPosition);
            MainPistol.Instance.ChargePistol(transform.localPosition);

        }

    }
}