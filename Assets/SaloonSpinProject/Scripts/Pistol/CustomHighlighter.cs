﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class CustomHighlighter : VRTK.Highlighters.VRTK_BaseHighlighter {
        public bool debug;
        [SerializeField] private OutlineController outlineController;
        public override void Highlight(Color? color = null, float duration = 0)
        {
            if(debug) Debug.Log("<color=green>[CustomHighlighter]Highlight</color>");
            outlineController.SetOutline(true);
        }

        public override void Initialise(Color? color = null, GameObject affectObject = null, Dictionary<string, object> options = null)
        {
            outlineController.SetOutline(false);
        }

        public override void ResetHighlighter()
        {
            if (debug) Debug.Log("<color=green>[CustomHighlighter]ResetHighlighter</color>");
            outlineController.SetOutline(false);
        }

        public override void Unhighlight(Color? color = null, float duration = 0)
        {
            if (debug) Debug.Log("<color=green>[CustomHighlighter]Unhighlight</color>");
            outlineController.SetOutline(false);
        }
    }
}
