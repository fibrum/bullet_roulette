﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class HintsPosition : MonoBehaviour
    {
        [SerializeField] private RectTransform hints;
        private float localY;
        private void Start()
        {
            localY = hints.localPosition.y;
        }
        private void Update()
        {
            hints.position = transform.position;
        }
    }
}
