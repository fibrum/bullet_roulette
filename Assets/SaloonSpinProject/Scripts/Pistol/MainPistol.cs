﻿using System;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using VRTK;
using Random = UnityEngine.Random;

namespace SaloonSpin
{
    public class MainPistol : MonoBehaviour
    {
        public static MainPistol Instance;
        public Transform bulletSpawnPoint; //отсюда летят рейкасты
        [SerializeField] private GameObject localBullet; //локальная пуля в пистолете, которая задвигается в барабн
        [SerializeField] private PistolSoundPool pistolSoundPool; //все звуки
        [SerializeField] private ParticleSystem shotVFX; //партиклы выстрела
        [SerializeField] private Rigidbody rootGunMechanism; //джоинт барабана пистолета
        [SerializeField] private Transform realBarrel;
        public Rigidbody RigidbodyGunMechanism {
            set { rootGunMechanism = value; }
        }
        [SerializeField] private TweenRotation tweenRotationCilinder;
        [SerializeField] private PistolBarrelMechanism barrelMechanism;
        [SerializeField] private MainPistolMoving pistolMoving;
        public MainPistolMoving PistolMoving => pistolMoving;
        public UnityEvent OnShot, OpenBarrelEvent, CloseBarrelEvent;

        private Vector3 defaultBarrelPosition;
        private ShootOnMesh shootOnMesh;
        private PhotonView photonView;
        public PhotonView PhotonView => photonView;
        [HideInInspector] public AbstractPlayer pistolHolder = null;
        private Coroutine waitForSpinPistol; //через 5 сек автоматически закрывается барабан (если открыт) и прокручивается пистолет, если игрок не сделал этого сам
        private Vector3 pointDropFromPosition = Vector3.zero; //позиция откуда падает пистолет в случае самоубийства нпс
        private Quaternion pointDropFromRotation = Quaternion.identity;//поворот в позиции откуда падает пистолет в случае самоубийства нпс

        private bool isBarrelOpened;
        private bool canHandSpin;
        private bool canOpenBarrel;
        private bool gunSpinned;
        private bool isGunCharged;

        public Text RechargingHintText;
        public bool IsBarellOpenedByJoint
        {
            get
            {
                return 180f < realBarrel.transform.localEulerAngles.x && realBarrel.transform.localEulerAngles.x < 300f;
            }
        }
        public bool IsAutoCloseAndSpinRunning { get { return waitForSpinPistol == null; } }
        
        public bool IsBarrelOpened {
            get { return isBarrelOpened; }
            private set {
                isBarrelOpened = value;
                if (!value)
                {
                    gunSpinned = false;
                    Debug.Log("<color=red>[MainPistol]gunspinned set false#1</color>");
                }
            }
        } //открыт ли барабан пистолета
        public bool CanHandSpin
        {
            get { return canHandSpin; }
            private set {
                canHandSpin = value;
                if (!value)
                {
                    gunSpinned = false;
                    Debug.Log("<color=red>[MainPistol]gunspinned set false#2</color>");
                }
            }
        } //доступна ли воможность прокрутить барабан пистолета
        public bool CanOpenBarrel
        {
            get { return canOpenBarrel; }
            private set {
                canOpenBarrel = value;                
                if (value)
                {
                    gunSpinned = false;
                    Debug.Log("<color=red>[MainPistol]gunspinned set false#3</color>");
                }
            }
        } //запрещает/разрешает открывать или закрывать барабан пистолета
        public bool GunSpinned
        {
            get { return gunSpinned; }
            private set {
                gunSpinned = value;
                Debug.Log("<color=red>[MainPistol]gunspinned set false#4</color>");
                if (value)
                {
                    canOpenBarrel = false;                    
                    canHandSpin = false;
                    isBarrelOpened = false;
                }
            }
        } //барабан пистолета прокручен после зарядки пистолета пулей либо использования карты прокрутки
        public bool IsGunCharged {
            get { return isGunCharged; }
            private set
            {
                isGunCharged = value;

                canHandSpin = value;
                if (!value)
                {
                    canOpenBarrel = true;
                    gunSpinned = false;
                    Debug.Log("<color=red>[MainPistol]gunspinned set false#5</color>");
                }
            }
        }//заряжена ли в пистолет пуля
        public bool InUse { get; private set; } //используется ли сейчас пистолет

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            photonView = GetComponent<PhotonView>();
            shootOnMesh = GetComponent<ShootOnMesh>();
        }
        private void Start()
        {
            defaultBarrelPosition = rootGunMechanism.transform.localPosition;
            GetComponentInChildren<PistolBarrel>().OnHandReload += PlayerMakedHandSpin;
            this.CanOpenBarrel = true;
            this.GunSpinned = false;
            GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectUngrabbed += MainPistol_InteractableObjectUngrabbed;
            GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectGrabbed += MainPistol_InteractableObjectGrabbed;
            barrelMechanism.ResetRotation = true;

            OnSecondBulletEvent_Start.AddListener(() => {
                barrelRotating = StartCoroutine(InsertSecondBulletNumerator());
                SecondBulletInserted = true;
            });
            OnSecondBulletEvent_End.AddListener(() => {
                StopCoroutine(barrelRotating);
                SecondBulletInserted = false;
            });
        }

        public event Action OnGrab, OnUngrab;
        private void Update()
        {
            Debug.DrawRay(bulletSpawnPoint.transform.position, bulletSpawnPoint.transform.forward * 3f, Color.red);
        }
        public bool InHand { get; private set; }
        private void MainPistol_InteractableObjectUngrabbed(object sender, VRTK.InteractableObjectEventArgs e) {
            InHand = false;
            StartCoroutine(DelayUnhighlight());
            if (photonView.Owner==null&&PhotonNetwork.IsMasterClient)
                photonView.TransferOwnership(PhotonNetwork.LocalPlayer);
            if(photonView.Owner.UserId == 
                NetworkManager.Instance.gameState.userTurn)
            {
                pistolMoving.DelayCheckPistolPosition();
            }
            OnUngrab?.Invoke();
        }
        private void MainPistol_InteractableObjectGrabbed(object sender, VRTK.InteractableObjectEventArgs e)
        {
            InHand = true;
            pistolMoving.DelayCheckPistolPositionStop();
            OnGrab?.Invoke();
        }
        private IEnumerator DelayUnhighlight()
        {
            yield return new WaitForSeconds(.05f);
            foreach (var part in GetComponentsInChildren<VRTK.VRTK_InteractObjectHighlighter>())
                part.Unhighlight();
        }
        public void SetUser(Player player)
        {
            foreach(var pv in transform.GetComponentsInChildren<PhotonView>())
                pv.TransferOwnership(player);

            pistolMoving.PistolGrabbed();//снимает флаг, означающий что пистолет лежит в центре
            if (photonView.Owner.IsLocal)
            {
                InUse = true;
            }

        }
        public void SetPistolHolder(AbstractPlayer player)
        {
            pistolHolder = player;
            bool myTurn = player.UserID == NetworkManager.Instance.gameState.userTurn;
            if (!IsGunCharged && myTurn)
            {
                StopAllCoroutines();
                //rootGunMechanism.isKinematic = false;
                barrelMechanism.ResetRotation = false;
                rootGunMechanism.constraints = RigidbodyConstraints.None;

            }
        }
        private IEnumerator StartResetingRotation()
        {
            yield return new WaitForSeconds(.25f);
            barrelMechanism.ResetRotation = true;
        }
        [PunRPC]
        private void RPC_PlayerUngrabbedPistol()
        {
            InUse = false;
            pistolHolder = null;

            if (photonView.Owner.IsLocal && rootGunMechanism.constraints != RigidbodyConstraints.FreezeRotationX) {
                rootGunMechanism.transform.localRotation=Quaternion.identity;
                rootGunMechanism.velocity = Vector3.zero;
                rootGunMechanism.angularVelocity=Vector3.zero;
                rootGunMechanism.constraints = RigidbodyConstraints.FreezeRotationX /*| RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ*/;
                //rootGunMechanism.isKinematic = true;
                StartCoroutine(CloseJointBarrel(.3f, true));
            }
        }
        public void NPC_UngrabbPistol()
        {
            InUse = false;
            pistolMoving.RPC_PistolOnCenterOfTable(true);
            pistolMoving.SetPistolRotation();
            transform.position = GameManager.Instance.tableCenter.position;
            if (PhotonNetwork.IsMasterClient)
            {
                NetworkManager.Instance.GetComponent<PhotonView>().RPC("SyncPistolState", RpcTarget.MasterClient);
            }
        }
        [PunRPC]
        private void RPC_DropPistol() //падение пистолета после самоубийства нпс
        {
            transform.SetParent(null, true);
            transform.position = pointDropFromPosition;
            transform.rotation = pointDropFromRotation;
            GetComponent<GrabbaleObjects>().IsGrabbed = false;
            GetComponent<GrabbaleObjects>().EnablingSync(true);
            GetComponent<Rigidbody>().isKinematic = false;

            if (PhotonNetwork.IsMasterClient)
            {
                NetworkManager.Instance.SyncPistolStateRequest();
            }
        }

        #region SeconBullet
        [SerializeField] float secondBulletEffectTime = 5f;
        public bool SecondBulletInserted { get; set; }
        private Coroutine barrelRotating;
        public UnityEvent OnSecondBulletEvent_Start, OnSecondBulletEvent_End;
        public void InsertSecondBulletStart(){
            NetworkManager.Instance.gameState.DoubleBulletSpin();

            photonView.RPC("RPC_InsertSecondBulletStart", RpcTarget.All);
        }
        public void InsertSecondBulletStop()
        {
            photonView.RPC("RPC_InsertSecondBulletStop", RpcTarget.All);
        }
        [PunRPC]
        private void RPC_InsertSecondBulletStart()
        {
            OnSecondBulletEvent_Start?.Invoke();
        }
        [PunRPC]
        private void RPC_InsertSecondBulletStop()
        {
            OnSecondBulletEvent_End?.Invoke();
        }
        private IEnumerator InsertSecondBulletNumerator()
        {
            float time = secondBulletEffectTime;
            while(time > 0)
            {
                tweenRotationCilinder.transform.Rotate(2 * 360 * Time.deltaTime, 0, 0, Space.Self);
                time -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            OnSecondBulletEvent_End?.Invoke();
        }
        #endregion

        #region Reload region
        private void OnBarrelClosed()
        {
            barrelMechanism.OnBarrelClosed.RemoveAllListeners();
            rootGunMechanism.constraints = RigidbodyConstraints.FreezeRotationX;
            rootGunMechanism.transform.localRotation = Quaternion.identity;
            rootGunMechanism.velocity = Vector3.zero;
            rootGunMechanism.angularVelocity = Vector3.zero;
            //rootGunMechanism.isKinematic = true;
        }
        public void GiveAccessToHandSpin() //дает возможность игроку прокрутить барабан пистолета
        {
            GunSpinned = false;
            canHandSpin = true;

            rootGunMechanism.transform.localRotation = Quaternion.identity;
            rootGunMechanism.velocity = Vector3.zero;
            rootGunMechanism.angularVelocity = Vector3.zero;
            rootGunMechanism.constraints = RigidbodyConstraints.FreezeRotationX/* | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ*/;

            waitForSpinPistol = StartCoroutine(AutoCloseAndSpin());//после того как карта прокрутки была использована, у игрока есть 5 сек чтобы сделать это, иначе это происходи автоматически
        }
        public void ChargePistol(Vector3 bulletHoleLocalPosition) //определения позиции с которой пуля начинает задвигаться в барабан
        {
            var localBulletPosition = localBullet.transform.localPosition;
            localBulletPosition.x = bulletHoleLocalPosition.x;
            localBulletPosition.y = bulletHoleLocalPosition.y;
            localBulletPosition.z = 0.7f;
            photonView.RPC("RPC_ChargePistol", RpcTarget.All, localBulletPosition);
            barrelMechanism.OnBarrelClosed.AddListener(() => OnBarrelClosed());
            barrelMechanism.WaitForBarrelClosing();
        }
        [PunRPC]
        private void RPC_ChargePistol(Vector3 localBulletPosition)
        {
            this.localBullet.transform.localPosition = localBulletPosition;
            this.localBullet.SetActive(true);
            StartCoroutine(MoveBulletToBarrel());
        } //выставляет локальную пулю в нужную позицию, а потом задвигает ее в барабан
        private IEnumerator MoveBulletToBarrel()
        {
            CanOpenBarrel = false;
            float length = 1.2f;
            int ticks = 30;
            var oldBulletPostion = localBullet.transform.localPosition;
            var newBulletPosition = oldBulletPostion;
            for (int i = 0; i < ticks; i++)
            {
                newBulletPosition.z -= length / (float)ticks;
                localBullet.transform.localPosition = newBulletPosition;
                yield return new WaitForEndOfFrame();
            }
            CanOpenBarrel = true;

            this.IsGunCharged = true;

            if (photonView.Owner.IsLocal)
                waitForSpinPistol = StartCoroutine(AutoCloseAndSpin());//после того как пуля оказалась в барабане, если игрок не закроет и(или) не прокрутит барабан - это происходит автоматически

        } //задвигает локальную пулю в барабан
        /*
            AutoCloseAndSpin
            закрывает и прокручивает барабан пистолета если игрок не сделал этого в течении 5 сек после того как зарядил пистолоет пулей либо использовал карту прокрутки 
        */
        private IEnumerator AutoCloseAndSpin()
        {
            yield return new WaitForSeconds(5f);
            //rootGunMechanism.isKinematic = true;

            rootGunMechanism.velocity = Vector3.zero;
            rootGunMechanism.angularVelocity = Vector3.zero;
            rootGunMechanism.constraints = RigidbodyConstraints.FreezeRotationX/* | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ*/;
            if (IsBarellOpenedByJoint)
            {
                if (photonView.Owner.IsLocal)
                {
                    canHandSpin = false;
                    yield return CloseJointBarrel(1f);
                }
            }

            if (!gunSpinned && isGunCharged)
            {
                photonView.RPC("VisualSpin", RpcTarget.All);
            }
            waitForSpinPistol = null;
        }//автоматически закрывает и прокручивает барабан если игрок не сделал этого сам

        private IEnumerator CloseJointBarrel(float time, bool _canOpenBarrel = false)
        {
            if (!_canOpenBarrel)
                CanOpenBarrel = _canOpenBarrel;

            barrelMechanism.ResetRotation = true;

            rootGunMechanism.transform.localRotation = Quaternion.identity;
            rootGunMechanism.velocity = Vector3.zero;
            rootGunMechanism.angularVelocity = Vector3.zero;
            rootGunMechanism.constraints = RigidbodyConstraints.FreezeRotationX/*| RigidbodyConstraints.FreezeRotationY| RigidbodyConstraints.FreezeRotationZ*/;

            realBarrel.GetComponent<TweenRotation>().PlayReverse();
            yield return new WaitForSeconds(time);

        }
        public void PlayerMakedHandSpin(float time)
        {
            if (pistolHolder != null) //если пистолет сейчас у игрока, надо выключить киниматик у барабана
            {
                if (!IsBarellOpenedByJoint && canHandSpin && isGunCharged)
                {
                    CanOpenBarrel = false;

                    rootGunMechanism.transform.localRotation = Quaternion.identity;
                    rootGunMechanism.velocity = Vector3.zero;
                    rootGunMechanism.angularVelocity = Vector3.zero;
                    rootGunMechanism.constraints = RigidbodyConstraints.FreezeRotationX /*| RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ*/;
                    //rootGunMechanism.isKinematic = true;
                    canHandSpin = false;
                    photonView.RPC("VisualSpin", RpcTarget.All);
                }
                return;
            }

            if (waitForSpinPistol != null)
            {
                Debug.Log("stop waitng hand spin");
                StopCoroutine(waitForSpinPistol); //игрок сам прокрутил и закрыл барабан, больше не надо это делать автоматически
                waitForSpinPistol = null;
            }

        } //прокурутка барабана рукой   
        private void RPC_SpinSound()
        {
            pistolSoundPool.audioSource.PlayOneShot(pistolSoundPool.spinSFX);
        } //звук прокрутки

        private int prevBulletIndex=0;
        [PunRPC]
        public void VisualSpin()
        {
            if(waitForSpinPistol!=null) StopCoroutine(waitForSpinPistol);
            if (photonView.Owner.IsLocal) //фактическая перезарядка пистолета
            {
                if (!SecondBulletInserted) {
                    NetworkManager.Instance.gameState.Spin();
                } //фактическая перезарядка происходит только если вторая пуля не была вставлена
                //NetworkManager.Instance.RequestToSync();
            }
            localBullet.SetActive(false);//после прокрутки барабана больше нет возможности его открывать/закрывать
            tweenRotationCilinder.ResetToBeginning();
            RPC_SpinSound();
            GunSpinned = true;
            StartCoroutine(StartResetingRotation());
            Debug.Log("<color=red>[MainPistol]VisualSpin: barrel spinned</color>");
        } //включение анимации прокрутки пистолета
        [PunRPC]
        public void OpenBarrel()
        {
            //barrelMechanism.ResetRotation = false;
            OpenBarrelEvent?.Invoke();
            GunSpinned = false;
            barrelMechanism.GetComponent<TweenRotation>().ResetToBeginning();
            IsBarrelOpened = true;
            StartCoroutine(StartResetingRotation());
        } //(включить анимацию) открыть барабан пистолета
        [PunRPC]
        public void CloseBarrel()
        {
            //barrelMechanism.ResetRotation = false;
            CloseBarrelEvent?.Invoke();
            barrelMechanism.GetComponent<TweenRotation>().PlayReverse();
            IsBarrelOpened = false;
            StartCoroutine(StartResetingRotation());
        } //(включить анимацию) закрыть барабан пистолета
        [PunRPC]
        public void ChargeGunByNPC() //включает флаг что пистолет заряжен (метод используется только для нпс)
        {
            isGunCharged = true;
        }
        [PunRPC]
        private void CheckIsGunCharged(bool isBullet)
        {
            //IsGunCharged = !isBullet;
            isGunCharged = !isBullet;
            if (isBullet)
            {
                canOpenBarrel = true;
                gunSpinned = false;
                Debug.Log("<color=red>[MainPistol]gunspinned set false#6" + "</color>");
            }

        }
        #endregion

        #region Shot region
        public void MakeShot(AbstractPlayer player, bool NPC_itselfShooting = false)
        {
            if (NetworkManager.Instance.gameState.userTurn != player.UserID){ //нельзя стрелять не в свою очередь
                Debug.LogError("[MainPistol]MakeShot: shot is cancelled"+ NetworkManager.Instance.gameState.userTurn+"   "+ player.UserID);
                return;

            }
            if(!GunSpinned)
            {
                Debug.LogError("[MainPistol]MakeShot: shot is cancelled. Gun not spinned");
                return;
            }

            StartCoroutine(ShotNumerator(player, NPC_itselfShooting));
        } //обработчик выстрела, NPC_itselfShooting показывает что нпс целится сам в себя, нужен чтобы повернуть пистолет в голову LookAt-ом, т.к. когда не смотришь на анимацию, она неправильно работает
        private IEnumerator ShotNumerator(AbstractPlayer player, bool NPC_itselfShooting)
        {
            //GameState gameState = NetworkManager.Instance.gameState;
            bool isBullet = NetworkManager.Instance.gameState.IsBullet();
            string msg = string.Format("<color=blue>{0} shot is {1}, gameState.currIndex: {2}, gameState.bulletIndex: {3}</color>",
                player.UserID, isBullet, NetworkManager.Instance.gameState.currIndex, NetworkManager.Instance.gameState.bulletIndex);

            yield return new WaitForEndOfFrame();
            
            photonView.RPC("OnShotEvent", RpcTarget.All);
            Transform targetToShoot = null; //если нпс будет в кого-то тсрелять, пистолет подварачивается к цели чтобюы выстрел был наверняка
            Quaternion oldPistolRotation = transform.rotation; //чтобы вернуть пистолет в исходное положения после использования LookAt
            if (player is NPC)
            {
                targetToShoot = ((NPC)player).TARGET_TO_SHOT;
               
                if (targetToShoot != null)
                {
                    transform.LookAt(targetToShoot);
                }
            }
            PlayerState myPlayerState = NetworkManager.Instance.gameState.GetPlayerState(player.UserID);
            int layerMask = 1 << LayerMask.NameToLayer("Head");
            RaycastHit hit;
            Ray ray = new Ray(bulletSpawnPoint.position, bulletSpawnPoint.forward * 10);

            if(Physics.Raycast(ray, out hit, 10f, layerMask))
            {
                if(hit.transform.CompareTag("Head")) //выстрел произошел в голову
                {
                    if (player is NPC && targetToShoot != null)
                    {
                        ((NPC)player).TARGET_TO_SHOT = null;
                        transform.rotation = oldPistolRotation;
                    }//пистолет возвращается в исходное состояние после поворота LookAt-ом

                    if (isBullet)
                    {
                        var _player = hit.transform.root.GetComponent<AbstractPlayer>();
                        if (player is NPC && player.Equals(_player))//если нпс убивает сам себя, запоминается позиция точки откуда будет падать пистолет
                        {
                            photonView.RPC("SavePointDropFrom", RpcTarget.All, transform.position, transform.rotation);
                        }
                        if (_player != null)
                        {
                            _player.PublicDeath(bulletSpawnPoint.forward);
                        }
                    }//нужно запомнить направление пули, чтобы задать направление полета головы. если самоубийство совершил нпс, запоминается точка откуда будет падать пистолет

                    if (hit.transform.root.name == player.transform.root.name)
                    {
                        Debug.Log("Выстрел себе в голову - bullet? " + isBullet);
                        if (!isBullet) //пистолет не был заряжен
                        {
                            myPlayerState.money += 1;
                            player.AddCoinsToPlayer(1);
                        }
                        else //пистолет был заряжен
                        {
                            //if (player.UserID == PlayingCardsManager.Instance.ProtectedUserID)
                            //{
                            //    PlayingCardsManager.Instance.PV.RPC("DisableProtectYourSelf", RpcTarget.MasterClient);
                            //}
                            myPlayerState.IsKilled = true;
                            if (myPlayerState.IsKilled) {
                                myPlayerState.money = 0;
                                player.RemoveCoin(-1);
                                Debug.Log(player.transform.root.name + " KILLED!");
                            }
                            else
                            {
                                myPlayerState.money++;
                                player.AddCoinsToPlayer(1);
                                //PlayingCardsManager.Instance.PV.RPC("DisableExtraLife", RpcTarget.MasterClient, );
                            }
                        }
                    } /*Выстрел себе в голову*/
                    else
                    {
                        Debug.Log("Выстрел другому игроку в голову: isBullet? " + isBullet);
                        if (myPlayerState.money > 0)
                        {
                            var shotDownPlayer = hit.transform.GetComponentInParent<AbstractPlayer>();

                            if(shotDownPlayer.UserID == PlayingCardsManager.Instance.ProtectedUserID)
                            {
                                yield break;
                            }
                            if (PlayingCardsManager.Instance.TargetUserID != string.Empty)
                            {
                                if (PlayingCardsManager.Instance.TargetUserID != shotDownPlayer.UserID)
                                    yield break;

                                //if (isBullet) PlayingCardsManager.Instance.PV.RPC("DisableSetTarget", RpcTarget.MasterClient);
                            }


                            if (!isBullet)
                            {                              
                                myPlayerState.money = 0;
                                player.RemoveCoin(-1);
                                if (shotDownPlayer is NPC)
                                {
                                    var shootedMePlayers = NetworkManager.Instance.gameState.GetPlayerState(hit.transform.GetComponentInParent<AbstractPlayer>().UserID).playerWhoShotMe;
                                    if (shootedMePlayers.Length > 0)
                                    {
                                        bool alreadyContains = false; //проверка записан ли уже в массив игрок который в меня стрелял
                                        for (int i = 0; i < shootedMePlayers.Length; i++)
                                        {
                                            if (shootedMePlayers[i] == myPlayerState.UserID)
                                            {
                                                alreadyContains = true;
                                                break;
                                            }
                                        }
                                        if (!alreadyContains) //если не записан, записываем
                                        {
                                            var tmpMass = new string[shootedMePlayers.Length + 1];
                                            tmpMass[tmpMass.Length - 1] = myPlayerState.UserID;
                                            shootedMePlayers = tmpMass;
                                        }
                                    }
                                    else
                                    {
                                        shootedMePlayers = new string[1];
                                        shootedMePlayers[0] = myPlayerState.UserID;
                                    }
                                    NetworkManager.Instance.gameState.GetPlayerState(hit.transform.GetComponentInParent<AbstractPlayer>().UserID).playerWhoShotMe = shootedMePlayers;
                                }
                            }
                            else
                            {
                                NetworkManager.Instance.gameState.GetPlayerState(shotDownPlayer.UserID).IsKilled = true;
                                if (NetworkManager.Instance.gameState.GetPlayerState(shotDownPlayer.UserID).IsKilled) { 

                                    myPlayerState.money += NetworkManager.Instance.gameState.GetPlayerState(shotDownPlayer.UserID).money;
                                    player.AddCoinsToPlayer(NetworkManager.Instance.gameState.GetPlayerState(shotDownPlayer.UserID).money);
                                }
                                else
                                {
                                    myPlayerState.money++;
                                    player.AddCoinsToPlayer(1);
                                    //PlayingCardsManager.Instance.PV.RPC("DisableExtraLife", RpcTarget.MasterClient);
                                }
                            }
                        }
                        else
                        {
                            yield break;
                        }
                    }/*Выстрел другому игроку в голову*/
                }
                else
                {                   
                    print("Выстрел не в игроков: isBullet? " + isBullet);
                    if (myPlayerState.money > 0)
                    {
                        if (isBullet)
                        {
                            myPlayerState.money += 1;
                            player.AddCoinsToPlayer(1);
                        }
                        else
                        {
                            myPlayerState.money -= 1;
                            player.RemoveCoin(1);
                        }

                    }
                    else
                    {
                        yield break;
                    }
                }/*Выстрел не в игроков*/
            }
            else
            {
                print("Выстрел в воздух: isBullet? " + isBullet);
                if (myPlayerState.money > 0)
                {
                    if (isBullet)
                    {
                        myPlayerState.money += 1;
                        player.AddCoinsToPlayer(1);
                    }
                    else
                    {
                        myPlayerState.money -= 1;
                        player.RemoveCoin(1);
                    }
                }
                else
                {
                    yield break;
                }
            } /*Выстрел в воздух*/


            photonView.RPC("RPC_ShotInHand", RpcTarget.All, isBullet);
            photonView.RPC("ShotSFX", RpcTarget.All, isBullet);
            photonView.RPC("ShotVFX", RpcTarget.All, isBullet);
            photonView.RPC("CheckIsGunCharged", RpcTarget.All, isBullet);

            if (isBullet && SecondBulletInserted)
                this.InsertSecondBulletStop();

            this.ShootSaloonNPC(isBullet);

            if (isBullet && NetworkManager.Instance.gameState.turnsClockwise) NetworkManager.Instance.gameState.turnsClockwise = false; //очередь стрельбы снова становится против часовой после выстрела (если была сыграна карта реверса)
            if(isBullet)
            {
                for (int i = 0; i < NetworkManager.Instance.gameState.players.Length; i++)
                {
                    if (!NetworkManager.Instance.gameState.players[i].IsKilled)
                        NetworkManager.Instance.gameState.players[i].lifeCount = 1;
                }
            } //сброс второй жизни у игрок сыгравшего карту доп жизни

            PlayingCardsManager.Instance.PV.RPC("PistolMakedShot", RpcTarget.All, isBullet);
            NetworkManager.Instance.gameState.currIndex++;
            NetworkManager.Instance.gameState.SwitchTurnToNextUser();
            NetworkManager.Instance.RequestToSync();

            if (player is PlayerController) //если стрелял не нпс, пистоле возвращается в центр стола принудительно
            {
                photonView.RPC("MovePistolDelayed", RpcTarget.All, 3f);
                for (int i = 0; i < 30; i++)
                {
                    yield return new WaitForSeconds(.1f);
                    if (!InUse)
                    {
                        yield return new WaitForEndOfFrame();
                        NetworkManager.Instance.SyncPistolStateRequest();
                        yield break;
                    }
                } //синхронизация состояния пистолета
                NetworkManager.Instance.SyncPistolStateRequest();
            }

            if(player is NPC)
            {
                if(isBullet)
                    photonView.RPC("MovePistolDelayed", RpcTarget.All, 3f);

                MainPistolMoving p_moving = GetComponent<MainPistolMoving>();

                while(!p_moving.PistolOnCenterOfTable)
                {
                    yield return new WaitForEndOfFrame();
                }
                NetworkManager.Instance.SyncPistolStateRequest();
            }

            if(waitForSpinPistol != null) { 
                StopCoroutine(waitForSpinPistol);
                waitForSpinPistol = null;
            }
        }//выстрел с задержкой 1 кадр
        private void ShootSaloonNPC(bool isBullet)
        {
            if (!isBullet)
                return;
            
            if (Physics.Raycast(bulletSpawnPoint.position, bulletSpawnPoint.forward * 50, out var hit, 50f, 1 << LayerMask.NameToLayer("Head")))
            {
                if (hit.transform.CompareTag("Torso"))
                {
                    Waitress.Instance.GetComponent<PhotonView>().RPC("Death", RpcTarget.MasterClient);
                }

                if (hit.transform.CompareTag("Pianist"))
                {
                    Pianist.Instance.GetComponent<PhotonView>().RPC("Death", RpcTarget.MasterClient);
                }

                if (hit.transform.CompareTag("Barmen"))
                {
                    Barmen.Instance.GetComponent<PhotonView>().RPC("Death", RpcTarget.MasterClient);
                }
            }
        } //проверка был ли выстрел по официантке/бармену/пианисту
        [PunRPC]
        private void ShotSFX(bool isBullet)
        {
            if(isBullet)
                pistolSoundPool.audioSource.PlayOneShot(pistolSoundPool.shotSFX);
            else
                pistolSoundPool.audioSource.PlayOneShot(pistolSoundPool.dryShotSFX);

        } //sfx выстрела
        [PunRPC]
        private void ShotVFX(bool isBullet)
        {
            if(isBullet)
            {
                shotVFX.Play();
            }
        } //vfx выстрела
        [PunRPC]
        private void OnShotEvent()
        {
            OnShot?.Invoke();
        }
        [PunRPC]
        private void RPC_ShotAnim(bool isShotWas)
        {
            OnShot?.Invoke();
        } //анимация выстрела (у пистолета)
        [PunRPC]
        private void RPC_ShotInHand(bool isBullet)
        {
            if(isBullet)
                shootOnMesh.Shoot();
        }//прострелить дырку в руке
        [PunRPC]
        private void SavePointDropFrom(Vector3 pos, Quaternion rot)
        {
            pointDropFromPosition = pos;
            pointDropFromRotation = rot;
        }//запомнить позицию из которой должен падать пистолет после самоубийства нпс
        #endregion

        [PunRPC]
        private void SyncPistolState(byte[] arr)
        {
            GameState state = (GameState)NetworkManager.Instance.ByteArrayToObject(arr);
            bool localPlayerTurn = PhotonNetwork.LocalPlayer.UserId == state.userTurn;
            if(localPlayerTurn)
            {
                //если у локального игрока очередь стрелять, включается доступ к пистолету (если его нет)
                bool isLocalPlayerCanGrabPistol = GetComponent<VRTK.VRTK_InteractableObject>().enabled;
                if(!isLocalPlayerCanGrabPistol)
                {
                    GetComponent<VRTK.VRTK_InteractableObject>().enabled = true;
                }
            }
            else
            {
                //если очередь стрелять не локального игрока, исключается его взаимодействие с пистолетом
                bool isLocalPlayerCanGrabPistol = GetComponent<VRTK.VRTK_InteractableObject>().enabled;
                if (isLocalPlayerCanGrabPistol)
                {
                    GetComponent<VRTK.VRTK_InteractableObject>().enabled = false;
                    MainPistol_InteractableObjectUngrabbed(null,new InteractableObjectEventArgs());
                }
            }

            if(state.currIndex >= state.bulletIndex)
            {
                IsGunCharged = false;
                if(PhotonNetwork.IsMasterClient)
                    Bullet.Instance.GetComponent<PhotonView>().RPC("RPC_EnableBullet", RpcTarget.All);
            }

            if(GameManager.Instance.AlivePlayerCount(state) <= 1) //запрещает брать пистолет единственном живому игроку
            {
                //NetworkManager.Instance.gameState.userTurn = string.Empty;
                bool isLocalPlayerCanGrabPistol = GetComponent<VRTK.VRTK_InteractableObject>().enabled;
                if (isLocalPlayerCanGrabPistol)
                {
                    GetComponent<VRTK.VRTK_InteractableObject>().enabled = false;
                }
            }

            PlayingCardsManager.Instance.ClearLastUsed();
        }
    }
}
