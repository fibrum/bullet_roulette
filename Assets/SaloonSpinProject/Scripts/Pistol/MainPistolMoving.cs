﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class MainPistolMoving : MonoBehaviour
    {
        [SerializeField] private float speed = 1.0F;
        private PhotonView photonView;
        private Vector3 startMarker;
        private Vector3 endMarker;
        private float startTime;
        private float journeyLength;
        private Coroutine movePistolInCenterAfterSeconds;

        public bool IsPistolMoving { get; private set; }
        public bool PistolOnCenterOfTable { get; private set; }
        private void Awake()
        {
            photonView = GetComponent<PhotonView>();
        }
        private void Start()
        {
            IsPistolMoving = false;
        }
        void Update()
        {
            if (IsPistolMoving)
            {
                float distCovered = (Time.time - startTime) * speed;
                float fracJourney = distCovered / journeyLength;    
                transform.position = Vector3.Lerp(startMarker, endMarker, fracJourney);

                if(Vector3.Distance(transform.position, endMarker) < 0.01f)
                {
                    SetPistolRotation();
                    transform.position = endMarker;
                    IsPistolMoving = false;
                    this.PistolSync(true);

                    if(PhotonNetwork.IsMasterClient)
                    {
                        photonView.RPC("RPC_PistolOnCenterOfTable", RpcTarget.All, true);
                    }

                }
            }
        }
        public void SetPistolRotation()
        {
            GameState state = NetworkManager.Instance.gameState;
            var pp = GameManager.Instance.playersPositions;
            int pos = state.GetPos(state.userTurn);
            switch (pos)
            {
                case 0:
                    transform.LookAt(pp[2].moneyText.transform);
                    break;
                case 1:
                    transform.LookAt(pp[3].moneyText.transform);
                    break;
                case 2:
                    transform.LookAt(pp[0].moneyText.transform);
                    break;
                case 3:
                    transform.LookAt(pp[1].moneyText.transform);
                    break;
            }
            Vector3 rot = transform.eulerAngles;
            rot.z = 90;
            transform.eulerAngles = rot;
        }
        [PunRPC]
        public void PutPistolOnTableCenter()
        {
            if (IsPistolMoving)
                return;

            var npcHolder = transform.GetComponentInParent<NPC>();
            if(npcHolder != null)
                return;
            
            GetComponent<GrabbaleObjects>().StopGrab(true);
            
            transform.SetParent(null);

            if (transform.parent != null) //кто-то держит пистолет
                return;

            this.PistolSync(false);

            startTime = Time.time;
            startMarker = transform.position;
            endMarker = GameManager.Instance.tableCenter.position;
            journeyLength = Vector3.Distance(startMarker, endMarker);
            IsPistolMoving = true;

            if(PhotonNetwork.IsMasterClient)
            {
                photonView.RPC("RPC_PistolOnCenterOfTable", RpcTarget.All, false);
            }
        }

        private void PistolSync(bool enable)
        {
            GetComponent<GrabbaleObjects>().EnablingSync(enable);
            GetComponent<Rigidbody>().isKinematic = !enable;
        }

        public void CheckMoving()
        {
            photonView.RPC("StopMoving", RpcTarget.All);
        }

        [PunRPC]
        private void MovePistolDelayed(float time)
        {
            if (movePistolInCenterAfterSeconds != null)
                StopCoroutine(movePistolInCenterAfterSeconds);

            movePistolInCenterAfterSeconds = StartCoroutine(DelayMovePistolInCenter(time));
        }

        private IEnumerator DelayMovePistolInCenter(float t)
        {
            yield return new WaitForSeconds(t);
            PutPistolOnTableCenter();
        }


        [PunRPC]
        private void StopMoving()
        {
            if (movePistolInCenterAfterSeconds != null)
                StopCoroutine(movePistolInCenterAfterSeconds);

            IsPistolMoving = false;
            this.PistolSync(true);
        }
        [PunRPC]
        public  void RPC_PistolOnCenterOfTable(bool b)
        {
            this.PistolOnCenterOfTable = b;
        }
        public void PistolGrabbed()
        {
            IsPistolMoving = false;
            PistolOnCenterOfTable = false;

            if (movePistolInCenterAfterSeconds != null)
                StopCoroutine(movePistolInCenterAfterSeconds);
        }

        private Coroutine checkPistol;
        public void DelayCheckPistolPosition()
        {
            checkPistol = StartCoroutine(DelayCheckPistolPositionNumerator());
        }
        public void DelayCheckPistolPositionStop()
        {
            if (checkPistol != null) StopCoroutine(checkPistol);
        }
        private IEnumerator DelayCheckPistolPositionNumerator()
        {
            yield return new WaitForSeconds(3f);
            if (!PistolOnCenterOfTable)
            {
                photonView.RPC("MovePistolDelayed", RpcTarget.All, .1f);
                checkPistol = null;
            }
        }
        
    }
}
