﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace SaloonSpin
{
    public class MainPistolPointer : MonoBehaviour
    {
        [SerializeField] private Transform bulletSpawnPoint;
        [SerializeField] private PlayerUI_LookAtCamera pistolUI;
        [SerializeField] private Image hintImage;
        [SerializeField] private Text hintText;
        [SerializeField] private Sprite no_bullets_sprite, no_coins_sprite,notpublicenemy,underprotection,shoot_yourself;
        [SerializeField] private Text plusText;
        [SerializeField] private Text minusText;
        [SerializeField] private GameObject textPanel;

        private VRTK.VRTK_InteractableObject interactableObject;
        private MainPistol mainPistol;
        private PhotonView photonView;
        private bool aimAtMyself;
        private AbstractPlayer aimingSelfPlayer;
        private Transform lastAimedTarget;
        private Coroutine hintsCoroutine;
        private bool hintsDelayRunning;
        private Head aimedWaitress, aimedBarmen, aimedPianist;

        private void Awake()
        {
            mainPistol = GetComponent<MainPistol>();
            photonView = GetComponent<PhotonView>();
            interactableObject = GetComponent<VRTK.VRTK_InteractableObject>();
        }
        private void Start()
        {
            GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectUngrabbed += MainPistolPointer_InteractableObjectUngrabbed; ;
            GetComponent<MainPistol>().OnShot.AddListener(OnShoot);
        }

        private void OnDisable() {
            textPanel.SetActive(false);
            hintImage.gameObject.SetActive(false);
            
        }

        private void OnShoot() {
            //NetworkManager.Instance.myPlayer.playerModel.Head.GetComponentInChildren<Head>().StopAimAtMyself?.Invoke();
            if (aimingSelfPlayer != null)
            {
                aimingSelfPlayer.playerModel.Head.GetComponentInChildren<Head>().StopAimAtMyself?.Invoke();
                aimAtMyself = false;
                aimingSelfPlayer = null;
            }
        }

        private void MainPistolPointer_InteractableObjectUngrabbed(object sender, VRTK.InteractableObjectEventArgs e)
        {
            hintImage.gameObject.SetActive(false);
            textPanel.SetActive(false);
            SaloonStaffAiming(new RaycastHit());
            //NetworkManager.Instance.myPlayer.playerModel.Head.GetComponentInChildren<Head>().StopAimAtMyself?.Invoke();
            if (aimingSelfPlayer != null )
            {
                aimingSelfPlayer.playerModel.Head.GetComponentInChildren<Head>().StopAimAtMyself?.Invoke();
                aimAtMyself = false;
            }

            if (!mainPistol.InUse && lastAimedTarget != null)
            {
                var lastAimedPlayer = lastAimedTarget.root.GetComponent<AbstractPlayer>();
                if (lastAimedPlayer is NPC)
                {
                    lastAimedTarget.root.GetComponent<NPC>().Fear(false, Vector3.zero);
                    lastAimedTarget.root.GetComponent<NPC>().HidePlayerInfo();
                }
                else
                {
                    lastAimedPlayer.HidePlayerInfo();
                }

                if (hintsDelayRunning)
                {
                    hintsDelayRunning = false;

                    if (hintsCoroutine != null)
                        StopCoroutine(hintsCoroutine);
                }
                

                lastAimedTarget = null;
                SaloonStaffAiming(new RaycastHit());
            }
            hintImage.gameObject.SetActive(false);
            textPanel.SetActive(false);
        }

        void Update()
        {
            if (!interactableObject.IsGrabbed())
                return;
            if (!NetworkManager.Instance.gameState.MyTurn) {
                hintImage.gameObject.SetActive(false);
                textPanel.SetActive(false);
                return;
            }
                
            if (mainPistol.InUse && photonView.IsMine)
            {
                if (transform.root.GetComponent<NPC>() != null)
                    return;

                RaycastHit hit;
                int layerMask = 1 << LayerMask.NameToLayer("Head");
                Ray ray = new Ray(bulletSpawnPoint.transform.position, bulletSpawnPoint.transform.forward * 3f);
                Debug.DrawRay(bulletSpawnPoint.transform.position, bulletSpawnPoint.transform.forward * 3f, Color.red);
                if (Physics.Raycast(ray, out hit, 30f, layerMask))
                {
                    if (hit.transform.CompareTag("Head"))
                    {
                        if (hit.transform.root.GetComponent<PhotonView>().Owner == PhotonNetwork.LocalPlayer)
                        {
                            if (!aimAtMyself && mainPistol.IsGunCharged && !mainPistol.IsBarellOpenedByJoint)
                            {
                                aimingSelfPlayer = hit.transform.GetComponentInParent<AbstractPlayer>();
                                aimingSelfPlayer.playerModel.Head.GetComponentInChildren<Head>().AimAtMyself?.Invoke();
                                aimAtMyself = true;

                                plusText.text = $"";
                                minusText.text = $"+1 $";
                                textPanel.SetActive(true);
                                hintImage.gameObject.SetActive(false);
                            }
                            return;
                        }
                        else if (aimAtMyself)
                        {
                            aimingSelfPlayer.playerModel.Head.GetComponentInChildren<Head>().StopAimAtMyself?.Invoke();
                            aimAtMyself = false;
                        }

                        if (hit.transform.root.CompareTag("NPC"))
                        {
                            if (mainPistol.IsGunCharged && !mainPistol.IsBarellOpenedByJoint)//анимация страха включается только если пистолет заряжен
                                hit.transform.root.GetComponent<NPC>().Fear(true, transform.root.position);

                            if (transform.root.GetComponent<PlayerController>() != null)
                                hit.transform.root.GetComponent<AbstractPlayer>().ShowPlayerInfo(transform.root.GetComponent<PlayerController>().PlayerCameraTransform);

                            else if (lastAimedTarget != null)
                            {
                                var lastAimedPlayer = lastAimedTarget.root.GetComponent<AbstractPlayer>();
                                lastAimedPlayer.HidePlayerInfo();
                            }
                            lastAimedTarget = hit.transform;


                        }
                        else if (hit.transform.root.CompareTag("Player"))
                        {
                            try
                            {
                                hit.transform.root.GetComponent<AbstractPlayer>().ShowPlayerInfo(transform.root.GetComponent<PlayerController>().PlayerCameraTransform);
                                lastAimedTarget = hit.transform;
                            }
                            catch

                            {
                                Debug.Log(string.Format("<color=red>line#91 error"));
                            }
                        }


                        if (!mainPistol.IsGunCharged && !hintImage.gameObject.activeSelf)
                        {
                            try
                            {
                                Debug.Log($"set no_bullets_sprite");
                                // hintImage.sprite = no_bullets_sprite;
                                hintText.text = I2.Loc.ScriptLocalization.hints_gun_pointer_no_bullet;
                                hintImage.gameObject.SetActive(true);
                                textPanel.SetActive(false);
                                pistolUI.Init(transform.root.GetComponent<AbstractPlayer>().playerModel.Head.transform);

                              //  if (!hintsDelayRunning)
                              //      hintsCoroutine = StartCoroutine(DelayHints());

                            }
                            catch
                            {

                            }

                        }
                        else if (NetworkManager.Instance.gameState.GetPlayerState(photonView.Owner.UserId).money == 0
                             && mainPistol.IsGunCharged && mainPistol.GunSpinned && (!hintImage.gameObject.activeSelf|| hintImage.sprite != no_coins_sprite))
                        {
                            Debug.Log($"set no_coins_sprite");
                            hintText.text = I2.Loc.ScriptLocalization.hints_gun_pointer_no_coins; // hintImage.sprite = no_coins_sprite;
                            hintImage.gameObject.SetActive(true);
                            textPanel.SetActive(false);
                            pistolUI.Init(transform.root.GetComponent<AbstractPlayer>().playerModel.Head.transform);

                         //   if (!hintsDelayRunning)
                         //       hintsCoroutine = StartCoroutine(DelayHints());
                        }else if (PlayingCardsManager.Instance.StatusOnPlayer(hit.transform.root.GetComponent<AbstractPlayer>().UserID,PlayingCardType.ProtectYourself) 
                                  && !hintImage.gameObject.activeSelf) {
                            hintText.text = I2.Loc.ScriptLocalization.hints_gun_pointer_protected_player;// hintImage.sprite = underprotection;
                            hintImage.gameObject.SetActive(true);
                            textPanel.SetActive(false);
                            pistolUI.Init(transform.root.GetComponent<AbstractPlayer>().playerModel.Head.transform);

                           //if (!hintsDelayRunning)
                           //    hintsCoroutine = StartCoroutine(DelayHints());
                        }else if (!string.IsNullOrEmpty(PlayingCardsManager.Instance.TargetUserID)&& PlayingCardsManager.Instance.TargetUserID!= hit.transform.root.GetComponent<AbstractPlayer>().UserID
                                                                                            && !hintImage.gameObject.activeSelf) {
                            hintText.text = I2.Loc.ScriptLocalization.hints_gun_pointer_not_public_enemy;// hintImage.sprite =notpublicenemy;
                            hintImage.gameObject.SetActive(true);
                            textPanel.SetActive(false);
                            pistolUI.Init(transform.root.GetComponent<AbstractPlayer>().playerModel.Head.transform);

                           // if (!hintsDelayRunning)
                           //     hintsCoroutine = StartCoroutine(DelayHints());
                        }else {
                            if (NetworkManager.Instance.gameState.GetPlayerState(photonView.Owner.UserId).money > 0 && mainPistol.IsGunCharged && mainPistol.GunSpinned) { 
                                plusText.text = $"+1 $";
                                minusText.text = $"-{NetworkManager.Instance.gameState.GetPlayerState(photonView.Owner.UserId).money} $";
                                textPanel.SetActive(true);

                            }
                        }
                    }
                }
                else if (lastAimedTarget != null)//Перевели с игрока на воздух
                {
                    if(NetworkManager.Instance.gameState.GetPlayerState(photonView.Owner.UserId).money != 0&&mainPistol.IsGunCharged && mainPistol.GunSpinned) { 
                        plusText.text = $"+1 $";
                        minusText.text = $"-1 $";
                        textPanel.SetActive(true);
                    }

                    var lastAimedPlayer = lastAimedTarget.root.GetComponent<AbstractPlayer>();
                    if (lastAimedPlayer is NPC)
                    {
                        lastAimedTarget.root.GetComponent<NPC>().Fear(false, Vector3.zero);
                        lastAimedTarget.root.GetComponent<NPC>().HidePlayerInfo();
                    }
                    else
                    {
                        lastAimedPlayer.HidePlayerInfo();
                    }

                    if (hintsDelayRunning)
                    {
                        hintsDelayRunning = false;

                        if (hintsCoroutine != null)
                            StopCoroutine(hintsCoroutine);
                    }
                    hintImage.gameObject.SetActive(false);
                    lastAimedTarget = null;
                }
                else if (aimAtMyself)
                {
                    aimingSelfPlayer.playerModel.Head.GetComponentInChildren<Head>().StopAimAtMyself?.Invoke();
                    aimAtMyself = false;
                } else if(mainPistol.IsGunCharged&&mainPistol.GunSpinned&&mainPistol.InHand&&NetworkManager.Instance.gameState.GetPlayerState(photonView.Owner.UserId).money == 0
                                                 &&!hintImage.gameObject.activeInHierarchy)
                {
                    Debug.Log($"set shoot_yourself");
                    hintText.text = I2.Loc.ScriptLocalization.hints_gun_pointer_shoot_youself;// hintImage.sprite = shoot_yourself;
                    hintImage.gameObject.SetActive(true);
                    textPanel.SetActive(false);
                }

                SaloonStaffAiming(hit);
            }
            else if (!mainPistol.InUse && lastAimedTarget != null)
            {
                var lastAimedPlayer = lastAimedTarget.root.GetComponent<AbstractPlayer>();
                if (lastAimedPlayer is NPC)
                {
                    lastAimedTarget.root.GetComponent<NPC>().Fear(false, Vector3.zero);
                    lastAimedTarget.root.GetComponent<NPC>().HidePlayerInfo();
                }
                else
                {
                    lastAimedPlayer.HidePlayerInfo();
                }

                if (hintsDelayRunning)
                {
                    hintsDelayRunning = false;

                    if (hintsCoroutine != null)
                        StopCoroutine(hintsCoroutine);
                }
                hintImage.gameObject.SetActive(false);
                textPanel.SetActive(false);

                lastAimedTarget = null;
                SaloonStaffAiming(new RaycastHit());
            }
            else if (aimAtMyself)
            {
                aimingSelfPlayer.playerModel.Head.GetComponentInChildren<Head>().StopAimAtMyself?.Invoke();
                aimAtMyself = false;
                SaloonStaffAiming(new RaycastHit());
                if (NetworkManager.Instance.gameState.GetPlayerState(photonView.Owner.UserId).money != 0 && mainPistol.IsGunCharged && mainPistol.GunSpinned)
                {
                    plusText.text = $"+1 $";
                    minusText.text = $"-1 $";
                    textPanel.SetActive(true);
                }
            }

            
        }
        /*private IEnumerator DelayHints()
        {
            hintsDelayRunning = true;
            yield return new WaitForSeconds(1f);

            if (!hintImage.gameObject.activeSelf)
                hintImage.gameObject.SetActive(true);

            hintsDelayRunning = false;
        }*/

        private void SaloonStaffAiming(RaycastHit hit)
        {
            if(hit.transform == null)
            {
                if (aimedWaitress != null)
                    aimedWaitress.AimingEffectTurnOff();

                if (aimedPianist != null)
                    aimedPianist.AimingEffectTurnOff();

                if (aimedBarmen != null)
                    aimedBarmen.AimingEffectTurnOff();

                return;
            }

            if (hit.transform.CompareTag("Torso"))
            {
                if (aimedWaitress == null)
                    aimedWaitress = hit.transform.GetComponent<Head>();

                aimedWaitress.AimingEffectTurnOn();
            }
            else if (aimedWaitress != null)
            {
                aimedWaitress.AimingEffectTurnOff();
            }

            if (hit.transform.CompareTag("Pianist"))
            {
                if (aimedPianist == null)
                    aimedPianist = hit.transform.GetComponent<Head>();

                aimedPianist.AimingEffectTurnOn();
            }
            else if (aimedPianist != null)
            {
                aimedPianist.AimingEffectTurnOff();
            }

            if (hit.transform.CompareTag("Barmen"))
            {
                if (aimedBarmen == null)
                    aimedBarmen = hit.transform.GetComponent<Head>();

                aimedBarmen.AimingEffectTurnOn();
            }
            else if (aimedBarmen != null)
            {
                aimedBarmen.AimingEffectTurnOff();
            }
        }
    }


}
