﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRTK;

namespace SaloonSpin {
    [System.Serializable]
    public class PistolSoundPool
    {
        public AudioSource audioSource;
        [Header("Звук выстрела")]
        public AudioClip shotSFX;/*звук выстрела*/
        [Header("Звук щелчка при холостом выстреле")]
        public AudioClip dryShotSFX;/*звук щелчка при холостом выстреле*/
        [Header("Звук прокрутки барабана")]
        public AudioClip spinSFX;/*звук прокрутки барабана*/
    }

    public class Pistol : MonoBehaviour
    {
        public static Pistol Instance;
        [SerializeField]
        public Transform bulletSpawnPoint; //точка из которой происходит выстрел
        [SerializeField]
        private ParticleSystem shotVFX;//партиклы выстрела
        [SerializeField]
        private PistolSoundPool pistolSoundPool;//пул звуков которые может издавать пистолет
        [SerializeField]
        private GameObject localBullet;//локальная пуля возле барабана пистолета, для визуализации зарядки
        [SerializeField]
        private PhotonView photonView;
        [SerializeField]
        private PlayerUI_LookAtCamera pistolUI;
        [SerializeField]
        private ShootOnMesh shootOnMesh;
        [SerializeField] private Sprite no_bullets_sprite, no_coins_sprite;
        public bool PistolOnTable { get; set; }//означает что пистолет лежит на столе и готов к тому чтобы его кто-то взял
        public bool IsGunCharged { get; private set; } //есть ли в барабане пистолета пуля

        public bool inUse { get; private set; }
        public bool IsBarrelOpen { get; set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }
        private void Reset()
        {
            Debug.Log("IsGunCharged? " + IsGunCharged);
            Debug.Log("Pistol on table? " + PistolOnTable);
        }
        private void Start()
        {
            GetComponentInChildren<PistolBarrel>().OnHandReload += HandSpin;
            //NetworkManager.Instance.onSyncGameState += InteractableSetter;
            Debug.Log("<color=red>PISTOL START</color>");
        }
        private void OnDisable()
        {
            GetComponentInChildren<PistolBarrel>().OnHandReload -= HandSpin;
            //NetworkManager.Instance.onSyncGameState -= InteractableSetter;
            Debug.Log("<color=red>PISTOL DISABLE</color>");
        }

        private Coroutine interactSetterRoutine;
        public void InteractableSetter(GameState gameState)
        {
            if (interactSetterRoutine != null)
                StopCoroutine(interactSetterRoutine);

            interactSetterRoutine = StartCoroutine(InteractableSetterNumerator(gameState));
        }

        private IEnumerator InteractableSetterNumerator(GameState gameState)
        {
            if (!(bool)PhotonNetwork.CurrentRoom.CustomProperties[RoomPropertiesKey.FirstGamePlayed])
                yield break;

            for(int i = 0; i < 35; i++)
            {
                yield return new WaitForSeconds(.1f);

                if (Pistol.Instance.PistolOnTable)
                    break;
            }

            //yield return new WaitForSeconds(3f);
            string userTurn = gameState.userTurn;
            string myID = PhotonNetwork.LocalPlayer.UserId;
            GetComponent<VRTK.VRTK_InteractableObject>().enabled = myID == userTurn;
            Debug.Log("pistol set access to ID: " + userTurn + " my ID: " + myID);

        }
        [PunRPC]
        private void RPC_EndPistolMoving()
        {
            PistolOnTable = true;
        }

        private Transform lastAimedTarget; // последний трансформ на который был наведен пистолет
        private Coroutine hintsCoroutine;
        private bool hintsDelayRunning;
        private IEnumerator DelayHints()
        {
            hintsDelayRunning = true;
            yield return new WaitForSeconds(1f);

            if(!pistolUI.gameObject.activeSelf)
                pistolUI.gameObject.SetActive(true);

            hintsDelayRunning = false;

        }
        private bool aimAtMyself = false;
        private AbstractPlayer aimingSelfPlayer;
        private void Update()
        {
            //Debug.DrawRay(bulletSpawnPoint.transform.position, bulletSpawnPoint.transform.forward * 6f, Color.cyan);

            if (moving)
            {
                float distCovered = (Time.time - startTime) * speed;

                float fracJourney = distCovered / journeyLength;

                transform.position = Vector3.Lerp(startMarker, GameManager.Instance.tableCenter.position, fracJourney);

                if (Vector3.Distance(transform.position, GameManager.Instance.tableCenter.position) < 0.005f)
                {
                    moving = false;
                    transform.position = GameManager.Instance.tableCenter.position;
                    GetComponent<Rigidbody>().isKinematic = false;
                    if (PhotonNetwork.IsMasterClient)
                        photonView.RPC("RPC_EndPistolMoving", RpcTarget.All);
                }

                return;
            }

            if (!photonView.IsMine)
                return;

            //Debug.LogFormat("inUse is {0}, IsBarrelOpen is {1}, canSpinByHand is {2}", inUse, IsBarrelOpen, canSpinByHand);
            if(inUse && photonView.Owner.UserId == NetworkManager.Instance.gameState.userTurn)//пистолет в руке игрока, чья очередь стрелять проверка наводят ли его на ботов
            {
                if (transform.root.GetComponent<NPC>() != null)
                    return;

                RaycastHit hit;
                int layerMask = 1 << LayerMask.NameToLayer("Head");
                Ray ray = new Ray(bulletSpawnPoint.transform.position, bulletSpawnPoint.transform.forward * 3f);
                Debug.DrawRay(bulletSpawnPoint.transform.position, bulletSpawnPoint.transform.forward * 3f, Color.red);
                if(Physics.Raycast(ray, out hit, 3f, layerMask))
                {
                    if (hit.transform.CompareTag("Head") )
                    {
                        if (hit.transform.root.GetComponent<PhotonView>().Owner == PhotonNetwork.LocalPlayer)
                        {
                            if (!aimAtMyself)
                            {
                                aimingSelfPlayer = hit.transform.GetComponentInParent<AbstractPlayer>();
                                aimingSelfPlayer.playerModel.Head.GetComponentInChildren<Head>().AimAtMyself?.Invoke();
                                aimAtMyself = true;
                            }
                            return;
                        }
                        else if (aimAtMyself)
                        {
                            aimingSelfPlayer.playerModel.Head.GetComponentInChildren<Head>().StopAimAtMyself?.Invoke();
                            aimAtMyself = false;
                        }

                        if (hit.transform.root.CompareTag("NPC"))
                        {
                            if(IsGunCharged)//анимация страха включается только если пистолет заряжен
                                hit.transform.root.GetComponent<NPC>().Fear(true, transform.root.position);

                            if (transform.root.GetComponent<PlayerController>() != null)
                                hit.transform.root.GetComponent<AbstractPlayer>().ShowPlayerInfo(transform.root.GetComponent<PlayerController>().PlayerCameraTransform);

                            else if(lastAimedTarget != null)
                            {
                                var lastAimedPlayer = lastAimedTarget.root.GetComponent<AbstractPlayer>();
                                lastAimedPlayer.HidePlayerInfo();
                            }
                            lastAimedTarget = hit.transform;


                        }
                        else if(hit.transform.root.CompareTag("Player"))
                        {
                            hit.transform.root.GetComponent<AbstractPlayer>().ShowPlayerInfo(transform.root.GetComponent<PlayerController>().PlayerCameraTransform);
                            lastAimedTarget = hit.transform;
                        }

                        
                         if (!IsGunCharged && !pistolUI.gameObject.activeSelf)
                         {
                            try
                            {
                                pistolUI.GetComponentInChildren<Image>().sprite = no_bullets_sprite;
                                pistolUI.Init(transform.root.GetComponent<AbstractPlayer>().playerModel.Head.transform);

                                if(!hintsDelayRunning)
                                    hintsCoroutine = StartCoroutine(DelayHints());
                                
                            }
                            catch
                            {

                            }

                         }
                         else if (NetworkManager.Instance.gameState.GetPlayerState(photonView.Owner.UserId).money == 0
                              && !pistolUI.gameObject.activeSelf)
                        {
                            pistolUI.GetComponentInChildren<Image>().sprite = no_coins_sprite;
                            pistolUI.Init(transform.root.GetComponent<AbstractPlayer>().playerModel.Head.transform);

                            if (!hintsDelayRunning)
                                hintsCoroutine = StartCoroutine(DelayHints());
                        }                       
                    }
                }
                else if (lastAimedTarget != null)
                {

                    var lastAimedPlayer = lastAimedTarget.root.GetComponent<AbstractPlayer>();
                    if (lastAimedPlayer is NPC)
                    {
                        lastAimedTarget.root.GetComponent<NPC>().Fear(false, Vector3.zero);
                        lastAimedTarget.root.GetComponent<NPC>().HidePlayerInfo();
                    }
                    else
                    {
                        lastAimedPlayer.HidePlayerInfo();
                    }
                    
                    if (hintsDelayRunning)
                    {
                        hintsDelayRunning = false;

                        if(hintsCoroutine != null)
                            StopCoroutine(hintsCoroutine);
                    }
                    pistolUI.gameObject.SetActive(false);
                    lastAimedTarget = null;
                }
                else if (aimAtMyself)
                {
                    aimingSelfPlayer.playerModel.Head.GetComponentInChildren<Head>().StopAimAtMyself?.Invoke();
                    aimAtMyself = false;
                }
            }
            else if (!inUse && lastAimedTarget != null)
            {
                var lastAimedPlayer = lastAimedTarget.root.GetComponent<AbstractPlayer>();
                if (lastAimedPlayer is NPC)
                {
                    lastAimedTarget.root.GetComponent<NPC>().Fear(false, Vector3.zero);
                    lastAimedTarget.root.GetComponent<NPC>().HidePlayerInfo();
                }
                else
                {
                    lastAimedPlayer.HidePlayerInfo();
                }

                if (hintsDelayRunning)
                {
                    hintsDelayRunning = false;

                    if (hintsCoroutine != null)
                        StopCoroutine(hintsCoroutine);
                }
                pistolUI.gameObject.SetActive(false);

                lastAimedTarget = null;
            }
            else if (aimAtMyself)
            {
                aimingSelfPlayer.playerModel.Head.GetComponentInChildren<Head>().StopAimAtMyself?.Invoke();
                aimAtMyself = false;
            }
        }
        public void MovePistolOnTable()
        {
            Debug.Log("Move");
            if (PistolOnTable)
                return;

            photonView.RPC("RPC_MovePistolOnTable", RpcTarget.All);
        }
        public void StopMoving()
        {
            photonView.RPC("RPC_StopMoving", RpcTarget.All);
        }
        private IEnumerator MovePistol()
        {
            transform.rotation = Quaternion.Euler(0, 75f, -90f);
            startTime = Time.time;
            journeyLength = Vector3.Distance(transform.position, GameManager.Instance.tableCenter.position);
            GetComponentInParent<Rigidbody>().isKinematic = true;
            moving = true;
            while(moving)
                yield return new WaitForEndOfFrame();

            inUse = false;
        }

        #region Moving variables
        Coroutine movingNumerator;
        float startTime;
        float journeyLength;
        public bool moving { get; private set; }
        float speed = 3f;
        Vector3 startMarker;
        #endregion

        #region Pistol charge & reload
        public void OpenBarrelByHandRotation()
        {
            photonView.RPC("RPC_OpenBarrel", RpcTarget.All);
        }
        public void CloseBarrelByHandRotation()
        {
            photonView.RPC("RPC_CloseBarrel", RpcTarget.All);
        }
        [PunRPC]
        private void RPC_OpenBarrel()
        {
            if (!IsGunCharged)
            {
                GetComponent<Animator>().ResetTrigger("PistolCloseBarrel");
                GetComponent<Animator>().SetTrigger("PistolOpenBarrel");
                IsBarrelOpen = true;
            }
        }
        [PunRPC]
        private void RPC_CloseBarrel()
        {
            GetComponent<Animator>().ResetTrigger("PistolOpenBarrel");
            GetComponent<Animator>().SetTrigger("PistolCloseBarrel");
            IsBarrelOpen = false;
        }
        [PunRPC]
        private void RPC_PistolUncharged()
        {
            IsGunCharged = false;
        }


        public void ChargePistol(Vector3 bulletHoleLocalPosition)//запуск проццесса движения пули в барабан
        {
            var localBulletPosition = localBullet.transform.localPosition;
            localBulletPosition.x = bulletHoleLocalPosition.x;
            localBulletPosition.y = bulletHoleLocalPosition.y;
            photonView.RPC("RPC_ChargePistol", RpcTarget.All, localBulletPosition);
        }
        private IEnumerator BulletMovingToBurrel()//двигает пулю в барабан пистолета
        {
            float length = 1.2f;
            int ticks = 30;
            var oldBulletPostion = localBullet.transform.localPosition;
            var newBulletPosition = oldBulletPostion;
            for (int i = 0; i < ticks; i++)
            {
                newBulletPosition.z -= length / (float)ticks;
                localBullet.transform.localPosition = newBulletPosition;
                yield return null;
            }
            IsGunCharged = true;
            StartCoroutine(DisableLocalBullet(oldBulletPostion));
            this.HandSpinPistol();
        }
        private IEnumerator DisableLocalBullet(Vector3 oldBulletPosition)
        {
            yield return new WaitForSeconds(10f);
            localBullet.SetActive(false);
            localBullet.transform.localPosition = oldBulletPosition;
        }
        private void HandSpin(float HandSpeed)
        {
            Debug.Log("hand spin: canSpinByHand " + canSpinByHand);
            if (canSpinByHand && photonView.IsMine)
            {
                photonView.RPC("SpinSFX", RpcTarget.All, HandSpeed);
                photonView.RPC("StopWaitingForHandSpin", RpcTarget.All);
            }
        }
        private bool canSpinByHand = false; //флаг возможно ручной прокуртки барабана
        private Coroutine waitForSpinByHand, delayMoveOnTable;
        public void HandSpinPistol()//ручная прокрутка барабана
        {
            canSpinByHand = true;
            waitForSpinByHand = StartCoroutine(WaitForHandSpin());
        }
        private IEnumerator WaitForHandSpin()//10 сек дается игроку чтобы прокрутить рукой барабан, иначе он прокручивается сам
        {
            yield return new WaitForSeconds(10f);
            if(canSpinByHand)
            {
                GetComponent<Animator>().ResetTrigger("PistolOpenBarrel");
                GetComponent<Animator>().SetTrigger("PistolCloseBarrel");
                canSpinByHand = false;
                IsGunCharged = true;
                IsBarrelOpen = false;
                yield return new WaitForSeconds(0.5f);
                PistolSpin();
            }
        }
        public void Reload() //Фактическая перезарядка пистолета
        {
            NetworkManager.Instance.gameState.Spin();
        }
        public void PistolSpin()
        {
            if(photonView.IsMine)
                photonView.RPC("SpinSFX", RpcTarget.All, 0.01f);
        }

        public void NPC_Reload()
        {
            GetComponent<Animator>().ResetTrigger("PistolOpenBarrel");
            GetComponent<Animator>().SetTrigger("PistolCloseBarrel");          
            IsGunCharged = true;
        }
        #endregion

        public void SetUser(Player player)
        {
            photonView.TransferOwnership(player);
            inUse = true;
            PistolOnTable = false;
            
            if(photonView.Owner.UserId == NetworkManager.Instance.gameState.userTurn) //если пистолет взял игрок, чья очередь стрелять - не нкжно принудительно возвращать пистолет на стол
            {
                photonView.RPC("RPC_DoNotReturnPistolOnTable", RpcTarget.All);
            }
        }
        private void ShootSaloonNPC(bool isBullet)
        {
            if (!isBullet)
                return;

            if (Physics.Raycast(bulletSpawnPoint.position, bulletSpawnPoint.forward * 50, out var hit, 50f, 1 << LayerMask.NameToLayer("Head")))
            {
                if (hit.transform.CompareTag("Torso"))
                {
                    Waitress.Instance.GetComponent<PhotonView>().RPC("Death", RpcTarget.MasterClient);
                }

                if (hit.transform.CompareTag("Pianist"))
                {
                    Pianist.Instance.GetComponent<PhotonView>().RPC("Death", RpcTarget.MasterClient);
                }

                if (hit.transform.CompareTag("Barmen"))
                {
                    Barmen.Instance.GetComponent<PhotonView>().RPC("Death", RpcTarget.MasterClient);
                }
            }
        }
        private IEnumerator ShotNumerator(AbstractPlayer player, bool NPC_itselfShooting = false)
        {
            yield return new WaitForEndOfFrame();
            bool isBullet = NetworkManager.Instance.gameState.IsBullet();
            string msg = string.Format("<color=blue>{0} shot is {1}, gameState.currIndex: {2}, gameState.bulletIndex: {3}</color>",
                player.UserID, isBullet, NetworkManager.Instance.gameState.currIndex, NetworkManager.Instance.gameState.bulletIndex);

            Debug.Log(msg);
            /***************************************************/
            var oldPistolRotation = transform.rotation;
            if (NPC_itselfShooting)
            {
                transform.LookAt(player.playerModel.Head.transform);
            }

            PlayerState myPlayerState = NetworkManager.Instance.gameState.GetPlayerState(player.UserID);
            int layerMask = 1 << LayerMask.NameToLayer("Head");
            RaycastHit hit;
            Ray ray = new Ray(bulletSpawnPoint.position, bulletSpawnPoint.forward * 10);
            

            if (Physics.Raycast(ray, out hit, 10f, layerMask))
            {

                if (hit.transform.CompareTag("Head"))
                {
                    if (NPC_itselfShooting)
                    {
                        //yield return new WaitForEndOfFrame();
                        transform.rotation = oldPistolRotation;
                    }

                    if (isBullet)//если выстрел был в нпс, нужно запомнить направление пули, чтобы задать направление полета головы
                    {
                        var _player = hit.transform.root.GetComponent<AbstractPlayer>();
                        if(player is NPC && player.Equals(_player))//если нпс убивает сам себя, запоминается позиция точки откуда будет падать пистолет
                        {
                            photonView.RPC("SavePointDropFrom", RpcTarget.All, transform.position, transform.rotation);
                        }
                        if(_player != null)
                        {
                            _player.PublicDeath(bulletSpawnPoint.forward);
                        }
                    }

                    if (hit.transform.root.name == player.transform.root.name)
                    {
                        print("Выстрел себе в голову");
                        /*Выстрел себе в голову*/
                        if (!isBullet)
                        {
                            myPlayerState.money += 1;
                            player.AddCoinsToPlayer(1);
                        }
                        else
                        {
                            photonView.RPC("RPC_ShotInHand", RpcTarget.All);
                            myPlayerState.IsKilled = true;
                            myPlayerState.money = 0;
                            transform.root.GetComponent<AbstractPlayer>().RemoveCoin(-1);
                            print(player.transform.root.name + " KILLED!");
                        }
                    }
                    else
                    {
                        /*Выстрел другому игроку в голову*/
                        print("Выстрел другому игроку в голову");
                        if (myPlayerState.money > 0)
                        {
                            if (!isBullet)
                            {
                                myPlayerState.money = 0;
                                player.RemoveCoin(-1);
                                var shotDownPlayer = hit.transform.GetComponentInParent<AbstractPlayer>();
                                if (shotDownPlayer is NPC)
                                {
                                    var shootedMePlayers = NetworkManager.Instance.gameState.GetPlayerState(hit.transform.GetComponentInParent<AbstractPlayer>().UserID).playerWhoShotMe;
                                    if (shootedMePlayers.Length > 0)
                                    {
                                        bool alreadyContains = false; //проверка записан ли уже в массив игрок который в меня стрелял
                                        for (int i = 0; i < shootedMePlayers.Length; i++)
                                        {
                                            if (shootedMePlayers[i] == myPlayerState.UserID)
                                            {
                                                alreadyContains = true;
                                                break;
                                            }
                                        }
                                        if (!alreadyContains) //если не записан, записываем
                                        {
                                            var tmpMass = new string[shootedMePlayers.Length + 1];
                                            tmpMass[tmpMass.Length - 1] = myPlayerState.UserID;
                                            shootedMePlayers = tmpMass;
                                        }
                                    }
                                    else
                                    {
                                        shootedMePlayers = new string[1];
                                        shootedMePlayers[0] = myPlayerState.UserID;
                                    }
                                    NetworkManager.Instance.gameState.GetPlayerState(hit.transform.GetComponentInParent<AbstractPlayer>().UserID).playerWhoShotMe = shootedMePlayers;
                                }
                            }
                            else
                            {
                                photonView.RPC("RPC_ShotInHand", RpcTarget.All);
                                NetworkManager.Instance.gameState.GetPlayerState(hit.transform.GetComponentInParent<AbstractPlayer>().UserID).IsKilled = true;
                                myPlayerState.money += NetworkManager.Instance.gameState.GetPlayerState(hit.transform.GetComponentInParent<AbstractPlayer>().UserID).money;
                                player.AddCoinsToPlayer(NetworkManager.Instance.gameState.GetPlayerState(hit.transform.GetComponentInParent<AbstractPlayer>().UserID).money);
                            }
                        }
                        else
                        {
                            yield break;
                        }
                    }
                }
                else
                {
                    /*Выстрел не в игроков*/
                    print("Выстрел не в игроков");
                    if (myPlayerState.money > 0)
                    {
                        if (isBullet)
                        {
                            photonView.RPC("RPC_ShotInHand", RpcTarget.All);
                            myPlayerState.money += 1;
                            player.AddCoinsToPlayer(1);
                        }
                        else
                        {
                            myPlayerState.money -= 1;
                            player.RemoveCoin(1);
                        }

                    }
                    else
                    {
                        yield break;
                    }
                }
            }
            else
            {
                /*Выстрел в воздух*/
                print("Выстрел в воздух");
                //Debug.Break();
                if (myPlayerState.money > 0)
                {
                    if (isBullet)
                    {
                        photonView.RPC("RPC_ShotInHand", RpcTarget.All);
                        myPlayerState.money += 1;
                        player.AddCoinsToPlayer(1);
                    }
                    else
                    {
                        myPlayerState.money -= 1;
                        player.RemoveCoin(1);
                    }
                }
                else
                {
                    yield break;
                }
            }

            photonView.RPC("RPC_ShotAnim", RpcTarget.All, isBullet);

            if (player is PlayerController)
            {
                var _player = (PlayerController)player;
                _player.gunInHand = false;
            }

            NetworkManager.Instance.gameState.currIndex++;
            NetworkManager.Instance.gameState.SwitchTurnToNextUser();
            Invoke("DelayRequestToSync", .05f);
            ShootSaloonNPC(isBullet);
            if (player is PlayerController) //если стрелял не нпс, через 1.5сек принудительно пистоле кладется на стол, нпс положит сам (если не застрелится)
            {
                //Invoke("MovePistolOnTable", 3.5f);
                if (delayMoveOnTable != null)
                    StopCoroutine(delayMoveOnTable);

                delayMoveOnTable = StartCoroutine(DelayMovePistolOnTable(3.5f));
            }
            else if (player is NPC && isBullet)
            {
                if (delayMoveOnTable != null)
                    StopCoroutine(delayMoveOnTable);

                delayMoveOnTable = StartCoroutine(DelayMovePistolOnTable(3.5f));
            }


            

        }
        public void Shot(AbstractPlayer player, bool NPCitseldShooting = false) //Обработка события выстрела
        {
            if (player is NPC)
            {
                StartCoroutine(ShotNumerator(player, NPCitseldShooting));
            }
            else
            {
                if (canSpinByHand) //пока есть возможность перезарядить пистолет рукой, стрелять нельзя
                    return;

                if (IsBarrelOpen) //нельзя стрелять пока барабан пистолета открыт
                    return;

                if (!IsGunCharged)
                    return;

                StartCoroutine(ShotNumerator(player));
            }

        }
        private IEnumerator DelayMovePistolOnTable(float delay)
        {
            yield return new WaitForSeconds(delay);
            MovePistolOnTable();
        }
        private void DelayRequestToSync()
        {
            NetworkManager.Instance.RequestToSync();
        }
        private bool isBulletWas = false; //был ли выстрел, для CallTo_RPC_ShotSFX()
        public void CallTo_RPC_ShotSFX() //sfx выстрела, вызывается из анимации выстрела
        {
            if(photonView.IsMine)
                photonView.RPC("RPC_ShotSFX", RpcTarget.All, isBulletWas);
        }
        //public void DelayPutOnTable()
        //{
        //    if (transform.GetComponentInParent<NPC>() == null)
        //    {
        //        GetComponent<GrabbaleObjects>().StopGrab(false);
        //        photonView.RPC("RPC_PutPistolOnTable", RpcTarget.All);
        //    }
        //}
        public void DelayPutOnTableNPC()
        {
            photonView.RPC("RPC_PutPistolOnTable", RpcTarget.All);
        }
        public void BarellOpened() //isBarrelOpen запрещает стрелять если открыт барабн пистолета. вызывается ивентом анимации OpenBarrel
        {
            //if(photonView.IsMine)
            //{
            //    photonView.RPC("CloseOpenBarrel", RpcTarget.All ,true, false);
            //}
            IsBarrelOpen = true;
            IsGunCharged = false;
        }
        public void BarrelClosed()//isBarrelOpen разрешает стрелять если закрыт барабан пистолета. вызывается ивентом анимации CloseBarrel
        {
            if (photonView.IsMine)
            {
                photonView.RPC("CloseOpenBarrel", RpcTarget.All, false, true);
            }
        }

        public void PlayerUngrabbedPistol()
        {
            photonView.RPC("RPC_PlayerUngrabbedPistol", RpcTarget.All);
        }
        private Vector3 pointDropFromPosition = Vector3.zero;
        private Quaternion pointDropFromRotation = Quaternion.identity;

        #region RPC methods
        [PunRPC]
        private void RPC_ShotInHand()
        {
            shootOnMesh.Shoot();
        }
        [PunRPC]
        private void SavePointDropFrom(Vector3 pos, Quaternion rot)
        {
            pointDropFromPosition = pos;
            pointDropFromRotation = rot;
        }
        [PunRPC]
        private void RPC_DropPistol()
        {
            transform.SetParent(null, true);
            transform.position = pointDropFromPosition;
            transform.rotation = pointDropFromRotation;
            Pistol.Instance.GetComponent<GrabbaleObjects>().IsGrabbed = false;
            Pistol.Instance.GetComponent<PhotonTransformView>().enabled = true;
            GetComponent<Rigidbody>().isKinematic = false;
        }
        [PunRPC]
        private void RPC_SetInteractAccess()
        {
            //string ut = NetworkManager.Instance.gameState.userTurn;
            //if (!NetworkManager.Instance.gameState.GetPlayerState(ut).isNPC && PhotonNetwork.LocalPlayer.UserId == ut)//если очередь стрелять не у нпс, игрокам возврщается возможность взаимодействовать с пистолетом
            //    Pistol.Instance.GetComponent<VRTK.VRTK_InteractableObject>().isGrabbable = true;
        }
        [PunRPC]
        private void RPC_DoNotReturnPistolOnTable()
        {
            if (delayMoveOnTable != null)
                StopCoroutine(delayMoveOnTable);
        }
        [PunRPC]
        private void RPC_PlayerUngrabbedPistol()
        {
            inUse = false;
        }
        [PunRPC]
        private void RPC_StopMoving()
        {
            if (delayMoveOnTable != null)
                StopCoroutine(delayMoveOnTable);
            if (movingNumerator != null)
                StopCoroutine(movingNumerator);

            //StopCoroutine(movingNumerator);
            moving = false;
            //GetComponent<Rigidbody>().isKinematic = false;
        }
        [PunRPC]
        private void RPC_MovePistolOnTable()
        {
            if (PistolOnTable)
                return;

            GetComponent<GrabbaleObjects>().StopGrab(false);
            GetComponent<VRTK.VRTK_InteractableObject>().Unhighlight();
            GetComponent<PhotonRigidbodyView>().enabled = true;
            GetComponent<PhotonTransformView>().enabled = true;

            transform.SetParent(null);
            startMarker = transform.position;

            if (movingNumerator != null)
                StopCoroutine(movingNumerator);
            movingNumerator = StartCoroutine(MovePistol());
        }
        [PunRPC]
        private void RPC_ShotAnim(bool isShotWas)
        {
            this.isBulletWas = isShotWas;
            GetComponent<Animator>().SetTrigger("PistolMakeShot");
        }
        [PunRPC]
        private void CloseOpenBarrel(bool IsBarrelOpen, bool IsGunCharged)
        {
            //this.IsBarrelOpen = IsBarrelOpen;
            //this.IsGunCharged = IsGunCharged;
        }
        [PunRPC]
        private void StopWaitingForHandSpin()
        {
            canSpinByHand = false;
            if (waitForSpinByHand != null)
                StopCoroutine(waitForSpinByHand);

            waitForSpinByHand = null;
        }        
        [PunRPC]
        private void SpinSFX(float time, PhotonMessageInfo info)
        {

            //print("spinSFX, sender: " + info.Sender.UserId);

            if (time > 0.2f)
            {
                GetComponent<Animator>().SetTrigger("ReloadPistolSlow");
            }
            else if(0.1f <= time && time <= 0.2f)
            {
                GetComponent<Animator>().SetTrigger("ReloadPistolMidle");
            }
            else
            {
                GetComponent<Animator>().SetTrigger("ReloadPistolFast");
            }
        }
        [PunRPC]
        public void RPC_SpinSound()
        {
            pistolSoundPool.audioSource.clip = pistolSoundPool.spinSFX;
            pistolSoundPool.audioSource.Play();
        }
        [PunRPC]
        private void RPC_ChargePistol(Vector3 bulletHoleLocalPosition)
        {
            this.localBullet.transform.localPosition = bulletHoleLocalPosition;
            this.localBullet.SetActive(true);
            this.IsGunCharged = true;
            this.IsBarrelOpen = false;
            StartCoroutine(BulletMovingToBurrel());
        }
        [PunRPC]
        private void RPC_PutPistolOnTable()
        {
            inUse = false;
            PistolOnTable = true;
            transform.position = GameManager.Instance.tableCenter.position;
            transform.rotation = Quaternion.Euler(0, 75f, -90f);
        }
        [PunRPC]
        void RPC_ShotSFX(bool isBulet) /*Воспроизводит звук выстрела у всех игроков*/
        {
            if (isBulet)
            {
                pistolSoundPool.audioSource.clip = pistolSoundPool.shotSFX;
                pistolSoundPool.audioSource.Play();
                shotVFX.Play();

                IsGunCharged = false;
                IsBarrelOpen = true;
            }
            else
            {
                pistolSoundPool.audioSource.clip = pistolSoundPool.dryShotSFX;
                pistolSoundPool.audioSource.Play();
            }

        }
        [PunRPC]
        private void RPC_PrepareToCharge()
        {
            IsGunCharged = false;
        }
        #endregion


    }
}
