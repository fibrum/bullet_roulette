﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public delegate void HandRelodad(float handSpeed);
    public class PistolBarrel : MonoBehaviour
    {
        private float timer = 0;
        private bool countTime = false;
        private Transform pistolRoot;
        private PhotonView photonView;
        GrabbaleObjects go;
        public event HandRelodad OnHandReload;
        private void Awake()
        {
            photonView = GetComponentInParent<PhotonView>();
            pistolRoot = transform.root;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (this.pistolRoot.parent == null)
                return;

            var player = pistolRoot.GetComponentInParent<AbstractPlayer>();
            if (player is NPC)
                return;
            
            if (other.CompareTag("Hand"))
            {

                bool isLeftHand = pistolRoot.GetComponentInParent<PlayerHand>().Hand == Hand.Left;
                bool otherIsLeftHand = other.transform.GetComponentInParent<PlayerHand>().Hand == Hand.Left;

                bool isRightHand = pistolRoot.GetComponentInParent<PlayerHand>().Hand == Hand.Right;
                bool otherIsRightHand = other.transform.GetComponentInParent<PlayerHand>().Hand == Hand.Right;

                if ((isLeftHand && otherIsRightHand) || (isRightHand && otherIsLeftHand)) //проверка что рука крутящая барабан не та же что держит пистолет
                {
                    timer = 0;
                    countTime = true;
                    return;
                }
            }

            if (other.GetComponent<Table>()) {
                Debug.Log("Table trigger enter");
                timer = 0;
                countTime = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (!countTime)
                return;

            if (pistolRoot.GetComponentInParent<AbstractPlayer>() != null)
            {
                if (NetworkManager.Instance.gameState.userTurn != pistolRoot.GetComponentInParent<AbstractPlayer>().UserID)
                    return;
            }

            if (other.CompareTag("Hand"))
            {

                /*
                 * timer > 0.10 - медлеено
                 * 0.05 < timer && timer < 0.10 - средняя скорость
                 * timer < 0.05 - быстро
                 * */

                countTime = false;
                OnHandReload?.Invoke(timer);
                return;
            }
            if (other.GetComponent<Table>())
            {
                Debug.Log("Table trigger exit");
                countTime = false;
                OnHandReload?.Invoke(timer);
            }
        }

        private void Update()
        {
            if(countTime)
            {
                timer += Time.deltaTime;
            }
        }


        [SerializeField] private FakeMechanismCreate fake;
        [SerializeField] private float val = -5;
        public void Action() {
            
            if (MainPistol.Instance.IsGunCharged == false && MainPistol.Instance.IsBarellOpenedByJoint == false)
            {
                Debug.Log("Action AddTorque");
                var rb=fake.GetComponentInChildren<Rigidbody>();
                    rb.AddTorque(val * rb.transform.right);
                if (opener == null) opener=StartCoroutine(OpenBarrel(rb.transform));
                    //MainPistol.Instance.OpenBarrel();
                    return;
            }
            if (MainPistol.Instance.IsGunCharged == true && MainPistol.Instance.IsBarellOpenedByJoint == true)
            {
                MainPistol.Instance.CloseBarrel();
                return;
            }
            if (MainPistol.Instance.IsGunCharged == true && MainPistol.Instance.IsBarrelOpened == false)
            {
                MainPistol.Instance.PlayerMakedHandSpin(0f);
                return;
            }
        }

        private Coroutine opener;
        public IEnumerator OpenBarrel(Transform tr) {
            float startTime = Time.time;
            while (Time.time-startTime<0.25f) {
                tr.localEulerAngles=new Vector3((Time.time - startTime)/0.25f*-70,0f,0f);
                yield return null;
            }

            opener = null;
        }
    }
}
