﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SaloonSpin
{
    public class PistolBarrelMechanism : MonoBehaviour
    {
        public UnityEvent OnBarrelClosed;
        private Coroutine waitForBarrelClosing;
        private Vector3 defaultLocalPosition;
        private HingeJoint joint;
        private bool resetRotation;
        public bool ResetRotation {
            get { return resetRotation; }
            set
            {
                resetRotation = value;
            }
        }
        private PhotonView pv;
        [SerializeField] private Transform folowing;
        public Transform Target {
            set { folowing = value; }
        }
        private void Start()
        {

            pv = GetComponent<PhotonView>();
            joint = GetComponent<HingeJoint>();
            pistol = GetComponentInParent<MainPistol>();
            defaultLocalPosition = transform.localPosition;
        }
        public void WaitForBarrelClosing()
        {
            waitForBarrelClosing = StartCoroutine(WaitForBarrelClose());
            OnBarrelClosed.AddListener(() => {
                if (waitForBarrelClosing!=null)
                    StopCoroutine(waitForBarrelClosing);
            });
        }
        private IEnumerator WaitForBarrelClose()
        {
            while(true)
            {
                if(Vector3.Angle(transform.forward,transform.parent.forward) < 1f)
                {
                    //Debug.LogError("WaitForBarrelClose"+Vector3.(transform.localEulerAngles, transform.forward));
                    ResetRotation = true;
                    transform.localEulerAngles = Vector3.zero;
                    transform.localPosition = defaultLocalPosition;
                    OnBarrelClosed?.Invoke();
                    break;
                }
                yield return new WaitForEndOfFrame();
            }
        }

        private MainPistol pistol;
        private void Update()
        {
            if (pv.IsMine)
            {
                if (!ResetRotation)
                {
                    transform.localRotation = folowing.localRotation;
                }
                else if(pistol.pistolHolder != null)
                {

                    transform.localRotation = Quaternion.identity;
                }
            }
        }
    }
}
