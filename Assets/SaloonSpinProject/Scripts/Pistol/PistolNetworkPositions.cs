﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class PistolNetworkPositions : MonoBehaviour
    {
        public List<PistolNetworkPosition> pistolNetworkPositions = new List<PistolNetworkPosition>();

        public Vector3 GetPosition(PlayerType player)
        {
            return pistolNetworkPositions.Find((pos) => pos.playerType == player).localPostion;
        }
        public Quaternion GetRotation(PlayerType player)
        {
            return pistolNetworkPositions.Find((pos) => pos.playerType == player).localRotation;
        }
        public Vector3 EulerAngels(PlayerType player)
        {
            return pistolNetworkPositions.Find((pos) => pos.playerType == player).localRotationEuler;
        }


        [ContextMenu("SaveNetwork position")]
        public void SavePositions()
        {
            var type = GetComponentInParent<PlayerController>().typeOfPlayer;
            pistolNetworkPositions.Find((pos) => pos.playerType == type).localPostion = GetComponentInChildren<MainPistol>().transform.localPosition;
            pistolNetworkPositions.Find((pos) => pos.playerType == type).localRotation = GetComponentInChildren<MainPistol>().transform.localRotation;

        }
        [ContextMenu("Save Network Euler")]
        public void ToEuler()
        {
            var type = GetComponentInParent<PlayerController>().typeOfPlayer;
            pistolNetworkPositions.Find((pos) => pos.playerType == type).localRotationEuler = GetComponentInChildren<MainPistol>().transform.localEulerAngles;
        }
    }

    [System.Serializable]
    public class PistolNetworkPosition
    {
        public PlayerType playerType;
        public Vector3 localPostion;
        public Quaternion localRotation;
        public Vector3 localRotationEuler;
    }
}
