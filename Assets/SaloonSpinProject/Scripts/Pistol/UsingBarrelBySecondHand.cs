﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class UsingBarrelBySecondHand : VRTK.SecondaryControllerGrabActions.VRTK_AxisScaleGrabAction
    {
        private List<float> list = new List<float>();
        private float prevMagnitude = 0f;
        private event HandRotationDelegate rotateLeft, rotateRight;

        private float bottomBound = 1f, topBound = 2f;
        
        private void Start()
        {
            
        }
        private void OnDisable()
        {
            
        }

        public override void ProcessUpdate() {
            
            if (secondaryGrabbingObject != null) {
                Debug.Log($"<color=blue>ProcessUpdate {secondaryGrabbingObject != null} {primaryGrabbingObject!=null}</color>");
            if ((secondaryGrabbingObject.controllerEvents.gripPressed||secondaryGrabbingObject.controllerEvents.triggerPressed)) {
                if (MainPistol.Instance.IsGunCharged == false&&MainPistol.Instance.IsBarellOpenedByJoint==false) {
                    MainPistol.Instance.OpenBarrel();
                }
                if (MainPistol.Instance.IsGunCharged == true && MainPistol.Instance.IsBarellOpenedByJoint == true)
                {
                    MainPistol.Instance.CloseBarrel();
                }
                if (MainPistol.Instance.IsGunCharged == true && MainPistol.Instance.IsBarrelOpened==false)
                {
                    MainPistol.Instance.PlayerMakedHandSpin(0f);
                }
            }
            }
        }

        protected override void UniformScale()
        {
            float adjustedLength = (grabbedObject.transform.position - secondaryGrabbingObject.transform.position).magnitude;
            float adjustedScale = initialScaleFactor * adjustedLength;
            
            if (adjustedScale > 1 && adjustedScale < 1.4)
                rotateRight?.Invoke();

            if (adjustedScale > 1.4)
                rotateLeft?.Invoke();
        }

        private void CheckSum(float delta)
        {
            if (list.Count > 90)
                list.RemoveAt(0);

            list.Add(delta);

            float sum = 0f;
            for(int i = 0; i < list.Count; i++)
            {
                sum += list[i];
            }

            if (sum > 0.1)
            {
                rotateLeft?.Invoke();
            }

            if (sum < -0.1)
            {
                rotateRight?.Invoke();
            }
        }
    }

}