﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public abstract class AbstractPlayer : MonoBehaviour
    {
        protected PhotonView photonView;
        public bool alive = true;//жив ли нпс
        public  PhotonView PlayerPhotonView {
            get { return photonView; }
        }
        public PlayerUI_LookAtCamera PlayerInfo { get { return playerInfoUI; } }
        public PlayerType typeOfPlayer;
        [SerializeField]
        protected PlayerUI_LookAtCamera playerInfoUI;
        [SerializeField]
        public PlayerParts playerModel; //модель игрока
        [SerializeField] private GameObject coinPrefab;
        [SerializeField] private bool coinsLocalOnly;
        public string UserID { get; set; } //Id игрока
        public List<GameObject> playerCoins = new List<GameObject>();

        public void InitShowingInfo(Transform target)
        {
            playerInfoUI.Init(target, true);
        }
        //private IEnumerator DelayInitLookInfo()
        //{
        //    yield return new WaitForSeconds(1f);
        //    foreach (var p in FindObjectsOfType<AbstractPlayer>())
        //    {
        //        if (p is PlayerController && p != this)
        //        {
        //            var _p = (PlayerController)p;
        //            playerInfoUI.Init(_p.PlayerCameraTransform);
        //        }
        //    }
        //}
        public abstract void ShowPlayer();
        public abstract void HidePlayer(bool withoutRagdall = false);
        public abstract void RetunCardOnTable();
        public void ShowPlayerInfo(Transform lookTarget)
        {
            if (playerInfoUI.MoneyEnabled)
                return;

            if (!NetworkManager.Instance.gameState.GetPlayerState(UserID).IsKilled)
            {
                playerModel.Head.GetComponentInChildren<Head>().AimingEffectTurnOn();
            }
            var myState = NetworkManager.Instance.gameState.GetPlayerState(this.UserID);
            //playerInfoUI.GetComponentInChildren<UnityEngine.UI.Text>().text = this.gameObject.name + "\n" + myState.money.ToString() + "$";
            //playerInfoUI.Init(lookTarget);
            //playerInfoUI.gameObject.SetActive(true);
            playerInfoUI.EnableMoney(myState.money, true);
            //PlayingCardsManager.Instance.EnablingCardEffect(UserID, false);
        }
        public void HidePlayerInfo()
        {
            playerInfoUI.EnableMoney(0);
            //playerInfoUI.gameObject.SetActive(false);
            playerModel.Head.GetComponentInChildren<Head>().AimingEffectTurnOff();
            //PlayingCardsManager.Instance.EnablingCardEffect(UserID, true);
        }

        public void AddCoinsToPlayer(int count)
        {
            Coins.Instance.CreateCoin(UserID, count);
        }
       

        public void RemoveCoin(int count)
        {
            Coins.Instance.RemoveCoin(UserID, count);
        }

        protected Vector3 moveHeadAfterDeathDir;
        public void PublicDeath(Vector3 shootingDirection)
        {
            photonView.RPC("RPC_RememberPistolDirection", RpcTarget.All, shootingDirection);
        }
    }
}
