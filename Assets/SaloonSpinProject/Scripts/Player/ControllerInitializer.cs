﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
namespace SaloonSpin
{
    public class ControllerInitializer : MonoBehaviour
    {
        [SerializeField]
        private Transform cameraRig;
        [SerializeField]
        private VRTK_SDKSetup vRTK;
        [SerializeField]
        private PlayerController playerController;
        [SerializeField]
        private FakePlayerController fakeplayerController;
        [SerializeField]
        private Transform centerEyeAnchors;
        [SerializeField]
        private Vector3 localOffset;
        [SerializeField]
        private PhotonView photonView;
        private void Start() {
            if (fakeplayerController) {
                fakeplayerController.sDKSetup = vRTK;
                return;}
            if (photonView.IsMine)
            {
                playerController.InitializeController(vRTK, ButtonInput);

                foreach (var handController in playerController.GetComponentsInChildren<HandAnimatorController>(true))
                {
                    handController.GetThumnstickInputEvent += ResetPlayerPostion;
                    if (handController.Hand == Hand.Right)
                    {
                        handController.A_ButtonInputEvent += EnablePlayerCanvas;
                        handController.B_ButtonInputEvent += ButtonInput;
                    }
                }
            }
            else
            {
                var cameras = transform.GetComponentsInChildren<Camera>();
                foreach (var cam in cameras)
                    cam.enabled = false;

                GetComponent<VRTK.VRTK_SDKSetup>().enabled = false;
            }

        }

        private void ButtonInput(Hand h, bool b)
        {          
            StartCoroutine(ResetPosition(playerController.UserID));
        }

        private bool leftPressedDown, rightPressedDown;

        private void ResetPlayerPostion(Hand hand, bool downPress)
        {
            switch (hand)
            {
                case Hand.Left:
                    leftPressedDown = downPress;
                    break;
                case Hand.Right:
                    rightPressedDown = downPress;
                    break;
            }

            if (rightPressedDown && leftPressedDown)
                ForceRecetPos(playerController.UserID);
        }

        public void ForceRecetPos(string id,float waitTime=0f) {
            if(waitTime==0f)
                StartCoroutine(ResetPosition(id));
            else {
                StartCoroutine(waitAndReset(id, waitTime));
            }
        }

        private IEnumerator waitAndReset(string id,float waitTime = 0f) {
            yield return new WaitForSeconds(waitTime);
            yield return ResetPosition(id);
        }


        private void EnablePlayerCanvas(Hand hand, bool downPress)
        {
            PlayerGameplayCanvas.Instance.InitCanvas(GetComponentInParent<PlayerController>(), centerEyeAnchors.position);
        }
        private IEnumerator ResetPosition(string UserID)
        {
            yield return new WaitForEndOfFrame();
            //Vector3 lookPos = GameManager.Instance.tableCenter.position - centerEyeAnchors.position;
            //lookPos.y = 0;
            //cameraRig.rotation = Quaternion.LookRotation(lookPos);
            TimeOfReset = Time.time;
            cameraRig.localEulerAngles = new Vector3(0f, -centerEyeAnchors.localEulerAngles.y, 0f);

            var difference = GameManager.Instance.playersPositions[NetworkManager.Instance.gameState.GetPos(UserID)].playerPlace.position - centerEyeAnchors.position;
            difference.y += 0.13f;
            cameraRig.position += difference;
            
        }

        public static float TimeOfReset;
    }
}