﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.XR;
using Valve.VR;

namespace SaloonSpin {
    public delegate void ControllersInput(Hand hand, bool downPress);
    public class HandAnimatorController : MonoBehaviour {
        [SerializeField] private Hand hand = Hand.Right;
        [SerializeField] private Animator anim;
        [SerializeField] private Photon.Pun.PhotonView photonView;
        [SerializeField] private SteamVR_TrackedObject steamTracker;
        private int animGrabTouchParamId;
        private int animGrabPressParamId;
        private int animTriggerParamId;
        private int animStickParamId;
        private bool oculus;

        public event ControllersInput GetThumnstickInputEvent, A_ButtonInputEvent, B_ButtonInputEvent;
        public event ControllersInput TriggerInput;

        public Hand Hand { get { return hand; } }
        private void Awake() {
            if(anim==null)anim = GetComponent<Animator>();
            animGrabTouchParamId=Animator.StringToHash("GrabTouch");
            animGrabPressParamId = Animator.StringToHash("GrabPress");
            animTriggerParamId = Animator.StringToHash("Trigger");
            animStickParamId = Animator.StringToHash("Stick");
            oculus = XRSettings.loadedDeviceName == "Oculus";
        }
        private OVRInput.Controller controller;
        private bool stickInput, grabTouchInput, grabPressInput, triggerInput;
        private void Update() {
            if(PhotonNetwork.IsConnected)
                if (photonView&&!photonView.IsMine)
                    return;

            if (oculus) {

                controller = hand == Hand.Right ? OVRInput.Controller.RTouch : OVRInput.Controller.LTouch;
                
                if(OVRInput.GetDown(OVRInput.Button.PrimaryThumbstick, controller))
                {
                    GetThumnstickInputEvent?.Invoke(hand, true);
                }
                if (OVRInput.GetUp(OVRInput.Button.PrimaryThumbstick, controller))
                {
                    GetThumnstickInputEvent?.Invoke(hand, false);
                }
                if(OVRInput.GetDown(OVRInput.Button.One))
                {
                    A_ButtonInputEvent?.Invoke(Hand.Right, true);
                }
                if (OVRInput.GetDown(OVRInput.Button.Two))
                {
                    B_ButtonInputEvent?.Invoke(Hand.Right, true);
                }

                stickInput = OVRInput.Get(OVRInput.Touch.One, controller) || OVRInput.Get(OVRInput.Touch.Two, controller) ||
                             OVRInput.Get(OVRInput.Touch.PrimaryThumbstick, controller);
                anim.SetBool(animStickParamId,stickInput);

                grabTouchInput = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, controller) > 0.05f;
                anim.SetBool(animGrabTouchParamId,grabTouchInput);

                grabPressInput = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, controller)>0.5f;
                anim.SetBool(animGrabPressParamId, grabPressInput);

                triggerInput = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, controller) > 0.35f;
                anim.SetBool(animTriggerParamId, triggerInput);
                if (triggerInput) TriggerInput?.Invoke(hand, true);

            } else {
                if (steamTracker) {
                    SteamVR_Controller.Device dev = SteamVR_Controller.Input((int)steamTracker.index);

                    if(dev.GetPressDown(EVRButtonId.k_EButton_SteamVR_Touchpad))
                    {
                        GetThumnstickInputEvent?.Invoke(hand, true);
                    }
                    if (dev.GetPressUp(EVRButtonId.k_EButton_SteamVR_Touchpad))
                    {
                        GetThumnstickInputEvent?.Invoke(hand, false);
                    }
                    if(dev.GetPressDown(EVRButtonId.k_EButton_ApplicationMenu))
                    {
                        A_ButtonInputEvent?.Invoke(hand, true);
                    }
                    stickInput = dev.GetTouch(EVRButtonId.k_EButton_SteamVR_Touchpad);
                    anim.SetBool(animStickParamId, stickInput);

                    grabTouchInput = dev.GetPress(EVRButtonId.k_EButton_Grip); 
                    anim.SetBool(animGrabTouchParamId, grabTouchInput);

                    grabPressInput = dev.GetPress(EVRButtonId.k_EButton_Grip);
                    anim.SetBool(animGrabPressParamId, grabPressInput);

                    triggerInput = dev.GetAxis(EVRButtonId.k_EButton_SteamVR_Trigger).x > 0.35f;
                    anim.SetBool(animTriggerParamId, triggerInput);

                    if (triggerInput) TriggerInput?.Invoke(hand, true);
                }
            }
            
        }

        private void Reset() {
            anim = GetComponent<Animator>();
        }

    }
}