﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using VRTK;

namespace SaloonSpin
{
    public class HandPointerActivator : MonoBehaviour
    {
        public List<ActivationBtnSetter> activationBtnSetters = new List<ActivationBtnSetter>();
        [SerializeField] private VRTK.VRTK_Pointer pointer;
        public VRTK.VRTK_Pointer Pointer => pointer;
        private void Start()
        {
            int index = activationBtnSetters.FindIndex((d) => d.Device == XRSettings.loadedDeviceName);
            if(index == -1)
            {
                Debug.LogError("loaded device not found");
            }
            else
            {
                pointer.activationButton = activationBtnSetters[index].ActivationButton;
            }
            
        }

        public void TurnOffPointer() {
            StartCoroutine(turenPointerOff());

        }

        private IEnumerator turenPointerOff() {
            pointer.Toggle(false);
            yield return new WaitForSeconds(0.1f);
            pointer.Toggle(false);

            pointer.enabled = false;
            yield return new WaitForSeconds(0.1f);
            pointer.enabled = true;
                Debug.Log($"<color=blue>turn {pointer.IsPointerActive()}</color>");
        }
    }

    [System.Serializable]
    public class ActivationBtnSetter
    {
        public string Device;
        public VRTK_ControllerEvents.ButtonAlias ActivationButton;
    }
}