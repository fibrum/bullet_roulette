﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using VRTK;

namespace SaloonSpin
{
    public class PlayerController : AbstractPlayer
    {
        [SerializeField] private List<Player_Renderers> p_renderers;
        [SerializeField] private Material ghostMaterial;
        [SerializeField] private NPC_Death playerDeath;
        private VRTK_SDKSetup sDKSetup;
        [SerializeField] private PlayerVisualParts playerVisualParts;
        public bool gunInHand { get; set; }
        public bool IsMuted { get; private set; }

        public Transform PlayerCameraTransform
        {
            get
            {
                if (sDKSetup == null)
                    return null;
                return sDKSetup.actualHeadset.transform;
            }
        }

        private VRTK_InteractableObject pistol;
        bool controllerInited = false;

        private void Awake()
        {
            photonView = GetComponent<PhotonView>();
            if (photonView.IsMine)
                base.playerInfoUI.gameObject.SetActive(false);
        }

        private void Start()
        {
            this.UserID = photonView.Owner.UserId;
            //if(XRSettings.loadedDeviceName!="Oculus")
                StartCoroutine(resetEnumerator());


        }

        private IEnumerator resetEnumerator() {
            var contr=GetComponentInChildren<ControllerInitializer>();
            while (contr==null) {
                yield return null;
                contr = GetComponentInChildren<ControllerInitializer>();
            }
            yield return new WaitForSeconds(0.1f);
            contr.ForceRecetPos(UserID);
            contr.ForceRecetPos(UserID,0.5f);
        }


        private void LateUpdate()
        {
            if (photonView.IsMine && controllerInited)
            {
                if (sDKSetup != null)
                {
                    playerModel.UpdateHeadTransform(sDKSetup.actualHeadset.transform);
                    playerModel.UpdateHandsTransform(sDKSetup.actualLeftController.transform, sDKSetup.actualRightController.transform);
                    playerModel.UpdateBodyTransform();
                }
                else
                {
                    //Debug.LogError("VRTK_SDKSetup not initialized!");
                }                               
            }
        }

        private void MainPistolMakeShot(object sender, InteractableObjectEventArgs e)
        {
            MainPistol.Instance.MakeShot(this);
        }
        public void InitInfoLookAt(Transform target)
        {
            playerInfoUI.Init(target, true);
        }

        #region Hand pointers
        private IEnumerator DelaySwitchPointer(Hand hand)
        {
            yield return new WaitForEndOfFrame();
            switch (hand)
            {
                case Hand.Left:
                    var leftPointer = sDKSetup.actualLeftController.GetComponentInChildren<VRTK.VRTK_Pointer>();
                    leftPointer.enabled = false;
                    leftPointer.pointerRenderer.enabled = false;

                    leftPointer.pointerRenderer = leftPointer.GetComponent<VRTK_StraightPointerRenderer>();
                    
                    leftPointer.pointerRenderer.enabled = true;
                    leftPointer.enabled = true;
                    playerModel.LeftHand.GetComponent<SelectPlayerPointer>().DisablePointer();
                    break;
                case Hand.Right:
                    var rightPointer = sDKSetup.actualRightController.GetComponentInChildren<VRTK.VRTK_Pointer>();
                    rightPointer.enabled = false;
                    rightPointer.pointerRenderer.enabled = false;

                    rightPointer.pointerRenderer = rightPointer.GetComponent<VRTK_StraightPointerRenderer>();

                    rightPointer.pointerRenderer.enabled = true;
                    rightPointer.enabled = true;
                    playerModel.RightHand.GetComponent<SelectPlayerPointer>().DisablePointer();
                    break;
            }
            yield return new WaitForEndOfFrame();


        }
        private void LeftHandPointer(Hand hand, bool b)
        {
            LayerMask mask = 1 << LayerMask.NameToLayer("Head");
            Vector3 point = sDKSetup.actualLeftController.transform.GetComponentInChildren<VRTK_StraightPointerRenderer>().GetPointerObjects()[0].transform.position;
            Vector3 direction = sDKSetup.actualLeftController.transform.GetComponentInChildren<VRTK_StraightPointerRenderer>().GetPointerObjects()[0].transform.forward * 10; 
            Ray ray = new Ray(point, direction);
            if (Physics.Raycast(ray, out var hit, 15f, mask))
            {
                AbstractPlayer target = hit.transform.root.GetComponent<AbstractPlayer>();
                if (target == null)
                    return;

                string targetUserID = target.UserID;
                if (!PlayingCardsManager.Instance.StatusOnPlayer(targetUserID, PlayingCardType.ExtraLife) && !PlayingCardsManager.Instance.StatusOnPlayer(targetUserID, PlayingCardType.ProtectYourself))
                {
                    PlayingCardsManager.Instance.PV.RPC("SetTargetID", RpcTarget.All, targetUserID);
                    playerModel.LeftHand.GetComponentInChildren<HandAnimatorController>().TriggerInput -= LeftHandPointer;
                    NetworkManager.Instance.OnTurnSwitched.RemoveListener(DisableHandPointer);
                    StartCoroutine(DelaySwitchPointer(hand));
                }
            }
        }
        private void RightHandPointer(Hand hand, bool b)
        {
            LayerMask mask = 1 << LayerMask.NameToLayer("Head");
            Vector3 point = sDKSetup.actualRightController.transform.GetComponentInChildren<VRTK_StraightPointerRenderer>().GetPointerObjects()[0].transform.position;
            Vector3 direction = sDKSetup.actualRightController.transform.GetComponentInChildren<VRTK_StraightPointerRenderer>().GetPointerObjects()[0].transform.forward * 10;
            Ray ray = new Ray(point, direction);
            if (Physics.Raycast(ray, out var hit, 15f, mask))
            {
                AbstractPlayer target = hit.transform.root.GetComponent<AbstractPlayer>();
                if (target == null)
                    return;

                string targetUserID = target.UserID;
                if (!PlayingCardsManager.Instance.StatusOnPlayer(targetUserID, PlayingCardType.ExtraLife) && !PlayingCardsManager.Instance.StatusOnPlayer(targetUserID, PlayingCardType.ProtectYourself))
                {
                    PlayingCardsManager.Instance.PV.RPC("SetTargetID", RpcTarget.All, targetUserID);
                    playerModel.RightHand.GetComponentInChildren<HandAnimatorController>().TriggerInput -= RightHandPointer;
                    NetworkManager.Instance.OnTurnSwitched.RemoveListener(DisableHandPointer);
                    StartCoroutine(DelaySwitchPointer(hand));
                }
            }
        }
        #endregion

        [PunRPC]
        public void EnableHandPointer()
        {
            var customPointer = playerModel.RightHand.GetComponent<PlayerHand>().customPointer;
            customPointer.enabled = true;
            switch (handWhichCard)
            {
                case Hand.Left:
                    sDKSetup.actualLeftController.GetComponentInChildren<VRTK.VRTK_Pointer>().pointerRenderer = customPointer;
                    playerModel.LeftHand.GetComponentInChildren<HandAnimatorController>().TriggerInput += LeftHandPointer;
                    playerModel.LeftHand.GetComponent<SelectPlayerPointer>().EnablePointer(sDKSetup.actualLeftController.transform.GetComponentInChildren<VRTK_StraightPointerRenderer>().GetPointerObjects()[0].transform);
                    break;
                case Hand.Right:
                    sDKSetup.actualRightController.GetComponentInChildren<VRTK.VRTK_Pointer>().pointerRenderer = customPointer;
                    playerModel.RightHand.GetComponentInChildren<HandAnimatorController>().TriggerInput += RightHandPointer;
                    playerModel.RightHand.GetComponent<SelectPlayerPointer>().EnablePointer(sDKSetup.actualRightController.transform.GetComponentInChildren<VRTK_StraightPointerRenderer>().GetPointerObjects()[0].transform);
                    break;
            }
            NetworkManager.Instance.OnTurnSwitched.AddListener(DisableHandPointer);//
        }

        private void DisableHandPointer() {
            NetworkManager.Instance.OnTurnSwitched.RemoveListener(DisableHandPointer);
            //var customPointer = playerModel.RightHand.GetComponent<PlayerHand>().customPointer;
            //customPointer.enabled = false;
            switch (handWhichCard) {
                case Hand.Left:
                    playerModel.LeftHand.GetComponentInChildren<HandAnimatorController>().TriggerInput -=
                        LeftHandPointer;
                    
                    break;
                case Hand.Right:
                    playerModel.RightHand.GetComponentInChildren<HandAnimatorController>().TriggerInput -=
                        RightHandPointer;
                    break;
            }
            StartCoroutine(DelaySwitchPointer(handWhichCard));


        }


        [PunRPC]
        private void HandSpinPistol() {
            MainPistol.Instance.GiveAccessToHandSpin();
            NetworkManager.Instance.gameState.GetPlayerState(UserID).canReload = false;
        }

        public void ReloadPistol(out bool isReloaded)
        {
            isReloaded = false;
            if (NetworkManager.Instance.gameState.GetPlayerState(base.UserID).canReload)
            {
                MainPistol.Instance.GiveAccessToHandSpin();
                NetworkManager.Instance.gameState.GetPlayerState(UserID).canReload = false;
                isReloaded = true;
            }
        }
        public void InitPlayer(PlayerType pType)
        {           
            if(photonView.IsMine)
                photonView.RPC("RPC_BaseInit", RpcTarget.AllBuffered, pType);
        }

        [PunRPC]
        private void RPC_BaseInit(PlayerType pType)
        {
            playerVisualParts.EnableParts(pType);
            this.playerDeath = playerVisualParts.GetDeathPart(pType);
            p_renderers.Clear();
            this.p_renderers = playerVisualParts.GetRenderers(pType);
            controllerInited = true;
            base.typeOfPlayer = pType;
            if (photonView.IsMine)
            {
                foreach (var r in playerModel.Head.GetComponentsInChildren<Renderer>())
                    r.enabled = false;
            }

            for (int i = 0; i < p_renderers.Count; i++)
            {
                p_renderers[i].rendererMaterials = new List<Material>();
                p_renderers[i].rendererMaterials.AddRange(p_renderers[i].renderer.materials);
            }

            GetComponentInChildren<PlayerMouth>().Iinitialize(); 
        }
        public void AcceptPosition(int pos, string id) /*Применение позиции игрока в соответсвии с его индексом*/
        {

            var new_pos = GameManager.Instance.playersPositions[pos].playerPlace.position;
            new_pos.y -= 1.5f;
            transform.position = new_pos;
            transform.rotation = GameManager.Instance.playersPositions[pos].playerPlace.rotation;
        }
        [PunRPC]
        private void RPC_RememberPistolDirection(Vector3 dir)
        {
            moveHeadAfterDeathDir = dir;
        }
        public override void HidePlayer(bool withoutRagdall=false)
        {
            if (!alive)
                return;

            base.playerModel.LeftHand.GetComponentInChildren<UvMeshFinder>().ResetToNormalState();
            base.playerModel.RightHand.GetComponentInChildren<UvMeshFinder>().ResetToNormalState();
            foreach (var r in p_renderers)
            {
                if ((r.renderer.CompareTag("Head") || r.renderer.CompareTag("Hat")) && photonView.IsMine)
                    continue;
                else
                {
                    r.ToGhost(ghostMaterial, this);
                }
            }

            int myIndex = NetworkManager.Instance.gameState.GetPos(base.UserID);
            var pointToDropHat = GameManager.Instance.playersPositions[myIndex].moneyText.transform.position;
            playerDeath.InitDeath(pointToDropHat, moveHeadAfterDeathDir);

            var _players = NetworkManager.Instance.gameState.players;//после смерти, удаляем у остальных игроков из списка стрелявших в него себ
            for (int i = 0; i < _players.Length; i++)
            {
                if(_players[i].UserID != base.UserID)
                {
                    List<string> shootedMe = new List<string>();
                    shootedMe.AddRange(_players[i].playerWhoShotMe);
                    if (shootedMe.Contains(base.UserID))
                    {
                        shootedMe.Remove(base.UserID);
                        _players[i].playerWhoShotMe = shootedMe.ToArray();
                    }
                }
            }
            alive = false;
            base.RemoveCoin(-1);
        }
        public override void ShowPlayer()
        {
            if (alive)
                return;

            playerDeath.Hat.gameObject.SetActive(false);


            foreach (var r in p_renderers)
            {
                //r.ToNormal();
                if ((r.renderer.CompareTag("Head") || r.renderer.CompareTag("Hat")) && photonView.IsMine)
                    continue;
                else
                {
                    r.ToNormal();
                }
            }
            alive = true;
        }

        public override void RetunCardOnTable()
        {
        }

        public void MutePlayer()
        {
            IsMuted = true;
            base.playerModel.Head.GetComponentInChildren<AudioSource>().volume = 0;
            //base.playerModel.Head.GetComponentInChildren<AudioSource>().mute = true;
        }
        public void UnMutePlayer()
        {
            IsMuted = false;
            base.playerModel.Head.GetComponentInChildren<AudioSource>().volume = 1;
            //base.playerModel.Head.GetComponentInChildren<AudioSource>().mute = false;
        }
        private Action<Hand, bool> returnPlayer;
        public void InitializeController(VRTK_SDKSetup sdk, Action<Hand, bool> returner)
        {
            this.sDKSetup = sdk;
            returnPlayer += returner;
        }
        public void ReturnPlayer()
        {
            returnPlayer?.Invoke(Hand.Left, true);
        }
        public void SetPlayerName(string name, string user_id)
        {
            if (photonView.IsMine)
                photonView.RPC("SetUpName", RpcTarget.AllBuffered, name, user_id);
        }
        public void UnEquipMainPistol(VRTK_InteractableObject pistol, Hand hand)
        {
            switch (hand)//выключить флаг в руке, который означает что она держит пистолет
            {
                case Hand.Left:
                    this.playerModel.LeftHand.GetComponentInChildren<Animator>().SetBool("Pistol", false);
                    break;
                case Hand.Right:
                    this.playerModel.RightHand.GetComponentInChildren<Animator>().SetBool("Pistol", false);
                    break;
            }
            pistol.InteractableObjectUsed -= MainPistolMakeShot;
            pistol.InteractableObjectUsed -= LeftHandShot;
            pistol.InteractableObjectUsed -= RightHandShot;
            MainPistol.Instance.PhotonView.RPC("RPC_PlayerUngrabbedPistol", RpcTarget.All);
        }
        private void LeftHandShot(object sender, InteractableObjectEventArgs e)
        {
            this.playerModel.LeftHand.GetComponentInChildren<Animator>().SetTrigger("Shoot");
        }
        private void RightHandShot(object sender, InteractableObjectEventArgs e)
        {
            this.playerModel.RightHand.GetComponentInChildren<Animator>().SetTrigger("Shoot");
        }
        public void EquipMainPistol(VRTK_InteractableObject pistol, Hand hand)
        {
            switch (hand)
            {
                case Hand.Left:
                    this.playerModel.LeftHand.GetComponentInChildren<Animator>().SetTrigger("GrabPistol");
                    this.playerModel.LeftHand.GetComponentInChildren<Animator>().SetBool("Pistol", true);
                    pistol.InteractableObjectUsed += MainPistolMakeShot;
                    pistol.InteractableObjectUsed += LeftHandShot;
                    
                    break;
                case Hand.Right:
                    this.playerModel.RightHand.GetComponentInChildren<Animator>().SetTrigger("GrabPistol");
                    this.playerModel.RightHand.GetComponentInChildren<Animator>().SetBool("Pistol", true);
                    pistol.InteractableObjectUsed += MainPistolMakeShot;
                    pistol.InteractableObjectUsed += RightHandShot;
                    break;
                    
            }
            MainPistol.Instance.SetPistolHolder(this);
            photonView.RPC("RPC_GetMainPistol", RpcTarget.All);
        }

        public void GetAKF_kill_Pistol(VRTK_InteractableObject pistol, Hand hand)
        {
            this.pistol = pistol;
            handWhichTheGun = hand;
            switch (hand)//включить флаг в руке, который означает что она держит пистолет
            {
                case Hand.Left:
                    this.playerModel.LeftHand.GetComponentInChildren<Animator>().SetTrigger("GrabPistol");
                    this.playerModel.LeftHand.GetComponentInChildren<Animator>().SetBool("Pistol", true);
                    break;
                case Hand.Right:
                    this.playerModel.RightHand.GetComponentInChildren<Animator>().SetTrigger("GrabPistol");
                    this.playerModel.RightHand.GetComponentInChildren<Animator>().SetBool("Pistol", true);
                    break;
            }
            this.pistol.InteractableObjectUsed += MakeShotAFK;
        }
        private Hand handWhichTheGun;//рука в которой сейчас находится пистолет
        public void GetCigarette(int cigaretteID)
        {
            photonView.RPC("RPC_GetCigarette", RpcTarget.All, cigaretteID);
        }
        public void GetGlass(int glassID)
        {
            photonView.RPC("RPC_GetGlass", RpcTarget.All, glassID);
        }
        private Hand handWhichCard = Hand.Right;
        public void GetPlayingCard(int cardID, Hand hand)
        {
            handWhichCard = hand;
            photonView.RPC("RPC_GetPlayingCard", RpcTarget.MasterClient, cardID);
        }
        public void GetBullet()
        {
            photonView.RPC("RPC_GetBullet", RpcTarget.All);
        }
        public void GetPepelka(int peplnitsaID)
        {
            photonView.RPC("RPC_GetPepelka", RpcTarget.All, peplnitsaID);
        }

        public void DropAFK_Pistol()
        {
            if (this.pistol != null)
                this.pistol.InteractableObjectUsed -= MakeShotAFK;

            switch (handWhichTheGun)//выключить флаг в руке, который означает что она держит пистолет
            {
                case Hand.Left:
                    this.playerModel.LeftHand.GetComponentInChildren<Animator>().SetBool("Pistol", false);
                    break;
                case Hand.Right:
                    this.playerModel.RightHand.GetComponentInChildren<Animator>().SetBool("Pistol", false);
                    break;
            }
            this.pistol = null;
        }

        private void MakeShotAFK(object sender, InteractableObjectEventArgs e)
        {
            switch (handWhichTheGun)//триггер выстрела для аниматора рук
            {
                case Hand.Left:
                    this.playerModel.LeftHand.GetComponentInChildren<Animator>().SetTrigger("Shoot");
                    break;
                case Hand.Right:
                    this.playerModel.RightHand.GetComponentInChildren<Animator>().SetTrigger("Shoot");
                    break;
            }
            Debug.Log("AFK Shot");
        }

        #region RPC methods
        [PunRPC]
        private void RPC_GetPlayingCard(int cardID, PhotonMessageInfo info)
        {
            PlayingCardsManager.Instance.SwitchOwnership(cardID, info.Sender);
        }
        [PunRPC]
        private void RPC_GetMainPistol(PhotonMessageInfo info)
        {
            MainPistol.Instance.SetUser(info.Sender);
        }

        [PunRPC]
        private void RPC_ClearCoins()
        {
            base.RemoveCoin(-1);
        }
        [PunRPC]
        private void SetUpName(string name, string user_id)
        {
            gameObject.name = name;
            base.UserID = user_id;
            base.playerInfoUI.SetNickname(name);
        }
        [PunRPC]
        void RPC_GetBullet(PhotonMessageInfo info) //Сообщает всем игрокам кто взял пистолет
        {
            Bullet.Instance.SetUser(info.Sender);
        }
        [PunRPC]
        void RPC_GetPepelka(int peplnitsaID, PhotonMessageInfo info)
        {
            foreach (var p in FindObjectsOfType<Pepelka>())
            {
                if (p.pepelnitsaID == peplnitsaID)
                {
                    p.SetUser(info.Sender);
                    break;
                }
            }
        }

        [PunRPC]
        private void RPC_GetCigarette(int cigaretteID, PhotonMessageInfo info) //Сообщает всем игрокам кто взял сигарету
        {
            foreach (var c in FindObjectsOfType<Cigarette>())
            {
                if (c.cigaretteID == cigaretteID)
                {
                    c.SetUser(info.Sender);
                    break;
                }
            }
        }

        [PunRPC]
        private void RPC_GetGlass(int glassID, PhotonMessageInfo info)//Сообщает всем игрокам кто взял стакан
        {
            foreach(var g in FindObjectsOfType<Glass>())
            {
                if(g.GlassID == glassID)
                {
                    g.SetUser(info.Sender);
                    break;
                }
            }
        }
        #endregion

    }

    [System.Serializable]
    public class PlayerParts
    {
        public GameObject Head;
        public GameObject Body;
        public GameObject LeftHand;
        public GameObject RightHand;
        public GameObject FullModel;
        public void UpdateHeadTransform(Transform headset)
        {
            var headPos = headset.position;
            Head.transform.position = headPos;
            Head.transform.rotation = headset.rotation;
        }

        public void UpdateHandsTransform(Transform left, Transform right)
        {

            LeftHand.transform.position = left.position;
            LeftHand.transform.rotation = left.rotation;

            RightHand.transform.position = right.position;
            RightHand.transform.rotation = right.rotation;
        }

        public void UpdateBodyTransform()
        {
            var headRot = Head.transform.forward;

            headRot.y = 0;
            Body.transform.forward = headRot;
            var newBodyPosition = Head.transform.localPosition;
            newBodyPosition.y -= .35f;
            Body.transform.localPosition = newBodyPosition;
        }
    }

}
