﻿using Photon.Pun;
using Photon.Realtime;
using Photon.Voice.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRTK;

namespace SaloonSpin
{
    public class PlayerGameplayCanvas : MonoBehaviourPunCallbacks
    {
        [SerializeField] private GameObject BackGround;
        [Header("Main Panel")]
        [SerializeField] private GameObject MainPanel;
        [SerializeField] private Image muteImgButton;
        [SerializeField] private Button kickButton;
        [SerializeField] private Text nicknameText;
        [Header("Kick Panel")]
        [SerializeField] private GameObject KickPanel;
        [SerializeField] private Transform KickPlayersList;
        [SerializeField] private GameObject KickPlayerElementPrefab;
        [Header("Mute Panel")]
        [SerializeField] private GameObject MutePlayersPanel;
        [SerializeField] private Transform MutePlayersList;
        [SerializeField] private GameObject MutePlayerElementPrefab;
        [SerializeField] private Sprite mute_mic, mic;
        [Header("Invite Panel")]
        [SerializeField] private GameObject InvitePanel;

        [Header("Restart button")] 
        [SerializeField] private GameObject restart;

        public static PlayerGameplayCanvas Instance;
        private bool microIsMuted = false;
        private PlayerController localPlayer = null;
        private void Awake()
        {
            Instance = this;
        }
        public override void OnJoinedRoom()
        {
            nicknameText.text = PhotonNetwork.LocalPlayer.NickName;
        }
        public void InitCanvas(PlayerController myPlayer, Vector3 lookAtPoint)
        {
            localPlayer = myPlayer;
            Vector3 _pos = GameManager.Instance.tableCenter.position;
            _pos.y += 0.4f;

            transform.position = _pos;
            transform.LookAt(lookAtPoint);
            Vector3 currentRotation = transform.eulerAngles;
            currentRotation.x = 0f;
            transform.eulerAngles = currentRotation;

            if(!BackGround.activeInHierarchy)
            {
                InvitePanel.SetActive(false);
                KickPanel.SetActive(false);
                MainPanel.SetActive(true);
                MutePlayersPanel.SetActive(false);
            }
            kickButton.interactable = Photon.Pun.PhotonNetwork.IsMasterClient;
            restart.SetActive(Photon.Pun.PhotonNetwork.IsMasterClient);
            var hand = localPlayer.GetComponentInChildren<HandPointerActivator>();
            StartCoroutine(WaitAndTurnOff(hand));
            

        }

        private IEnumerator WaitAndTurnOff(HandPointerActivator hand) {
            yield return new WaitForSeconds(0.2f);
            if (BackGround.activeInHierarchy != hand.Pointer.IsPointerActive())
                BackGround.SetActive(hand.Pointer.IsPointerActive());
            Debug.Log($"[PlayerGameplayCanvas] InitCanvas {hand.Pointer.IsPointerActive()}");
            
                
            /*yield return new WaitForSeconds(0.12f);
            if (BackGround.activeInHierarchy != hand.Pointer.IsPointerActive()) {
                BackGround.SetActive(hand.Pointer.IsPointerActive());
                yield break;
            }*/
            
            
           // 
           // BackGround.SetActive(hand.Pointer.IsPointerActive());
        }



        public void TurnOffMenu()
        {
            BackGround.SetActive(false);
            var hand = localPlayer.GetComponentInChildren<HandPointerActivator>();
            if(hand)hand.TurnOffPointer();
        }

        public void OnClickExitDebug()
        {
            Debug.Log("click exit debug");
        }
        public void OnClick_MutePlayer()
        {
            foreach (Transform child in MutePlayersList)
                Destroy(child.gameObject);

            var networkPlayers = PhotonNetwork.PlayerList;
            List<PlayerController> players = new List<PlayerController>();
            players.AddRange(FindObjectsOfType<PlayerController>());

            for (int i = 0; i < networkPlayers.Length; i++)
            {
                if (networkPlayers[i].IsLocal)
                    continue;

                var muteBtnElement = Instantiate(MutePlayerElementPrefab, MutePlayersList);
                muteBtnElement.transform.GetComponentInChildren<Text>().text = networkPlayers[i].NickName;

                var muteBtns = muteBtnElement.GetComponentsInChildren<Button>();

                var p = players.Find((_player) => _player.UserID == networkPlayers[i].UserId);

                if (p.IsMuted)
                {
                    muteBtns[0].gameObject.SetActive(false);
                    muteBtns[1].gameObject.SetActive(true);
                }
                else
                {
                    muteBtns[0].gameObject.SetActive(true);
                    muteBtns[1].gameObject.SetActive(false);
                }


                muteBtns[0].GetComponent<Button>().onClick.AddListener(() => 
                {
                    muteBtns[0].gameObject.SetActive(false);
                    muteBtns[1].gameObject.SetActive(true);
                    p.MutePlayer();
                });

                muteBtns[1].GetComponent<Button>().onClick.AddListener(() =>
                {
                    muteBtns[0].gameObject.SetActive(true);
                    muteBtns[1].gameObject.SetActive(false);
                    p.UnMutePlayer();
                });

            }
        }
        public void OnClick_KickPlayer()
        {
            foreach (Transform child in KickPlayersList)
                Destroy(child.gameObject);

            var networkPlayers = PhotonNetwork.PlayerList;

            for(int i = 0; i < networkPlayers.Length; i++)
            {
                var go = Instantiate(KickPlayerElementPrefab, KickPlayersList);
                go.transform.GetComponentInChildren<Text>().text = networkPlayers[i].NickName;
                Player kickedPlayer = networkPlayers[i];
                if(!networkPlayers[i].IsLocal)
                {
                    go.transform.GetComponentInChildren<Button>(true).gameObject.SetActive(true);
                    go.transform.GetComponentInChildren<Button>(true).onClick.RemoveAllListeners();
                    go.transform.GetComponentInChildren<Button>(true).onClick.AddListener(() => 
                    {
                        Lobby.Instance.GetComponent<PhotonView>().RPC("OnClick_LeaveRoomFromGame", kickedPlayer);
                        var list = new List<Player>();
                        list.AddRange(networkPlayers);
                        list.Remove(kickedPlayer);
                        KickPlayersUpdate(list.ToArray());
                    });
                    
                }
            }
        }
        private void KickPlayersUpdate(Player[] networkPlayers)
        {
            foreach (Transform child in KickPlayersList)
                Destroy(child.gameObject);
            
            for (int i = 0; i < networkPlayers.Length; i++)
            {
                var go = Instantiate(KickPlayerElementPrefab, KickPlayersList);
                go.transform.GetComponentInChildren<Text>().text = networkPlayers[i].NickName;
                Player kickedPlayer = networkPlayers[i];
                if (!networkPlayers[i].IsLocal)
                {
                    go.transform.GetComponentInChildren<Button>(true).gameObject.SetActive(true);
                    go.transform.GetComponentInChildren<Button>(true).onClick.RemoveAllListeners();
                    go.transform.GetComponentInChildren<Button>(true).onClick.AddListener(() =>
                    {
                        Lobby.Instance.GetComponent<PhotonView>().RPC("OnClick_LeaveRoomFromGame", kickedPlayer);
                        var list = new List<Player>();
                        list.AddRange(networkPlayers);
                        list.Remove(kickedPlayer);
                        KickPlayersUpdate(list.ToArray());
                    });

                }
            }
        }

        public void OnClick_MuteMicro()
        {
            if(!microIsMuted)
            {
                muteImgButton.sprite = mute_mic;
            }
            else
            {
                muteImgButton.sprite = mic;
            }
            microIsMuted = !microIsMuted;
            

            if(localPlayer != null)
            {
                localPlayer.GetComponent<Recorder>().TransmitEnabled = !microIsMuted;
            }
            else
            {
                Debug.LogError("local player is null");
            }
        }
    }
}
