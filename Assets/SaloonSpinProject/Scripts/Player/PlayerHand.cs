﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SaloonSpin
{
    public delegate void HandRotationDelegate();
    public class PlayerHand : MonoBehaviour
    {
        private bool pointerEnabled;
        public Hand Hand;
        [SerializeField] private ParticleSystem ps;
        [SerializeField] private PhotonView photonView;
        [SerializeField] private float timeToRotatePistol = 0.1f;
        public VRTK.VRTK_StraightPointerRenderer customPointer;
        public UnityEvent GiveA_Five; 

        private void OnTriggerEnter(Collider other)
        {
            if(!PhotonNetwork.IsConnected)
                return;
            if(other.CompareTag("Hand"))
            {
                if (other.GetComponent<PhotonView>().Owner != photonView.Owner)
                {
                    photonView.RPC("RPC_EmitPS", RpcTarget.All);
                    GiveA_Five?.Invoke();
                }             
            }
        }

        [PunRPC]
        private void RPC_EmitPS()
        {
            if(!ps.isPlaying)
                ps.Play();
        }


    }
}
