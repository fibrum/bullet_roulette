﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class PlayerMouth : MonoBehaviour
    {
        private TweenRotation tweenRotation;

        private Vector3 defaultStart, defaultEnd;
        public bool IsMouthOpen { get; private set; }
        public void Iinitialize()
        {
            StartCoroutine(Initialize());
        }
        private IEnumerator Initialize()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            tweenRotation = transform.parent.GetComponentInChildren<TweenRotation>();

            if (tweenRotation == null)
                Debug.LogError("mouth rotation not found");

            defaultStart = tweenRotation.startRotation;
            defaultEnd = tweenRotation.endRotation;
        }

        public void OpenMouth()
        {
            if (IsMouthOpen)
                return;

            IsMouthOpen = true;
            tweenRotation.startRotation = defaultStart;
            tweenRotation.endRotation = defaultEnd;
            tweenRotation.ResetToBeginning();
        }
        public void CloseMouth()
        {
            if (!IsMouthOpen)
                return;

            IsMouthOpen = false;
            tweenRotation.startRotation = defaultEnd;
            tweenRotation.endRotation = defaultStart;
            tweenRotation.ResetToBeginning();
        }
    }
}
