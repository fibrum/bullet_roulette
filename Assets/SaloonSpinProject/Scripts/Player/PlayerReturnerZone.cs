﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class PlayerReturnerZone : MonoBehaviour
    {
        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                Debug.Log("player exit");
                other.transform.root.GetComponent<PlayerController>().ReturnPlayer();
            }
        }
    }
}