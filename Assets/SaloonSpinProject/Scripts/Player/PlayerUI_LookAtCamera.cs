﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SaloonSpin
{
    public class PlayerUI_LookAtCamera : MonoBehaviour
    {
        [SerializeField] private Text nickname;
        [SerializeField] private GameObject money;
        [SerializeField] private Image cardEffect;
        [SerializeField] private Sprite extralife, sheriff_protectoin, target;
        private Transform lookTarget;
        private bool canLook;
        private bool lockZaxis;
        public bool MoneyEnabled { get { return money.gameObject.activeSelf; } }


        public void Init(Transform target, bool lockZaxis)
        {
            if (target == null)
                return;

            lookTarget = target;
            canLook = true;
            this.lockZaxis = lockZaxis;
        }
        public void Init(Transform target)
        {
            if (target == null)
                return;

            lookTarget = target;
            canLook = true;
        }
        public void EnableMoney(int value, bool enable = false)
        {
            money.GetComponentInChildren<Text>().text = value.ToString();
            money.SetActive(enable);
        }
        public void EnableCardEffect(PlayingCardType pct, bool enable)
        {
            if (enable)
            {
                switch (pct)
                {
                    case PlayingCardType.ExtraLife:
                        cardEffect.sprite = extralife;
                        break;
                    case PlayingCardType.ProtectYourself:
                        cardEffect.sprite = sheriff_protectoin;
                        break;
                    case PlayingCardType.SetTarget:
                        cardEffect.sprite = target;
                        break;
                }
                cardEffect.gameObject.SetActive(true);
            }
            else
            {
                switch (pct)
                {
                    case PlayingCardType.ExtraLife:
                        if(cardEffect.sprite == extralife)
                            cardEffect.gameObject.SetActive(false);
                        break;
                    case PlayingCardType.ProtectYourself:
                        if (cardEffect.sprite == sheriff_protectoin)
                            cardEffect.gameObject.SetActive(false);
                        break;
                    case PlayingCardType.SetTarget:
                        if (cardEffect != null && cardEffect.gameObject != null) { 
                            if (cardEffect.sprite == target)
                                cardEffect.gameObject.SetActive(false);
                            cardEffect.sprite = target;

                        } else {
                            Debug.LogError("[PlayerUI_LookAtCamera]EnableCardEffect: card effect or cardEffect.gameObject is null");
                        }
                        break;
                }
            }
        }

        

        public void SetNickname(string name)
        {
            nickname.text = name;
        }

        private void Update()
        {
            if (canLook && lookTarget != null)
            {
                transform.LookAt(lookTarget);
                if (lockZaxis)
                {
                    Vector3 localRot = transform.eulerAngles;
                    localRot.z = 0;
                    transform.eulerAngles = localRot;
                }
            }
        }
        private void OnDisable()
        {
            canLook = false;
            lookTarget = null;
        }
    }
}
