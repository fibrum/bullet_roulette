﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SaloonSpin
{
    public class PlayerVisualParts : MonoBehaviour
    {
        [Header("Gentlman, Bandit, PoliceFemale, Dama, Ranger, Indian")]
        [SerializeField] private List<Parts> visualParts = new List<Parts>();

        public void EnableParts(PlayerType pType)
        {
            visualParts[(int)pType].Head.SetActive(true);
            visualParts[(int)pType].Body.SetActive(true);
            visualParts[(int)pType].LeftHand.SetActive(true);
            visualParts[(int)pType].RightHand.SetActive(true);
        }
        public NPC_Death GetDeathPart(PlayerType pType)
        {
            return visualParts[(int)pType].deathParts;
        }
        public GameObject BaseHead(PlayerType pType)
        {
            return visualParts[(int)pType].Head.transform.parent.gameObject;
        }
        public List<Player_Renderers> GetRenderers(PlayerType pType)
        {
            var list = new List<Player_Renderers>();
            foreach (var r in transform.GetComponentsInChildren<Renderer>(false))
            {
                var p_renderer = new Player_Renderers();
                p_renderer.renderer = r;
                //if(!(p_renderer.renderer is ParticleSystem))
                list.Add(p_renderer);
            }
            return list;
        }
    }
    [System.Serializable]
    public class Parts
    {
        public GameObject Head;
        public GameObject Body;
        public GameObject LeftHand;
        public GameObject RightHand;
        public NPC_Death deathParts;
        public List<Player_Renderers> p_renderers = new List<Player_Renderers>();
    }
}

