﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class SelectPlayerPointer : MonoBehaviour
    {
        public bool PointerEnabled { get; private set; }
        [SerializeField] private LayerMask mask;
        private AbstractPlayer target = null;
        private Transform raycastPoint;
        public void EnablePointer(Transform point) {
            raycastPoint = point;
            PointerEnabled = true;
        }
        public void DisablePointer()
        {
            PointerEnabled = false;
            if (target != null)
            {
                target.playerModel.Head.GetComponent<Head>().AimingEffectTurnOff();
                target = null;
            }
        }

        private void Update()
        {
            if(PointerEnabled)
            {
                Vector3 point = raycastPoint.position;
                Vector3 direction = raycastPoint.forward * 10;
                Ray ray = new Ray(point, direction);
                if (Physics.Raycast(ray, out var hit, 15f, mask))
                {
                    if (!hit.transform.CompareTag("Head"))
                        return;

                    target = hit.transform.root.GetComponent<AbstractPlayer>();
                    if (target == null )
                        return;

                    try
                    {
                        if(target.UserID != Photon.Pun.PhotonNetwork.LocalPlayer.UserId)
                            target.playerModel.Head.GetComponent<Head>().AimingEffectTurnOn();
                    }
                    catch
                    {
                        Debug.LogError(string.Format("name: {0}, playerModel is null? {1}, playerModel.Head is null?, {2} playerModel.Head.GetComponent<Head>() is null? {3}",
                            target.name, target.playerModel == null, target.playerModel.Head == null, target.playerModel.Head.GetComponent<Head>() == null));
                    }
                }
                else if(target != null)
                {
                    if (target.UserID != Photon.Pun.PhotonNetwork.LocalPlayer.UserId)
                        target.playerModel.Head.GetComponent<Head>().AimingEffectTurnOff();
                    target = null;
                }

            }
        }
    }
}
