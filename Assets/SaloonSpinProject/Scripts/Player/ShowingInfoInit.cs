﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaloonSpin
{
    public class ShowingInfoInit : MonoBehaviour
    {
        private PhotonView pv;
        private AbstractPlayer player;
        private void Awake()
        {
            pv = GetComponent<PhotonView>();
            player = GetComponent<AbstractPlayer>();
        }

        private void Start()
        {
            pv.RPC("InitShowingInfo", RpcTarget.All);
        }

        [PunRPC]
        private void InitShowingInfo()
        {
            if(player is PlayerController)
            {
                StartCoroutine(DelayInitLookInfo1());
            }
        }

        private IEnumerator DelayInitLookInfo1()
        {
            Debug.Log("initialize look info");
            yield return new WaitForSeconds(.5f);
            var me = (PlayerController)player;
            Transform target = me.PlayerCameraTransform;
            foreach (var p in FindObjectsOfType<AbstractPlayer>())
            {
                if (p != this)
                {
                    p.InitShowingInfo(target);
                }
            }
        }
    }
}
