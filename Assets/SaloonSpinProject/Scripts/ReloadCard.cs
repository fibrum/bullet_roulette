﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SaloonSpin
{
    public class ReloadCard : MonoBehaviour
    {
        [SerializeField] private Transform cardRoot;
        [SerializeField] private GameObject cardMesh;
        public bool isNPCcard;
        public string OwnerUserID { get; private set; }
        private PhotonView photonView;
        private PlayerController cardHolderPlayer;
        private Vector3 defaultPosition;
        private Quaternion defaultRotation;
        private Coroutine movingNumerator;
        private UnityEvent OnCardUsed;
        public UnityEvent UseCard;
        private void Awake()
        {
            photonView = GetComponent<PhotonView>();
            if (photonView == null)
            {
                Debug.LogError("#my error: " + " Reload card photon view is null!");
            }
        }

        public void InitCardHolder(string HolderUserID, bool isNPS = false)
        {
            photonView.RPC("RPC_AcceptCard", RpcTarget.AllBuffered, HolderUserID, isNPS);
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("ReloadTrigger"))
            {
                if(isNPCcard)
                {
                    if(PhotonNetwork.IsMasterClient)
                        photonView.RPC("UsedCard", RpcTarget.All, null); 
                }
                else if (photonView.IsMine && photonView.Owner.UserId == NetworkManager.Instance.gameState.userTurn)
                {
                    

                    bool isReloaded = false;
                    cardHolderPlayer.ReloadPistol(out isReloaded);

                    if(isReloaded)
                        isReloaded = MainPistol.Instance.IsGunCharged;

                    if (isReloaded)
                    {
                        photonView.RPC("UsedCard", RpcTarget.All, null);
                    }
                    else
                    {
                        RPC_ResetCardPosition();
                    }

                    photonView.RPC("RPC_UseCard", RpcTarget.All);
                }
            }
        }
        public void SetDefaultRotationPosition(Vector3 defaultPosition, Quaternion defaultRotation)
        {
            this.defaultPosition = defaultPosition;
            this.defaultRotation = defaultRotation;
            transform.root.position = defaultPosition;
            transform.root.rotation = defaultRotation;
            if (photonView.IsMine)
            {
                photonView.RPC("SaveDefaultRoationPosition", RpcTarget.OthersBuffered, defaultPosition, defaultRotation);
            }

            //photonView.RPC("RPC_ResetCardPosition", RpcTarget.AllBuffered, null);
        }
        public void ResetCardPostion()
        {
            //GetComponentInParent<VRTK.VRTK_InteractableObject>().isGrabbable = true;
            photonView.RPC("RPC_ResetCardPosition", RpcTarget.All, null);
        }
        public void SetCardHolder(PlayerController cardHolderPlayer)
        {
            this.cardHolderPlayer = cardHolderPlayer;
        }
        public void StopMoving()
        {
            photonView.RPC("RPC_StopMoving", RpcTarget.All);
        }
        [PunRPC]
        private void RPC_StopMoving()
        {
            StopCoroutine(movingNumerator);
            moving = false;
            GetComponentInParent<Rigidbody>().isKinematic = false;
            GetComponentInParent<VRTK.VRTK_InteractableObject>().isGrabbable = true;//после того как карта вернулась, ее снова можно взять
        }
        [PunRPC]
        private void RPC_AcceptCard(string holderUserID, bool isNPS)
        {
            this.isNPCcard = isNPS;
            var players = FindObjectsOfType<AbstractPlayer>();
            foreach (var p in players)
            {
                if (p.UserID == holderUserID)
                {
                    this.OwnerUserID = p.UserID;
                    if(PhotonNetwork.LocalPlayer.UserId != holderUserID)
                    {
                        GetComponentInParent<VRTK.VRTK_InteractableObject>().enabled = false;
                        GetComponentInParent<VRTK.VRTK_InteractableObject>().disableWhenIdle = false;
                    }

                    string newName = "spin card; holder id: " + holderUserID;
                    transform.parent.gameObject.name = newName;
                    break;
                }
            }
        }

        [PunRPC]
        private void SaveDefaultRoationPosition(Vector3 defaultPosition, Quaternion defaultRotation)
        {
            this.defaultPosition = defaultPosition;
            this.defaultRotation = defaultRotation;

            transform.root.position = defaultPosition;
            transform.root.rotation = defaultRotation;
        }

        [PunRPC]
        private void UsedCard()
        {
            GetComponentInParent<VRTK.VRTK_InteractableObject>().isGrabbable = false;
            StartCoroutine(CardUsedNumerator());
        }
        [PunRPC]
        private void RPC_UseCard()
        {
            UseCard?.Invoke();
        }
        private IEnumerator CardUsedNumerator()
        {
            yield return new WaitForSeconds(.1f);
            cardMesh.gameObject.SetActive(false);
            OnCardUsed?.Invoke();
        }

        [PunRPC]
        private void RPC_ResetCardPosition()
        {
            GetComponentInParent<GrabbaleObjects>().StopGrab(false);
            GetComponentInParent<VRTK.VRTK_InteractableObject>().isGrabbable = false;//пока карта возвращается на место ее нельзя взять
            startMarker = transform.root.position;
            movingNumerator = StartCoroutine(MoveCard());
        }

        private IEnumerator MoveCard()
        {
            startTime = Time.time;
            cardRoot.rotation = defaultRotation;
            journeyLength = Vector3.Distance(cardRoot.position, defaultPosition);
            GetComponentInParent<Rigidbody>().isKinematic = true;
            moving = true;
            yield return new WaitForSeconds(0.4f);
            GetComponentInParent<Rigidbody>().isKinematic = false;
            GetComponentInParent<VRTK.VRTK_InteractableObject>().isGrabbable = true;//после того как карта вернулась, ее снова можно взять
            if(!cardMesh.gameObject.activeSelf)
                cardMesh.gameObject.SetActive(true);
        }

        private void Update()
        {

            if (moving)
            {
                float distCovered = (Time.time - startTime) * speed;

                float fracJourney = distCovered / journeyLength;

                cardRoot.position = Vector3.Lerp(startMarker, defaultPosition, fracJourney);

                if (cardRoot.position == defaultPosition)
                {
                    moving = false;
                }
            }
        }

        #region Moving variables
        float startTime;
        float journeyLength;
        public bool moving { get; private set; }
        float speed = 1f;
        Vector3 startMarker;
        #endregion


    }
}
