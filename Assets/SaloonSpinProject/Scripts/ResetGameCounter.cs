﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace SaloonSpin
{
    public class ResetGameCounter : MonoBehaviour
    {
        [SerializeField]
        private Text txt;

        private void OnEnable()
        {
            StartCoroutine(ResetCounter());
        }

        private IEnumerator ResetCounter()
        {
            int i = 10;
            while(true)
            {
                txt.text = "restart .";
                yield return new WaitForSeconds(.5f);
                txt.text = "restart ..";
                yield return new WaitForSeconds(.5f);
                txt.text = "restart ...";
                yield return new WaitForSeconds(.5f);

            }
            //gameObject.SetActive(false);
        }
    }
}
