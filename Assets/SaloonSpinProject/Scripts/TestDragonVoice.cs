﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class TestDragonVoice : MonoBehaviour {
    [SerializeField] private float timeBetweenVoice = 30;
    [SerializeField] private float audioSourceMaxDistance = 50f;
    [SerializeField] private List<AudioClip> Sounds = null;
    [SerializeField] private AudioSource audioSource = null;
    private float voiceTime;

    void Awake () {
        if (audioSource)
            audioSource.maxDistance = audioSourceMaxDistance;
        else
            enabled = false;
    }

    private void Start()
    {
        StartCoroutine(PlayMusic());
    }

    private IEnumerator PlayMusic()
    {
        while (true)
        {
            int index = Random.Range(0, Sounds.Count);
            voiceTime = Sounds[index].length;
            audioSource.clip = Sounds[index];
            audioSource.Play();
            yield return new WaitForSeconds(voiceTime);
        }
    }
    //private void OnEnable()
    //{
    //    voiceTime = Time.time - timeBetweenVoice;
    //}

    // Update is called once per frame
    //private void Update() {
    //    if (Time.time - voiceTime > timeBetweenVoice) {
    //        int sound = Random.Range(0, Sounds.Count - 1);
    //        voiceTime = Time.time;
    //        play(sound);
    //    }
    //}

    //public void play(int number) {
    //    if (audioSource && Sounds.Count != 0) {
    //        audioSource.clip = Sounds[number];
    //        //timeBetweenVoice = audioSource.clip.length;

    //        if (!audioSource.isActiveAndEnabled) Debug.Log("" + gameObject.name);

    //        if(!audioSource.isPlaying)audioSource.Play();


    //    }
    //}

    public void Pause(float time)
    {
        audioSource.Pause();
        voiceTime += time;
    }
    public void UnPause()
    {
        audioSource.UnPause();
    }
}
