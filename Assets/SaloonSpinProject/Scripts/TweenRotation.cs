﻿using UnityEngine;

public class TweenRotation : MonoBehaviour {

    [SerializeField] private TweenType type=TweenType.simple;
    public Vector3 startRotation = new Vector3();
    public Vector3 endRotation = new Vector3();
    [SerializeField] private AnimationCurve curve = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));
    [SerializeField] private float delayTime;
    [SerializeField] private float time;
    private float innerTime;
    private void OnEnable()
    {
        transform.localEulerAngles = startRotation;
        innerTime = 0;
    }

    private void OnDisable()
    {

    }

    public void ResetToBeginning()
    {
        transform.localEulerAngles = startRotation;
        innerTime = 0;
        forwardPlay = true;
        enabled = true;
    }
    private bool forwardPlay = true;
    public void PlayReverse()
    {
        transform.localEulerAngles = endRotation;
        innerTime = 0;
        forwardPlay = false;
        enabled = true;
    }


    private void Update()
    {
        innerTime += Time.deltaTime;
        if (innerTime < delayTime)
            return;
        if (forwardPlay)
            transform.localEulerAngles =
                Vector3.Lerp(startRotation, endRotation, curve.Evaluate((innerTime - delayTime) / time));
        else {
            transform.localEulerAngles =
                Vector3.Lerp(startRotation, endRotation, curve.Evaluate(1f-(innerTime - delayTime) / time));
        }

        if (innerTime - delayTime > time){
            switch (type){
                case TweenType.simple:
                    if(forwardPlay)
                        transform.localEulerAngles = Vector3.Lerp(startRotation, endRotation, curve.Evaluate(1f));
                    else
                        transform.localEulerAngles = Vector3.Lerp(startRotation, endRotation, curve.Evaluate(0f));
                    enabled = false;
                    break;
                case TweenType.loop:
                    innerTime -= time;
                    break;
            }

        }
    }

    void Reset() {
        startRotation = transform.localEulerAngles;
        endRotation = transform.localEulerAngles;
    }
}
