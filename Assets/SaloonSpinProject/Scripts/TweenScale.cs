﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class TweenScale : MonoBehaviour {
    [SerializeField] private TweenType type;
    [SerializeField] private Vector3 startScale = new Vector3();
    [SerializeField] private Vector3 endScale = new Vector3();
    [SerializeField] private AnimationCurve curve = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));
    [SerializeField] private float delayTime;
    [SerializeField] private float time;
    [SerializeField] private UnityEvent reversePlayFinishEvent;
    private float innerTime;
    private void OnEnable()
    {
        transform.localScale = startScale;
        innerTime = skipDelay?delayTime:0;
        skipDelay = false;

    }

    private void OnDisable()
    {

    }

    public void ResetToBeginning()
    {
        transform.localScale = startScale;
        innerTime = 0;
        enabled = true;
        forwardPlay = true;
    }

    private bool skipDelay;
    private bool forwardPlay = true;
    public void PlayReverse(bool withoutdelay=false)
    {
        transform.localScale = endScale;
        if(!enabled)
            skipDelay = withoutdelay;
        innerTime = withoutdelay?delayTime:0;
        forwardPlay = false;
        enabled = true;
    }public void PlayReverse()
    {
        transform.localScale = endScale;
        innerTime = 0;
        forwardPlay = false;
        enabled = true;
    }

    private void Update()
    {
        innerTime += Time.deltaTime;
        if (innerTime < delayTime)
            return;
        if (forwardPlay)
            transform.localScale =
                Vector3.LerpUnclamped(startScale, endScale, curve.Evaluate((innerTime - delayTime) / time));
        else transform.localScale =
            Vector3.LerpUnclamped(startScale, endScale, curve.Evaluate(1f-(innerTime - delayTime) / time));
        if (innerTime - delayTime > time)
        {
            switch (type) {
                case TweenType.simple:
                    enabled = false;
                    if (forwardPlay)
                        transform.localScale = Vector3.LerpUnclamped(startScale, endScale, curve.Evaluate(1f));
                    else {
                        transform.localScale = Vector3.LerpUnclamped(startScale, endScale, curve.Evaluate(0f));
                        reversePlayFinishEvent.Invoke();
                    }
                    
                    break;
                case TweenType.loop:
                    innerTime -= time;
                    break;
            }
            
        }
    }

    void Reset() {
        startScale = transform.localScale;
        endScale = transform.localScale;
    }
}

public enum TweenType {
    simple,
    loop
}