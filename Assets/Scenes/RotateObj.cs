﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObj : MonoBehaviour
{
    public Transform []points = new Transform[4];
    public float smooth = 2f;
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            StopAllCoroutines();
            StartCoroutine(RotateObjects(0));
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            StopAllCoroutines();
            StartCoroutine(RotateObjects(1));
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            StopAllCoroutines();
            StartCoroutine(RotateObjects(2));
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            StopAllCoroutines();
            StartCoroutine(RotateObjects(3));
        }
    }

    IEnumerator RotateObjects(int index)
    {
        Quaternion target = points[index].rotation;
        while (Quaternion.Angle(transform.rotation, target) > .5f)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
            yield return new WaitForEndOfFrame();
        }
    }
}
