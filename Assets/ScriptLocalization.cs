﻿using UnityEngine;

namespace I2.Loc
{
	public static class ScriptLocalization
	{

		public static string RESTART 		{ get{ return LocalizationManager.GetTranslation ("RESTART"); } }
		public static string START 		{ get{ return LocalizationManager.GetTranslation ("START"); } }
		public static string card_divide_all_money_description 		{ get{ return LocalizationManager.GetTranslation ("card_divide_all_money_description"); } }
		public static string card_extra_live_description 		{ get{ return LocalizationManager.GetTranslation ("card_extra_live_description"); } }
		public static string card_protect_yourself_description 		{ get{ return LocalizationManager.GetTranslation ("card_protect yourself_description"); } }
		public static string card_second_bullet_description 		{ get{ return LocalizationManager.GetTranslation ("card_second_bullet_description"); } }
		public static string card_see_bullet_description 		{ get{ return LocalizationManager.GetTranslation ("card_see_bullet_description"); } }
		public static string card_set_target_description 		{ get{ return LocalizationManager.GetTranslation ("card_set_target_description"); } }
		public static string card_skip_turn_description 		{ get{ return LocalizationManager.GetTranslation ("card_skip_turn_description"); } }
		public static string card_spin_pistol_description 		{ get{ return LocalizationManager.GetTranslation ("card_spin_pistol_description"); } }
		public static string card_turm_move_direction_description 		{ get{ return LocalizationManager.GetTranslation ("card_turm_move_direction_description"); } }
		public static string card_undo_last_played_description 		{ get{ return LocalizationManager.GetTranslation ("card_undo_last_played_description"); } }
		public static string create_room_menu_button_private 		{ get{ return LocalizationManager.GetTranslation ("create_room_menu_button_private"); } }
		public static string create_room_menu_button_public 		{ get{ return LocalizationManager.GetTranslation ("create_room_menu_button_public"); } }
		public static string create_room_menu_title 		{ get{ return LocalizationManager.GetTranslation ("create_room_menu_title"); } }
		public static string find_game_menu_title 		{ get{ return LocalizationManager.GetTranslation ("find_game_menu_title"); } }
		public static string hints_gun_pointer_no_bullet 		{ get{ return LocalizationManager.GetTranslation ("hints_gun_pointer_no_bullet"); } }
		public static string hints_gun_pointer_no_coins 		{ get{ return LocalizationManager.GetTranslation ("hints_gun_pointer_no_coins"); } }
		public static string hints_gun_pointer_not_public_enemy 		{ get{ return LocalizationManager.GetTranslation ("hints_gun_pointer_not_public_enemy"); } }
		public static string hints_gun_pointer_protected_player 		{ get{ return LocalizationManager.GetTranslation ("hints_gun_pointer_protected_player"); } }
		public static string hints_gun_pointer_shoot_youself 		{ get{ return LocalizationManager.GetTranslation ("hints_gun_pointer_shoot_youself"); } }
		public static string hints_recharging_close_barrel 		{ get{ return LocalizationManager.GetTranslation ("hints_recharging_close_barrel"); } }
		public static string hints_recharging_insert_bullet 		{ get{ return LocalizationManager.GetTranslation ("hints_recharging_insert_bullet"); } }
		public static string hints_recharging_open_barrel 		{ get{ return LocalizationManager.GetTranslation ("hints_recharging_open_barrel"); } }
		public static string hints_recharging_spin_barrel 		{ get{ return LocalizationManager.GetTranslation ("hints_recharging_spin_barrel"); } }
		public static string hints_recharging_take_bullet 		{ get{ return LocalizationManager.GetTranslation ("hints_recharging_take_bullet"); } }
		public static string hints_take_bullet 		{ get{ return LocalizationManager.GetTranslation ("hints_take_bullet"); } }
		public static string hints_take_revolver 		{ get{ return LocalizationManager.GetTranslation ("hints_take_revolver"); } }
		public static string invite_from_friend_menu_button_accept 		{ get{ return LocalizationManager.GetTranslation ("invite_from_friend_menu_button_accept"); } }
		public static string invite_from_friend_menu_description_part1 		{ get{ return LocalizationManager.GetTranslation ("invite_from_friend_menu_description_part1"); } }
		public static string invite_from_friend_menu_description_part2 		{ get{ return LocalizationManager.GetTranslation ("invite_from_friend_menu_description_part2"); } }
		public static string invite_from_friend_menu_tittle 		{ get{ return LocalizationManager.GetTranslation ("invite_from_friend_menu_tittle"); } }
		public static string invite_menu_title 		{ get{ return LocalizationManager.GetTranslation ("invite_menu_title"); } }
		public static string main_menu_button_create_game 		{ get{ return LocalizationManager.GetTranslation ("main_menu_button_create_game"); } }
		public static string main_menu_button_find_game 		{ get{ return LocalizationManager.GetTranslation ("main_menu_button_find_game"); } }
		public static string main_menu_button_settings 		{ get{ return LocalizationManager.GetTranslation ("main_menu_button_settings"); } }
		public static string no_found_game_menu_create_game 		{ get{ return LocalizationManager.GetTranslation ("no_found_game_menu_create_game"); } }
		public static string no_found_game_menu_title 		{ get{ return LocalizationManager.GetTranslation ("no_found_game_menu_title"); } }
		public static string no_found_game_menu_try_again 		{ get{ return LocalizationManager.GetTranslation ("no_found_game_menu_try_again"); } }
		public static string offline_menu_button_play_offline 		{ get{ return LocalizationManager.GetTranslation ("offline_menu_button_play_offline"); } }
		public static string offline_menu_button_try_reconnect 		{ get{ return LocalizationManager.GetTranslation ("offline_menu_button_try_reconnect"); } }
		public static string offline_menu_title 		{ get{ return LocalizationManager.GetTranslation ("offline_menu_title"); } }
		public static string player_game_menu_button_mute_player 		{ get{ return LocalizationManager.GetTranslation ("player_game_menu_button_mute_player"); } }
		public static string player_game_menu_button_restart_session 		{ get{ return LocalizationManager.GetTranslation ("player_game_menu_button_restart_session"); } }
		public static string player_game_menu_kick_panel_button_kick 		{ get{ return LocalizationManager.GetTranslation ("player_game_menu_kick_panel_button_kick"); } }
		public static string player_game_menu_kick_panel_tittle 		{ get{ return LocalizationManager.GetTranslation ("player_game_menu_kick_panel_tittle"); } }
		public static string player_game_menu_mute_panel_button_mute 		{ get{ return LocalizationManager.GetTranslation ("player_game_menu_mute_panel_button_mute"); } }
		public static string player_game_menu_mute_panel_button_unmute 		{ get{ return LocalizationManager.GetTranslation ("player_game_menu_mute_panel_button_unmute"); } }
		public static string player_game_menu_mute_panel_tittle 		{ get{ return LocalizationManager.GetTranslation ("player_game_menu_mute_panel_tittle"); } }
		public static string room_members_menu_button_invite_friends 		{ get{ return LocalizationManager.GetTranslation ("room_members_menu_button_invite_friends"); } }
		public static string room_members_menu_button_kick 		{ get{ return LocalizationManager.GetTranslation ("room_members_menu_button_kick"); } }
		public static string room_members_menu_button_start_game 		{ get{ return LocalizationManager.GetTranslation ("room_members_menu_button_start_game"); } }
		public static string settings_menu_master 		{ get{ return LocalizationManager.GetTranslation ("settings_menu_master"); } }
		public static string settings_menu_microphone 		{ get{ return LocalizationManager.GetTranslation ("settings_menu_microphone"); } }
		public static string settings_menu_music 		{ get{ return LocalizationManager.GetTranslation ("settings_menu_music"); } }
		public static string settings_menu_sfx 		{ get{ return LocalizationManager.GetTranslation ("settings_menu_sfx"); } }
		public static string settings_menu_tittle 		{ get{ return LocalizationManager.GetTranslation ("settings_menu_tittle"); } }
		public static string settings_menu_voice 		{ get{ return LocalizationManager.GetTranslation ("settings_menu_voice"); } }
	}

    public static class ScriptTerms
	{

		public const string RESTART = "RESTART";
		public const string START = "START";
		public const string card_divide_all_money_description = "card_divide_all_money_description";
		public const string card_extra_live_description = "card_extra_live_description";
		public const string card_protect_yourself_description = "card_protect yourself_description";
		public const string card_second_bullet_description = "card_second_bullet_description";
		public const string card_see_bullet_description = "card_see_bullet_description";
		public const string card_set_target_description = "card_set_target_description";
		public const string card_skip_turn_description = "card_skip_turn_description";
		public const string card_spin_pistol_description = "card_spin_pistol_description";
		public const string card_turm_move_direction_description = "card_turm_move_direction_description";
		public const string card_undo_last_played_description = "card_undo_last_played_description";
		public const string create_room_menu_button_private = "create_room_menu_button_private";
		public const string create_room_menu_button_public = "create_room_menu_button_public";
		public const string create_room_menu_title = "create_room_menu_title";
		public const string find_game_menu_title = "find_game_menu_title";
		public const string hints_gun_pointer_no_bullet = "hints_gun_pointer_no_bullet";
		public const string hints_gun_pointer_no_coins = "hints_gun_pointer_no_coins";
		public const string hints_gun_pointer_not_public_enemy = "hints_gun_pointer_not_public_enemy";
		public const string hints_gun_pointer_protected_player = "hints_gun_pointer_protected_player";
		public const string hints_gun_pointer_shoot_youself = "hints_gun_pointer_shoot_youself";
		public const string hints_recharging_close_barrel = "hints_recharging_close_barrel";
		public const string hints_recharging_insert_bullet = "hints_recharging_insert_bullet";
		public const string hints_recharging_open_barrel = "hints_recharging_open_barrel";
		public const string hints_recharging_spin_barrel = "hints_recharging_spin_barrel";
		public const string hints_recharging_take_bullet = "hints_recharging_take_bullet";
		public const string hints_take_bullet = "hints_take_bullet";
		public const string hints_take_revolver = "hints_take_revolver";
		public const string invite_from_friend_menu_button_accept = "invite_from_friend_menu_button_accept";
		public const string invite_from_friend_menu_description_part1 = "invite_from_friend_menu_description_part1";
		public const string invite_from_friend_menu_description_part2 = "invite_from_friend_menu_description_part2";
		public const string invite_from_friend_menu_tittle = "invite_from_friend_menu_tittle";
		public const string invite_menu_title = "invite_menu_title";
		public const string main_menu_button_create_game = "main_menu_button_create_game";
		public const string main_menu_button_find_game = "main_menu_button_find_game";
		public const string main_menu_button_settings = "main_menu_button_settings";
		public const string no_found_game_menu_create_game = "no_found_game_menu_create_game";
		public const string no_found_game_menu_title = "no_found_game_menu_title";
		public const string no_found_game_menu_try_again = "no_found_game_menu_try_again";
		public const string offline_menu_button_play_offline = "offline_menu_button_play_offline";
		public const string offline_menu_button_try_reconnect = "offline_menu_button_try_reconnect";
		public const string offline_menu_title = "offline_menu_title";
		public const string player_game_menu_button_mute_player = "player_game_menu_button_mute_player";
		public const string player_game_menu_button_restart_session = "player_game_menu_button_restart_session";
		public const string player_game_menu_kick_panel_button_kick = "player_game_menu_kick_panel_button_kick";
		public const string player_game_menu_kick_panel_tittle = "player_game_menu_kick_panel_tittle";
		public const string player_game_menu_mute_panel_button_mute = "player_game_menu_mute_panel_button_mute";
		public const string player_game_menu_mute_panel_button_unmute = "player_game_menu_mute_panel_button_unmute";
		public const string player_game_menu_mute_panel_tittle = "player_game_menu_mute_panel_tittle";
		public const string room_members_menu_button_invite_friends = "room_members_menu_button_invite_friends";
		public const string room_members_menu_button_kick = "room_members_menu_button_kick";
		public const string room_members_menu_button_start_game = "room_members_menu_button_start_game";
		public const string settings_menu_master = "settings_menu_master";
		public const string settings_menu_microphone = "settings_menu_microphone";
		public const string settings_menu_music = "settings_menu_music";
		public const string settings_menu_sfx = "settings_menu_sfx";
		public const string settings_menu_tittle = "settings_menu_tittle";
		public const string settings_menu_voice = "settings_menu_voice";
	}
}