﻿using System;
using System.Collections.Generic;
using SaloonSpin;
using UnityEngine;
[CreateAssetMenu(fileName ="Positions",menuName = "PositionsAsset")]
public class PositionOfObjects : ScriptableObject {
    public Avatar[] avatars;
    public ColliderPosition[] controllerColliders;
    public ControllerPosAndRot GetAvatarPosAndRot(PlayerType type,string controllerName) {
        Avatar av = GetAvatarByType(type);
        if (av != null)
            return av.GetPosAndRot(controllerName);
        return new ControllerPosAndRot();
    }

    private Avatar GetAvatarByType(PlayerType type) {
        for (int i = 0; i < avatars.Length; i++) {
            if (avatars[i].type == type)
                return avatars[i];
        }

        return null;
    }

    public void SetPositionAndRotation(PlayerType type,string controllerName, Hand Hand,Vector3 pos,Vector3 rot) {
        for(int i = 0; i < avatars.Length; i++) {
            if (avatars[i].type == type) {
                for (int j = 0; j < avatars[i].diffrentControllers.Length; j++) {
                    if (avatars[i].diffrentControllers[j].isEqual(controllerName)) {
                        switch (Hand) {
                            case Hand.Left:
                                avatars[i].diffrentControllers[j].PosL = pos;
                                avatars[i].diffrentControllers[j].RotL = rot;
                                break;
                            case Hand.Right:
                                avatars[i].diffrentControllers[j].PosR = pos;
                                avatars[i].diffrentControllers[j].RotR = rot;
                                break;
                            default:
                                throw new ArgumentOutOfRangeException(nameof(Hand), Hand, null);
                        }
                        return;
                    }
                }
                List<ControllerPosAndRot> controllers=new List<ControllerPosAndRot>(avatars[i].diffrentControllers);
                var controller = new ControllerPosAndRot();
                controller.Name = controllerName;
                controller.ControllersName = new[] {controllerName};
                switch (Hand)
                {
                    case Hand.Left:
                        controller.PosL = pos;
                        controller.RotL = rot;
                        break;
                    case Hand.Right:
                        controller.PosR = pos;
                        controller.RotR = rot;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(Hand), Hand, null);
                }


                controllers.Add(controller);
                avatars[i].diffrentControllers = controllers.ToArray();

                return;
            }
        }
        List<Avatar> avs=new List<Avatar>(avatars);
        var av=new Avatar();
        av.type = type;
        av.Name = type.ToString();
        var contr = new ControllerPosAndRot();
        contr.Name = controllerName;
        contr.ControllersName = new[] { controllerName };
        switch (Hand){
            case Hand.Left:
                contr.PosL = pos;
                contr.RotL = rot;
                break;
            case Hand.Right:
                contr.PosR = pos;
                contr.RotR = rot;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(Hand), Hand, null);
        }

        av.diffrentControllers = new[] {contr};
        avs.Add(av);
        avatars = avs.ToArray();
    }

    public void SetColliderSettings(string controllerName,Hand hand,Vector3 pos,float r) {
        for (int i = 0; i < controllerColliders.Length; i++) {
            if (controllerName.ToLower().Contains(controllerColliders[i].Name)) {
                
                switch (hand) {
                    case Hand.Left:
                        controllerColliders[i].radiusL = r;
                        controllerColliders[i].posL = pos;
                        break;
                    case Hand.Right:

                        controllerColliders[i].radiusR = r;
                        controllerColliders[i].posR = pos;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(hand), hand, null);
                }
                return;
            }
        }

        var list = new List<ColliderPosition>(controllerColliders);
        var col=new ColliderPosition();
        col.Name = controllerName.ToLower();
        switch (hand)
        {
            case Hand.Left:
                col.radiusL = r;
                col.posL = pos;
                break;
            case Hand.Right:

                col.radiusR = r;
                col.posR = pos;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(hand), hand, null);
        }

        list.Add(col);
        controllerColliders = list.ToArray();
    }


    public ColliderPosition GetColliderSettings(string controllerName) {
        for (int i = 0; i < controllerColliders.Length; i++) {
            if (controllerName.ToLower().Contains(controllerColliders[i].Name))
                return controllerColliders[i];
        }

        return new ColliderPosition();
    }
}

[Serializable]
public class Avatar {
    public string Name;
    public PlayerType type;
    public ControllerPosAndRot[] diffrentControllers;

    public ControllerPosAndRot GetPosAndRot(string controllerName) {
        for (int i = 0; i < diffrentControllers.Length; i++) {
            if (diffrentControllers[i].isEqual(controllerName))
                return diffrentControllers[i];
        }
        return new ControllerPosAndRot();
    }
}

[Serializable]
public class ControllerPosAndRot {
    public string Name;
    public string[] ControllersName;
    public Vector3 PosL;
    public Vector3 RotL;
    public Vector3 PosR;
    public Vector3 RotR;

    public bool isEqual(string controller) {
        for (int i = 0; i < ControllersName.Length; i++)
            if(ControllersName[i].Contains(controller))
                return true;
        return false;
    }
}

[Serializable]
public class ColliderPosition {
    public string Name;
    public Vector3 posL;
    public Vector3 posR;
    public float radiusL;
    public float radiusR;
}