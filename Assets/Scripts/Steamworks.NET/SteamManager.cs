// The SteamManager is designed to work with Steamworks.NET
// This file is released into the public domain.
// Where that dedication is not recognized you are granted a perpetual,
// irrevocable license to copy and modify this file as you see fit.
//
// Version: 1.0.7

#if UNITY_ANDROID || UNITY_IOS || UNITY_TIZEN || UNITY_TVOS || UNITY_WEBGL || UNITY_WSA || UNITY_PS4 || UNITY_WII || UNITY_XBOXONE || UNITY_SWITCH
#define DISABLESTEAMWORKS
#endif

#if !DISABLESTEAMWORKS

using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using SaloonSpin;
using Steamworks;


//
// The SteamManager provides a base implementation of Steamworks.NET on which you can build upon.
// It handles the basics of starting up and shutting down the SteamAPI for use.
//
[DisallowMultipleComponent]
public class SteamManager : MonoBehaviour {
	private static SteamManager s_instance;
	private static SteamManager Instance {
		get {
			if (s_instance == null) {
				return new GameObject("SteamManager").AddComponent<SteamManager>();
			}
			else {
				return s_instance;
			}
		}
	}

	private static bool s_EverInitialized;

	private bool m_bInitialized;
	public static bool Initialized {
		get {
			return Instance.m_bInitialized;
		}
	}

	private SteamAPIWarningMessageHook_t m_SteamAPIWarningMessageHook;
	private static void SteamAPIDebugTextHook(int nSeverity, System.Text.StringBuilder pchDebugText) {
		Debug.LogWarning(pchDebugText);
	}

	private void Awake() {
		// Only one instance of SteamManager at a time!
		if (s_instance != null) {
			Destroy(gameObject);
			return;
		}
		s_instance = this;

		if(s_EverInitialized) {
			// This is almost always an error.
			// The most common case where this happens is when SteamManager gets destroyed because of Application.Quit(),
			// and then some Steamworks code in some other OnDestroy gets called afterwards, creating a new SteamManager.
			// You should never call Steamworks functions in OnDestroy, always prefer OnDisable if possible.
			throw new System.Exception("Tried to Initialize the SteamAPI twice in one session!");
		}

		// We want our SteamManager Instance to persist across scenes.
		DontDestroyOnLoad(gameObject);

		if (!Packsize.Test()) {
			Debug.LogError("[Steamworks.NET] Packsize Test returned false, the wrong version of Steamworks.NET is being run in this platform.", this);
		}

		if (!DllCheck.Test()) {
			Debug.LogError("[Steamworks.NET] DllCheck Test returned false, One or more of the Steamworks binaries seems to be the wrong version.", this);
		}

		try {
			// If Steam is not running or the game wasn't started through Steam, SteamAPI_RestartAppIfNecessary starts the
			// Steam client and also launches this game again if the User owns it. This can act as a rudimentary form of DRM.

			// Once you get a Steam AppID assigned by Valve, you need to replace AppId_t.Invalid with it and
			// remove steam_appid.txt from the game depot. eg: "(AppId_t)480" or "new AppId_t(480)".
			// See the Valve documentation for more information: https://partner.steamgames.com/doc/sdk/api#initialization_and_shutdown
			if (SteamAPI.RestartAppIfNecessary(AppId_t.Invalid)) {
				Application.Quit();
				return;
			}
		}
		catch (System.DllNotFoundException e) { // We catch this exception here, as it will be the first occurrence of it.
			Debug.LogError("[Steamworks.NET] Could not load [lib]steam_api.dll/so/dylib. It's likely not in the correct location. Refer to the README for more details.\n" + e, this);

			Application.Quit();
			return;
		}

		// Initializes the Steamworks API.
		// If this returns false then this indicates one of the following conditions:
		// [*] The Steam client isn't running. A running Steam client is required to provide implementations of the various Steamworks interfaces.
		// [*] The Steam client couldn't determine the App ID of game. If you're running your application from the executable or debugger directly then you must have a [code-inline]steam_appid.txt[/code-inline] in your game directory next to the executable, with your app ID in it and nothing else. Steam will look for this file in the current working directory. If you are running your executable from a different directory you may need to relocate the [code-inline]steam_appid.txt[/code-inline] file.
		// [*] Your application is not running under the same OS user context as the Steam client, such as a different user or administration access level.
		// [*] Ensure that you own a license for the App ID on the currently active Steam account. Your game must show up in your Steam library.
		// [*] Your App ID is not completely set up, i.e. in Release State: Unavailable, or it's missing default packages.
		// Valve's documentation for this is located here:
		// https://partner.steamgames.com/doc/sdk/api#initialization_and_shutdown
		m_bInitialized = SteamAPI.Init();
		if (!m_bInitialized) {
			Debug.LogError("[Steamworks.NET] SteamAPI_Init() failed. Refer to Valve's documentation or the comment above this line for more information.", this);

			return;
        } else {
		    GetCurrentUserName();
		    GetFriendsList();
		    inviteRequest = Callback<LobbyInvite_t>.Create(OnLobbyInvite);
		    joinRequest = Callback<GameLobbyJoinRequested_t>.Create(OnGameLobbyJoinRequested);
            OnLobbyCreatedCallResult = CallResult<LobbyCreated_t>.Create(OnLobbyCreated);
            m_LobbyEnter = Callback<LobbyEnter_t>.Create(OnLobbyEnter);
		    OnLobbyEnterCallResult = CallResult<LobbyEnter_t>.Create(OnLobbyEnter);
        }

		s_EverInitialized = true;
	}

    
    #region GetusersInfo
    public static string GetCurrentUserName() {
        if (!Initialized)
            return "";
        var name = SteamFriends.GetPersonaName();
        Debug.Log("[SteamManager]GetSteamName:" + name);
        PhotonNetwork.NickName = name;
        return name;
    }
    private void GetFriendsList() {
        if (!Initialized)
            return;

        int friendCount = SteamFriends.GetFriendCount(EFriendFlags.k_EFriendFlagImmediate);
        Debug.Log("[STEAM-FRIENDS] Listing " + friendCount + " Friends.");
        for (int i = 0; i < friendCount; ++i)
        {
            CSteamID friendSteamId = SteamFriends.GetFriendByIndex(i, EFriendFlags.k_EFriendFlagImmediate);
            friends.Add(new SteamFriend(friendSteamId));
            //string friendName = SteamFriends.GetFriendPersonaName(friendSteamId);
            //EPersonaState friendState = SteamFriends.GetFriendPersonaState(friendSteamId);
            
            //Debug.Log(friendName + " is " + friendState);
        }
    }    

    #endregion

    public static List<Friend> Friends {
        get { return Instance.friends; }
    }
    public List<Friend> friends=new List<Friend>();
    void OnLobbyInvite(LobbyInvite_t pCallback) {
        //���-�� ���������� ��� � �����. ��� ������� ��������������� ��� ������ �� ����� ������, ��������� ��������� Steam ������� ����������� � ��������� ���� ������������� <user> ��������� ��� � �����, ��������������?�.
        Debug.Log("[" + LobbyInvite_t.k_iCallback + " - LobbyInvite] - " + pCallback.m_ulSteamIDUser + " -- " + pCallback.m_ulSteamIDLobby + " -- " + pCallback.m_ulGameID);
        
        if (!PhotonNetwork.InRoom) {
            m_Lobby = (CSteamID)pCallback.m_ulSteamIDLobby;
            SteamMatchmaking.RequestLobbyData(m_Lobby);
            Lobby.Instance.ShowInviteDialog(SteamFriends.GetFriendPersonaName(new CSteamID(pCallback.m_ulSteamIDUser)), pCallback.m_ulSteamIDLobby);
            
            Debug.Log(m_Lobby.IsLobby() + "  " );
            var room=SteamMatchmaking.GetLobbyData(m_Lobby, "room");
            //Debug.Log(m_Lobby.IsLobby()+"  "+room);
            //
        }else {
            Debug.LogError("Now In room");
        }

    }

    private Callback<LobbyEnter_t> m_LobbyEnter;
    private CallResult<LobbyEnter_t> OnLobbyEnterCallResult;

    public static void JoinLobby(ulong lobbyId) {
        SteamAPICall_t handle = SteamMatchmaking.JoinLobby(new CSteamID(lobbyId));
        var room = SteamMatchmaking.GetLobbyData(new CSteamID(lobbyId), "room");
        if (!string.IsNullOrEmpty(room))
            PhotonNetwork.JoinRoom(room);
        Instance.OnLobbyEnterCallResult.Set(handle);
    }

    void OnLobbyEnter(LobbyEnter_t pCallback, bool bIOFailure)
    {//���������� ����� ������� ����� � �����. ���������� ����� �������� ��� ������������� ����� ��������� ����� ��������� ������.
        Debug.Log("[" + LobbyEnter_t.k_iCallback + " - LobbyEnter] - " + pCallback.m_ulSteamIDLobby + " -- " + pCallback.m_rgfChatPermissions + " -- " + pCallback.m_bLocked + " -- " + pCallback.m_EChatRoomEnterResponse);

        m_Lobby = (CSteamID)pCallback.m_ulSteamIDLobby;
        var room = SteamMatchmaking.GetLobbyData(m_Lobby, "room");
        if (!string.IsNullOrEmpty(room))
            PhotonNetwork.JoinRoom(room);
    }



    void OnLobbyEnter(LobbyEnter_t pCallback)
    {//���������� ����� ������� ����� � �����. ���������� ����� �������� ��� ������������� ����� ��������� ����� ��������� ������.
        Debug.Log("[" + LobbyEnter_t.k_iCallback + " - LobbyEnter] - " + pCallback.m_ulSteamIDLobby + " -- " + pCallback.m_rgfChatPermissions + " -- " + pCallback.m_bLocked + " -- " + pCallback.m_EChatRoomEnterResponse);

        m_Lobby = (CSteamID)pCallback.m_ulSteamIDLobby;
        var room = SteamMatchmaking.GetLobbyData(m_Lobby, "room");
        if (!string.IsNullOrEmpty(room))
            Lobby.Instance.LeftLobbyAndJoinRoom(room);
    }


    private Callback<GameLobbyJoinRequested_t> joinRequest;
    private Callback<LobbyInvite_t> inviteRequest;

    private CallResult<LobbyCreated_t> OnLobbyCreatedCallResult;
    public static void CreateLobby() {
        if (Initialized) {
            SteamAPICall_t handle=SteamMatchmaking.CreateLobby(ELobbyType.k_ELobbyTypePublic, 4);
            Instance.OnLobbyCreatedCallResult.Set(handle);
            Debug.Log("SteamMatchmaking.CreateLobby(" + ELobbyType.k_ELobbyTypePublic + ", " + 4 + ") : " + handle);
        }
    }

    public static void LeaveLobby() {
        if (Instance.m_Lobby.m_SteamID != 0) { 
            SteamMatchmaking.LeaveLobby(Instance.m_Lobby);
            Instance.m_Lobby=new CSteamID(0);
        }

    }

    void OnLobbyCreated(LobbyCreated_t pCallback, bool bIOFailure)
    {
        Debug.Log("[" + LobbyCreated_t.k_iCallback + " - LobbyCreated] - " + pCallback.m_eResult + " -- " + pCallback.m_ulSteamIDLobby);

        m_Lobby = (CSteamID)pCallback.m_ulSteamIDLobby;
        ;
        Debug.Log("SteamMatchmaking.SetLobbyData room=Room" + Lobby.Instance.RoomId+"   "+SteamMatchmaking.SetLobbyData(m_Lobby, "room", "Room" + Lobby.Instance.RoomId));
    }

    private CSteamID m_Lobby;


    public static bool InviteFriend(CSteamID friend, int roomName) {
        if(SteamFriends.GetFriendGamePlayed(friend, out var friendGameInfo)){
            if (friendGameInfo.m_gameID.m_GameID == 1103880) {
                var k = SteamMatchmaking.InviteUserToLobby(Instance.m_Lobby, friend);
                Debug.Log("InviteUserToLobby" +k);
                return k;
            }
        }
        Debug.Log("InviteUserToGame");
        return SteamFriends.InviteUserToGame(friend, "-invite Room" + roomName);
    }

    

    void OnGameLobbyJoinRequested(GameLobbyJoinRequested_t pCallback){
        //���������� ������� ������������ � �����. ���� ��������� ������������ �������� ����������������, ���� ������������ ��������� � ����,
        Debug.Log("[" + GameLobbyJoinRequested_t.k_iCallback + " - GameLobbyJoinRequested] - " + pCallback.m_steamIDLobby + " -- " + pCallback.m_steamIDFriend);
        
    }


    // This should only ever get called on first load and after an Assembly reload, You should never Disable the Steamworks Manager yourself.
    private void OnEnable() {
		if (s_instance == null) {
			s_instance = this;
		}

		if (!m_bInitialized) {
			return;
		}

		if (m_SteamAPIWarningMessageHook == null) {
			// Set up our callback to receive warning messages from Steam.
			// You must launch with "-debug_steamapi" in the launch args to receive warnings.
			m_SteamAPIWarningMessageHook = new SteamAPIWarningMessageHook_t(SteamAPIDebugTextHook);
			SteamClient.SetWarningMessageHook(m_SteamAPIWarningMessageHook);

        }
	}

	// OnApplicationQuit gets called too early to shutdown the SteamAPI.
	// Because the SteamManager should be persistent and never disabled or destroyed we can shutdown the SteamAPI here.
	// Thus id tot is not recommende perform any Steamworks work in other OnDestroy functions as the order of execution can not be garenteed upon Shutdown. Prefer OnDisable().
	private void OnDestroy() {
		if (s_instance != this) {
			return;
		}

		s_instance = null;

		if (!m_bInitialized) {
			return;
		}

		SteamAPI.Shutdown();
	    System.Threading.Thread.Sleep(1000);
    }

	private void Update() {
		if (!m_bInitialized) {
			return;
		}

		// Run Steam client callbacks
		SteamAPI.RunCallbacks();
	}

    public static void OpenOverlay() {
        if (Initialized) {
            SteamFriends.ActivateGameOverlay("friends");
        }
    }
}
public class SteamFriend:Friend {
    public CSteamID steamId;
    public SteamFriend(CSteamID id) {
        uniqId = id.m_SteamID;
        steamId = id;
        name = SteamFriends.GetFriendPersonaName(id);
        EPersonaState friendState = SteamFriends.GetFriendPersonaState(id);
        switch (friendState) {
            case EPersonaState.k_EPersonaStateOffline:
                status = OnlineState.Offline;
                break;
            case EPersonaState.k_EPersonaStateOnline:
            case EPersonaState.k_EPersonaStateBusy:
            case EPersonaState.k_EPersonaStateAway:
            case EPersonaState.k_EPersonaStateSnooze:
            case EPersonaState.k_EPersonaStateLookingToTrade:
            case EPersonaState.k_EPersonaStateLookingToPlay:
            case EPersonaState.k_EPersonaStateMax:
                status = OnlineState.Online;
                break;
        }

        avatarTexture = GetAvatar(id);
       // Debug.Log(name + " is " + friendState+"  "+id.m_SteamID);
       // Debug.Log(SteamFriends.GetFriendPersonaName(new CSteamID(76561198066349748)) + "  "+ (Int64)(76561198066349748));
    }
    internal static Texture2D GetAvatar()
    {
        return GetAvatar(SteamUser.GetSteamID());
    }

    internal static Texture2D GetAvatar(CSteamID steamid)
    {
        Texture2D result = Texture2D.blackTexture;
        int image = SteamFriends.GetMediumFriendAvatar(steamid);

        if (image > -1)
        {
            uint t1, t2;
            if (SteamUtils.GetImageSize(image, out t1, out t2))
            {
                byte[] bytes = new byte[4 * t1 * t2 * sizeof(char)];
                if (SteamUtils.GetImageRGBA(image, bytes, bytes.Length))
                {
                    result = GetAvatar(bytes, t1, t2);
                }
            }
        }
        return result;
    }

    internal static Texture2D GetAvatar(byte[] bytes, uint t1, uint t2)
    {
        var texture = new Texture2D((int)t1, (int)t2, TextureFormat.RGBA32, false);
        texture.LoadRawTextureData(bytes);
        texture.Apply(false);
        return texture;
    }

    internal static Texture2D GetAvatar(byte[] bytes)
    {
        var size = bytes.Length / 2;
        var texture = new Texture2D(size, size, TextureFormat.RGBA32, false);
        texture.LoadImage(bytes);
        return texture;
    }
}


#endif // !DISABLESTEAMWORKS
public abstract class Friend {
    public ulong uniqId;
    public string name;
    public OnlineState status;
    public Texture2D avatarTexture;
        
    public enum OnlineState {
        Online,
        Offline
    }
}

