﻿using System;
using System.Collections;
using EasyButtons;
using Photon.Pun;
using SaloonSpin;
using UnityEngine;

public class SetPosition : MonoBehaviour {
    private PhotonView view;
    [SerializeField] private Vector3 winMRpos;
    [SerializeField] private Vector3 winMRrot;
    IEnumerator Start() {
        view = GetComponent<PhotonView>();
        if(PhotonNetwork.IsConnected)
            if(view)
                if(!view.IsMine)
                    yield break;
        while (Input.GetJoystickNames().Length==0) {
            yield return null;
        }
        //Debug.Log(Input.GetJoystickNames().Length);
        foreach (var VARIABLE in Input.GetJoystickNames())
        {
            if (VARIABLE.ToLower().Contains("windowsmr")) {
                Debug.Log("[SetPosition]windowsmr");
                if(PhotonNetwork.IsConnected && view)view.RPC("SetPosAndRot",RpcTarget.Others, "windowsmr");
                SetPosAndRot("windowsmr");
                break;
            }
            if (VARIABLE.ToLower().Contains("knuckles")){
                Debug.Log("[SetPosition]knuckles");
                if (PhotonNetwork.IsConnected && view) view.RPC("SetPosAndRot", RpcTarget.Others, "knuckles");
                SetPosAndRot("knuckles");
                break;
            }
            if (VARIABLE.ToLower().Contains("vive"))
            {
                Debug.Log("[SetPosition]vive");
                if (PhotonNetwork.IsConnected && view) view.RPC("SetPosAndRot", RpcTarget.Others, "vive");
                SetPosAndRot("vive");
                break;
            }
            if (VARIABLE.ToLower().Contains("oculus touch"))
            {
                Debug.Log("[SetPosition]oculus touch");
                if (PhotonNetwork.IsConnected&&view) view.RPC("SetPosAndRot", RpcTarget.Others, "oculus touch");
                SetPosAndRot("oculus touch");
                break;
            }
            Debug.Log(VARIABLE + " not recognized");
        }
    }

    public float GetValue(int index) {
        switch (index) {
            case 0:
                return winMRpos.x;
            case 1:
                return winMRpos.y;
            case 2:
                return winMRpos.z;
            case 3:
                return winMRrot.x;
            case 4:
                return winMRrot.y;
            case 5:
                return winMRrot.z;
        }

        return 0;
    }

    public void SetValue(int index, float newValue,string controller) {
        var pos = Resources.Load<PositionOfObjects>("IntaractableObjectsPositions");
        var hand = GetComponent<HandAnimatorController>();
        switch (index)
        {
            case 0:
                winMRpos.x = newValue;
                break;
            case 1:
                winMRpos.y = newValue;
                break;
            case 2:
                winMRpos.z = newValue;
                break;
            case 3:
                winMRrot.x = newValue;
                break;
            case 4:
                winMRrot.y = newValue;
                break;
            case 5:
                winMRrot.z = newValue;
                break;
        }
        transform.localPosition = winMRpos;
        transform.localEulerAngles = winMRrot;
        pos.SetPositionAndRotation(type, controller, hand.Hand, winMRpos, winMRrot);
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(pos);
        
#endif
        
    }

    [PunRPC]
    private void SetPosAndRot(string controller) {
        var pos = Resources.Load<PositionOfObjects>("IntaractableObjectsPositions");
        var posrot = pos.GetAvatarPosAndRot(type, controller);
        var hand = GetComponent<HandAnimatorController>();
        switch (hand.Hand) {
            case Hand.Left:
                winMRpos = posrot.PosL;
                winMRrot = posrot.RotL;
                break;
            case Hand.Right:
                winMRpos = posrot.PosR;
                winMRrot = posrot.RotR;
                break;
        }
        
        /*switch (controller) {
            case "windowsmr":
                
                break;
            case "knuckles":
                
                break;
        }*/
        transform.localPosition = winMRpos;
        transform.localEulerAngles = winMRrot;
    }

    private void Reset() {
        winMRpos = transform.localPosition;
        winMRrot = transform.localEulerAngles;
    }
    public PlayerType type;
#if UNITY_EDITOR
    
    //[Button]
    private void SaveToAssets() {
        var pos=Resources.Load<PositionOfObjects>("IntaractableObjectsPositions");
        var hand = GetComponent<HandAnimatorController>();

        pos.SetPositionAndRotation(type, "oculus touch", hand.Hand,transform.localPosition,transform.localEulerAngles);
        pos.SetPositionAndRotation(type, "vive", hand.Hand,transform.localPosition,transform.localEulerAngles);
        pos.SetPositionAndRotation(type, "windowsmr", hand.Hand, winMRpos, winMRrot);
        Debug.Log("Update successful");
    }
#endif
}
