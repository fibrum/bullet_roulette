﻿using System.Collections;
using System.Collections.Generic;
using Photon.Voice.DemoVoiceUI;
using Photon.Voice.Unity;
using SaloonSpin;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsUI : MonoBehaviour {
    [SerializeField] private AudioMixer mixer;
    [SerializeField] private Slider masterSlider;
    [SerializeField] private Slider sfxSlider;
    [SerializeField] private Slider musicSlider;
    [SerializeField] private Slider voiceSlider;
    [SerializeField] private Dropdown micDropdown;
    private void Awake() {
        //mixer.GetFloat("Master", out var val);
        //masterSlider.value = val;
        masterSlider.onValueChanged.AddListener(OnChangeMaster);

        //mixer.GetFloat("SFX", out val);
        //sfxSlider.value = val;
        sfxSlider.onValueChanged.AddListener(OnChangeSfx);

        //mixer.GetFloat("Env", out val);
        //musicSlider.value = val;
        musicSlider.onValueChanged.AddListener(OnChangeEnv);

        //mixer.GetFloat("Voice", out val);
        //voiceSlider.value = val;
        voiceSlider.onValueChanged.AddListener(OnChangeVoice);
    }
    public void OnChangeMaster(float value) {
        mixer.SetFloat("Master", Mathf.Log10(value) * 20);
    }

    public void OnChangeSfx(float value) {
        mixer.SetFloat("SFX", Mathf.Log10(value) * 20);
    }
    public void OnChangeEnv(float value) {
        mixer.SetFloat("Music", Mathf.Log10(value) * 20);
    }
    public void OnChangeVoice(float value) {
        mixer.SetFloat("Voice", Mathf.Log10(value) * 20);
    }


    List<MicRef> micOptions;

    [SerializeField]
    private GameObject RefreshButton;

    public Recorder recorder;
    private void Start()
    {
        this.RefreshMicrophones();
    }

    private void SetupMicDropdown()
    {
        this.micDropdown.ClearOptions();

        this.micOptions = new List<MicRef>();
        List<string> micOptionsStrings = new List<string>();

        foreach (string x in Microphone.devices)
        {
            this.micOptions.Add(new MicRef(x));
            micOptionsStrings.Add(string.Format("[Unity] {0}", x));
        }

        if (Recorder.PhotonMicrophoneEnumerator.IsSupported)
        {
            this.RefreshButton.SetActive(true);
            for (int i = 0; i < Recorder.PhotonMicrophoneEnumerator.Count; i++)
            {
                string n = Recorder.PhotonMicrophoneEnumerator.NameAtIndex(i);
                this.micOptions.Add(new MicRef(n, Recorder.PhotonMicrophoneEnumerator.IDAtIndex(i)));
                micOptionsStrings.Add(string.Format("[Photon] {0}", n));
            }
        }
        else
        {
            this.RefreshButton.SetActive(true);
        }

        this.micDropdown.AddOptions(micOptionsStrings);
        this.micDropdown.onValueChanged.RemoveAllListeners();
        this.micDropdown.onValueChanged.AddListener(delegate { this.MicDropdownValueChanged(this.micOptions[this.micDropdown.value]); });

        this.SetCurrentValue();
    }

    private void MicDropdownValueChanged(MicRef mic) {
        GameManager.Instance.microphoneSettings = mic;
        GameManager.Instance.isMicSetted = true;
        this.recorder.MicrophoneType = mic.MicType;

        switch (mic.MicType)
        {
            case Recorder.MicType.Unity:
                this.recorder.UnityMicrophoneDevice = mic.Name;
                break;
            case Recorder.MicType.Photon:
                this.recorder.PhotonMicrophoneDeviceId = mic.PhotonId;
                break;
        }

        if (this.recorder.RequiresRestart)
        {
            this.recorder.RestartRecording();
        }
    }

    private void SetCurrentValue()
    {
        int valueIndex = 0;
        for (; valueIndex < this.micOptions.Count; valueIndex++)
        {
            MicRef val = this.micOptions[valueIndex];
            if (this.recorder.MicrophoneType == val.MicType)
            {
                if (this.recorder.MicrophoneType == Recorder.MicType.Unity &&
                    val.Name.Equals(this.recorder.UnityMicrophoneDevice))
                {
                    this.micDropdown.value = valueIndex;
                    break;
                }
                if (this.recorder.MicrophoneType == Recorder.MicType.Photon &&
                    val.PhotonId == this.recorder.PhotonMicrophoneDeviceId)
                {
                    this.micDropdown.value = valueIndex;
                    break;
                }
            }
        }
    }

    public void PhotonMicToggled(bool on)
    {
        this.micDropdown.gameObject.SetActive(!on);
        this.RefreshButton.SetActive(!on);
        if (on)
        {
            this.recorder.MicrophoneType = Recorder.MicType.Photon;
           // this.recorder.RestartRecording();
        }
        else
        {
            this.RefreshMicrophones();
            this.MicDropdownValueChanged(this.micOptions[this.micDropdown.value]);
        }
    }

    public void RefreshMicrophones()
    {
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
        //Debug.Log("Refresh Mics");
        Recorder.PhotonMicrophoneEnumerator.Refresh();
#endif
        this.SetupMicDropdown();
    }

#if UNITY_EDITOR
    // sync. UI in case a change happens from the Unity Editor Inspector
    private void PhotonVoiceCreated()
    {
        this.SetCurrentValue();
    }
#endif
}
