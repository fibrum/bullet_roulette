﻿Shader "Custom/Chandalier Glass"
{
    Properties
    {
    	[Header(Main Material Properties)]
        _Color ("Color", Color) = (1,1,1,1)
        _FresnelBias ("Fresnel Bias", Range(0, 2)) = 1
		_FresnelScale ("Fresnel Scale", Range(0, 1)) = 1
		_FresnelPower ("Fresnel Power", Range(0, 2)) = 1
        _Roughness ("Roughness Amount", Range(0.0, 10.0)) = 0.0
        _Specular("Specular Amount", Range(0, 1)) = 1
        _SpecularColor ("Specular Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "LightMode"="ForwardBase"}
        LOD 100

        ZWrite Off
    	Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0

            #include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                half3 normal : NORMAL;
                
				UNITY_VERTEX_INPUT_INSTANCE_ID //Insert
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                half3 worldNormal : TEXCOORD1;
                half3 worldPos : TEXCOORD4;

				UNITY_VERTEX_OUTPUT_STEREO //Insert
            };

            fixed3 _Color, _SpecularColor;
            fixed _FresnelPower, _FresnelScale, _FresnelBias, _Roughness, _Specular; 

            v2f vert (appdata v)
            {
                v2f o;

				UNITY_SETUP_INSTANCE_ID(v); //Insert
				UNITY_INITIALIZE_OUTPUT(v2f, o); //Insert
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o); //Insert

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);

                return o;
            }

            half3 BoxProjection(half3 direction, half3 position, half4 cubemapPosition, half3 boxMin, half3 boxMax)
            {
                
                half3 factors = ((direction > 0 ? boxMax : boxMin) - position) / direction;
                half scalar = min(min(factors.x, factors.y), factors.z);
                direction = direction * scalar + (position - cubemapPosition);
                return direction;
            }

            fixed4 frag (v2f i) : SV_Target
            {
            	fixed4 col = fixed4(_Color, 1);

                half3 viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);

                //Calculate Reflection Vector
                half3 reflection = reflect(-viewDir, i.worldNormal);   
                float3 boxReflection = BoxProjection(reflection, i.worldPos, unity_SpecCube0_ProbePosition,
                                                    unity_SpecCube0_BoxMin, unity_SpecCube0_BoxMax); 
                
                half4 skyData = UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, boxReflection, _Roughness * 2);
                half3 skyColor = DecodeHDR (skyData, unity_SpecCube0_HDR);
                col.rgb += skyColor * col.rgb;

                
                //Calculate Main Light
                half3 normalizedMainLightDirection = clamp(_WorldSpaceLightPos0.xyz, float3( -2,-2,-2 ), float3( 2,2,2));
                fixed resultMainLightNormal = dot(i.worldNormal , normalizedMainLightDirection);

                col.rgb = lerp (col.rgb * fixed3(0.5, 0.5, 0.5) , col.rgb * _LightColor0, resultMainLightNormal);

                fixed fresnel = (0.75 - _FresnelBias) + _FresnelScale * pow(1 + dot(viewDir, i.worldNormal), _FresnelPower);
                
                col.a = clamp(1 - fresnel, 0, 1);

                
                //Specular
				half3 halfDir = normalize(normalizedMainLightDirection + viewDir);
				fixed spec = clamp(dot(halfDir, i.worldNormal), 0.0, 1.0);
				spec = pow(spec, _Specular * 200);

                col.rgb = (col.rgb + spec * _SpecularColor) * 1.3;
                col.a = col.a + spec;

                return col;
            }
            ENDCG
        }
    }
}
