﻿using UnityEngine;

public class ShootOnMesh : MonoBehaviour {
    [SerializeField] private LayerMask mask;
    [SerializeField] private Transform shootingPoint;
    private void Update() {
        if(Input.GetKeyDown(KeyCode.Space))
            Shoot();
    }


    public void Shoot() {

        if (Physics.Raycast(shootingPoint.position, shootingPoint.forward, out var hit, 10f, mask)) {
            Debug.Log("shot in: " + hit.transform.name);
            var uv = hit.transform.GetComponent<UvMeshFinder>();

            if (uv) {
                uv.Bake();
                if (Physics.Raycast(shootingPoint.position, shootingPoint.forward, out hit, 10f, mask)) {
                    Vector2 hole1=hit.textureCoord;
                    Vector3 point1 = hit.point;
					Vector3 point1normal = hit.normal;
                    if (Physics.Raycast(hit.point + shootingPoint.forward*0.25f, -shootingPoint.forward, out var hit2, 10f, mask)) {
                        	//uv.MakeHall(hole1, hit.textureCoord,point1,hit.point);
                        	uv.MakeHall(hole1, hit2.textureCoord,point1,hit2.point,point1normal,hit2.normal);
                    }
                }
                uv.TurnOff();

            }
        }
    }

}
