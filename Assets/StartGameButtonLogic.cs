﻿using System.Collections;
using System.Collections.Generic;
using SaloonSpin;
using UnityEngine;

public class StartGameButtonLogic : MonoBehaviour {
    private float enableTime;
    private void OnEnable() {
        enableTime = Time.time;
    }
    public void StartClick() {
        if (Time.time-enableTime<0.5f||Time.time - ControllerInitializer.TimeOfReset < 0.25f) {
            Debug.Log("Miss click");
            return;
        }

        NetworkManager.Instance.StartGame();
        gameObject.SetActive(false);

    }
}
