﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnRenderer : MonoBehaviour {
    private Renderer[] renderers;

    private void Start() {
        hide();
        
    }

    private bool init;
    private float lastTime;
    private void Update() {
        if (init&&Time.time- lastTime>1f) {
            turnOffRends();
        }
    }

    private void hide() {
        StartCoroutine(GetControllersRenderer());
    }

    private IEnumerator GetControllersRenderer() {
        while (true) {
            var rends=GetComponentsInChildren<Renderer>();
            if (rends.Length > 0) {
                renderers = rends;
                break;
            }

            yield return null;
        }
        turnOffRends();
        init = true;
        

        //InvokeRepeating("turnOffRends", 10f,10f);
    }

    private void turnOffRends() {
        for (int i = 0; i < renderers.Length; i++) {
            renderers[i].enabled = false;
        }
        lastTime = Time.time;
    }
}
