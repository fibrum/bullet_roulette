﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UvMeshFinder : MonoBehaviour {
    private SkinnedMeshRenderer skin;
    private MeshCollider collider;
    private SphereCollider sphere;
    private void Awake() {
        skin = GetComponent<SkinnedMeshRenderer>();
        collider = GetComponent<MeshCollider>();
        sphere = GetComponent<SphereCollider>();
        mat=skin.sharedMaterial.mainTexture;
    }

    private Texture mat;
    public void Bake() {
        sphere.enabled = false;
        Mesh mesh=new Mesh();
        skin.BakeMesh(mesh);
        collider.sharedMesh = mesh;
        collider.enabled = true;
        //Debug.Break();
    }

    public void TurnOff() {
        collider.enabled = false;
        sphere.enabled = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            ResetToNormalState();
    }

    [SerializeField] private int radius=25;
    [SerializeField] private GameObject HolePrefab;
    public void MakeHall(Vector2 hole1,Vector2 hole2,Vector3 point1, Vector3 point2,Vector3 point1Normal,Vector3 point2Normal) {
        Texture texture = new Texture2D(skin.material.mainTexture.width, skin.material.mainTexture.height);
        Graphics.CopyTexture(skin.material.mainTexture,texture);
        Texture2D editedTexture2D= texture as Texture2D;
        if (editedTexture2D) { 
            
            Debug.Log((int)(editedTexture2D.width*hole1.x)+" "+ (int)(editedTexture2D.height*hole1.y));
            int x =(int)(editedTexture2D.width * hole1.x);
            int y = (int)(editedTexture2D.height * hole1.y);

            for (int i = -radius; i < radius; i++) {
                for (int j = -radius; j < radius; j++) {
                    if (Mathf.Sqrt(Mathf.Pow(i, 2) + Mathf.Pow(j, 2)) < radius) {
                        if(editedTexture2D.width> x + i&& x + i>0&& editedTexture2D.height> y + j&&y+j>0)
                            editedTexture2D.SetPixel(x+i,y+j,Color.clear);
                    }
                }
                
            }

            x = (int)(editedTexture2D.width * hole2.x);
            y = (int)(editedTexture2D.height * hole2.y);

            for (int i = -radius; i < radius; i++)
            {
                for (int j = -radius; j < radius; j++)
                {
                    if (Mathf.Sqrt(Mathf.Pow(i, 2) + Mathf.Pow(j, 2)) < radius)
                    {
                        if (editedTexture2D.width > x + i && x + i > 0 && editedTexture2D.height > y + j && y + j > 0)
                            editedTexture2D.SetPixel(x + i, y + j, Color.clear);
                    }
                }

            }


            editedTexture2D.Apply();
            skin.material.mainTexture = editedTexture2D;
            if(HolePrefab){ 
                var hole=Instantiate(HolePrefab, transform);
                //hole.transform.parent = transform;
                hole.GetComponent<HoleController>().MakeHoleScale(point1,point2,point1Normal,point2Normal,transform.parent.up);
                holes.Add(hole);
            }

        }

    }
    private List<GameObject> holes=new List<GameObject>();

    public void ResetToNormalState() {
        for (int i = 0; i < holes.Count; i++) {
            Destroy(holes[i]);
        }
        holes.Clear();
        skin.material.mainTexture = mat;
    }

}
