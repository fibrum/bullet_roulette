﻿using System.Collections;
using System.Collections.Generic;
using SaloonSpin;
using UnityEngine;

public class WaterFade : MonoBehaviour {
    private SkinnedMeshRenderer skinrenderer;
    [SerializeField] private Color fullColor;
    [SerializeField] private Color emptyColor=Color.clear;

    public float waterInGlass {
        set {
            if (skinrenderer) skinrenderer.material.SetColor(colorId, Color.Lerp(emptyColor, fullColor, value));
        }
    }

    private int colorId=Shader.PropertyToID("_Color");
    void Awake() {
        skinrenderer = GetComponent<SkinnedMeshRenderer>();
        
        GetComponentInParent<Glass>().onWaterInGlassChange += WaterFade_onWaterInGlassChange;
    }

    private void WaterFade_onWaterInGlassChange(float obj) {
        //Debug.Log(obj);
        waterInGlass = obj;
        if (obj < 0.05f)
            skinrenderer.enabled = false;
    }

    void Reset() {
        Awake();
        fullColor = skinrenderer.sharedMaterial.color;
    }
    
}
